package com.ternaknesia.sobaternak.Manager;

/*
 * Created by A MADRISH on 10/10/2017
 */

import android.content.Context;
import android.content.SharedPreferences;

import com.ternaknesia.sobaternak.Utils.AESEnDecryption;

import org.json.JSONException;
import org.json.JSONObject;


public class PrefManager {
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;

    // Shared preferences file name
    private String TOP_NEWS = "11e7b41d";
    private String ALL_NEWS = "21f7c71e";
    private static final String KEY = "a9aedef6-47d1-462c-8029-004c368f7ff6";
    private static final String IV = "4c368f7ff6";
    private static final String PREF_NAME = "smartternak_88c3e6ba-ec69-11e7-8c3f-9a214cf093ae";
    private static final String IS_FIRST_TIME_LAUNCH = "84ca879a-ec68-11e7-8c3f-9a214cf093ae";

    private static final String IS_LOG = "84ca8e3e-ec68-11e7-8c3f-9a214cf093ae";
    private static final String TOKEN = "84ca90d2-ec68-11e7-8c3f-9a214cf093ae";

    private static final String REG_ID_FIREBASE = "84caa54a-ec68-11e7-8c3f-9a214cf093ae";
    private static final String PROFILE_ = "84cab40e-ec68-11e7-8c3f-9d214cf093ae";
    private static final String MSG_PUSH = "84cab40d-ec66-11e7-8c3f-9d214cf093ae";
    private static final String DEL_MSG = "84cab41d-ec66-11e7-4c3f-3d214cf093ae";
    private static final String LIST_COVERAGES_QURBAN = "84c66-11e7-4cf093ae";


    public PrefManager(Context context) {
        int PRIVATE_MODE = 0;
        pref = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public PrefManager(Context context, String name) {
        int PRIVATE_MODE = 0;
        pref = context.getSharedPreferences(name, PRIVATE_MODE);
        editor = pref.edit();
    }


    private String enc(String ok){
        String encryptedMsg="";
        try {
            encryptedMsg = AESEnDecryption.encryptStrAndToBase64(IV, KEY, ok);
        }catch (Exception e) {
            //e.printStackTrace();
        }
        return encryptedMsg;
    }

    private String dec(String ok){
        String decryptedMsg="";
        try {
            decryptedMsg = AESEnDecryption.decryptStrAndFromBase64(IV, KEY, ok);
        } catch (Exception e) {
            //e.printStackTrace();
        }
        return decryptedMsg;
    }




    public void setIDFB(String mId){
        editor.putString(REG_ID_FIREBASE, mId);
        editor.commit();
    }

    public String getIDFB(){
        return pref.getString(REG_ID_FIREBASE, "");
    }


    public void setToken(String mToken){
        editor.putString(TOKEN, enc(mToken));
        editor.commit();
    }

    public String getToken(){
        return dec(pref.getString(TOKEN, ""));
    }

    public void setLog(boolean isDestroy){
        editor.putBoolean(IS_LOG, isDestroy);
        editor.commit();
    }

    public boolean isLog(){
        return pref.getBoolean(IS_LOG, false);
    }



    public void setFirstTimeLaunch(boolean isFirstTime) {
        editor.putBoolean(IS_FIRST_TIME_LAUNCH, isFirstTime);
        editor.commit();
    }

    public boolean isFirstTimeLaunch() {
        return pref.getBoolean(IS_FIRST_TIME_LAUNCH, true);
    }

    public boolean logout() {
        setLog(false);
        setDataProfile("");
        setDataMsg("");
        setToken("");
        return true;
    }


    public void setDataMsg(String mData){
        editor.putString(MSG_PUSH, enc(mData));
        editor.commit();
    }

    public String getDataMsg() {
        return dec(pref.getString(MSG_PUSH, ""));
    }

    public void setDelMsg(boolean mData){
        editor.putBoolean(DEL_MSG, mData);
        editor.commit();
    }

    public boolean getDelMsg() {
        return pref.getBoolean(DEL_MSG, true);
    }

    public void setDataProfile(String mData){
        editor.putString(PROFILE_, enc(mData));
        editor.commit();
    }

    public String getDataProfile() {
        return dec(pref.getString(PROFILE_, ""));
    }

    public String getSingleDataProfile(String var){
        try {
            JSONObject a = new JSONObject(getDataProfile());
            JSONObject b = a.getJSONObject("result");
            if(b.has(var))return b.getString(var);
            else return "null";
        } catch (JSONException e) {
            //e.printStackTrace();
            return "null";
        }
    }

    public void setDataOrderQurban(String s) {
        editor.putString("DataOrderQurban", enc(s));
        editor.commit();
    }

    public String getDataOrderQurban() {
        return dec(pref.getString("DataOrderQurban", ""));
    }


    public void setListCoveragesQurban(String mData){
        editor.putString(LIST_COVERAGES_QURBAN, enc(mData));
        editor.commit();
    }

    public String getListCoveragesQurban(){
        return dec(pref.getString(LIST_COVERAGES_QURBAN, ""));
    }

    public String getDataTopNews() {
        return dec(pref.getString(TOP_NEWS, ""));
    }

    public void setDataTopNews(String mData){
        editor.putString(TOP_NEWS, enc(mData));
        editor.commit();
    }

    public String getDataAllNews() {
        return dec(pref.getString(ALL_NEWS, ""));
    }

    public void setDataAllNews(String mData){
        editor.putString(ALL_NEWS, enc(mData));
        editor.commit();
    }
}