package com.ternaknesia.sobaternak.main.cashier.offline_database;

/**
 * Created by Abyan Dafa on 08/06/2018.
 */

public class SalesOrder {

    public static final String TABLE_NAME = "SalesOrders";

    public static final String COLUMN_ID = "id";
    public static final String COLUMN_NAMA = "nama";
    public static final String COLUMN_ALAMAT = "alamat";
    public static final String COLUMN_TANGGAL_PENGIRIMAN = "tanggal_kirim";
    public static final String COLUMN_TELEPON = "telepon";
    public static final String COLUMN_STATUS = "status";

    public static final String CREATE_TABLE =
            "CREATE TABLE " + TABLE_NAME + "("
                    + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                    + COLUMN_NAMA + " TEXT,"
                    + COLUMN_ALAMAT + " TEXT,"
                    + COLUMN_TANGGAL_PENGIRIMAN + " TEXT,"
                    + COLUMN_TELEPON + " TEXT,"
                    + COLUMN_STATUS + " TEXT"
                    + ")";

    public SalesOrder() {
    }

    private int id;
    private String nama;
    private String alamat;
    private String tanggalKirim;
    private String telepon;
    private String status;

    public SalesOrder(int id, String nama, String alamat, String tanggalKirim, String telepon, String status) {
        this.id = id;
        this.nama = nama;
        this.alamat = alamat;
        this.tanggalKirim = tanggalKirim;
        this.telepon = telepon;
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getTanggalKirim() {
        return tanggalKirim;
    }

    public void setTanggalKirim(String tanggalKirim) {
        this.tanggalKirim = tanggalKirim;
    }

    public String getTelepon() {
        return telepon;
    }

    public void setTelepon(String telepon) {
        this.telepon = telepon;
    }
}
