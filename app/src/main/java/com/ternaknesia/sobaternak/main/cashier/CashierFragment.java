package com.ternaknesia.sobaternak.main.cashier;


import android.app.AlertDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.ternaknesia.sobaternak.Manager.PrefManager;
import com.ternaknesia.sobaternak.R;
import com.ternaknesia.sobaternak.main.TernakActivity;
import com.ternaknesia.sobaternak.main.cashier.bill.BillAdapter;
import com.ternaknesia.sobaternak.main.cashier.bill.BillClass;
import com.ternaknesia.sobaternak.main.cashier.offline_database.OfflineDB;
import com.ternaknesia.sobaternak.main.cashier.offline_database.OfflineSOLDB;
import com.ternaknesia.sobaternak.main.cashier.offline_database.SalesOrder;
import com.ternaknesia.sobaternak.main.cashier.offline_database.SalesOrderLine;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormatSymbols;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

/**
 * A simple {@link Fragment} subclass.
 */
public class CashierFragment extends Fragment implements CashierAdapter.OnItemListener {

    //widget step 1
    private RelativeLayout step1;
    private RecyclerView listKambing, listSapi, listDomba;
    private CashierAdapter adapterKambing, adapterSapi, adapterDomba;
    private Button action;
    private ImageView item1, item2, item3;
    private LinearLayout layoutKambing, layoutSapi, layoutDomba;
    private List<SellProductClass> itemToSell;
    private CardView cardKambing, cardSapi;

    private boolean isCreated = false;



    //widget step 2
    private LinearLayout parentStep2;
    private EditText formName;
    private EditText formAddress;
    private EditText formPhoneNumber;
    private TextView formDeliveryDate;
    private Button toStep3;
    private Calendar calendar;

    private BillAdapter billAdapter;

    //widget step3
    private ScrollView step3;
    private RecyclerView billRecycler;
    private TextView sumPrice, detailName, detailAddress, detailPhoneNumber, detailDeliveryDate;
    private Button saveTransaction;



    //model
    private PrefManager spUser;

    //variabel
    private boolean isKambingShowed, isSapiShowed, isDombaShowed;



    private RecyclerView.LayoutManager recyclerLayoutManager;

    public CashierFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_cashier, container, false);
        isCreated = true;
        initView(view);
        initContent();
        showStep1();

        return view;
    }

    private void initContent() {
        //initContent step 1
        recyclerLayoutManager = new LinearLayoutManager(getContext());
        listKambing.setLayoutManager(recyclerLayoutManager);
        listKambing.setAdapter(adapterKambing);
        recyclerLayoutManager = new LinearLayoutManager(getContext());
        listSapi.setLayoutManager(recyclerLayoutManager);
        listSapi.setAdapter(adapterSapi);
        recyclerLayoutManager = new LinearLayoutManager(getContext());
        listDomba.setLayoutManager(recyclerLayoutManager);
        listDomba.setAdapter(adapterDomba);

        layoutKambing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!isKambingShowed)
                {
                    isKambingShowed = true;
                    item1.setImageResource(R.drawable.arrow_up);
                    listKambing.setVisibility(View.VISIBLE);
                    cardKambing.setVisibility(View.VISIBLE);
                }
                else
                {
                    isKambingShowed = false;
                    item1.setImageResource(R.drawable.arrow_down);
                    listKambing.setVisibility(View.GONE);
                    cardKambing.setVisibility(View.GONE);
                }
            }
        });

        layoutSapi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!isSapiShowed)
                {
                    isSapiShowed = true;
                    item2.setImageResource(R.drawable.arrow_up);
                    listSapi.setVisibility(View.VISIBLE);
                    cardSapi.setVisibility(View.VISIBLE);
                }
                else
                {
                    isSapiShowed = false;
                    item2.setImageResource(R.drawable.arrow_down);
                    listSapi.setVisibility(View.GONE);
                    cardSapi.setVisibility(View.GONE);
                }
            }
        });

        layoutDomba.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!isDombaShowed)
                {
                    isDombaShowed = true;
                    item3.setImageResource(R.drawable.arrow_up);
                    listDomba.setVisibility(View.VISIBLE);
                }
                else
                {
                    isDombaShowed = false;
                    item3.setImageResource(R.drawable.arrow_down);
                    listDomba.setVisibility(View.GONE);
                }
            }
        });

        listKambing.setVisibility(View.GONE);
        listSapi.setVisibility(View.GONE);
        listDomba.setVisibility(View.GONE);

        getData();

        action.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                itemToSell  = new ArrayList<>();
                List<SellProductClass> itemKambing, itemSapi, itemDomba;
                itemKambing = adapterKambing.getSelectedProducts();
                itemSapi = adapterSapi.getSelectedProducts();
                itemDomba = adapterDomba.getSelectedProducts();
                if(itemKambing.size() !=0 )
                {
                    for(int i = 0; i< itemKambing.size(); i++) itemToSell.add(itemKambing.get(i));
                }
                if(itemSapi.size() !=0 )
                {
                    for(int i = 0; i< itemSapi.size(); i++) itemToSell.add(itemSapi.get(i));
                }
                if(itemDomba.size() !=0 )
                {
                    for(int i = 0; i< itemDomba.size(); i++) itemToSell.add(itemDomba.get(i));
                }
                if(itemToSell.size() == 0)
                    Toast.makeText(getContext(), "Produk yg dijual tidak ada", Toast.LENGTH_SHORT).show();
                else
                    showStep2();
            }
        });

        //initContent step 2
        toStep3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(formName.getText().toString().isEmpty() || formAddress.getText().toString().isEmpty() ||
                        formDeliveryDate.getText().toString().isEmpty() || formPhoneNumber.getText().toString().isEmpty())
                    Toast.makeText(getContext(), "Please fill all form", Toast.LENGTH_SHORT).show();
                else showStep3();
            }
        });


        billAdapter = new BillAdapter();


    }

    private void getData() {
        if(this.isDetached()) return;

        adapterKambing.removeData();
        adapterDomba.removeData();
        adapterSapi.removeData();
        JSONObject obj = new JSONObject();
/*
        JsonObjectRequest req = new JsonObjectRequest(Request.Method.GET,
                getResources().getString(R.string.baseUrl)+"/api/qurban/mymarket", obj, response -> {
            try {

                Log.d("Ternaknesia", response.getString("response"));

                if(response.getString("response").equals("success"))
                {
                    JSONArray data = response.getJSONObject("result").getJSONArray("products");
                    adapterKambing.addData(data);
                    adapterDomba.addData(data);
                    adapterSapi.addData(data);
                    setView();
                }



            } catch (JSONException e) {

                e.printStackTrace();
            }
        }, error -> {
            error.printStackTrace();
        })
        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                Log.d("Ternaknesia", "getHeaders: " + spUser.getToken());
                headers.put("Authorization", "Bearer " + spUser.getToken() );
                return headers;
            }
        };
        req.setRetryPolicy(new DefaultRetryPolicy(0,0,0));
        RequestSingleton.getInstance(getContext()).addToRequestQueue(req);*/
    }

    private void initView(View v) {
        spUser = new PrefManager(getContext());

        //init view step 1

        step1 = v.findViewById(R.id.first_step);
        isKambingShowed = isDombaShowed = isSapiShowed = false;
        listKambing = v.findViewById(R.id.recycler_view1);
        listSapi = v.findViewById(R.id.recycler_view2);
        listDomba = v.findViewById(R.id.recycler_view3);
        item1 = v.findViewById(R.id.arrow1);
        item2 = v.findViewById(R.id.arrow2);
        item3 = v.findViewById(R.id.arrow3);
        action = v.findViewById(R.id.action_button);
        layoutKambing = v.findViewById(R.id.category1);
        layoutSapi = v.findViewById(R.id.category2);
        layoutDomba = v.findViewById(R.id.category3);
        adapterKambing = new CashierAdapter(this, 1);
        adapterSapi = new CashierAdapter(this, 2);
        adapterDomba= new CashierAdapter(this, 3);
        cardKambing = v.findViewById(R.id.card_kambing);
        cardSapi = v.findViewById(R.id.card_sapi);

        //initView step 2
        parentStep2 = v.findViewById(R.id.second_step);
        formName = v.findViewById(R.id.pemesan_name);
        formAddress = v.findViewById(R.id.pemesan_address);
        formPhoneNumber = v.findViewById(R.id.pemesan_number);
        formDeliveryDate = v.findViewById(R.id.pemesan_departure_date);
        formDeliveryDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getDate();
            }
        });
        toStep3 = v.findViewById(R.id.action_button_step_2);

//
//        private ScrollView step3;
//        private RecyclerView billRecycler;
//        private TextView sumPrice, detailName, detailAddress, detailPhoneNumber, detailDeliveryDate;
//        private Button saveTransaction;
        //initView step 3
        step3 = v.findViewById(R.id.third_step);
        billRecycler = v.findViewById(R.id.recycler_bill);
        sumPrice = v.findViewById(R.id.total_price);
        detailName = v.findViewById(R.id.detail_name);
        detailAddress = v.findViewById(R.id.detail_address);
        detailDeliveryDate = v.findViewById(R.id.detail_delivery_date);
        detailPhoneNumber = v.findViewById(R.id.detail_phone_number);
        saveTransaction = v.findViewById(R.id.action_button_step_3);


    }

    private void getDate() {
        calendar = Calendar.getInstance();
        /*DatePickerDialog.OnDateSetListener dateListener = (datePicker, i, i1, i2) -> {
            calendar.set(Calendar.YEAR, i);
            calendar.set(Calendar.MONTH, i1);
            calendar.set(Calendar.DAY_OF_MONTH, i2);
            showDate(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
        };
        new DatePickerDialog(getContext(), AlertDialog.THEME_DEVICE_DEFAULT_DARK, dateListener,
                calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH)).show();*/

    }
    private void showDate(int year, int month, int day) {
        formDeliveryDate.setText(new StringBuilder().append(day).append(" ")
                .append(DateFormatSymbols.getInstance(new Locale("in", "ID")).getMonths()[month]
                ).append(" ").append(year));
    }
    private void setView() {
        if(adapterKambing.isEmpty()) layoutKambing.setVisibility(View.GONE);
        if(adapterSapi.isEmpty()) layoutSapi.setVisibility(View.GONE);
        if(adapterDomba.isEmpty()) layoutDomba.setVisibility(View.GONE);
    }

    @Override
    public void onLongClick(SellProductClass productItemClass, int position) {


        AlertDialog.Builder mBuilder = new AlertDialog.Builder(getContext());
        View mView = getLayoutInflater().inflate(R.layout.change_stock_layout, null);

        Button subtract, add, save;
        final EditText currentStock;

        currentStock =mView.findViewById(R.id.current_stock);
        subtract = mView.findViewById(R.id.subtract);
        add = mView.findViewById(R.id.add);
        save = mView.findViewById(R.id.save_change);

        mBuilder.setView(mView);
        currentStock.setText("0");
        final AlertDialog dialog = mBuilder.create();
        dialog.show();

        /*subtract.setOnClickListener(view -> {
            currentStock.setText(String.valueOf(Integer.valueOf(currentStock.getText().toString()) - 1));
            if(Integer.valueOf(currentStock.getText().toString())<=0) currentStock.setText("0");
        });
        add.setOnClickListener(view -> currentStock.setText(String.valueOf(Integer.valueOf(currentStock.getText().toString()) + 1)));

        save.setOnClickListener(view -> {
            updateProduct(productItemClass, Integer.valueOf(currentStock.getText().toString()), position);
            dialog.dismiss();
        });*/
    }

    @Override
    public void onClick(SellProductClass productItemClass, int position) {

        if(productItemClass.getStock() < productItemClass.getJumlah() + 1)
        {
            Toast.makeText(getContext(), "Melebihi stok yang tersedia", Toast.LENGTH_SHORT).show();
            return;
        }

        if(adapterKambing.getCategory() == productItemClass.getCategory_id())
            adapterKambing.updateTotal(productItemClass.getJumlah() + 1, position);
        else if(adapterSapi.getCategory() == productItemClass.getCategory_id())
            adapterSapi.updateTotal(productItemClass.getJumlah() + 1, position);
        else if(adapterDomba.getCategory() == productItemClass.getCategory_id())
            adapterDomba.updateTotal(productItemClass.getJumlah()+ 1, position);
    }

    private void updateProduct(SellProductClass productItemClass, Integer integer, int position) {
        if(productItemClass.getStock() < integer)
        {
            Toast.makeText(getContext(), "Melebihi stok yang tersedia", Toast.LENGTH_SHORT).show();
            return;
        }

        if(adapterKambing.getCategory() == productItemClass.getCategory_id())
            adapterKambing.updateTotal(integer, position);
        else if(adapterSapi.getCategory() == productItemClass.getCategory_id())
            adapterSapi.updateTotal(integer, position);
        else if(adapterDomba.getCategory() == productItemClass.getCategory_id())
            adapterDomba.updateTotal(integer, position);
    }



    private void showStep1()
    {
        step1.setVisibility(View.VISIBLE);
        step3.setVisibility(View.GONE);
        parentStep2.setVisibility(View.GONE);
    }
    private void showStep2()
    {
        step1.setVisibility(View.GONE);
        step3.setVisibility(View.GONE);
        parentStep2.setVisibility(View.VISIBLE);

    }
    private void showStep3()
    {
        step1.setVisibility(View.GONE);
        step3.setVisibility(View.VISIBLE);
        parentStep2.setVisibility(View.GONE);
        initContentStep3();
    }

    private void initContentStep3() {
        detailPhoneNumber.setText("No. HP \t\t +62" +formPhoneNumber.getText().toString());
        detailDeliveryDate.setText("Tanggal Pengiriman \t" +formDeliveryDate.getText().toString());
        detailName.setText("Nama \t\t" +formName.getText().toString());
        detailAddress.setText("Alamat \t\t" +formAddress.getText().toString());

        Log.d("selasa ternaknesia", "initContentStep3: " + itemToSell.size());
        billAdapter.addProducts(itemToSell);
        recyclerLayoutManager = new LinearLayoutManager(getContext());
        billRecycler.setLayoutManager(recyclerLayoutManager);
        billRecycler.setAdapter(billAdapter);


        NumberFormat formatter = new DecimalFormat("#,###");
        double myNumber = Double.valueOf(billAdapter.totalPriceProduct());
        String itemPrice = formatter.format(myNumber);
        sumPrice.setText(itemPrice);

//        saveTransaction.setOnClickListener(view -> addToDatabase());
    }

    private void addToDatabase() {
        OfflineDB db = new OfflineDB(getContext());
        OfflineSOLDB dbsol = new OfflineSOLDB(getContext());

        long id = db.insertSalesOrder(formName.getText().toString(),
                formAddress.getText().toString(),
                formDeliveryDate.getText().toString(),
                "+62" + formPhoneNumber.getText().toString(),
                "BARU");


        SalesOrder n = db.getSalesOrder(id);
        List<BillClass> allproduct = billAdapter.allProduct();





        for(int i = 0; i < allproduct.size(); i++)
        {
            BillClass current = allproduct.get(i);

            long id_sol = dbsol.insertSalesOrderLine(n.getId(), 0, "belum ada", "belum ada",
                    current.getItem().getNama(), current.getItem().getJumlah(),
                    formAddress.getText().toString(), formDeliveryDate.getText().toString());
            Log.d("RAIHAN", "addToDatabase: " + id_sol);
            SalesOrderLine sol = dbsol.getSalesOrderLine(id_sol);
            Log.d("RAIHAN", "addToDatabase: " + sol.getJumlah());
            Log.d("RAIHAN", "addToDatabase: " + current.getItem().getJumlah());

        }
        prepareUpdateServer(allproduct, allproduct.size()-1);



    }

    private void prepareUpdateServer(List<BillClass> allproduct, int size) {
        Log.d("KEADAAN SEKARANG", "prepareUpdateServer: " + size);
        if(size<0)
        {
            Toast.makeText(getContext(), "berhasil menambah transaksi offline", Toast.LENGTH_SHORT).show();
            clearTransaction();
            ((TernakActivity)getActivity()).backToHome();
            return;
        }
        SellProductClass currentProduct = allproduct.get(size).getItem();
        int stock = currentProduct.getStock() - currentProduct.getJumlah();
        updateToServer(currentProduct, stock, size, allproduct);
    }

    private void clearTransaction() {
        formDeliveryDate.setText("");
        formPhoneNumber.setText("");
        formAddress.setText("");
        formName.setText("");
    }




    private void updateToServer(SellProductClass productItemClass, Integer stock, final int size, List<BillClass> allproduct) {

        ((TernakActivity)getActivity()).showProgressBar();
        JSONObject obj = new JSONObject();
        try {
            obj.accumulate("product_id", productItemClass.getProduct_id());
            obj.accumulate("market_id", productItemClass.getMarket_id());
            obj.accumulate("category_id", productItemClass.getCategory_id());
            obj.accumulate("nama", productItemClass.getNama());
            obj.accumulate("stok", stock);
//            obj.accumulate("image", productItemClass.getImage());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        /*JsonObjectRequest req = new JsonObjectRequest(Request.Method.POST,
                getResources().getString(R.string.baseUrl)+"/api/qurban/product/edit", obj, response -> {
            try {

                Log.d("Ternaknesia", response.getString("response"));
                if(response.getString("response").equals("success"))
                {
                    prepareUpdateServer(allproduct, size - 1);
                }
                else
                {
                    Toast.makeText(getContext(), "Terjadi kesalahan", Toast.LENGTH_SHORT).show();
                    clearTransaction();
                    ((TernakActivity)getActivity()).backToHome();
                }


                ((TernakActivity)getActivity()).hideProgressBar();

            } catch (JSONException e) {

                e.printStackTrace();
                ((TernakActivity)getActivity()).hideProgressBar();
            }

        }, error -> {
            error.printStackTrace();
            Toast.makeText(getContext(), "Terjadi kesalahan", Toast.LENGTH_SHORT).show();
            ((TernakActivity)getActivity()).hideProgressBar();
            clearTransaction();
            ((TernakActivity)getActivity()).backToHome();
        })
        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                Log.d("Ternaknesia", "getHeaders: " + spUser.getToken());
                headers.put("Authorization", "Bearer " + spUser.getToken() );
                return headers;
            }
        };
        req.setRetryPolicy(new DefaultRetryPolicy(0,0,0));
        RequestSingleton.getInstance(getContext()).addToRequestQueue(req);*/
    }



}
