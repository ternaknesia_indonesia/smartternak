package com.ternaknesia.sobaternak.main;

import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.ternaknesia.sobaternak.Fragment.ReadyOrderFragment;
import com.ternaknesia.sobaternak.R;

public class TernakActivity extends AppCompatActivity {

    //toolbar widget
    private NavigationView navigationView;
    private DrawerLayout drawerLayout;


    //progress bar
    private RelativeLayout loadingScreen;

    //fragment
    private FragmentManager fragmentManager;
    private FragmentTransaction fragmentTransaction;



    //transaction layout
    private LinearLayout buttonLayout;
    private Button toNew, toProgress, toDone, toReady;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ternak);
        initView();
        initToolbar();
        initContent();
    }

    public void showFragment(int option)
    {
        fragmentManager = getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        switch (option)
        {
            /*case 0:
                buttonLayout.setVisibility(View.GONE);
                StockActivity stockFragment= new StockActivity();
                fragmentTransaction.replace(R.id.container, stockFragment);
                break;*/
            /*case 1:
                buttonLayout.setVisibility(View.GONE);
                CashierFragment cashierFragment= new CashierFragment();
                fragmentTransaction.replace(R.id.container, cashierFragment);
                break;*/
            /*case 2:
                buttonLayout.setVisibility(View.GONE);
                ArmadaActivity driverFragment= new ArmadaActivity();
                fragmentTransaction.replace(R.id.container, driverFragment);
                break;*/
            case 3:
                buttonLayout.setVisibility(View.VISIBLE);
                break;
            case 4:
                buttonLayout.setVisibility(View.VISIBLE);
                break;
            case 5:
                buttonLayout.setVisibility(View.VISIBLE);
                break;
            /*case 6:
                buttonLayout.setVisibility(View.GONE);
                OrderFragment orderFragment= new OrderFragment();
                fragmentTransaction.replace(R.id.container, orderFragment);
                break;*/
            case 9:
                buttonLayout.setVisibility(View.VISIBLE);
                ReadyOrderFragment readyOrderFragment = new ReadyOrderFragment();
                fragmentTransaction.replace(R.id.container, readyOrderFragment);
                break;
        }
        fragmentManager.popBackStack();
        fragmentTransaction.commit();
    }

    private void initContent() {
        /*toNew.setOnClickListener(view -> showFragment(3));
        toDone.setOnClickListener(view -> showFragment(5));
        toProgress.setOnClickListener(view ->  showFragment(4));
        toReady.setOnClickListener(view -> showFragment(9));*/
        showFragment(2);

    }

    /*private void showProgressFragment() {
        toNew.setBackgroundTintList(getResources().getColorStateList(R.color.black_overlay));
        toDone.setBackgroundTintList(getResources().getColorStateList(R.color.black_overlay));
        toProgress.setBackgroundTintList(getResources().getColorStateList(R.color.ternaknesia));
        toReady.setBackgroundTintList(getResources().getColorStateList(R.color.black_overlay));
        showFragment(4);
    }

    private void showDoneFragment() {
        toNew.setBackgroundTintList(getResources().getColorStateList(R.color.black_overlay));
        toDone.setBackgroundTintList(getResources().getColorStateList(R.color.ternaknesia));
        toProgress.setBackgroundTintList(getResources().getColorStateList(R.color.black_overlay));
        toReady.setBackgroundTintList(getResources().getColorStateList(R.color.black_overlay));
        showFragment(5);
    }

    private void showFreshFragment() {
        toNew.setBackgroundTintList(getResources().getColorStateList(R.color.ternaknesia));
        toDone.setBackgroundTintList(getResources().getColorStateList(R.color.black_overlay));
        toProgress.setBackgroundTintList(getResources().getColorStateList(R.color.black_overlay));
        toReady.setBackgroundTintList(getResources().getColorStateList(R.color.black_overlay));
        showFragment(3);
    }
    private void showReadyFragment() {
        toNew.setBackgroundTintList(getResources().getColorStateList(R.color.black_overlay));
        toDone.setBackgroundTintList(getResources().getColorStateList(R.color.black_overlay));
        toProgress.setBackgroundTintList(getResources().getColorStateList(R.color.black_overlay));
        toReady.setBackgroundTintList(getResources().getColorStateList(R.color.ternaknesia));
        showFragment(9);
    }*/

    @Override
    public void onBackPressed() {
        finish();
    }

    private void requestPermission() {
        if (ActivityCompat.checkSelfPermission
                        (this, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED ||
            ActivityCompat.checkSelfPermission
                        (this, android.Manifest.permission.INTERNET) != PackageManager.PERMISSION_GRANTED)  {
            ActivityCompat.requestPermissions(this,
                    new String[]{
                            android.Manifest.permission.CALL_PHONE, android.Manifest.permission.ACCESS_COARSE_LOCATION,
                            android.Manifest.permission.INTERNET, android.Manifest.permission.ACCESS_COARSE_LOCATION,

                    }, 20
            );
        } else {
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 20: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    return;
                }
            }
        }
    }

    private void initToolbar() {
        /*setSupportActionBar(toolbar);

        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                int size = navigationView.getMenu().size();
                for (int i = 0; i < size; i++) {
                    navigationView.getMenu().getItem(i).setChecked(false);
                }
                if(item.isChecked()) item.setChecked(false);
                else item.setChecked(true);

                drawerLayout.closeDrawers();
                switch (item.getItemId())
                {
                    case R.id.main_menu:
                        showFragment(3);
                        return true;
                    case R.id.menu1:
                        showFragment(0);
                        return true;
                    case R.id.menu2:
                        showFragment(1);

                        return true;
                    case R.id.menu3:
                        showFragment(6);
                        return true;
                    case R.id.menu4:
                        showFragment(2);

                        return true;
                    case R.id.contact_us:
                        return true;
                    case R.id.help:
                        return true;
                    case R.id.logout:
                        spUser.logout();
                        return true;
                    default:
                        Toast.makeText(getApplicationContext(),"Kesalahan Terjadi ",Toast.LENGTH_SHORT).show();
                        return true;
                }
            }
        });*/

        /*ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.openDrawer,
                R.string.closeDrawer){
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }
        };
        drawerLayout.setDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();*/

    }



    public void backToHome()
    {
        showFragment(3);
    }




    private void initView() {
        requestPermission();
        //toolbar = findViewById(R.id.toolbar);
        navigationView = findViewById(R.id.navigation_view);
        drawerLayout = findViewById(R.id.drawer);
        buttonLayout = findViewById(R.id.transaction_layout);
        toDone = findViewById(R.id.button_done);
        toNew = findViewById(R.id.button_fresh);
        toProgress = findViewById(R.id.button_progress);
        toReady = findViewById(R.id.button_ready);
        loadingScreen = findViewById(R.id.loading);
    }

    public void showProgressBar()
    {
        if(loadingScreen.getVisibility() == View.GONE)
        loadingScreen.setVisibility(View.VISIBLE);
        toNew.setEnabled(false);
        toProgress.setEnabled(false);
        toDone.setEnabled(false);
        //toolbar.setEnabled(false);

    }

    public void hideProgressBar()
    {
        if(loadingScreen.getVisibility() == View.VISIBLE)
        loadingScreen.setVisibility(View.GONE);
        toNew.setEnabled(true);
        toProgress.setEnabled(true);
        toDone.setEnabled(true);
        //toolbar.setEnabled(true);
    }





}
