package com.ternaknesia.sobaternak.main.cashier.bill;

import com.ternaknesia.sobaternak.main.cashier.SellProductClass;

/**
 * Created by Abyan Dafa on 05/06/2018.
 */

public class BillClass {
    private SellProductClass item;
    private int sum;
    private int sumPrice;

    public BillClass(SellProductClass item, int sum) {
        this.item = item;
        this.sum = sum;
        setSumPrice(item.getHarga() * sum);
    }

    public SellProductClass getItem() {
        return item;
    }

    public void setItem(SellProductClass item) {
        this.item = item;
    }

    public int getSum() {
        return sum;
    }

    public void setSum(int sum) {
        this.sum = sum;
    }

    public int getSumPrice() {
        return sumPrice;
    }

    public void setSumPrice(int sumPrice) {
        this.sumPrice = sumPrice;
    }
}
