package com.ternaknesia.sobaternak.main.cashier;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ternaknesia.sobaternak.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Abyan Dafa on 02/06/2018.
 */

public class CashierAdapter extends RecyclerView.Adapter<CashierAdapter.ViewHolder> {

    private List<SellProductClass> listProduct;
    private OnItemListener listener;
    private int category;

    public CashierAdapter(OnItemListener listener, int category) {
        this.listener = listener;
        this.listProduct = new ArrayList<>();
        this.category = category;
    }

    @Override
    public CashierAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_cashier, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(CashierAdapter.ViewHolder holder, int position) {

        final SellProductClass currentItem = listProduct.get(position);
        NumberFormat formatter = new DecimalFormat("#,###");
        double myNumber = Double.valueOf(currentItem.getHarga());
        String price = formatter.format(myNumber);

        holder.price.setText("Rp. " + price);
        if(currentItem.getBerat_max()== 0) holder.range.setText(currentItem.getBerat_min() + " - tidak hingga kg" );
        else holder.range.setText(currentItem.getBerat_min() + " - " + currentItem.getBerat_max() + " kg");
        holder.stock.setText(String.valueOf(currentItem.getStock()));
        holder.productName.setText(currentItem.getNama());
        holder.total.setText(String.valueOf(currentItem.getJumlah()));
        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
               //listener.onLongClick(currentItem, position);
                return true;
            }
        });
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //listener.onClick(currentItem, position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return listProduct.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView productName, stock, range, price, total;
        public ViewHolder(View itemView) {
            super(itemView);
            productName = itemView.findViewById(R.id.item);
            stock = itemView.findViewById(R.id.stock);
            range = itemView.findViewById(R.id.range);
            price = itemView.findViewById(R.id.price);
            total = itemView.findViewById(R.id.total);
        }
    }

    public int getCategory() {
        return category;
    }
    public void updateTotal(int total, int position)
    {
        listProduct.get(position).setJumlah(total);
        notifyDataSetChanged();
    }

    public void removeData()
    {
        listProduct.clear();
    }

    public void addData(JSONArray data) {
        listProduct.clear();
        Log.d("Ternaknesia", "addData: " + data.length());
        for(int i = 0; i<data.length(); i++)
        {
            try {
                JSONObject product = data.getJSONObject(i);
                int product_id = product.getInt("product_id");
                int market_id = product.getInt("market_id");
                int category_id = product.getInt("category_id");
                int stock = product.getInt("stok");
                int berat_min = product.getInt("berat_min");
                int berat_max = product.getInt("berat_max");
                int tinggi_min = product.getInt("tinggi_min");
                int tinggi_max = product.getInt("tinggi_max");
                int beef = product.getInt("beef");
                int harga = product.getInt("harga");
                String nama = product.getString("nama");
                String image = product.getString("image");

                if(stock <= 0) continue;


                String name = product.getString("nama");
                SellProductClass temp = new SellProductClass(product_id, market_id, category_id, stock,
                        berat_min, berat_max, tinggi_min, tinggi_max, beef, harga, nama, image);
                if(temp.getCategory_id() == this.category) listProduct.add(temp);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        notifyDataSetChanged();
    }

    public boolean isEmpty() {
        boolean empty;
        if(listProduct.size()==0) empty = true;
        else empty =false;
        return empty;
    }

    public interface OnItemListener{
        void onLongClick(SellProductClass productItemClass, int position);
        void onClick(SellProductClass productItemClass, int position);
    }

    public List<SellProductClass> getSelectedProducts()
    {
        List<SellProductClass> selectedProducts = new ArrayList<>();
        for(int i=0; i<listProduct.size(); i++)
        {
            SellProductClass current = listProduct.get(i);
            if(current.getJumlah()>0) selectedProducts.add(current);
        }

        return selectedProducts;

    }
}
