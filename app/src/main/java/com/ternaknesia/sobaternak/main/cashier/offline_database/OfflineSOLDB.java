package com.ternaknesia.sobaternak.main.cashier.offline_database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;


import com.ternaknesia.sobaternak.DataModel.DataPendingOrder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Abyan Dafa on 09/06/2018.
 */

public class OfflineSOLDB extends SQLiteOpenHelper {


    private static final int DATABASE_VERSION = 4;
    private Context context;
    // Database Name
    private static final String DATABASE_NAME = "offline_dbsol";

    public OfflineSOLDB(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(SalesOrderLine.CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + SalesOrderLine.TABLE_NAME);

        // Create tables again
        onCreate(sqLiteDatabase);
    }

    public long insertSalesOrderLine(int soId, int terkirim, String driverName,
                                 String driverPhone,
                                 String item, int jumlah,
                                 String destination, String sendTime) {
        // get writable database as we want to write data
        SQLiteDatabase db = this.getWritableDatabase();
        Log.d("RAIHAN", "insertSalesOrderLine: " + jumlah);
        ContentValues values = new ContentValues();
        // `id` and `timestamp` will be inserted automatically.
        // no need to add them
        values.put(SalesOrderLine.COLUMN_ID_SOL, soId);
        values.put(SalesOrderLine.COLUMN_TERKIRIM, terkirim);
        values.put(SalesOrderLine.COLUMN_DRIVER_NAME, driverName);
        values.put(SalesOrderLine.COLUMN_DRIVER_PHONE, driverPhone);
        values.put(SalesOrderLine.COLUMN_ITEM, item);
        values.put(SalesOrderLine.COLUMN_JUMLAH, jumlah);
        values.put(SalesOrderLine.COLUMN_DESTINATION, destination);
        values.put(SalesOrderLine.COLUMN_SEND_TIME, sendTime);
        values.put(SalesOrderLine.COLUMN_STATUS, "BARU");

        // insert row
        long id = db.insert(SalesOrderLine.TABLE_NAME, null, values);

        // close db connection
        db.close();

        // return newly inserted row id
        return id;
    }

    public SalesOrderLine getSalesOrderLine(long id) {
        // get readable database as we are not inserting anything
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(SalesOrderLine.TABLE_NAME,
                new String[]{SalesOrderLine.COLUMN_ID, SalesOrderLine.COLUMN_ID_SOL,
                        SalesOrderLine.COLUMN_TERKIRIM,
                        SalesOrderLine.COLUMN_DRIVER_NAME,
                        SalesOrderLine.COLUMN_DRIVER_PHONE,
                        SalesOrderLine.COLUMN_ITEM,
                        SalesOrderLine.COLUMN_JUMLAH,
                        SalesOrderLine.COLUMN_DESTINATION,
                        SalesOrderLine.COLUMN_SEND_TIME,
                        SalesOrderLine.COLUMN_STATUS},
                SalesOrderLine.COLUMN_ID + "=?",
                new String[]{String.valueOf(id)}, null, null, null, null);

        if (cursor != null)
            cursor.moveToFirst();

        // prepare note object
        SalesOrderLine salesOrder = new SalesOrderLine(
                cursor.getInt(cursor.getColumnIndex(SalesOrderLine.COLUMN_ID)),
                cursor.getInt(cursor.getColumnIndex(SalesOrderLine.COLUMN_ID_SOL)),
                cursor.getInt(cursor.getColumnIndex(SalesOrderLine.COLUMN_TERKIRIM)),
                cursor.getString(cursor.getColumnIndex(SalesOrderLine.COLUMN_DRIVER_NAME)),
                cursor.getString(cursor.getColumnIndex(SalesOrderLine.COLUMN_DRIVER_PHONE)),
                cursor.getString(cursor.getColumnIndex(SalesOrderLine.COLUMN_ITEM)),
                cursor.getString(cursor.getColumnIndex(SalesOrderLine.COLUMN_DESTINATION)),
                cursor.getString(cursor.getColumnIndex(SalesOrderLine.COLUMN_SEND_TIME)),
                cursor.getString(cursor.getColumnIndex(SalesOrderLine.COLUMN_STATUS)));
        // close the db connection
        cursor.close();
        return salesOrder;
    }
    public List<SalesOrderLine> SalesOrderLine() {
        List<SalesOrderLine> salesOrderLines = new ArrayList<>();

        // Select All Query
        String selectQuery = "SELECT  * FROM " + SalesOrderLine.TABLE_NAME + " ORDER BY " +
                SalesOrderLine.COLUMN_ID + " DESC";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                SalesOrderLine salesOrderLine = new SalesOrderLine();
                salesOrderLine.setSales_order_line_id(cursor.getInt(cursor.getColumnIndex(SalesOrderLine.COLUMN_ID)));
                salesOrderLine.setSales_order_id(cursor.getInt(cursor.getColumnIndex(SalesOrderLine.COLUMN_ID_SOL)));
                salesOrderLine.setTerkirim(cursor.getInt(cursor.getColumnIndex(SalesOrderLine.COLUMN_TERKIRIM)));
                salesOrderLine.setCourier_name(cursor.getString(cursor.getColumnIndex(SalesOrderLine.COLUMN_DRIVER_NAME)));
                salesOrderLine.setDriver_phone(cursor.getString(cursor.getColumnIndex(SalesOrderLine.COLUMN_DRIVER_PHONE)));
                salesOrderLine.setItem(cursor.getString(cursor.getColumnIndex(SalesOrderLine.COLUMN_ITEM)));
                salesOrderLine.setDestination(cursor.getString(cursor.getColumnIndex(SalesOrderLine.COLUMN_DESTINATION)));
                salesOrderLine.setSendTime(cursor.getString(cursor.getColumnIndex(SalesOrderLine.COLUMN_SEND_TIME)));
                salesOrderLine.setStatus(cursor.getString(cursor.getColumnIndex(SalesOrderLine.COLUMN_STATUS)));
                salesOrderLine.setJumlah(cursor.getInt(cursor.getColumnIndex(SalesOrderLine.COLUMN_JUMLAH)));
                OfflineDB offlineDB = new OfflineDB(context);
                SalesOrder sl = offlineDB.getSalesOrder(salesOrderLine.getSales_order_id());
                salesOrderLine.setPemesan(sl.getNama());
                salesOrderLines.add(salesOrderLine);
            } while (cursor.moveToNext());
        }

        // close db connection
        db.close();

        // return notes list
        return salesOrderLines;
    }

    public int updateSalesOrderLine(DataPendingOrder item) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(SalesOrderLine.COLUMN_STATUS, "PROSES");
        values.put(SalesOrderLine.COLUMN_DRIVER_PHONE, item.getDriver_phone());
        values.put(SalesOrderLine.COLUMN_DRIVER_NAME, item.getCatatan());

        // updating row
        return db.update(SalesOrderLine.TABLE_NAME, values, SalesOrderLine.COLUMN_ID + " = ?",
                new String[]{String.valueOf(item.getSales_order_line_id())});
    }
    public int updateSalesOrderLine(DataPendingOrder item, String status) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(SalesOrderLine.COLUMN_STATUS, status);
        values.put(SalesOrderLine.COLUMN_DRIVER_PHONE, item.getDriver_phone());
        values.put(SalesOrderLine.COLUMN_DRIVER_NAME, item.getCatatan());

        // updating row
        return db.update(SalesOrderLine.TABLE_NAME, values, SalesOrderLine.COLUMN_ID + " = ?",
                new String[]{String.valueOf(item.getSales_order_line_id())});
    }
    public int updateTerkirimSalesOrderLine(DataPendingOrder item, String terkirim) {
        SQLiteDatabase db = this.getWritableDatabase();
        String status = "PROSES";
        int send = item.getTerkirim() + Integer.valueOf(terkirim);
        Log.d("RAIHAN", "updateTerkirimSalesOrderLine: " + send + "   0  " + item.getJumlah());
        if(send >= item.getJumlah())
        {
            status = "TERKIRIM";
        }
        //item.setTerkirim(item.getTerkirim() + Integer.valueOf(terkirim));
        ContentValues values = new ContentValues();
        values.put(SalesOrderLine.COLUMN_STATUS, status);
        values.put(SalesOrderLine.COLUMN_TERKIRIM, item.getTerkirim());
        // updating row
        return db.update(SalesOrderLine.TABLE_NAME, values, SalesOrderLine.COLUMN_ID + " = ?",
                new String[]{String.valueOf(item.getSales_order_line_id())});
    }

}
