package com.ternaknesia.sobaternak.main.cashier.bill;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ternaknesia.sobaternak.R;
import com.ternaknesia.sobaternak.main.cashier.SellProductClass;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Abyan Dafa on 05/06/2018.
 */

public class BillAdapter extends RecyclerView.Adapter<BillAdapter.ViewHolder> {

    List<BillClass> bills;

    public BillAdapter() {
        this.bills = new ArrayList<>();
    }

    @Override
    public BillAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_bill, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(BillAdapter.ViewHolder holder, int position) {
        BillClass currentItem = bills.get(position);

        NumberFormat formatter = new DecimalFormat("#,###");
        double myNumber = Double.valueOf(currentItem.getItem().getHarga());
        String itemPrice = formatter.format(myNumber);
        myNumber = Double.valueOf(currentItem.getSumPrice());
        String subPrice = formatter.format(myNumber);

        holder.price.setText("Rp. " + itemPrice);
        holder.sumPrice.setText("Rp. " + subPrice);

        if(currentItem.getItem().getBerat_max()== 0) holder.range.setText(currentItem.getItem().getBerat_min() + " - tidak hingga kg" );
        else holder.range.setText(currentItem.getItem().getBerat_min() + " - " + currentItem.getItem().getBerat_max() + " kg");

        holder.productName.setText(currentItem.getItem().getNama());
        holder.total.setText(String.valueOf(currentItem.getSum()));

    }

    @Override
    public int getItemCount() {
        return bills.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView productName, range, price, total, sumPrice;
        public ViewHolder(View itemView) {
            super(itemView);
            productName = itemView.findViewById(R.id.item);
            range = itemView.findViewById(R.id.range);
            price = itemView.findViewById(R.id.price);
            total = itemView.findViewById(R.id.total);
            sumPrice = itemView.findViewById(R.id.sum_price);
        }
    }

    public void addProducts(List<SellProductClass> selectedProducts)
    {
        for(int i = 0; i<selectedProducts.size(); i++)
        {
            SellProductClass current = selectedProducts.get(i);
            bills.add(new BillClass(current, current.getJumlah()));
        }

        notifyDataSetChanged();
        Log.d("selasa ternaknesia", "addProducts: " + bills.size());
    }
    public int totalPriceProduct()
    {
        int sumPrice = 0;
        for(int i = 0; i<bills.size(); i++) sumPrice += bills.get(i).getSumPrice();

        return sumPrice;
    }

    public List<BillClass> allProduct()
    {
        return bills;
    }
}
