package com.ternaknesia.sobaternak.main.cashier.offline_database;

/**
 * Created by Abyan Dafa on 08/06/2018.
 */

public class SalesOrderLine {

    private int sales_order_id, sales_order_line_id;
    private int terkirim;
    private String courier_name, driver_phone;
    private String item;
    private int jumlah;
    private String destination;
    private String sendTime;
    private int market_id;
    private String status;
    private String pemesan;

    public String getPemesan() {
        return pemesan;
    }

    public void setPemesan(String pemesan) {
        this.pemesan = pemesan;
    }

    public static final String TABLE_NAME = "SalesOrderline";

    public static final String COLUMN_ID = "id";
    public static final String COLUMN_ID_SOL = "id_sol";
    public static final String COLUMN_TERKIRIM = "terkirim";
    public static final String COLUMN_DRIVER_NAME = "courier_name";
    public static final String COLUMN_DRIVER_PHONE = "courier_phone";
    public static final String COLUMN_ITEM = "item";
    public static final String COLUMN_JUMLAH = "jumlah";
    public static final String COLUMN_DESTINATION = "destination";
    public static final String COLUMN_SEND_TIME = "send_time";
    public static final String COLUMN_STATUS = "status";

    public static final String CREATE_TABLE =
            "CREATE TABLE " + TABLE_NAME + "("
                    + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                    + COLUMN_ID_SOL + " INTEGER,"
                    + COLUMN_TERKIRIM + " INTEGER,"
                    + COLUMN_DRIVER_NAME + " TEXT,"
                    + COLUMN_DRIVER_PHONE + " TEXT,"
                    + COLUMN_ITEM + " TEXT,"
                    + COLUMN_JUMLAH + " INTEGER,"
                    + COLUMN_DESTINATION + " TEXT,"
                    + COLUMN_SEND_TIME + " TEXT,"
                    + COLUMN_STATUS + " TEXT"
                    + ")";



    public SalesOrderLine() {
    }


    public SalesOrderLine(int sales_order_id, int sales_order_line_id,
                          int terkirim, String courier_name, String driver_phone,
                          String item, String destination, String sendTime, String status) {
        this.sales_order_id = sales_order_id;
        this.sales_order_line_id = sales_order_line_id;
        this.terkirim = terkirim;
        this.courier_name = courier_name;
        this.driver_phone = driver_phone;
        this.item = item;
        this.destination = destination;
        this.sendTime = sendTime;
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getSales_order_id() {
        return sales_order_id;
    }

    public void setSales_order_id(int sales_order_id) {
        this.sales_order_id = sales_order_id;
    }

    public int getSales_order_line_id() {
        return sales_order_line_id;
    }

    public void setSales_order_line_id(int sales_order_line_id) {
        this.sales_order_line_id = sales_order_line_id;
    }

    public int getTerkirim() {
        return terkirim;
    }

    public void setTerkirim(int terkirim) {
        this.terkirim = terkirim;
    }

    public String getCourier_name() {
        return courier_name;
    }

    public void setCourier_name(String courier_name) {
        this.courier_name = courier_name;
    }

    public String getDriver_phone() {
        return driver_phone;
    }

    public void setDriver_phone(String driver_phone) {
        this.driver_phone = driver_phone;
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public int getJumlah() {
        return jumlah;
    }

    public void setJumlah(int jumlah) {
        this.jumlah = jumlah;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getSendTime() {
        return sendTime;
    }

    public void setSendTime(String sendTime) {
        this.sendTime = sendTime;
    }



    public int getMarket_id() {
        return market_id;
    }

    public void setMarket_id(int market_id) {
        this.market_id = market_id;
    }
}
