package com.ternaknesia.sobaternak.main.cashier;

/**
 * Created by Abyan Dafa on 02/06/2018.
 */

public class SellProductClass {
    private int product_id, market_id, category_id, stock, berat_min, berat_max, tinggi_min, tinggi_max, beef, harga;
    private String nama, image;
    private int jumlah, index;

    public SellProductClass(int product_id, int market_id, int category_id, int stock, int berat_min, int berat_max, int tinggi_min, int tinggi_max, int beef, int harga, String nama, String image) {
        this.product_id = product_id;
        this.market_id = market_id;
        this.category_id = category_id;
        this.stock = stock;
        this.berat_min = berat_min;
        this.berat_max = berat_max;
        this.tinggi_min = tinggi_min;
        this.tinggi_max = tinggi_max;
        this.beef = beef;
        this.harga = harga;
        this.nama = nama;
        this.image = image;
        this.jumlah = 0;
    }

    public int getProduct_id() {
        return product_id;
    }

    public void setProduct_id(int product_id) {
        this.product_id = product_id;
    }

    public int getMarket_id() {
        return market_id;
    }

    public void setMarket_id(int market_id) {
        this.market_id = market_id;
    }

    public int getCategory_id() {
        return category_id;
    }

    public void setCategory_id(int category_id) {
        this.category_id = category_id;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public int getBerat_min() {
        return berat_min;
    }

    public void setBerat_min(int berat_min) {
        this.berat_min = berat_min;
    }

    public int getBerat_max() {
        return berat_max;
    }

    public void setBerat_max(int berat_max) {
        this.berat_max = berat_max;
    }

    public int getTinggi_min() {
        return tinggi_min;
    }

    public void setTinggi_min(int tinggi_min) {
        this.tinggi_min = tinggi_min;
    }

    public int getTinggi_max() {
        return tinggi_max;
    }

    public void setTinggi_max(int tinggi_max) {
        this.tinggi_max = tinggi_max;
    }

    public int getBeef() {
        return beef;
    }

    public void setBeef(int beef) {
        this.beef = beef;
    }

    public int getHarga() {
        return harga;
    }

    public void setHarga(int harga) {
        this.harga = harga;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getJumlah() {
        return jumlah;
    }

    public void setJumlah(int jumlah) {
        this.jumlah = jumlah;
    }
}
