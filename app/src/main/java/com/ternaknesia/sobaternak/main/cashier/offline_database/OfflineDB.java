package com.ternaknesia.sobaternak.main.cashier.offline_database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.ternaknesia.sobaternak.SQLiteDB.Driver;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Abyan Dafa on 08/06/2018.
 */

public class OfflineDB extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 3;

    // Database Name
    private static final String DATABASE_NAME = "offline_dbs";
    public OfflineDB(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(SalesOrder.CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + SalesOrder.TABLE_NAME);

        // Create tables again
        onCreate(sqLiteDatabase);

    }

    public long insertSalesOrder(String nama, String alamat, String tanggalKirim, String telepon, String status) {
        // get writable database as we want to write data
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        // `id` and `timestamp` will be inserted automatically.
        // no need to add them
        values.put(SalesOrder.COLUMN_NAMA, nama);
        values.put(SalesOrder.COLUMN_ALAMAT, alamat);
        values.put(SalesOrder.COLUMN_TANGGAL_PENGIRIMAN, tanggalKirim);
        values.put(SalesOrder.COLUMN_TELEPON, telepon);
        values.put(SalesOrder.COLUMN_STATUS, status);

        // insert row
        long id = db.insert(SalesOrder.TABLE_NAME, null, values);

        // close db connection
        db.close();

        // return newly inserted row id
        return id;
    }

    public SalesOrder getSalesOrder(long id) {
        // get readable database as we are not inserting anything
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(SalesOrder.TABLE_NAME,
                new String[]{SalesOrder.COLUMN_ID, SalesOrder.COLUMN_NAMA,
                        SalesOrder.COLUMN_ALAMAT,
                        SalesOrder.COLUMN_TANGGAL_PENGIRIMAN,
                        SalesOrder.COLUMN_TELEPON,
                        SalesOrder.COLUMN_STATUS},
                SalesOrder.COLUMN_ID + "=?",
                new String[]{String.valueOf(id)}, null, null, null, null);

        if (cursor != null)
            cursor.moveToFirst();

        // prepare note object
        SalesOrder salesOrder = new SalesOrder(
                cursor.getInt(cursor.getColumnIndex(SalesOrder.COLUMN_ID)),
                cursor.getString(cursor.getColumnIndex(SalesOrder.COLUMN_NAMA)),
                cursor.getString(cursor.getColumnIndex(SalesOrder.COLUMN_ALAMAT)),
                cursor.getString(cursor.getColumnIndex(SalesOrder.COLUMN_TANGGAL_PENGIRIMAN)),
                cursor.getString(cursor.getColumnIndex(SalesOrder.COLUMN_TELEPON)),
                cursor.getString(cursor.getColumnIndex(SalesOrder.COLUMN_STATUS)));
        // close the db connection
        cursor.close();
        return salesOrder;
    }
    public List<SalesOrder> getAllSalesOrder() {
        List<SalesOrder> salesOrders = new ArrayList<>();

        // Select All Query
        String selectQuery = "SELECT  * FROM " + SalesOrder.TABLE_NAME + " ORDER BY " +
                Driver.COLUMN_ID + " DESC";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                SalesOrder salesOrder = new SalesOrder();
                salesOrder.setId(cursor.getInt(cursor.getColumnIndex(SalesOrder.COLUMN_ID)));
                salesOrder.setNama(cursor.getString(cursor.getColumnIndex(SalesOrder.COLUMN_NAMA)));
                salesOrder.setAlamat(cursor.getString(cursor.getColumnIndex(SalesOrder.COLUMN_ALAMAT)));
                salesOrder.setTanggalKirim(cursor.getString(cursor.getColumnIndex(SalesOrder.COLUMN_TANGGAL_PENGIRIMAN)));
                salesOrder.setTelepon(cursor.getString(cursor.getColumnIndex(SalesOrder.COLUMN_TELEPON)));
                salesOrder.setStatus(cursor.getString(cursor.getColumnIndex(SalesOrder.COLUMN_STATUS)));
                Log.d("Ternaknesia", "getAllDrivers: " + salesOrder.getNama());

                salesOrders.add(salesOrder);
            } while (cursor.moveToNext());
        }

        // close db connection
        db.close();

        // return notes list
        return salesOrders;
    }

//    public SalesOrder getSalesOrder(int id) {
//        SalesOrder salesOrder = new SalesOrder();
//
//        // Select All Query
//        String selectQuery = "SELECT  * FROM " + SalesOrder.TABLE_NAME + "WHERE ID = " + id + " ORDER BY " +
//                Driver.COLUMN_ID + " DESC";
//
//        SQLiteDatabase db = this.getWritableDatabase();
//        Cursor cursor = db.rawQuery(selectQuery, null);
//
//        // looping through all rows and adding to list
//        if (cursor.moveToFirst()) {
//            do {
//                salesOrder.setId(cursor.getInt(cursor.getColumnIndex(SalesOrder.COLUMN_ID)));
//                salesOrder.setNama(cursor.getString(cursor.getColumnIndex(SalesOrder.COLUMN_NAMA)));
//                salesOrder.setAlamat(cursor.getString(cursor.getColumnIndex(SalesOrder.COLUMN_ALAMAT)));
//                salesOrder.setTanggalKirim(cursor.getString(cursor.getColumnIndex(SalesOrder.COLUMN_TANGGAL_PENGIRIMAN)));
//                salesOrder.setTelepon(cursor.getString(cursor.getColumnIndex(SalesOrder.COLUMN_TELEPON)));
//                salesOrder.setStatus(cursor.getString(cursor.getColumnIndex(SalesOrder.COLUMN_STATUS)));
//                Log.d("Ternaknesia", "getAllDrivers: " + salesOrder.getNama());
//
//            } while (cursor.moveToNext());
//        }
//
//        // close db connection
//        db.close();
//
//        // return notes list
//        return salesOrder;
//    }
}
