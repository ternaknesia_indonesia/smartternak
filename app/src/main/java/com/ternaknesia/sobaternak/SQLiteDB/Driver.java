package com.ternaknesia.sobaternak.SQLiteDB;

/**
 * Created by Abyan Dafa on 27/05/2018.
 */

public class Driver {
    public static final String TABLE_NAME = "Drivers";

    public static final String COLUMN_ID = "id";
    public static final String COLUMN_NAMA = "nama";
    public static final String COLUMN_TELEPON = "telepon";

    private int id;
    private String nama;
    private String telepon;

    public Driver(int id, String nama, String telepon) {
        this.id = id;
        this.nama = nama;
        this.telepon = telepon;
    }

    public String getTelepon() {
        return telepon;
    }

    public void setTelepon(String telepon) {
        this.telepon = telepon;
    }

    // Create table SQL query
    public static final String CREATE_TABLE =
            "CREATE TABLE " + TABLE_NAME + "("
                    + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                    + COLUMN_NAMA + " TEXT,"
                    + COLUMN_TELEPON + " TEXT"
                    + ")";

    public Driver() {
    }

//    public Driver(int id, String nama) {
//        this.id = id;
//        this.nama = nama;
//    }



    public int getId() {
        return id;
    }


    public void setId(int id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }
}
