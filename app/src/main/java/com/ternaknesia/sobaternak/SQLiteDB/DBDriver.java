package com.ternaknesia.sobaternak.SQLiteDB;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Abyan Dafa on 27/05/2018.
 */

public class DBDriver extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "driver_dbs";

    public DBDriver(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(Driver.CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + Driver.TABLE_NAME);

        // Create tables again
        onCreate(sqLiteDatabase);
    }

    public long insertDriver(String nama, String telepon) {
        // get writable database as we want to write data
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        // `id` and `timestamp` will be inserted automatically.
        // no need to add them
        values.put(Driver.COLUMN_NAMA, nama);
        values.put(Driver.COLUMN_TELEPON, telepon);

        // insert row
        long id = db.insert(Driver.TABLE_NAME, null, values);

        // close db connection
        db.close();

        // return newly inserted row id
        return id;
    }

    public Driver getNote(long id) {
        // get readable database as we are not inserting anything
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(Driver.TABLE_NAME,
                new String[]{Driver.COLUMN_ID, Driver.COLUMN_NAMA, Driver.COLUMN_TELEPON},
                Driver.COLUMN_ID + "=?",
                new String[]{String.valueOf(id)}, null, null, null, null);

        if (cursor != null)
            cursor.moveToFirst();

        // prepare note object
        Driver driver = new Driver(
                cursor.getInt(cursor.getColumnIndex(Driver.COLUMN_ID)),
                cursor.getString(cursor.getColumnIndex(Driver.COLUMN_NAMA)),
                cursor.getString(cursor.getColumnIndex(Driver.COLUMN_TELEPON)));

        // close the db connection
        cursor.close();

        return driver;
    }

    public List<Driver> getAllDrivers() {
        List<Driver> notes = new ArrayList<>();

        // Select All Query
        String selectQuery = "SELECT  * FROM " + Driver.TABLE_NAME + " ORDER BY " +
                Driver.COLUMN_ID + " DESC";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Driver note = new Driver();
                note.setId(cursor.getInt(cursor.getColumnIndex(Driver.COLUMN_ID)));
                note.setNama(cursor.getString(cursor.getColumnIndex(Driver.COLUMN_NAMA)));
                note.setTelepon(cursor.getString(cursor.getColumnIndex(Driver.COLUMN_TELEPON)));
                Log.d("Ternaknesia", "getAllDrivers: " + note.getNama());

                notes.add(note);
            } while (cursor.moveToNext());
        }

        // close db connection
        db.close();

        // return notes list

        return notes;
    }

    public int getDriverCount() {
        String countQuery = "SELECT  * FROM " + Driver.TABLE_NAME;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);

        int count = cursor.getCount();
        cursor.close();


        // return count
        return count;
    }

    public int updateDriver(Driver driver) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(Driver.COLUMN_NAMA, driver.getNama());
        values.put(Driver.COLUMN_TELEPON, driver.getTelepon());

        // updating row
        return db.update(Driver.TABLE_NAME, values, Driver.COLUMN_ID + " = ?",
                new String[]{String.valueOf(driver.getId())});
    }

    public void deleteDriver(Driver driver) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(Driver.TABLE_NAME, Driver.COLUMN_ID + " = ?",
                new String[]{String.valueOf(driver.getId())});
        db.close();
    }
}
