package com.ternaknesia.sobaternak.activity;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.ternaknesia.sobaternak.Adapter.DriverAdapter;
import com.ternaknesia.sobaternak.DataModel.DataDriver;
import com.ternaknesia.sobaternak.R;
import com.ternaknesia.sobaternak.SQLiteDB.DBDriver;
import com.ternaknesia.sobaternak.SQLiteDB.Driver;
import com.ternaknesia.sobaternak.Utils.Item;

import java.util.ArrayList;
import java.util.List;

import static com.ternaknesia.sobaternak.Application.App.getContext;

/**
 * A simple {@link Fragment} subclass.
 */
public class ArmadaActivity extends AppCompatActivity {
    private EditText name, phoneNumber;
    private Button add;
    private DBDriver db;
    private RecyclerView recyclerView;
    private List<Driver> dlist;
    private ArrayList<Item> drivers_data;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_driver);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Data Armada");


        name = findViewById(R.id.driver_name);
        add = findViewById(R.id.driver_add);
        phoneNumber = findViewById(R.id.driver_phone_number);
        recyclerView = findViewById(R.id.list_driver);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setNestedScrollingEnabled(false);

        showData();

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (name.getText().toString().equalsIgnoreCase("") || phoneNumber.getText().toString().equalsIgnoreCase(""))
                    Toast.makeText(getContext(), "Lengkapi isian!", Toast.LENGTH_LONG).show();
                else {
                    addDriver(name.getText().toString(), phoneNumber.getText().toString());
                    name.setText("");
                    phoneNumber.setText("");
                }
            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    private void showData() {
        db = new DBDriver(this);
        dlist = db.getAllDrivers();
        if (db.getDriverCount() == 0) {
            Toast.makeText(this, "Data armada masih kosong", Toast.LENGTH_SHORT).show();
        } else {
            drivers_data = new ArrayList<>(db.getDriverCount());
            for (int i = 0; i < db.getDriverCount(); i++) {
                String name = dlist.get(i).getNama();
                String phone = dlist.get(i).getTelepon();
                drivers_data.add(new DataDriver(name, phone));
            }
            recyclerView.setAdapter(new DriverAdapter(drivers_data, this));
        }
    }

    public void addDriver(String name, String phoneNumber) {
        long id = db.insertDriver(name, phoneNumber);

        Driver n = db.getNote(id);

        if (n != null) {
            Toast.makeText(this, "Berhasil menambahkan driver", Toast.LENGTH_SHORT).show();
            showData();
        }
    }
}
