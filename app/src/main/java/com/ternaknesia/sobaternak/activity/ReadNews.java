package com.ternaknesia.sobaternak.activity;
/*        Mohon untuk tidak melakukan hal-hal yang melanggar hukum.
        Anda tidak berhak melakukan cracking terhadap system kami tanpa ada persetujuan.
        Terimakasih*/

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.ternaknesia.sobaternak.Connection.GetRequest;
import com.ternaknesia.sobaternak.Interface.EndpointAPI;
import com.ternaknesia.sobaternak.Interface.RequestComplete;
import com.ternaknesia.sobaternak.R;
import com.ternaknesia.sobaternak.Utils.DateFormatter;
import com.ternaknesia.sobaternak.Utils.ImageViewer;

import org.json.JSONException;
import org.json.JSONObject;

public class ReadNews extends AppCompatActivity implements RequestComplete {
    //String __title, __author, __date, __content;
    String __id = "", __slug = "";
    //int type = 0;
    ImageView img;
    TextView title;
    TextView author;
    TextView date;
    WebView content;
    TextView views;
    RelativeLayout loading;
    ScrollView news;
    String _image = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_read_news);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) getSupportActionBar().setTitle("Baca Artikel");
        if (getSupportActionBar() != null) getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        img = findViewById(R.id.news_img);
        title = findViewById(R.id.txtTitle);
        author = findViewById(R.id.txtAuthor);
        date = findViewById(R.id.txtDate);
        content = findViewById(R.id.contentWeb);
        loading = findViewById(R.id.loading);
        news = findViewById(R.id.myscview);
        views = findViewById(R.id.countViews);

        loading.setVisibility(View.VISIBLE);
        news.setVisibility(View.GONE);

        /*init get data from previous*/
        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if (extras == null) {
                __id = "";
                __slug = "";
            } else {
                if(extras.containsKey("blog_id"))
                    __id = extras.getString("blog_id");
                if(extras.containsKey("blog_slug"))
                    __slug = extras.getString("blog_slug");
            }
        } else {
            if(savedInstanceState.containsKey("blog_id"))
                __id = (String) savedInstanceState.getSerializable("blog_id");
            if(savedInstanceState.containsKey("blog_slug"))
                __slug = (String) savedInstanceState.getSerializable("blog_slug");

        }

        GetRequest connection = new GetRequest(this);
        if(__id != null && !__id.equalsIgnoreCase(""))
            connection.SendGetData(EndpointAPI.ReadNews + __id,"", 1);
        else if (__slug != null && !__slug.equalsIgnoreCase(""))
            connection.SendGetData(EndpointAPI.ReadNews + __slug,"", 1);
        connection.execute();

        /*title.setText(__title);
        author.setText(__author);
        date.setText(__date);
        content.setText(Html.fromHtml(__content));
        img.setImageResource(__img);*/

        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent a = new Intent(ReadNews.this, ImageViewer.class);
                Bundle extras = new Bundle();
                extras.putString("imgs", _image);
                extras.putInt("position", 0);
                a.putExtras(extras);
                startActivity(a);
            }
        });
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // save file url in bundle as it will be null on scren orientation
        // changes
        outState.putString("blog_id", __id);
        outState.putString("blog_slug", __slug);
        //outState.putInt("type", type);
    }

    /*
     * Here we restore the fileUri again
     */
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        // get the file url
        __id = savedInstanceState.getString("blog_id");
        __slug = savedInstanceState.getString("blog_slug");
        //type = savedInstanceState.getInt("type");
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.read_news_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.menu_item_share:
                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                String shareBodyText = title.getText().toString() + ", Lihat selengkapnya... \nUnduh aplikasi Ternaknesia di Play Store : https://play.google.com/store/apps/details?id=com.ternaknesia.app";
                sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "Ternaknesia");
                sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBodyText);
                startActivity(Intent.createChooser(sharingIntent, "Bagikan artikel"));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        this.finish();
        return super.onSupportNavigateUp();
    }

    @Override
    public void onBackPressed() {
        this.finish();
        super.onBackPressed();
    }

    @Override
    public void onRequestComplete(JSONObject jsonObject, Integer REQUEST_CODE) {
        try {
            if (jsonObject.getString("response").equalsIgnoreCase("success")) {
                JSONObject dat = jsonObject.getJSONObject("result");
                int id = dat.getInt("id");
                String _title = dat.getString("title");
                _image = EndpointAPI.HostStorage + dat.getString("image");
                String _date = dat.getString("created_at");
                String _content = dat.getString("body");
                String _views = dat.getString("views");


                JSONObject getauthor = dat.getJSONObject("author");
                String _author = getauthor.getString("name");

                title.setText(_title);
                views.setText(_views);
                author.setText(_author);

                Glide.with(this)
                        .load(_image)
                        .placeholder(R.drawable.news_placeholder)
                        .centerCrop()
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .skipMemoryCache(true)
                        .into(img);

                date.setText(new DateFormatter().convertDate(_date, "EEEE, dd MMMM yyyy HH:mm"));
                content.loadDataWithBaseURL("", _content, "text/html", "UTF-8", "");
                loading.setVisibility(View.GONE);
                news.setVisibility(View.VISIBLE);
            } else {
                //Toast.makeText(this, "Kesalahan server", Toast.LENGTH_LONG).show();
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage(R.string.try_again)
                        .setCancelable(false)
                        .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                                finish();
                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();
            }
        } catch (JSONException e) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage(R.string.try_again)
                    .setCancelable(false)
                    .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.dismiss();
                            finish();
                        }
                    });
            AlertDialog alert = builder.create();
            alert.show();
            //e.printStackTrace();
        }
    }
}
