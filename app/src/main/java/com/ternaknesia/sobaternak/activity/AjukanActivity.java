package com.ternaknesia.sobaternak.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.ternaknesia.sobaternak.Connection.PostRequest;
import com.ternaknesia.sobaternak.Interface.RequestComplete;
import com.ternaknesia.sobaternak.Manager.PrefManager;
import com.ternaknesia.sobaternak.R;

import org.json.JSONObject;

import okhttp3.FormBody;
import okhttp3.RequestBody;


public class AjukanActivity extends AppCompatActivity implements RequestComplete {
    private EditText name;
    private EditText address;
    private EditText telp;
    private EditText jenis;
    private EditText kapasitas;
    private CheckBox setujui;
    private PrefManager prefManager;
    private PostRequest postRequest;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ajukan);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Ajukan Pendanaan");

        prefManager = new PrefManager(this);

        name = findViewById(R.id.name);
        address = findViewById(R.id.address);
        telp = findViewById(R.id.telp);
        jenis = findViewById(R.id.jenis_ternak);
        kapasitas = findViewById(R.id.kapasitas_kandang);
        setujui = findViewById(R.id.c_setuju);

        Button ajukan = findViewById(R.id.buttonAjukan);
        ajukan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(setujui.isChecked()) {
                    String _name = name.getText().toString();
                    String _address = address.getText().toString();
                    String _telp = telp.getText().toString();
                    String _jenis = jenis.getText().toString();
                    String _kapasitas = kapasitas.getText().toString();

                    postRequest = new PostRequest(AjukanActivity.this);
                    RequestBody requestBody = new FormBody.Builder()
                            .add("name", _name)
                            .add("address", _address)
                            .add("telp", _telp)
                            .add("jenis", _jenis)
                            .add("kapasitas", _kapasitas)
                            .build();
                    postRequest.SendPostData("",prefManager.getToken(), requestBody, 1);
                    postRequest.execute();

                } else {
                    Toast.makeText(getBaseContext(), "Silakan mencentang persetujuan.", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    @Override
    public void onRequestComplete(JSONObject jsonObject, Integer REQUEST_CODE) {

    }
}
