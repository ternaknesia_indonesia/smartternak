package com.ternaknesia.sobaternak.activity;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.ternaknesia.sobaternak.Connection.PostRequest;
import com.ternaknesia.sobaternak.Interface.EndpointAPI;
import com.ternaknesia.sobaternak.Interface.RequestComplete;
import com.ternaknesia.sobaternak.Manager.PrefManager;
import com.ternaknesia.sobaternak.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import okhttp3.FormBody;
import okhttp3.RequestBody;


public class BeliHewanActivity extends AppCompatActivity implements RequestComplete {
    private static final int DATE_DIALOG_ID = 0;
    private PostRequest postRequest;
    private PostRequest postRequestKandang;
    private PrefManager prefManager;
    private RelativeLayout pilih_foto_hewan;
    private ImageView foto_hewan;
    private static int PERMISSION_ALL = 1;
    private String[] PERMISSIONS = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA};
    private static final int CAMERA_REQUEST_CODE = 0;
    private static final int GALLERY_REQUEST_CODE = 1;
    private Uri FileUriOutput = null;//Uri to capture image
    private String pathImg = "";
    private Integer id;
    private Integer instance_id;
    private Spinner instance;
    private String instance_code;
    private LinearLayout otherField;
    private Integer project_id;
    private Spinner project;
    private Integer animal_type_id;
    private Integer animal_count;
    private Spinner animalType;
    private String jenis_kelamin;
    private Spinner genderType;
    private EditText buy_date;
    private EditText harvest_date;
    private EditText qty;
    private EditText buy_price_unit;
    private EditText buy_price_kg;
    private AlertDialog dialog;
    private Button tambah;
    private Button previous;
    private int pYear;
    private int pMonth;
    private int pDay;
    private String date;
    private HashMap<Integer, Integer> instancesMap;
    private ArrayList<String> instancesLabel;
    private ArrayList<String> instancesCode;
    private HashMap<Integer, Integer> projectsMap;
    private JSONArray animals;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_beli_hewan);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Beli Hewan");

        init();

        String[] animal_options = new String[]{"Jenis Hewan", "Kambing", "Sapi", "Domba"};
        ArrayAdapter<String> animalAdapter = new ArrayAdapter<String>(this, R.layout.simple_spinner_item, animal_options);
        animalType.setAdapter(animalAdapter);

        String[] gender_options = new String[]{"Jenis Kelamin", "Jantan", "Betina"};
        ArrayAdapter<String> genderAdapter = new ArrayAdapter<String>(this, R.layout.simple_spinner_item, gender_options);
        genderType.setAdapter(genderAdapter);

        prefManager = new PrefManager(this);
        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if (extras == null) {
                instance_id = null;
            } else {
                instance_id = extras.getInt("instance_id");
            }

        } else {
            instance_id = (Integer) savedInstanceState.getSerializable("instance_id");
        }

        RequestBody data = new FormBody.Builder()
                .build();

        postRequestKandang = new PostRequest(BeliHewanActivity.this);
        postRequestKandang.SendPostData(EndpointAPI.myInstances, prefManager.getToken(), data, 0);
        postRequestKandang.execute();

        instance.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(position != 0) {
                    instance_id = instancesMap.get(instance.getSelectedItemPosition());
                    RequestBody data = new FormBody.Builder()
                            .add("instance_id", instance_id.toString())
                            .build();

                    postRequestKandang = new PostRequest(BeliHewanActivity.this);
                    postRequestKandang.SendPostData(EndpointAPI.getAddHewan, prefManager.getToken(), data, 1);
                    postRequestKandang.execute();
                } else {
                    otherField.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                otherField.setVisibility(View.GONE);
            }
        });

        tambah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    instance_code = instancesCode.get(instance.getSelectedItemPosition());
                    project_id = projectsMap.get(project.getSelectedItemPosition());
                    animal_type_id = animalType.getSelectedItemPosition();
                    jenis_kelamin = genderType.getSelectedItem().toString().toLowerCase();

                    JSONArray animals = new JSONArray();
                    Integer jml = Integer.parseInt(qty.getText().toString());
                    for(int i = 0; i < jml; i++) {
                        String instance_label = instancesLabel.get(instance.getSelectedItemPosition());
                        JSONObject item = new JSONObject();
                        item.put("instance_id", instance_id);
                        item.put("instance_label", instance_label);
                        item.put("code_tag", instance_code);
                        item.put("project_id", project_id);
                        item.put("animal_type_id", animal_type_id);
                        item.put("kandang_id", 0);
                        item.put("jenis_kelamin", jenis_kelamin);
                        item.put("status", "hidup");
                        item.put("pemesan", "");
                        item.put("buy_date", buy_date.getText().toString());
                        item.put("harvest_date", harvest_date.getText().toString());
                        item.put("harvest_age", 0);
                        item.put("buy_price_unit", buy_price_unit.getText().toString());
                        item.put("buy_price_kg", buy_price_kg.getText().toString());
                        item.put("weight", 0);
                        item.put("height", 0);
                        animals.put(i, item);
                    }
                    completeDataHewan(animals);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        buy_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GetDate();
            }
        });
    }

    private void completeDataHewan(JSONArray animals) {
            Intent hewan = new Intent(getBaseContext(), EditHewanActivity.class);
            Bundle hewanData = new Bundle();
            hewanData.putInt("instance_id", instance_id);
            hewanData.putInt("index_animals", 0);
            hewanData.putString("animals", animals.toString());
            hewan.putExtras(hewanData);
            startActivity(hewan);
            finish();
    }

    private void GetDate() {
        Calendar c = Calendar.getInstance();
        pYear = c.get(Calendar.YEAR);
        pMonth = c.get(Calendar.MONTH);
        pDay = c.get(Calendar.DAY_OF_MONTH);
        showDialog(DATE_DIALOG_ID);
    }

    public Dialog onCreateDialog(int id) {
        switch (id) {
            case DATE_DIALOG_ID:
                DatePickerDialog.OnDateSetListener pDateSetListener = new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        //pYear = year;
                        //pMonth = monthOfYear;
                        //pDay = dayOfMonth;
                        String __date_of = String.valueOf(new StringBuilder().append(dayOfMonth).append("/").append(monthOfYear + 1).append("/").append(year));
                        buy_date.setText(__date_of);
                        //setDate(new DateFormatter().newDateFrom(__date_of, "dd-MM-yyyy"));
                    }
                };
                DatePickerDialog dialog = new DatePickerDialog(BeliHewanActivity.this, pDateSetListener, pYear, pMonth, pDay);

                //Date newDate = new DateFormatter().newDateFrom(qurban_date, "yyyy-MM-dd HH:mm:ss");
                Date newDate = Calendar.getInstance().getTime();

                /*if (newDate != null) {
                    dialog.getDatePicker().setMinDate(newDate.getTime() - (1000 * 60 * 60 * 24 * day_min));
                    dialog.getDatePicker().setMaxDate(newDate.getTime() + (1000 * 60 * 60 * 24 * day_max));
                }*/

                /*if (newDate != null) {
                    dialog.getDatePicker().updateDate(newDate.getYear(), newDate.getMonth(), newDate.getDay());
                }*/

                return dialog;
            default:
                return null;
        }
    }

    private void init() {
        instance = findViewById(R.id.instance);
        otherField = findViewById(R.id.otherFieldContainer);
        project = findViewById(R.id.project);
        animalType = findViewById(R.id.animalType);
        genderType = findViewById(R.id.genderType);
        buy_date = findViewById(R.id.buy_date);
        harvest_date = findViewById(R.id.harvest_date);
        qty = findViewById(R.id.qty);
        buy_price_unit = findViewById(R.id.buy_price_unit);
        buy_price_kg = findViewById(R.id.buy_price_kg);
        tambah = findViewById(R.id.btn_tambah);
        previous = findViewById(R.id.btn_previous);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    @Override
    public void onRequestComplete(JSONObject jsonObject, Integer REQUEST_CODE) {
        if (REQUEST_CODE == 2) {
            try {
                if (jsonObject.getString("response").equalsIgnoreCase("success")) {
                    Toast.makeText(this, "Berhasil menambah transaksi", Toast.LENGTH_SHORT).show();

                    animals = jsonObject.getJSONArray("result");
                    completeDataHewan(animals);
                } else {
                    Toast.makeText(this, "Gagal menambah transaksi", Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else if(REQUEST_CODE == 1) {
            try {
                ArrayList<String> projectsLabel = new ArrayList<String>();
                projectsLabel.add("Pilih Project");

                projectsMap = new HashMap<Integer, Integer>();
                projectsMap.put(0, 0);

                if (jsonObject.getString("response").equalsIgnoreCase("success")) {
                    JSONObject result = jsonObject.getJSONObject("result");
                    JSONArray project = result.getJSONArray("projects");

                    for (int f = 0; f < project.length(); f++){
                        JSONObject ix = project.getJSONObject(f);
                        Integer id = ix.getInt("id");
                        String name = ix.getString("name");
                        projectsMap.put(f + 1, id);
                        projectsLabel.add(name);
                    }
                }

                ArrayAdapter<String> projectAdapter = new ArrayAdapter<String>(this, R.layout.simple_spinner_item, projectsLabel);
                project.setAdapter(projectAdapter);
                otherField.setVisibility(View.VISIBLE);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else if(REQUEST_CODE == 0) {
            try {
                instancesLabel = new ArrayList<String>();
                instancesLabel.add("Pilih Peternakan");
                instancesCode = new ArrayList<String>();
                instancesCode.add("");
                instancesMap = new HashMap<Integer, Integer>();
                instancesMap.put(0, 0);
                if (jsonObject.getString("response").equalsIgnoreCase("success")) {
                    JSONArray result = jsonObject.getJSONArray("result");

                    for (int f = 0; f < result.length(); f++){
                        JSONObject ix = result.getJSONObject(f);
                        Integer id = ix.getInt("id");
                        String name = ix.getString("name");
                        String code = ix.getString("instance_code");
                        instancesMap.put(f + 1, id);
                        instancesLabel.add(name);
                        instancesCode.add(code);
                    }
                }

                ArrayAdapter<String> instanceAdapter = new ArrayAdapter<String>(this, R.layout.simple_spinner_item, instancesLabel);
                instance.setAdapter(instanceAdapter);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
