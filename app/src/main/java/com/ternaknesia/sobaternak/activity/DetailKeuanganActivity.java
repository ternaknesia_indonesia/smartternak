package com.ternaknesia.sobaternak.activity;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.ternaknesia.sobaternak.Connection.PostRequest;
import com.ternaknesia.sobaternak.Interface.EndpointAPI;
import com.ternaknesia.sobaternak.Interface.RequestComplete;
import com.ternaknesia.sobaternak.Manager.PrefManager;
import com.ternaknesia.sobaternak.R;
import com.ternaknesia.sobaternak.Utils.DateFormatter;
import com.ternaknesia.sobaternak.Utils.MyDatePicker;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.Locale;

import okhttp3.FormBody;
import okhttp3.RequestBody;


public class DetailKeuanganActivity extends AppCompatActivity implements RequestComplete, MyDatePicker.OnDateSet {
    private LinearLayout list_pemasukan, list_pengeluaran;
    private Button show, new_transaction;
    private PostRequest postRequest, postRequest1;
    private PrefManager prefManager;
    private String project_id;
    private TextView t_pemasukan, t_pengeluaran, saldo, report_pemasukan, report_pengeluaran;
    private Spinner s_month;
    private Spinner s_year;
    private String month_ = "";
    private String year_ = "";
    private EditText date_start, date_end;
    private ImageView img_date_start, img_date_end;
    private MyDatePicker myDatePicker;
    public static Activity dkeu;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        dkeu = this;
        setContentView(R.layout.activity_detail_keuangan);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Detail Keuangan");
        prefManager = new PrefManager(this);
        myDatePicker = new MyDatePicker(this);
        list_pemasukan = findViewById(R.id.list_pemasukan);
        list_pengeluaran = findViewById(R.id.list_pengeluaran);
        show = findViewById(R.id.btn_show);
        t_pemasukan = findViewById(R.id.total_income);
        t_pengeluaran = findViewById(R.id.total_expense);
        saldo = findViewById(R.id.saldo);
        report_pemasukan = findViewById(R.id.report_pemasukan);
        report_pengeluaran = findViewById(R.id.report_pengeluaran);
        s_month = findViewById(R.id.s_month);
        s_year = findViewById(R.id.s_year);
        new_transaction = findViewById(R.id.btn_new_transaksi);
        date_start = findViewById(R.id.date_start);
        date_end = findViewById(R.id.date_end);
        img_date_start = findViewById(R.id.img_date_start);
        img_date_end = findViewById(R.id.img_date_end);

        initSpinner();
        initDate();

        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if (extras == null) {
                project_id = "";
            } else {
                project_id = extras.getString("project_id");
            }
        } else {
            project_id = (String) savedInstanceState.getSerializable("project_id");
        }

        assert project_id != null;
        RequestBody data = new FormBody.Builder()
                .add("project_id", project_id)
                .build();

        postRequest1 = new PostRequest(DetailKeuanganActivity.this);
        postRequest1.SendPostData(EndpointAPI.saldo, prefManager.getToken(), data, 2);
        postRequest1.execute();

        new_transaction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent add = new Intent(DetailKeuanganActivity.this, AddTransaksiActivity.class);
                Bundle f = new Bundle();
                f.putString("project_id", project_id);
                add.putExtras(f);
                startActivity(add);
            }
        });

        img_date_start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //GetDate();
                myDatePicker.SetDatePicker("dd/MM/yyyy", date_start.getText().toString(),1);
                myDatePicker.show();
            }
        });

        img_date_end.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //GetDate();
                myDatePicker.SetDatePicker("dd/MM/yyyy", date_end.getText().toString(),2);
                myDatePicker.show();
            }
        });

        show.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
/*                if(!month_.equalsIgnoreCase("")) {
                    if(!year_.equalsIgnoreCase("")) {*/
                //month_ = String.valueOf(s_month.getSelectedItemPosition() + 1);
                //year_ = s_year.getSelectedItem().toString();

                //Log.e("De", month_);
                String ds = new DateFormatter().convertDate(date_start.getText().toString(), "dd/MM/yyyy", "yyyy-MM-dd");
                String de = new DateFormatter().convertDate(date_end.getText().toString(), "dd/MM/yyyy", "yyyy-MM-dd");

                RequestBody data = new FormBody.Builder()
                        .add("project_id", project_id)
                        .add("start_date", ds)
                        .add("end_date", de)
                        .build();

                postRequest = new PostRequest(DetailKeuanganActivity.this);
                postRequest.SendPostData(EndpointAPI.listTransc, prefManager.getToken(), data, 1);
                postRequest.execute();
                clearList();
/*                    } else {
                        Snackbar.make(v, "Pilih tahun!", Snackbar.LENGTH_LONG).show();
                    }
                } else {
                    Snackbar.make(v, "Pilih bulan!", Snackbar.LENGTH_LONG).show();
                }*/
            }
        });
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("project_id", project_id);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        if (savedInstanceState != null){
            project_id = savedInstanceState.getString("project_id");
        }
    }

    private void initDate() {
        Calendar c = Calendar.getInstance();
        int pYear = c.get(Calendar.YEAR);
        int pMonth = c.get(Calendar.MONTH);
        int pDay = c.get(Calendar.DAY_OF_MONTH);
        String __date_of = String.valueOf(new StringBuilder().append(String.format(Locale.getDefault(), "%02d", pDay)).append("/").append(pMonth + 1).append("/").append(pYear));
        date_start.setText(__date_of);
        date_end.setText(__date_of);
    }

    private void initSpinner() {
        String[] list_months = new String[]{"Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"};
        ArrayAdapter<String> adapter_months = new ArrayAdapter<String>(this, R.layout.item_simple_spinner, list_months);
        s_month.setAdapter(adapter_months);

        Calendar c = Calendar.getInstance();
        int pYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mYear = 2017, x = 0;
        int cr = (pYear - mYear) + 1;
        //ArrayList<String> itemsYear = new ArrayList<>();
        String[] list_year = new String[cr];
        for (int m = mYear; m <= pYear; m++) {
            //itemsYear.add(String.valueOf(mYear));
            list_year[x] = String.valueOf(m);
            x = x + 1;
        }
        ArrayAdapter<String> adapter_year = new ArrayAdapter<String>(this, R.layout.item_simple_spinner, list_year);
        s_year.setAdapter(adapter_year);

        s_month.setSelection(mMonth);
        s_year.setSelection(x - 1);

    }

    void onAddIncome(String project_id, String id, String title, String date, int val, String desc, int cat, String id_cat) {
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        assert inflater != null;
        final View rowView = inflater.inflate(R.layout.item_income_expense, null);

        TextView _key = rowView.findViewById(R.id.transaksi);
        TextView _date = rowView.findViewById(R.id.date);
        TextView _val = rowView.findViewById(R.id.jumlah);

        _key.setText(title);
        _date.setText(date);
        _val.setText(String.format(Locale.getDefault(), "Rp. %,d", val).replaceAll(",", "."));
        rowView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //dialog
                AlertDialog.Builder builder = new AlertDialog.Builder(DetailKeuanganActivity.this);
                builder.setMessage(desc)
                        .setCancelable(true)
                        .setPositiveButton("Tutup", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                //prefManager.logout();
                                //Intent l = new Intent(DetailKeuanganActivity.this, LoginActivity.class);
                                //startActivity(l);
                                //finish();
                                dialog.cancel();
                            }
                        })
                        .setNegativeButton("Ubah", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Intent e = new Intent(DetailKeuanganActivity.this, EditTransaksiActivity.class);
                                Bundle ex = new Bundle();
                                ex.putString("id", id);
                                ex.putString("project_id", project_id);
                                ex.putString("title", title);
                                ex.putString("value", String.valueOf(val));
                                ex.putString("date_trans", date);
                                ex.putString("id_cat", id_cat);
                                ex.putString("jenis", String.valueOf(cat));
                                ex.putString("desc", desc);
                                e.putExtras(ex);
                                startActivityForResult(e, 3000);

                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();
            }
        });

        list_pemasukan.addView(rowView, list_pemasukan.getChildCount());
    }

    void onAddExpense(String project_id, String id, String title, String date, int val, String desc, int cat, String id_cat) {
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        assert inflater != null;
        final View rowView = inflater.inflate(R.layout.item_income_expense, null);

        TextView _key = rowView.findViewById(R.id.transaksi);
        TextView _date = rowView.findViewById(R.id.date);
        TextView _val = rowView.findViewById(R.id.jumlah);

        _key.setText(title);
        _date.setText(date);
        _val.setText(String.format(Locale.getDefault(), "Rp. %,d", val).replaceAll(",", "."));

        rowView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //dialog
                AlertDialog.Builder builder = new AlertDialog.Builder(DetailKeuanganActivity.this);
                builder.setMessage(desc)
                        .setCancelable(true)
                        .setPositiveButton("Tutup", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                //prefManager.logout();
                                //Intent l = new Intent(DetailKeuanganActivity.this, LoginActivity.class);
                                //startActivity(l);
                                //finish();
                                dialog.cancel();
                            }
                        })
                        .setNegativeButton("Ubah", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Intent e = new Intent(DetailKeuanganActivity.this, EditTransaksiActivity.class);
                                Bundle ex = new Bundle();
                                ex.putString("id", id);
                                ex.putString("project_id", project_id);
                                ex.putString("title", title);
                                ex.putString("value", String.valueOf(val));
                                ex.putString("date_trans", date);
                                ex.putString("id_jenis", id_cat);
                                ex.putString("id_cat", id_cat);
                                ex.putString("jenis", String.valueOf(cat));
                                ex.putString("desc", desc);
                                e.putExtras(ex);
                                startActivityForResult(e, 3000);

                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();
            }
        });

        list_pengeluaran.addView(rowView, list_pengeluaran.getChildCount());
    }

    void clearList() {
        list_pemasukan.removeAllViews();
        list_pengeluaran.removeAllViews();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.e("HA", "masuk pak eko");
        super.onActivityResult(requestCode, resultCode, data);
        //if(resultCode == RESULT_OK){
            //Log.e("HA", "masuk pak eko");
            if(requestCode == 3000){
                Log.e("HA", "masuk pak eko");
                show.performClick();
            }
        //}
    }

    @Override
    public void onRequestComplete(JSONObject jsonObject, Integer REQUEST_CODE) {
        //Log.e("detailkeu", jsonObject.toString());
        if (REQUEST_CODE == 1) {
            try {
                if (jsonObject.getString("response").equalsIgnoreCase("success")) {
                    JSONObject res = jsonObject.getJSONObject("result");
                    int total_income = res.getInt("total_pemasukan");
                    int total_expense = res.getInt("total_pengeluaran");

                    JSONArray items = res.getJSONArray("list");
                    for (int f = 0; f < items.length(); f++) {
                        JSONObject ix = items.getJSONObject(f);
                        String id = ix.getString("id");
                        String project_id = ix.getString("projects_id");
                        String id_cat = ix.getString("projects_resume_category_id");
                        String title = ix.getString("title");
                        String date = ix.getString("transaction_date");
                        String desc = ix.getString("desc");
                        if (desc.equalsIgnoreCase("null")) desc = "Tidak ada deskripsi";

                        int value = ix.getInt("value");
                        int type;

                        if (ix.isNull("parent_id")) type = ix.getInt("projects_resume_category_id");
                        else type = ix.getInt("parent_id");

                        String date_ = new DateFormatter().convertDate(date, "dd/MM/yyyy");

                        if (type == 1)
                            onAddExpense(project_id, id, title, date_, value, desc, type, id_cat);
                        else if (type == 2)
                            onAddIncome(project_id, id, title, date_, value, desc, type, id_cat);
                    }

                    t_pemasukan.setText(String.format(Locale.getDefault(), "Rp. %,d", total_income).replaceAll(",", "."));
                    t_pengeluaran.setText(String.format(Locale.getDefault(), "Rp. %,d", total_expense).replaceAll(",", "."));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else if (REQUEST_CODE == 2) {
            try {
                if (jsonObject.getString("response").equalsIgnoreCase("success")) {
                    clearList();
                    JSONObject res = jsonObject.getJSONObject("result");
                    int saldo_ = res.getInt("saldo");
                    int income_ = res.getInt("pemasukan");
                    int expense_ = res.getInt("pengeluaran");

                    saldo.setText(String.format(Locale.getDefault(), "%,d", saldo_).replaceAll(",", "."));
                    report_pemasukan.setText(String.format(Locale.getDefault(), "Rp. %,d", income_).replaceAll(",", "."));
                    report_pengeluaran.setText(String.format(Locale.getDefault(), "Rp. %,d", expense_).replaceAll(",", "."));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onDateSet(String date, int iDate, int iMonth, int iYear, Integer CODE) {
        if(CODE == 1){
            date_start.setText(date);
        } else if (CODE == 2){
            date_end.setText(date);
        }
    }
}
