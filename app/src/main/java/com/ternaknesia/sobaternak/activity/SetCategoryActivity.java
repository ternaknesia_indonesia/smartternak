package com.ternaknesia.sobaternak.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

import com.ternaknesia.sobaternak.Adapter.SetCatAdapter;
import com.ternaknesia.sobaternak.Connection.PostRequest;
import com.ternaknesia.sobaternak.DataModel.DataCategory;
import com.ternaknesia.sobaternak.Interface.EndpointAPI;
import com.ternaknesia.sobaternak.Interface.RequestComplete;
import com.ternaknesia.sobaternak.Manager.PrefManager;
import com.ternaknesia.sobaternak.R;
import com.ternaknesia.sobaternak.Utils.Item;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import okhttp3.FormBody;
import okhttp3.RequestBody;

public class SetCategoryActivity extends AppCompatActivity implements RequestComplete {
    private final String TAG = SetCategoryActivity.class.getSimpleName();
    public static boolean isReload = false;

    RecyclerView recyclerView;
    PostRequest postRequest;
    PrefManager prefManager;
    private LinearLayout not_found;
    private SetCatAdapter adapter;
    List<Item> items;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_category);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Objects.requireNonNull(getSupportActionBar()).setTitle("Atur Kategori");
        prefManager = new PrefManager(this);

        not_found = findViewById(R.id.not_found);
        recyclerView = findViewById(R.id.cat_list);

        items = new ArrayList<>();
        adapter = new SetCatAdapter(items, this);


        //this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);


        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setNestedScrollingEnabled(false);
        //recyclerView.setFocusable(false);

        findViewById(R.id.btn_add).setOnClickListener(v -> {
            Intent a = new Intent(SetCategoryActivity.this, EditCategoryActivity.class);
            startActivity(a);
        });

        getData(EndpointAPI.listCategory);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == RESULT_OK && requestCode == 6000) getData(EndpointAPI.ListBobot);
    }

    private void getData(String url) {

        RequestBody data = new FormBody.Builder()
                .add("instance_id", prefManager.getSingleDataProfile("instance_id"))
                .build();

        postRequest = new PostRequest(SetCategoryActivity.this);
        postRequest.SendPostData(url, prefManager.getToken(), data, 1);
        postRequest.execute();
    }

    @Override
    public void onRequestComplete(JSONObject jsonObject, Integer REQUEST_CODE) {
        //Log.e("json : ", jsonObject.toString());
        Log.d(SetCategoryActivity.class.getSimpleName(), "onRequestComplete: " + jsonObject.toString());
        try {
            if (jsonObject.getString("response").equalsIgnoreCase("success")) {
                JSONArray result = jsonObject.getJSONArray("result");
                ArrayList<Item> item = new ArrayList<>(result.length());
                for (int f = 0; f < result.length(); f++) {
                    JSONObject ix = result.getJSONObject(f);
                    item.add(new DataCategory(ix));
                }

                items.addAll(item);
                recyclerView.setAdapter(adapter);

                if (item.size() == 0) {
                    recyclerView.setVisibility(View.GONE);
                    not_found.setVisibility(View.VISIBLE);
                } else {
                    recyclerView.setVisibility(View.VISIBLE);
                    not_found.setVisibility(View.GONE);
                }

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return super.onSupportNavigateUp();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (isReload) {
            items.clear();
            adapter.notifyDataSetChanged();
            getData(EndpointAPI.listCategory);
        }

    }
}
