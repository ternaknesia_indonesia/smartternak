package com.ternaknesia.sobaternak.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import com.ternaknesia.sobaternak.Adapter.DokterAdapter;
import com.ternaknesia.sobaternak.DataModel.DataDokter;
import com.ternaknesia.sobaternak.R;
import com.ternaknesia.sobaternak.Utils.Item;

import java.util.ArrayList;

public class DokterHewanActivity extends AppCompatActivity {
    RecyclerView recyclerView;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dokter_hewan);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Panggil Dokter Hewan");

        recyclerView = findViewById(R.id.recycler_view);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        //RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 1);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setFocusable(false);

        ArrayList<Item> arrayList = new ArrayList<>(3);
        arrayList.add(new DataDokter("0", "drh. Gagang", "+6283845423361", "Surabaya", "Sabtu, Jam 15:00", "Obat sudah disediakan oleh dokter"));
        arrayList.add(new DataDokter("1", "drh. Ikhwanul", "+6283845423361", "Surabaya", "Minggu, Jam 15:00", "Obat beli sendiri"));
        arrayList.add(new DataDokter("2", "drh. Akbar", "+6283845423361", "Surabaya", "Rabu, Jam 09:00", "Alat sudah disediakan"));

        recyclerView.setAdapter(new DokterAdapter(arrayList, this));

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

}
