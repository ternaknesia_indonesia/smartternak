package com.ternaknesia.sobaternak.activity;
/*        Mohon untuk tidak melakukan hal-hal yang melanggar hukum.
        Anda tidak berhak melakukan cracking terhadap system kami tanpa ada persetujuan.
        Terimakasih*/
import android.content.DialogInterface;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.ternaknesia.sobaternak.Adapter.DataProductPasar;
import com.ternaknesia.sobaternak.Adapter.ProductPasarAdapter;
import com.ternaknesia.sobaternak.Connection.PostRequest;
import com.ternaknesia.sobaternak.Interface.EndpointAPI;
import com.ternaknesia.sobaternak.Interface.RequestComplete;
import com.ternaknesia.sobaternak.Manager.PrefManager;
import com.ternaknesia.sobaternak.R;
import com.ternaknesia.sobaternak.Utils.CheckConnection;
import com.ternaknesia.sobaternak.Utils.Item;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.FormBody;
import okhttp3.RequestBody;

public class PasarActivity extends AppCompatActivity implements RequestComplete {
    ArrayList<Item> contentAdding = null;
    PrefManager prefManager;
    RecyclerView recyclerView;
    PostRequest postRequest;
    /*String [][] dataProject={
            {"1","Project Domba Mas Aringga","12","Rp 1.500.000"},
            {"2","Project Pak Imin","0","Rp 2.500.000"},
            {"3","Project Mas Agis","4","Rp 3.000.000"},
            {"4","Project Domba Mas Aringga","12","Rp 1.500.000"},
            {"5","Project Pak Imin","0","Rp 2.500.000"},
            {"6","Project Mas Agis","4","Rp 3.000.000"},
            {"7","Project Domba Mas Aringga","12","Rp 1.500.000"},
            {"8","Project Pak Imin","0","Rp 2.500.000"},
            {"9","Project Mas Agis","4","Rp 3.000.000"}
    };*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pasar);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Pasar Ternak");
        prefManager = new PrefManager(this);

        recyclerView = findViewById(R.id.mRecyclerV);
        int spanCount = 2; // 2 columns
        int spacing = 1; // 50px
        //boolean includeEdge = false;


        recyclerView.addItemDecoration(new GridSpacingItemDecoration(spanCount, spacing, false));
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 2);
        recyclerView.setLayoutManager(mLayoutManager);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(), DividerItemDecoration.VERTICAL);
        DividerItemDecoration dividerItemDecoration_ = new DividerItemDecoration(recyclerView.getContext(), DividerItemDecoration.HORIZONTAL);
        //dividerItemDecoration.setDrawable(getResources().getDrawable(R.drawable.divider));
        //dividerItemDecoration_.setDrawable(getResources().getDrawable(R.drawable.divider));
        recyclerView.addItemDecoration(dividerItemDecoration);
        recyclerView.addItemDecoration(dividerItemDecoration_);
        recyclerView.setItemAnimator(new DefaultItemAnimator());



/*        contentAdding = new ArrayList<>(3);
        contentAdding.add(new DataProject("0", "Project Domba X", "40", "1200000", "users/coba.jpeg"));
        contentAdding.add(new DataProject("1", "Project Domba X", "40", "1200000", "https://static.turbosquid.com/Preview/001210/656/YU/domestic-goat-3D_200.jpg"));
        contentAdding.add(new DataProject("2", "Project Domba X", "40", "1200000", "http://ternaknesia.id/users/default.png"));
        recyclerView.setAdapter(new CatalogInvestAdapter(contentAdding, this));
        recyclerView.getLayoutManager().scrollToPosition(0);*/

        boolean internetAvailable = new CheckConnection().isInternetAvailable(this);

        if(!prefManager.getToken().equalsIgnoreCase("")){ //EDIT
            try {
                String jsonObj = prefManager.getToken(); // EDIT
                JSONObject a = new JSONObject(jsonObj);
                //JSONObject b = a.getJSONObject("result");
                JSONArray c = a.getJSONArray("result");
                int lenArray = c.length();
                contentAdding = new ArrayList<>(lenArray);
                for (int i=0; i<lenArray; i++){
                    JSONObject dat = c.getJSONObject(i);
                    String id = dat.getString("id");
                    String nama_project = dat.getString("name");
                    String link_img = EndpointAPI.HostStorage+dat.getString("feat_img");
                    int price = dat.getInt("price");
                    int remain = dat.getInt("stock");
                    String roi_min = dat.getString("est_roi_min");
                    String roi_max = dat.getString("est_roi_max");
                    String roi = roi_min+"% - "+roi_max+"%";
                    int _remain = dat.getInt("stock");
                    int ori = dat.getInt("total_stock");
                    int progress = 100-(_remain*100)/ori;
                    String status = dat.getString("status");
                    contentAdding.add(new DataProductPasar(id,nama_project,String.valueOf(remain),String.valueOf(price),roi, progress, link_img, status));
                }
                recyclerView.setAdapter(new ProductPasarAdapter(contentAdding, this));
                recyclerView.getLayoutManager().scrollToPosition(0);
            } catch (JSONException e) {
                //e.printStackTrace();
                //Log.d("MyErr", "Ceklist");
                //Toast.makeText(getBaseContext(), "Tidak ada data", Toast.LENGTH_LONG).show();
            }
            if(internetAvailable){
                RequestBody data = new FormBody.Builder()
                        .build();
                postRequest = new PostRequest(this);
//                postRequest.SendPostData(EndpointAPI.ListAllProject,"", data, 1); // EDIT
                postRequest.execute();
            }
        }else{
            if(internetAvailable) {
                RequestBody data = new FormBody.Builder()
                        .build();
                postRequest = new PostRequest(this);
//                postRequest.SendPostData(EndpointAPI.ListAllProject,"", data, 1); // EDIT
                postRequest.execute();
            } else {
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage("Tidak ada koneksi internet, mohon periksa kembali.")
                        .setCancelable(false)
                        .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                                finish();
                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();
            }
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        this.finish();
        return super.onSupportNavigateUp();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        //inflater.inflate(R.menu.menu_catalog_invest, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int idMenu = item.getItemId();
        /*switch (idMenu) {
            case R.id.menu_waitlist:
                showdialogWait();
                break;
            default:
                break;
        }*/
        return super.onOptionsItemSelected(item);
    }

    private void showdialogWait() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Fitur ini merupakan fitur waitinglist bagi Anda yang ingin berinvestasi namun belum mendapatkan bagian saham. Ingin melanjutkan?")
                .setCancelable(false)
                .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if(prefManager.isLog()) {
                            //Intent intent = new Intent(InvestCatalogActivity.this, MainHome.class); // EDIT
                            //finish();
                            //startActivity(intent);
                        } else {
                            //Toast.makeText(InvestCatalogActivity.this, "Anda harus login terlebih dahulu.", Toast.LENGTH_LONG).show();
                            //Intent intent = new Intent(InvestCatalogActivity.this, LoginActivity.class);
                            //finish();
                            //startActivity(intent);
                        }
                    }
                })
                .setNegativeButton("Batal", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public void onRequestComplete(JSONObject jsonObject, Integer CODE_REQUEST) {
        try {
            if (jsonObject.getString("response").equalsIgnoreCase("success")) {
                //prefManager.setDataAllProject(jsonObject.toString()); // EDIT
                JSONArray c = jsonObject.getJSONArray("result");
                int lenArray = c.length();
                contentAdding = new ArrayList<>(lenArray);
                for (int i = 0; i < lenArray; i++) {
                    JSONObject dat = c.getJSONObject(i);
                    String id = dat.getString("id");
                    String nama_project = dat.getString("name");
                    String link_img = EndpointAPI.HostStorage+dat.getString("feat_img");
                    int price = dat.getInt("price");
                    int remain = dat.getInt("stock");
                    String roi_min = dat.getString("est_roi_min");
                    String roi_max = dat.getString("est_roi_max");
                    String roi = roi_min+"% - "+roi_max+"%";
                    int _remain = dat.getInt("stock");
                    int ori = dat.getInt("total_stock");
                    int progress = 100-(_remain*100)/ori;
                    String status = dat.getString("status");
                    //contentAdding.add(new DataProject(id, nama_project, String.valueOf(remain), String.valueOf(price),roi, progress, link_img, status));
                }
                //recyclerView.setAdapter(new CatalogInvestAdapter(contentAdding, this));
                recyclerView.getLayoutManager().scrollToPosition(0);
            }
        } catch (JSONException e) {
            //e.printStackTrace();
            //Toast.makeText(getBaseContext(), "Tidak ada data", Toast.LENGTH_LONG).show();
        }
    }

    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;
        private int[] ATTRS = new int[]{android.R.attr.listDivider};

        private Drawable divider;


        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }



    /* Converting dp to pixel
    */
    /*private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }*/
}
