package com.ternaknesia.sobaternak.activity;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v4.widget.NestedScrollView;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.ternaknesia.sobaternak.Adapter.PostFeedAdapter;
import com.ternaknesia.sobaternak.BuildConfig;
import com.ternaknesia.sobaternak.Connection.GetRequest;
import com.ternaknesia.sobaternak.Connection.PostRequest;
import com.ternaknesia.sobaternak.DataModel.DataFeed;
import com.ternaknesia.sobaternak.Interface.EndpointAPI;
import com.ternaknesia.sobaternak.Interface.RequestComplete;
import com.ternaknesia.sobaternak.Manager.PrefManager;
import com.ternaknesia.sobaternak.R;
import com.ternaknesia.sobaternak.Utils.CameraUtils;
import com.ternaknesia.sobaternak.Utils.Footer;
import com.ternaknesia.sobaternak.Utils.ImageConverter;
import com.ternaknesia.sobaternak.Utils.ImageFilePath;
import com.ternaknesia.sobaternak.Utils.Item;
import com.ternaknesia.sobaternak.Utils.RoundedCornersTransformation;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class MainHome extends AppCompatActivity implements RequestComplete, SwipeRefreshLayout.OnRefreshListener {
    private PrefManager prefManager;
    private TextView market_name, addr, msg_title, msg, version, caption, btn_post;
    private ImageView img_market, btn_close, img_up;
    private RelativeLayout msg_push;
    private int type = 0;
    private String activity = "", data_bundle = "";
    private PostRequest postRequest;
    private String[] PERMISSIONS = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA};
    private static int PERMISSION_ALL = 1;
    private String pathImg = "";
    private static final int CAMERA_REQUEST_CODE = 0;
    private static final int GALLERY_REQUEST_CODE = 1;
    private Uri FileUriOutput = null;//Uri to capture image
    private AlertDialog dialog;
    private File fileCompressed = null;
    private RecyclerView recyclerView;
    private NestedScrollView nestedScrollView;
    private SwipeRefreshLayout swipeRefreshLayout;
    private CardView box_post;
    private ImageView img_verified;
    ArrayList<Item> contentAdding2 = null;
    private boolean hasMore = false;
    Boolean isLoadMore;
    String next_page = "";
    private GetRequest connection2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.nav_home);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        prefManager = new PrefManager(this);
        isLoadMore = false;

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        TextView name = findViewById(R.id.name);
        TextView email = findViewById(R.id.email);
        ImageView avatar = findViewById(R.id.avatar);
        addr = findViewById(R.id.address);
        market_name = findViewById(R.id.market_name);
        img_market = findViewById(R.id.img_market);
        msg_push = findViewById(R.id.msg_push);
        msg_title = findViewById(R.id.msg_title);
        msg = findViewById(R.id.msg);
        btn_close = findViewById(R.id.btn_close);
        img_up = findViewById(R.id.img_up);
        caption = findViewById(R.id.caption);
        btn_post = findViewById(R.id.btn_post);
        recyclerView = findViewById(R.id.recycler_post_feed);
        nestedScrollView = findViewById(R.id.nestedScroll);
        swipeRefreshLayout = findViewById(R.id.swipe_refresh_layout_home);
        box_post = findViewById(R.id.box_new_post);
        img_verified = findViewById(R.id.img_verified);
        findViewById(R.id.more_menu).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showMenu(v);
            }
        });

        version.setText(BuildConfig.VERSION_NAME);

        GetDataBundle(savedInstanceState);

        name.setText(prefManager.getSingleDataProfile("name"));
        email.setText(prefManager.getSingleDataProfile("email"));
        //email.setText(Html.fromHtml("halo</br>oke"));

        if (prefManager.getSingleDataProfile("address").equalsIgnoreCase("null")) {
            addr.setText("-");
        } else addr.setText(prefManager.getSingleDataProfile("address"));


        Glide.with(this)
                .load(EndpointAPI.HostStorage + prefManager.getSingleDataProfile("avatar"))
                .placeholder(R.drawable.placeholder_avatar)
                .centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .skipMemoryCache(true)
                .bitmapTransform(new RoundedCornersTransformation(this, 10, 0))
                .into(avatar);

        btn_post.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!caption.getText().toString().equalsIgnoreCase("")) {
                    RequestBody requestBody;
                    if (fileCompressed != null) {
                        requestBody = new MultipartBody.Builder()
                                .setType(MultipartBody.FORM)
                                .addFormDataPart("image", "image_cap",
                                        RequestBody.create(MediaType.parse("image/jpg"), fileCompressed))
                                .addFormDataPart("caption", caption.getText().toString())
                                .build();
                        postRequest = new PostRequest(MainHome.this);
                        postRequest.SendPostData(EndpointAPI.updateAct, prefManager.getToken(), requestBody, 2);
                        postRequest.execute();
                    } else {
                        Toast.makeText(MainHome.this, "Ambil foto aktivitas Anda!", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(MainHome.this, "Tulis aktivitas Anda!", Toast.LENGTH_SHORT).show();
                }
            }
        });

        img_up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                File f = new File(Environment.getExternalStorageDirectory().toString(), "SmartTernak/Image/cache.jpg");
                if (f.exists())
                    if (f.delete()) Log.e("success", "ok");

                AlertDialog.Builder builder = new AlertDialog.Builder(MainHome.this);
                builder.setMessage("Pilih foto")
                        .setCancelable(true)
                        .setNegativeButton("KAMERA", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                                if (!hasPermissions(MainHome.this, PERMISSIONS)) {
                                    ActivityCompat.requestPermissions(MainHome.this, PERMISSIONS, PERMISSION_ALL);
                                } else {
                                    StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
                                    StrictMode.setVmPolicy(builder.build());
                                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);//Start intent with Action_Image_Capture
                                    FileUriOutput = CameraUtils.getOutputMediaFileUri(MainHome.this);//get FileUriOutput from CameraUtils
                                    intent.putExtra(MediaStore.EXTRA_OUTPUT, FileUriOutput);//Send FileUriOutput with intent
                                    startActivityForResult(intent, CAMERA_REQUEST_CODE);//start activity for result with CAMERA_REQUEST_CODE
                                }
                            }
                        })
                        .setPositiveButton("GALERI", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                                if (!hasPermissions(MainHome.this, PERMISSIONS)) {
                                    ActivityCompat.requestPermissions(MainHome.this, PERMISSIONS, PERMISSION_ALL);
                                } else {

                                    Intent pickPhoto = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                    startActivityForResult(pickPhoto, GALLERY_REQUEST_CODE);//one can be replaced with any action code
                                }
                            }
                        });
                dialog = builder.create();
                dialog.show();
            }
        });

        findViewById(R.id.sub_w).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (prefManager.getSingleDataProfile("is_verified").equalsIgnoreCase("true")) {
                    Intent a = new Intent(MainHome.this, SubPeternakActivity.class);
                    startActivity(a);
                } else {
                    Toast.makeText(getBaseContext(), "Fitur tidak dapat digunakan. Akun belum terverifikasi.", Toast.LENGTH_SHORT).show();
                }
            }
        });

        //manajemen kandang
        findViewById(R.id.kan_w).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (prefManager.getSingleDataProfile("is_verified").equalsIgnoreCase("true")) {
                    Intent a = new Intent(MainHome.this, KandangActivity.class);
                    startActivity(a);
                } else {
                    Toast.makeText(getBaseContext(), "Fitur tidak dapat digunakan. Akun belum terverifikasi.", Toast.LENGTH_SHORT).show();
                }
            }
        });

        findViewById(R.id.hew_w).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (prefManager.getSingleDataProfile("is_verified").equalsIgnoreCase("true")) {
                    Intent a = new Intent(MainHome.this, HewanActivity.class);
                    startActivity(a);
                } else {
                    Toast.makeText(getBaseContext(), "Fitur tidak dapat digunakan. Akun belum terverifikasi.", Toast.LENGTH_SHORT).show();
                }
            }
        });


        findViewById(R.id.pasar).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (prefManager.getSingleDataProfile("is_verified").equalsIgnoreCase("true")) {
                    //Toast.makeText(getBaseContext(), "Sementara fitur belum tersedia", Toast.LENGTH_LONG).show();
                    Intent a = new Intent(MainHome.this, PasarActivity.class);
                    startActivity(a);
                } else {
                    Toast.makeText(getBaseContext(), "Fitur tidak dapat digunakan. Akun belum terverifikasi.", Toast.LENGTH_SHORT).show();
                }
            }
        });
        findViewById(R.id.mesin).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getBaseContext(), "Sementara fitur belum tersedia", Toast.LENGTH_LONG).show();
            }
        });

        findViewById(R.id.dokter).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getBaseContext(), "Sementara fitur belum tersedia", Toast.LENGTH_LONG).show();
                Toast.makeText(getBaseContext(), "Sementara data dummy yang disajikan", Toast.LENGTH_LONG).show();
                Intent d = new Intent(MainHome.this, DokterHewanActivity.class);
                startActivity(d);
            }
        });


        //Qurban
        findViewById(R.id.pesanan).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (prefManager.getSingleDataProfile("is_verified").equalsIgnoreCase("true")) {
                    Intent a = new Intent(MainHome.this, OrderQurbanActivity.class);
                    startActivity(a);
                } else {
                    Toast.makeText(getBaseContext(), "Fitur tidak dapat digunakan. Akun belum terverifikasi.", Toast.LENGTH_SHORT).show();
                }
            }
        });


        findViewById(R.id.stock).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (prefManager.getSingleDataProfile("is_verified").equalsIgnoreCase("true")) {
                    Intent s = new Intent(MainHome.this, StockActivity.class);
                    startActivity(s);
                } else {
                    Toast.makeText(getBaseContext(), "Fitur tidak dapat digunakan. Akun belum terverifikasi.", Toast.LENGTH_SHORT).show();
                }
            }
        });

        findViewById(R.id.driver_qurban).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (prefManager.getSingleDataProfile("is_verified").equalsIgnoreCase("true")) {
                    Intent d = new Intent(MainHome.this, ArmadaActivity.class);
                    startActivity(d);
                } else {
                    Toast.makeText(getBaseContext(), "Fitur tidak dapat digunakan. Akun belum terverifikasi.", Toast.LENGTH_SHORT).show();
                }
            }
        });


        //keuangan
        findViewById(R.id.keu_w).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (prefManager.getSingleDataProfile("is_verified").equalsIgnoreCase("true")) {
                    Intent a = new Intent(MainHome.this, KelolaKeuanganActivity.class);
                    startActivity(a);
                } else {
                    Toast.makeText(getBaseContext(), "Fitur tidak dapat digunakan. Akun belum terverifikasi.", Toast.LENGTH_SHORT).show();
                }
            }
        });

        findViewById(R.id.ajukan).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent a = new Intent(MainHome.this, AjukanActivity.class);
                startActivity(a);
            }
        });

        //proyek
        findViewById(R.id.kelola_proyek).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent a = new Intent(MainHome.this, KelolaKeuanganActivity.class);
                startActivity(a);
                //Toast.makeText(getBaseContext(), "Sementara fitur belum tersedia", Toast.LENGTH_LONG).show();
            }
        });

        GetRequest getRequest = new GetRequest(this);
        getRequest.SendGetData(EndpointAPI.getMyMarket, prefManager.getToken(), 1);
        getRequest.execute();

        actionFromNotif();
        actionCheckFarmer();

        if (!prefManager.getDataMsg().equalsIgnoreCase("")) {
            try {
                JSONObject a = new JSONObject(prefManager.getDataMsg());
                JSONObject b = a.getJSONObject("data");
                String c = b.getString("title");
                String d = b.getString("message");
                msg_title.setText(c);
                msg.setText(d);
                msg_push.setVisibility(View.VISIBLE);
            } catch (JSONException e) {
                //e.printStackTrace();
            }
        }

        btn_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                msg_push.setVisibility(View.GONE);
                if (prefManager.getDelMsg()) prefManager.setDataMsg("");
            }
        });

        RecyclerView.LayoutManager mLayoutManager2 = new GridLayoutManager(this, 1);
        recyclerView.setLayoutManager(mLayoutManager2);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setNestedScrollingEnabled(false);

        nestedScrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                //Log.e("masuk AA", next_page);
                if (v.getChildAt(v.getChildCount() - 1) != null) {
                    //Log.e("masuk BB", next_page);
                    if ((scrollY >= (v.getChildAt(v.getChildCount() - 1).getMeasuredHeight() - v.getMeasuredHeight())) &&
                            scrollY > oldScrollY) {
                        //Log.e("masuk CC", next_page);
                        //Log.e("masuk C1", String.valueOf(hasMore));
                        //Log.e("masuk C2", String.valueOf(!hasFooter()));
                        if (hasMore && (!hasFooter())) {
                            LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                            //position starts at 0
                            //Log.e("masuk C3", String.valueOf(layoutManager.findLastCompletelyVisibleItemPosition()));
                            //Log.e("masuk C4", String.valueOf(layoutManager.getItemCount()));
                            if (layoutManager.findLastCompletelyVisibleItemPosition() == layoutManager.getItemCount() - 1) {
                                contentAdding2.add(new Footer());
                                recyclerView.getAdapter().notifyDataSetChanged();
                                connection2 = new GetRequest(MainHome.this);
                                //Log.e("masuk 1", next_page);
                                connection2.SendGetData(next_page, "", 3);
                                connection2.execute();
                            }
                        }
                    }
                }
            }
        });

        swipeRefreshLayout.setOnRefreshListener(this);

        swipeRefreshLayout.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        swipeRefreshLayout.setRefreshing(true);
                                        connection2 = new GetRequest(MainHome.this);
                                        connection2.SendGetData(EndpointAPI.GetPostStatus, "", 3);
                                        connection2.execute();
                                    }
                                }
        );

        connection2 = new GetRequest(this);
        connection2.SendGetData(EndpointAPI.GetPostStatus, "", 3);
        connection2.execute();

    }

    public void showMenu(View v) {
        PopupMenu popup = new PopupMenu(this, v);
        popup.inflate(R.menu.home_menu);

        //popup.getMenu().findItem(R.id.menu_name).setTitle(text);
        invalidateOptionsMenu();
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

            @Override
            public boolean onMenuItemClick(MenuItem item) {
                /*if (item.getItemId() == R.id.menu_item_edit) {
                    Toast.makeText(MainHome.this, "Sementara fitur belum tersedia", Toast.LENGTH_LONG).show();
                }*/ /*else if (item.getItemId() == R.id.menu_item_logout) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(MainHome.this);
                    builder.setMessage("Anda yakin akan keluar dari akun Anda?")
                            .setCancelable(false)
                            .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    prefManager.logout();
                                    Intent l = new Intent(MainHome.this, LoginActivity.class);
                                    startActivity(l);
                                    finish();
                                }
                            })
                            .setNegativeButton("Batal", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                } */
                if (item.getItemId() == R.id.menu_item_ajukan) {
                    Intent t = new Intent(MainHome.this, AjukanActivity.class);
                    startActivity(t);
                    //Toast.makeText(this, "Sementara fitur belum tersedia", Toast.LENGTH_LONG).show();
                }
                return item.isChecked();
            }

        });
        popup.show();
    }

    private void actionCheckFarmer() {
        if (prefManager.getSingleDataProfile("is_peternak").equalsIgnoreCase("true")) {
            if (prefManager.getSingleDataProfile("is_verified").equalsIgnoreCase("false")) {
                //Toast.makeText(this, "Akun peternak belum terverifikasi.", Toast.LENGTH_LONG).show();
                msg_title.setText("Akun peternak belum terverifikasi");
                msg.setText("Mohon menunggu verifikasi dari Admin maksimal 1 x 24 jam. Jika belum terverifikasi mohon sekiranya menghubungi Admin.");
                msg_push.setVisibility(View.VISIBLE);
                box_post.setVisibility(View.GONE);
                img_verified.setVisibility(View.GONE);
            } else {
                img_verified.setVisibility(View.VISIBLE);
            }
        }
    }


    private boolean hasFooter() {
        return contentAdding2.get(contentAdding2.size() - 1) instanceof Footer;
    }

    private static boolean hasPermissions(Context context, String... permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK && (data != null || FileUriOutput != null)) {
            File a = null;
            long capacity = 0;

            if (requestCode == CAMERA_REQUEST_CODE) {
                a = new File(FileUriOutput.getPath());
                //String realPath = ImageFilePath.getPath(InvestDetailOrderActivity.this, data.getData());
                //a = new File(realPath);
            } else if (requestCode == GALLERY_REQUEST_CODE) {
                String realPath = ImageFilePath.getPath(this, data.getData());
                a = new File(realPath);
            }

            if (a != null) {

                pathImg = a.getAbsolutePath();
                Glide.with(getBaseContext())
                        .load(a.getAbsolutePath())
                        .asBitmap()
                        .centerCrop()
                        .into(new BitmapImageViewTarget(img_up) {
                            @Override
                            protected void setResource(Bitmap resource) {
                                RoundedBitmapDrawable rounded =
                                        RoundedBitmapDrawableFactory.create(getResources(), resource);
                                rounded.setCircular(true);
                                Bitmap b = ImageConverter.getRoundedCornerBitmap(resource, 10);
                                img_up.setImageBitmap(b);
                            }
                        });

                try {
                    long length = a.length();
                    length = length / 1024;
                    capacity = length;
                } catch (Exception ignored) {

                }

                BitmapFactory.Options bmOptions = new BitmapFactory.Options();
                Bitmap bitmap = BitmapFactory.decodeFile(a.getAbsolutePath(), bmOptions);
                bitmap = resize(bitmap, 800, 800);

                ///Bitmap thumbnail = BitmapFactory.decodeFile(imagePath);
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                assert bitmap != null;
                int percentage = 95 - (((int) capacity / 1000) * 5);
                bitmap.compress(Bitmap.CompressFormat.JPEG, percentage, bytes);
                fileCompressed = saveCompressed(bytes);

                //Log.e("cek0", "File saved");
                //cropped = bitmap;
                //ktp.setImageBitmap(bitmap);
            } else {
                //Log.e("cek1", "File is null");
            }
        } else {
            //Log.e("cek2", "File Uri is null");
        }
    }


    private static Bitmap resize(Bitmap image, int maxWidth, int maxHeight) {
        if (maxHeight > 0 && maxWidth > 0) {
            int width = image.getWidth();
            int height = image.getHeight();
            float ratioBitmap = (float) width / (float) height;
            float ratioMax = (float) maxWidth / (float) maxHeight;

            int finalWidth = maxWidth;
            int finalHeight = maxHeight;
            if (ratioMax > ratioBitmap) {
                finalWidth = (int) ((float) maxHeight * ratioBitmap);
            } else {
                finalHeight = (int) ((float) maxWidth / ratioBitmap);
            }
            image = Bitmap.createScaledBitmap(image, finalWidth, finalHeight, true);
            return image;
        } else {
            return image;
        }
    }


    private static File saveCompressed(ByteArrayOutputStream bmpB) {
        File mediaStorageDir = new File(Environment.getExternalStorageDirectory().toString(), "SmartTernak");
        File mediaStorageDir1 = new File(Environment.getExternalStorageDirectory().toString(), "SmartTernak/Image");
        //If File is not present create directory
        if (!mediaStorageDir.exists()) {
            if (mediaStorageDir.mkdir())
                if (!mediaStorageDir1.exists()) {
                    if (mediaStorageDir1.mkdir())
                        Log.e("Success", "Ok");
                }
        } else {
            if (!mediaStorageDir1.exists()) {
                if (mediaStorageDir1.mkdir())
                    Log.e("Success", "Ok");
            }
        }

        File f = new File(Environment.getExternalStorageDirectory().toString(), "SmartTernak/Image" + File.separator + "cache.jpg");

        byte[] bitmapdata = bmpB.toByteArray();
        FileOutputStream fos;
        try {
            if (f.createNewFile()) {
                fos = new FileOutputStream(f, false);
                fos.write(bitmapdata);
                fos.flush();
                fos.close();
            }
        } catch (IOException e) {
            //e.printStackTrace();
        }
        return f;
    }


    private void actionFromNotif() {
        if (type == 3) {
            //Log.e("cek", String.valueOf(activity));
            //Log.e("cek", String.valueOf(data_bundle));

            if (!activity.equalsIgnoreCase("")) {
                //Log.e("cek1", String.valueOf(data_bundle));
                try {
                    //Log.e("cek2", String.valueOf(data_bundle));
                    Class<?> c = Class.forName(activity);
                    Intent intent = new Intent(this, c);
                    Bundle extras = new Bundle();

                    JSONObject jObject;
                    //Log.e("cek3", String.valueOf(data_bundle));
                    try {
                        //Log.e("cek4", String.valueOf(data_bundle));
                        jObject = new JSONObject(data_bundle);
                        Iterator<?> keys = jObject.keys();
                        //Log.e("cek41", String.valueOf(data_bundle));

                        while (keys.hasNext()) {
                            //Log.e("cek42", String.valueOf(data_bundle));
                            String key = (String) keys.next();
                            //Log.e("cek43", key);
                            //if (jObject.get(key) instanceof JSONObject) {
                            extras.putString(key, jObject.getString(key));
                            //Log.e("cek5", key);
                            //Log.e("cek5", jObject.getString(key));
                            //}
                        }
                        //Log.e("cek", String.valueOf(activity));
                        intent.putExtras(extras);
                        startActivity(intent);
                    } catch (JSONException e) {
                        //e.printStackTrace();
                    }
                } catch (ClassNotFoundException ignored) {
                }

            }
        }
    }

    private void GetDataBundle(Bundle savedInstanceState) {
        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if (extras == null) {
                type = 0;

                data_bundle = null;
                activity = null;
            } else {
                type = extras.getInt("type");

                data_bundle = extras.getString("data_bundle");
                activity = extras.getString("activity");
            }
        } else {
            type = savedInstanceState.getInt("type");

            data_bundle = (String) savedInstanceState.getSerializable("data_bundle");
            activity = (String) savedInstanceState.getSerializable("activity");
        }
    }

/*    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.home_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        if (item.getItemId() == R.id.menu_item_edit) {
            Toast.makeText(this, "Sementara fitur belum tersedia", Toast.LENGTH_LONG).show();
        } else if (item.getItemId() == R.id.menu_item_logout) {
            AlertDialog.Builder builder = new AlertDialog.Builder(MainHome.this);
            builder.setMessage("Anda yakin akan keluar dari akun Anda?")
                    .setCancelable(false)
                    .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            prefManager.logout();
                            Intent l = new Intent(MainHome.this, LoginActivity.class);
                            startActivity(l);
                            finish();
                        }
                    })
                    .setNegativeButton("Batal", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });
            AlertDialog alert = builder.create();
            alert.show();
        } else if (item.getItemId() == R.id.menu_item_ajukan) {
            Intent t = new Intent(this, AjukanActivity.class);
            startActivity(t);
            //Toast.makeText(this, "Sementara fitur belum tersedia", Toast.LENGTH_LONG).show();
        }
        return super.onOptionsItemSelected(item);
    }*/

    @Override
    public void onRequestComplete(JSONObject jsonObject, Integer REQUEST_CODE) {
        if (REQUEST_CODE == 1) {
            try {
                if (jsonObject.getString("response").equalsIgnoreCase("success")) {
                    JSONObject result = jsonObject.getJSONObject("result");
                    String market_name_ = result.getString("nama_toko");
                    String img_link_ = result.getString("feat_image");
                    String address_ = result.getString("alamat");

                    addr.setText(address_);
                    market_name.setText(market_name_);
                /*Glide.with(this)
                        .load(EndpointAPI.Host + img_link_)
                        .placeholder(R.drawable.placeholder_home)
                        .centerCrop()
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .skipMemoryCache(true)
                        .bitmapTransform(new RoundedCornersTransformation(this, 10, 0))
                        .into(img_market);*/

                    Glide.with(this)
                            .load(EndpointAPI.Host + img_link_)
                            .asBitmap()
                            .centerCrop()
                            .into(new BitmapImageViewTarget(img_market) {
                                @Override
                                protected void setResource(Bitmap resource) {
                                    RoundedBitmapDrawable rounded =
                                            RoundedBitmapDrawableFactory.create(getResources(), resource);
                                    rounded.setCircular(true);
                                    Bitmap b = ImageConverter.getRoundedCornerBitmap(resource, 10);
                                    img_market.setImageBitmap(b);
                                }
                            });
                }
            } catch (JSONException e) {
                //e.printStackTrace();
                //Toast.makeText(getBaseContext(), "Akun Anda bukan akun peternak mitra.", Toast.LENGTH_LONG).show();
            }
        } else if (REQUEST_CODE == 2) {
            try {
                if (jsonObject.getString("response").equalsIgnoreCase("success")) {
                    caption.setText("");
                    fileCompressed = null;
                    Glide.with(getBaseContext())
                            .load("")
                            .placeholder(R.drawable.placeholder_home)
                            .centerCrop()
                            .into(img_up);

                    Toast.makeText(this, "Berhasil update aktivitas", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(this, "Gagal update", Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else if (REQUEST_CODE == 3) {
            ArrayList<Item> _contentAdding;
            int current_page;
            try {
                String author_name;
                JSONObject c = jsonObject.getJSONObject("result");
                next_page = c.getString("next_page_url");
                current_page = c.getInt("current_page");

                //if (!jsonObject.toString().equalsIgnoreCase(prefManager.getDataAllNews())) {
                JSONArray data = c.getJSONArray("data");
                int lenArray = data.length();
                _contentAdding = new ArrayList<>();
                for (int i = 0; i < lenArray; i++) {
                    JSONObject dat = data.getJSONObject(i);
                    String id = dat.getString("id");
                    String u_name = dat.getString("user_name");
                    String avatar = EndpointAPI.HostStorage + dat.getString("avatar");
                    String date = dat.getString("created_at");
                    String caption = dat.getString("caption");
                    String image = dat.getString("image");

                    _contentAdding.add(new DataFeed(id, u_name, avatar, date, image, caption));

                }
                //_contentAdding.add(new DataFeed("1", "Ahmad Aris Habibi", EndpointAPI.HostStorage + prefManager.getSingleDataProfile("avatar"), "10-10-2018 00:00:00", "https://images.pexels.com/photos/462118/pexels-photo-462118.jpeg?auto=compress&cs=tinysrgb&h=350", "Ternaknesia Bagus mantab oke lah"));
                //_contentAdding.add(new DataFeed("1", "Ahmad Aris Habibi", EndpointAPI.HostStorage + prefManager.getSingleDataProfile("avatar"), "10-10-2018 00:00:00", "", "Ternaknesia Bagus mantab oke lah Ternaknesia Bagus mantab oke lah Ternaknesia Bagus mantab oke lah Ternaknesia Bagus mantab oke lah"));
                //_contentAdding.add(new DataFeed("1", "Ahmad Aris Habibi", EndpointAPI.HostStorage + prefManager.getSingleDataProfile("avatar"), "10-10-2018 00:00:00", "https://images.pexels.com/photos/462118/pexels-photo-462118.jpeg?auto=compress&cs=tinysrgb&h=350", "Ternaknesia Bagus mantab oke lah Ternaknesia Bagus mantab oke lah Ternaknesia Bagus mantab oke lah Ternaknesia Bagus mantab oke lah Ternaknesia Bagus mantab oke lah Ternaknesia Bagus mantab oke lah Ternaknesia Bagus mantab oke lah Ternaknesia Bagus mantab oke lah Ternaknesia Bagus mantab oke lah Ternaknesia Bagus mantab oke lah"));
                //Log.e("cek1", "error");
                hasMore = !next_page.equalsIgnoreCase("null");
                if (isLoadMore) {
                    //Log.e("masuk 4", "true");
                    if (contentAdding2.size() > 0)
                        deleteItem(contentAdding2.size() - 1);

                    contentAdding2.addAll(_contentAdding);
                    recyclerView.getAdapter().notifyDataSetChanged();
                } else {
                    //Log.e("masuk 3", String.valueOf(isLoadMore));
                    contentAdding2 = _contentAdding;
                    isLoadMore = hasMore;

                    recyclerView.setAdapter(new PostFeedAdapter(contentAdding2, this));
                }


                //Log.e("masuk 2", next_page);
                //mRecyclerView2.setAdapter(new AllNewsAdapter(contentAdding2, getContext()));
                //mRecyclerView2.getLayoutManager().scrollToPosition(0);
                swipeRefreshLayout.setRefreshing(false);
                //if (current_page == 1)
                //prefManager.setDataAllNews(jsonObject.toString());

                //} else {
                /*    hasMore = !next_page.equalsIgnoreCase("null");
                    if(isLoadMore){
                        Log.e("masuk 4", String.valueOf(isLoadMore));
                        //deleteItem(contentAdding2.size() - 1);
                        //contentAdding2.addAll(contentAdding2);
                        //mRecyclerView2.getAdapter().notifyDataSetChanged();
                    }else {
                        Log.e("masuk 3", String.valueOf(isLoadMore));
                        isLoadMore = hasMore;
                        mRecyclerView2.setAdapter(new AllNewsAdapter(contentAdding2, getActivity()));
                    }
                    swipeRefreshLayout.setRefreshing(false);
                }*/
            } catch (JSONException e) {
                //Log.e("cek", "error");
                swipeRefreshLayout.setRefreshing(false);
                //Toast.makeText(getContext(), "Kesalahan server", Toast.LENGTH_SHORT).show();
                if (contentAdding2 != null)
                    if (contentAdding2.size() > 0)
                        deleteItem(contentAdding2.size() - 1);
                //e.printStackTrace();
            }
        }

    }

    public void deleteItem(int pos) {
        contentAdding2.remove(pos);
        recyclerView.getAdapter().notifyDataSetChanged();
    }

    @Override
    public void onRefresh() {
        connection2 = new GetRequest(this);
        connection2.SendGetData(EndpointAPI.GetPostStatus, "", 3);
        connection2.execute();
    }
}
