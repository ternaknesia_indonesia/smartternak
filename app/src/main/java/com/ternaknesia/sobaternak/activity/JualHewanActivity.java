package com.ternaknesia.sobaternak.activity;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ternaknesia.sobaternak.Adapter.HewanAdapter;
import com.ternaknesia.sobaternak.Connection.PostRequest;
import com.ternaknesia.sobaternak.DataModel.DataHewan;
import com.ternaknesia.sobaternak.Interface.EndpointAPI;
import com.ternaknesia.sobaternak.Interface.RequestComplete;
import com.ternaknesia.sobaternak.Manager.PrefManager;
import com.ternaknesia.sobaternak.R;
import com.ternaknesia.sobaternak.Utils.Item;
import com.ternaknesia.sobaternak.Utils.JSONGetter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Locale;

import okhttp3.FormBody;
import okhttp3.RequestBody;


public class JualHewanActivity extends AppCompatActivity implements RequestComplete {
    private PostRequest postRequest;
    private PrefManager prefManager;
    private EditText pemesan;
    private TextView jumlah;
    private TextView textTotal;
    private TextView textTotal_bottom;
    private Integer tot_price;
    private EditText d_code_tag;
    private Button d_search;
    private LinearLayout d_fail_container;
    private TextView d_fail;
    private LinearLayout d_info_container;
    private TextView d_info_hewan;
    private TextView d_instance;
    private TextView d_project;
    private TextView d_pemesan;
    private TextView d_buy_price;
    private TextView d_buy_date ;
    private TextView d_harvest_date;
    private LinearLayout d_sell_price;
    private EditText d_sell_price_unit;
    private EditText d_sell_price_kg;
    private Button d_add;
    private RecyclerView recyclerView;
    private Button tambah;
    private Button save;
    private DataHewan hewan;
    private ArrayList<Item> sell_animal;
    private ArrayList<Integer> sell_id;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_jual_hewan);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Jual Hewan");

        init();

        prefManager = new PrefManager(this);
        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if (extras == null) {
//                instance_id = null;
            } else {
//                instance_id = extras.getInt("instance_id");
            }

        } else {
//            instance_id = (Integer) savedInstanceState.getSerializable("instance_id");
        }

        RequestBody data = new FormBody.Builder()
                .build();

        postRequest = new PostRequest(JualHewanActivity.this);
        postRequest.SendPostData(EndpointAPI.myInstances, prefManager.getToken(), data, 0);
        postRequest.execute();

        sell_animal = new ArrayList<>();

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setFocusable(false);
        recyclerView.setAdapter(new HewanAdapter(sell_animal, this));

        tambah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onAddHewanClicked();
            }
        });

        save.setEnabled(false);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JSONArray json_data = new JSONArray();

                for(int i = 0; i < sell_animal.size(); i++) {
                    DataHewan item = (DataHewan) sell_animal.get(i);
                    item.setPemesan(pemesan.getText().toString());
                    JSONObject jsonObject= item.getJSONData();
                    try {
                        json_data.put(i, jsonObject);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                RequestBody data = new FormBody.Builder()
                        .add("data", json_data.toString())
                        .build();

                postRequest = new PostRequest(JualHewanActivity.this);
                postRequest.SendPostData(EndpointAPI.batchUpdateAnimal, prefManager.getToken(), data, 2);
                postRequest.execute();
            }
        });

        pemesan.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                checkOk();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        addHewanCheck();
    }

    private void checkOk() {
        save.setEnabled(!pemesan.getText().toString().equalsIgnoreCase("") && sell_animal.size() > 0);
    }

    public void onAddHewanClicked() {
        final Dialog _dialog = new Dialog(JualHewanActivity.this);
        _dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        _dialog.setContentView(R.layout.dialog_jual_hewan);
        d_code_tag = _dialog.findViewById(R.id.code_tag);
        d_search = _dialog.findViewById(R.id.btn_search);
        d_fail_container = _dialog.findViewById(R.id.info_fail);
        d_fail = _dialog.findViewById(R.id.fail);
        d_info_container = _dialog.findViewById(R.id.info_container);
        d_info_hewan = _dialog.findViewById(R.id.info_hewan);
        d_instance = _dialog.findViewById(R.id.instance);
        d_project = _dialog.findViewById(R.id.project);
        d_pemesan = _dialog.findViewById(R.id.pemesan);
        d_buy_price = _dialog.findViewById(R.id.buy_price);
        d_buy_date = _dialog.findViewById(R.id.buy_date);
        d_harvest_date = _dialog.findViewById(R.id.harvest_date);
        d_sell_price = _dialog.findViewById(R.id.sell_price);
        d_sell_price_unit = _dialog.findViewById(R.id.sell_price_unit);
        d_sell_price_kg = _dialog.findViewById(R.id.sell_price_kg);
        d_add = _dialog.findViewById(R.id.btn_tambah);

        d_code_tag.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    findTag();
                    return true;
                }
                return false;
            }
        });

        d_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                findTag();
            }
        });

        d_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hewan.setSellPriceUnitNum(Double.parseDouble(d_sell_price_unit.getText().toString()));
                hewan.setSellPriceKgNum(Double.parseDouble(d_sell_price_kg.getText().toString()));
                sell_animal.add(hewan);
                recyclerView.getAdapter().notifyDataSetChanged();
                _dialog.hide();
                addHewanCheck();
            }
        });

        d_sell_price_unit.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                changeText(false);
                return false;
            }
        });

        d_sell_price_kg.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                changeText(true);
                return false;
            }
        });

        _dialog.show();
    }

    private void changeText(boolean is_kg) {
        JSONObject data = hewan.getJSONData();
        Integer w = JSONGetter.getInt(data, "weight", 0);
        Integer unit = getInteger(d_sell_price_unit.getText().toString());
        Integer kg = getInteger(d_sell_price_kg.getText().toString());

        if (w > 0) {
            if ((unit > 0 && kg > 0) || (unit > 0 && kg == 0) && !is_kg) {
                kg = unit / w;
                d_sell_price_kg.setText(String.valueOf(kg));
            } else if (unit == 0 && kg > 0 || is_kg) {
                unit = kg * w;
                d_sell_price_unit.setText(String.valueOf(unit));
            }
        }
    }

    private Integer getInteger(String source) {
        try {
            return Integer.parseInt(source);
        } catch (Exception e) {
            return 0;
        }
    }

    private void addHewanCheck() {
        if(!pemesan.getText().toString().equalsIgnoreCase("")) {
            if(sell_animal.size() > 0) {
                save.setEnabled(true);
            }
        }

        Integer cnt = sell_animal.size();
        Double tot = 0.0;
        for(int i = 0; i < sell_animal.size(); i++) {
            DataHewan data = (DataHewan) sell_animal.get(i);
            tot += data.getSellPriceUnitNum();
        }

        jumlah.setText(String.valueOf(cnt));
        textTotal.setText(String.format(Locale.getDefault(), "Rp. %,.0f", tot).replaceAll(",", "."));
        textTotal_bottom.setText(String.format(Locale.getDefault(), "Rp. %,.0f", tot).replaceAll(",", "."));
    }

    private void findTag() {
        RequestBody data = new FormBody.Builder()
                .add("t", d_code_tag.getText().toString())
                .build();

        postRequest = new PostRequest(JualHewanActivity.this);
        postRequest.SendPostData(EndpointAPI.findHewanByTag, prefManager.getToken(), data, 1);
        postRequest.execute();
    }

    private void init() {
        pemesan = findViewById(R.id.pemesan);
        jumlah = findViewById(R.id.qty);
        textTotal = findViewById(R.id.total_price);
        textTotal_bottom = findViewById(R.id.total_price_bottom);
        recyclerView = findViewById(R.id.r_list_hewan);
        tambah = findViewById(R.id.btn_tambah);
        save = findViewById(R.id.btn_save);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    @Override
    public void onRequestComplete(JSONObject jsonObject, Integer REQUEST_CODE) {
        if (REQUEST_CODE == 2) {
            try {
                if (jsonObject.getString("response").equalsIgnoreCase("success")) {
                    Toast.makeText(this, "Berhasil menambah transaksi", Toast.LENGTH_SHORT).show();
                    finish();
                    //HewanActivity.hewanActivity.recreate();
                    setResult(5000);
                } else {
                    Toast.makeText(this, "Gagal menambah transaksi", Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else if(REQUEST_CODE == 1) {
            try {
                if (jsonObject.getString("response").equalsIgnoreCase("success")) {
                    JSONObject result = jsonObject.getJSONObject("result");
                    JSONArray data = result.getJSONArray("data");
                    //Log.e("[OK]", data.toString());
                    if(data.length() > 0) {
                        hewan = new DataHewan(data.getJSONObject(0));
                        d_info_hewan.setText(hewan.getJenisHewan() + " " + hewan.getJenisKelamin() + ", " + hewan.getStatus() + "\n" +
                                "Berat: " + hewan.getWeight() + ", Tinggi: " + hewan.getHeight());
                        d_instance.setText("Instansi: " + hewan.getInstance());
                        d_project.setText("Proyek: " + hewan.getProject());
                        d_pemesan.setText("Pemesan: " + hewan.getPemesan());
                        d_buy_price.setText("Harga Beli: " + hewan.getBuyPriceUnit() + hewan.getBuyPriceKg());
                        d_harvest_date.setText("Tanggal Panen: " + hewan.getHarvestDate() + " " + hewan.getHarvestAge());
                        Double vp_unit = hewan.getSellPriceUnitNum();
                        String p_unit = vp_unit > 0 ? String.format("%.0f", vp_unit) : "";
                        d_sell_price_unit.setText(String.valueOf(p_unit));
                        Double vp_kg = hewan.getSellPriceKgNum();
                        String p_kg = vp_kg > 0 ? String.format("%.0f", vp_kg) : "";
                        d_sell_price_kg.setText(String.valueOf(p_kg));
                        d_info_container.setVisibility(View.VISIBLE);
                        d_fail_container.setVisibility(View.GONE);
                        if (!data.getJSONObject(0).getString("pemesan").equalsIgnoreCase("null")) {
                            d_sell_price.setVisibility(View.GONE);
                            d_sell_price_unit.setEnabled(false);
                            d_sell_price_kg.setEnabled(false);
                            d_add.setEnabled(false);
                            d_add.setBackground(getResources().getDrawable(R.color.gray400));
                            d_fail.setText("Hewan ini sudah dipesan");
                            d_fail_container.setVisibility(View.VISIBLE);
                        } else {
                            d_sell_price.setVisibility(View.VISIBLE);
                            d_sell_price_unit.setEnabled(true);
                            d_sell_price_kg.setEnabled(true);
                            d_add.setEnabled(true);
                            d_add.setBackground(getResources().getDrawable(R.color.colorAccent));
                        }
                    } else {
                        d_fail.setText("Kode Tagging hewan tidak ditemukan");
                        d_fail_container.setVisibility(View.VISIBLE);
                        d_info_container.setVisibility(View.GONE);
                        d_sell_price.setVisibility(View.GONE);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
