package com.ternaknesia.sobaternak.activity;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.ternaknesia.sobaternak.DataModel.DataBobot;
import com.ternaknesia.sobaternak.R;
import com.ternaknesia.sobaternak.Utils.KgFormatter;

import java.util.ArrayList;
import java.util.List;

public class GrafikActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grafik);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        int tahun = getIntent().getIntExtra("tahun", 0);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle("Grafik Tahun " + tahun);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        LineChart lineChart = findViewById(R.id.barChart);
        ArrayList<DataBobot> data = getIntent().getParcelableArrayListExtra("data");

        ArrayList<String> month = new ArrayList<>();
        List<Entry> entries = new ArrayList<>();
        for (int i = 0; i < data.size(); i++) {
            month.add("Bulan " + data.get(i).getBulan());
            String weight = data.get(i).getWeight();
            entries.add(new Entry(i, Integer.valueOf(weight.substring(0, weight.length() - 3))));
        }

        LineDataSet set = new LineDataSet(entries, "Grafik Bobot tahun " + tahun);

        set.setLineWidth(1.75f);
        set.setCircleRadius(5f);
        set.setCircleHoleRadius(2.5f);
        set.setColor(Color.RED);
        set.setCircleColor(Color.RED);
        set.setHighLightColor(Color.RED);

        LineData lineData = new LineData(set);
        lineData.setValueFormatter(new KgFormatter());
        lineData.setValueTextSize(9f);

        Description description = new Description();
        description.setText("Bobot Per Kg");
        description.setTextSize(11f);

        lineChart.setDrawGridBackground(false);
        lineChart.setTouchEnabled(true);
        lineChart.setDragEnabled(true);
        lineChart.setScaleEnabled(true);

        lineChart.getAxisRight().setEnabled(false);

        lineChart.getXAxis().setGranularity(1f);
        lineChart.getXAxis().setValueFormatter((value, axis) -> {
            if (value >= 0) {
                if (value < month.size()) return month.get((int) value);
                return "";
            }
            return "";
        });

        lineChart.animateX(500);
        lineChart.getAxisLeft().setAxisMinimum(0f);
        lineChart.setPinchZoom(true);

        lineChart.setDescription(description);
        lineChart.setData(lineData);
        lineChart.invalidate();
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return super.onSupportNavigateUp();
    }
}
