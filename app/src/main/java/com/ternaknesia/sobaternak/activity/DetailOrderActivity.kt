package com.ternaknesia.sobaternak.activity

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.GridLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import com.ternaknesia.sobaternak.DataModel.DataAllOrder
import com.ternaknesia.sobaternak.DataModel.SalesOrder.Line
import com.ternaknesia.sobaternak.DataModel.sales_order.Data
import com.ternaknesia.sobaternak.R
import com.ternaknesia.sobaternak.Utils.DateFormatter
import com.ternaknesia.sobaternak.Utils.setHtml
import com.ternaknesia.sobaternak.model.LinesItem
import kotlinx.android.synthetic.main.activity_qurban_detail_order.*
import kotlinx.android.synthetic.main.item_pesanan_order_qurban.*
import timber.log.Timber
import java.util.*

class DetailOrderActivity : AppCompatActivity() {

    val tag = DetailOrderActivity::class.java.simpleName

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_qurban_detail_order)

        setSupportActionBar(toolbar)
        supportActionBar?.title = "Detail Penjualan"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val data: DataAllOrder? = intent.getParcelableExtra("data")
        data?.apply {

            atas_nama.text = pemesan
            tgl_pesan.text = DateFormatter().convertDate(tanggalPesan, "dd-MM-yyyy HH:mm")
            waktu_kirim.text = DateFormatter().convertDate(waktuKirim, "dd-MM-yyyy")
            alamat_kirim.text = alamat
            this@DetailOrderActivity.catatan.text = if (catatan.isNullOrEmpty()) "-" else catatan
            text_diskon.text = idrFormat(diskon)
            this@DetailOrderActivity.total.text = idrFormat(grandTotal)
            this@DetailOrderActivity.dp.text = idrFormat(uangMuka)
            this@DetailOrderActivity.kodeunik.text = kodeUnik.toString()

            when {
                status.equals("unpaid", ignoreCase = true) -> {
                    this@DetailOrderActivity.status.text = "Menunggu pembayaran"
                    this@DetailOrderActivity.status
                            .setBackgroundResource(R.drawable.rectangle_rounded_fill_blue)
                }

                status.equals("pending", ignoreCase = true) -> {
                    this@DetailOrderActivity.status.text = "Belum lunas"
                    this@DetailOrderActivity.status
                            .setBackgroundResource(R.drawable.rectangle_rounded_fill_purple)
                }

                status.equals("50paid", ignoreCase = true) -> {
                    this@DetailOrderActivity.status.text = "50% / lebih lunas"
                    this@DetailOrderActivity.status
                            .setBackgroundResource(R.drawable.rectangle_rounded_fill_green)
                }

                status.equals("paid", ignoreCase = true) -> {
                    this@DetailOrderActivity.status.text = "Lunas"
                    this@DetailOrderActivity.status
                            .setBackgroundResource(R.drawable.rectangle_rounded_fill_green)
                    info_pelunasan?.visibility = View.GONE
                    sisa_box!!.visibility = View.GONE
                }

                status.equals("cancelled", ignoreCase = true) -> {
                    this@DetailOrderActivity.status.text = "Dibatalkan"
                    this@DetailOrderActivity.status
                            .setBackgroundResource(R.drawable.rectangle_rounded_fill_gray)
                    info_pelunasan?.visibility = View.GONE
                    sisa_box!!.visibility = View.GONE
                }
            }

            when {
                deliveryStatus.equals("unprocess", ignoreCase = true) ->
                    this@DetailOrderActivity.status_delivery.text = "Belum diproses"
                deliveryStatus.equals("pending", ignoreCase = true) ->
                    this@DetailOrderActivity.status_delivery.text = "Pending"
                deliveryStatus.equals("prepared", ignoreCase = true) ->
                    this@DetailOrderActivity.status_delivery.text = "Disiapkan"
                deliveryStatus.equals("processed", ignoreCase = true) ->
                    this@DetailOrderActivity.status_delivery.text = "Sedang dikirim"
                deliveryStatus.equals("delivered", ignoreCase = true) ->
                    this@DetailOrderActivity.status_delivery.text = "Terkirim"
                deliveryStatus.equals("not_delivered", ignoreCase = true) ->
                    this@DetailOrderActivity.status_delivery.text = "Belum terkirim"
                deliveryStatus.equals("cancelled", ignoreCase = true) ->
                    this@DetailOrderActivity.status_delivery.text = "Dibatalkan"
            }

            products.forEach { onAddOrder(it) }

        }

    }

    @SuppressLint("SetTextI18n")
    private fun onAddOrder(data: Line) {
        val inflater = getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val rowView = inflater.inflate(R.layout.item_pesanan_order_qurban, null)
        parent_pesanan?.addView(rowView, parent_pesanan!!.childCount)

        tv_iq_contact.setTextIsSelectable(true)

        if (data.jumlah == data.prepared) {
            tv_iq_stt_prepared.visibility = View.VISIBLE
        }

        if (data.terkirim!! >= 0 && data.terkirim < data.jumlah!!) {
            tv_iq_stt_kirim.text = "Dalam Proses Kirim"
            tv_iq_stt_kirim.visibility = View.VISIBLE
        } else if (data.jumlah == data.terkirim) {
            tv_iq_stt_kirim.text = "Sudah Terkirim"
            tv_iq_stt_kirim.visibility = View.VISIBLE
        }

        //_id.setText(id);
//        tv_iq_contact.text = data.market?.telp
        tv_iq_pesanan.setHtml(data.nama)
        tv_iq_harga.text = idrFormat(data.harga)
        tv_iq_jumlah.text = data.jumlah.toString()
        tv_iq_subtotal.text = idrFormat(data.subtotal)

        Timber.d("harga : ${data.harga} jumlah : ${data.jumlah}")
    }

    private fun idrFormat(strNumber: Int): String {
        return String.format(Locale.getDefault(), "Rp%,d", strNumber)
                .replace(",".toRegex(), ".")
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return super.onSupportNavigateUp()
    }

}