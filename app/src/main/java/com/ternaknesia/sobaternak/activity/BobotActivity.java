package com.ternaknesia.sobaternak.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.ternaknesia.sobaternak.Adapter.BobotAdapter;
import com.ternaknesia.sobaternak.Connection.PostRequest;
import com.ternaknesia.sobaternak.DataModel.DataBobot;
import com.ternaknesia.sobaternak.Interface.EndpointAPI;
import com.ternaknesia.sobaternak.Interface.RequestComplete;
import com.ternaknesia.sobaternak.Manager.PrefManager;
import com.ternaknesia.sobaternak.R;
import com.ternaknesia.sobaternak.Utils.Item;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import okhttp3.FormBody;
import okhttp3.RequestBody;

public class BobotActivity extends AppCompatActivity implements RequestComplete {
    RecyclerView recyclerView;
    PostRequest postRequest;
    PrefManager prefManager;
    private Spinner tahun;
    private ArrayList<Integer> kandangsMap;
    private Integer tahun_int = 2018;
    private String status;
    //    private Button cari;
    private LinearLayout not_found;
    private NestedScrollView nestedScrollView;
    private FloatingActionButton fab;
    private TextView tvGrafik;
    private ProgressBar progressBar;
    private List<DataBobot> dataBobots;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bobot);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Objects.requireNonNull(getSupportActionBar()).setTitle("Catatan Bobot");
        prefManager = new PrefManager(this);

        tahun = findViewById(R.id.tahun);
//        cari = findViewById(R.id.cari);
        not_found = findViewById(R.id.not_found);
        nestedScrollView = findViewById(R.id.nested1);
        recyclerView = findViewById(R.id.kandang_list);
        fab = findViewById(R.id.add_record);
//        tvGrafik = findViewById(R.id.tv_grafik);
//        progressBar = findViewById(R.id.progresBar);

        ArrayList<String> kandangsLabel = new ArrayList<String>();
        kandangsMap = new ArrayList<>();
        dataBobots = new ArrayList<>();

        for (int f = 2018; f >= 2015; f--) {
            String name = String.valueOf(f);
            kandangsLabel.add(name);
            kandangsMap.add(f);
        }

        ArrayAdapter<String> kandangAdapter = new ArrayAdapter<String>(this, R.layout.simple_spinner_item, kandangsLabel);
        tahun.setAdapter(kandangAdapter);
        tahun.setSelection(kandangsMap.indexOf(tahun_int));

        //this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);


        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setNestedScrollingEnabled(false);
        //recyclerView.setFocusable(false);

/*        cari.setOnClickListener(v -> {
            //arrayList = new ArrayList<>();
            //recyclerView.getAdapter().notifyDataSetChanged();
            getData(EndpointAPI.ListBobot);
        });*/

        tahun.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                getData();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        nestedScrollView.setOnScrollChangeListener((NestedScrollView.OnScrollChangeListener)
                (v, scrollX, scrollY, oldScrollX, oldScrollY) -> {
                    if (scrollY > oldScrollY) {
                        fab.hide();
                    } else {
                        fab.show();
                    }
                });

        fab.setOnClickListener(v -> {
            Intent x = new Intent(BobotActivity.this, EditBobotActivity.class);
            startActivityForResult(x, 6000);
        });

        getData();
        tvGrafik.setOnClickListener(v -> {
            Intent intent = new Intent(this, GrafikActivity.class);
            intent.putExtra("tahun", tahun_int);
            intent.putParcelableArrayListExtra("data", new ArrayList<>(dataBobots));
            startActivity(intent);
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 6000) getData();
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void getData() {
        progressBar.bringToFront();
        progressBar.setVisibility(View.VISIBLE);
        try {
            tahun_int = kandangsMap.get(tahun.getSelectedItemPosition());
        } catch (Exception e) {
            e.printStackTrace();
        }

        RequestBody data = new FormBody.Builder()
                .add("instance_id", prefManager.getSingleDataProfile("instance_id"))
                .add("tahun", String.valueOf(tahun_int))
                .build();

        postRequest = new PostRequest(BobotActivity.this);
        postRequest.SendPostData(EndpointAPI.ListBobot, prefManager.getToken(), data, 1);
        postRequest.execute();
    }

    @Override
    public void onRequestComplete(JSONObject jsonObject, Integer REQUEST_CODE) {
        //Log.e("json : ", jsonObject.toString());
        progressBar.setVisibility(View.GONE);
        try {
            if (jsonObject.getString("response").equalsIgnoreCase("success")) {
                JSONArray result = jsonObject.getJSONArray("result");
                ArrayList<Item> item = new ArrayList<>(result.length());
                dataBobots.clear();
                for (int f = 0; f < result.length(); f++) {
                    JSONObject ix = result.getJSONObject(f);
                    item.add(new DataBobot(ix));
                    dataBobots.add(new DataBobot(ix));
                }

                recyclerView.setAdapter(new BobotAdapter(item, this));

                if (item.size() == 0) {
                    recyclerView.setVisibility(View.GONE);
                    not_found.setVisibility(View.VISIBLE);
                } else {
                    recyclerView.setVisibility(View.VISIBLE);
                    not_found.setVisibility(View.GONE);
                }

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }
}
