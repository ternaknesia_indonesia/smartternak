package com.ternaknesia.sobaternak.activity;


import android.content.Intent;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.ternaknesia.sobaternak.Adapter.KandangProyekAdapter;
import com.ternaknesia.sobaternak.Connection.PostRequest;
import com.ternaknesia.sobaternak.DataModel.DataKandangProyek;
import com.ternaknesia.sobaternak.Interface.EndpointAPI;
import com.ternaknesia.sobaternak.Interface.RequestComplete;
import com.ternaknesia.sobaternak.Manager.PrefManager;
import com.ternaknesia.sobaternak.R;
import com.ternaknesia.sobaternak.Utils.Item;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.FormBody;
import okhttp3.RequestBody;


public class KandangActivity extends AppCompatActivity implements RequestComplete {

    private static final String TAG = "KandangActivity";
    RecyclerView mRecyclerView;
    ArrayList<Item> content = null;
    String[][] data = {{"0", "Kandang Kambing 1", "Kandang Pak Agus", "http://ternaknesia.id/img/img.jpg", "20-11-2017", "300 Ekor"},
            {"1", "Kandang Kambing 2", "Kandang Pak Budi", "http://ternaknesia.id/img/img.jpg", "24-11-2017", "240 Ekor"},
            {"2", "Kandang Kambing 3", "Kandang Pak Mukidi", "http://ternaknesia.id/img/img.jpg", "26-11-2017", "456 Ekor"}
    };
    private String instance_id;
    private PostRequest postRequest;
    private PrefManager prefManager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kandang);

        prefManager = new PrefManager(this);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Kandang Ternak");

        mRecyclerView = findViewById(R.id.mRecyclerView);

        //mRecyclerView.setHasFixedSize(true);
        //mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        //mRecyclerView.setNestedScrollingEnabled(false);

        mRecyclerView.addItemDecoration(new GridSpacingItemDecoration(2, 1, false));
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 2);
        mRecyclerView.setLayoutManager(mLayoutManager);

        getData();
    }

    private void getData() {
        if (content != null) content.clear();
        RequestBody data = new FormBody.Builder()
                .build();
        postRequest = new PostRequest(this);
        postRequest.SendPostData(EndpointAPI.listKandang, prefManager.getToken(), data, 1);
        postRequest.execute();
    }

    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;
        private int[] ATTRS = new int[]{android.R.attr.listDivider};

        private Drawable divider;


        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.kandang_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        if (item.getItemId() == R.id.menu_item_tambah_kandang) {
            Intent t = new Intent(KandangActivity.this, TambahKandangActivity.class);
            Bundle b = new Bundle();
            b.putString("instance_id", instance_id);
            t.putExtras(b);
            startActivityForResult(t, 4000);
            //Toast.makeText(this, "Sementara fitur belum tersedia", Toast.LENGTH_LONG).show();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 4000) {
            getData();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    @Override
    public void onRequestComplete(JSONObject jsonObject, Integer REQUEST_CODE) {
        Log.d(TAG, "kandang : " + jsonObject.toString());
        if (REQUEST_CODE == 1) {
            try {
                if (jsonObject.getString("response").equalsIgnoreCase("success")) {
                    JSONArray c = jsonObject.getJSONArray("result");
                    content = new ArrayList<>(c.length());

                    for (int j = 0; j < c.length(); j++) {
                        JSONObject item = c.getJSONObject(j);
                        instance_id = item.getString("instance_id");
                        Log.d(TAG, "instance id " + instance_id);
                        String id = item.getString("id");
                        String name = item.getString("name");
                        String loc = item.getString("lokasi");
                        String desc = item.getString("desc");
                        String anakk = item.getString("anak_kandang");
                        String count = item.getString("animals");
                        String capacity = item.getString("capacity");
                        String type_id = item.getString("animal_types_id");
                        String type_name = item.getString("animal_type");

                        content.add(new DataKandangProyek(id, instance_id, loc, name, desc, anakk, count, capacity, type_id, type_name));
                        //Log.e("kandang ke ", id);
                    }

                    mRecyclerView.setAdapter(new KandangProyekAdapter(content, this));

                } else {
                    Toast.makeText(getBaseContext(), "Kesalahan server / Periksa koneksi Anda.", Toast.LENGTH_LONG).show();
                }
            } catch (JSONException e) {
                //showProgress(false);
                Toast.makeText(getBaseContext(), "Kesalahan server / Periksa koneksi Anda.", Toast.LENGTH_LONG).show();
            }
        }
    }
}

