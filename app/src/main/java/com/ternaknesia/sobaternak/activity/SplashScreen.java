package com.ternaknesia.sobaternak.activity;
/*        Mohon untuk tidak melakukan hal-hal yang melanggar hukum.
        Anda tidak berhak melakukan cracking terhadap system kami tanpa ada persetujuan.
        Terimakasih*/

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.CountDownTimer;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;

import com.ternaknesia.sobaternak.Connection.PostRequest;
import com.ternaknesia.sobaternak.Interface.EndpointAPI;
import com.ternaknesia.sobaternak.Interface.RequestComplete;
import com.ternaknesia.sobaternak.Manager.PrefManager;
import com.ternaknesia.sobaternak.R;
import com.ternaknesia.sobaternak.Utils.CheckConnection;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.FormBody;
import okhttp3.RequestBody;

public class SplashScreen extends AppCompatActivity implements RequestComplete {
    MyCounter myTimer;
    String currentVersion;
    PostRequest postRequest;
    PrefManager prefManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        /*if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("com.ternaknesia.com")) {

            // do what you want

            // and this for killing app if we dont want to start
            android.os.Process.killProcess(android.os.Process.myPid());

        } else {

            //continue to app
        }*/

        prefManager = new PrefManager(this);

        myTimer = new MyCounter(30 * 100, 100);
        myTimer.start();


        try {
            currentVersion = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            //e.printStackTrace();
        }

        boolean internetAvailable = new CheckConnection().isInternetAvailable(this);
        if (internetAvailable) {
            RequestBody data = new FormBody.Builder()
                    .build();
            postRequest = new PostRequest(this);
            postRequest.SendPostData(EndpointAPI.GetVersion, "", data, 1);
            postRequest.execute();
            //Log.e("execute", "pass");
        }
    }

    protected void onPause() {
        super.onPause();
        myTimer.cancel();
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            myTimer.cancel();
            SplashScreen.this.finish();
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onRequestComplete(JSONObject jsonObject, Integer CODE_REQUEST) {
        //Log.e("get version", jsonObject.toString());

        try {
            String onVersion = jsonObject.getString("version");
            if (onVersion != null && !onVersion.isEmpty()) {
                int i_onver = Integer.parseInt(onVersion.replaceAll("\\D+", ""));
                int i_currver = Integer.parseInt(currentVersion.replaceAll("\\D+", ""));

                //Toast.makeText(this, onVersion, Toast.LENGTH_LONG).show();
                if (i_currver < i_onver) {
                    myTimer.cancel();
                    //show dialog
                    final AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setMessage("Aplikasi ini dalam pengembangan. Mohon segera meng-update aplikasi untuk mendapatkan fitur / pembaruan terbaru.")
                            .setCancelable(false)
                            .setPositiveButton("Update", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.ternaknesia.app"));
                                    startActivity(browserIntent);
                                }
                            })
                            .setNegativeButton("Batal", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();
                                    SplashScreen.this.finish();
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                }
            }

        } catch (JSONException e) {
            /*e.printStackTrace();*/
        }
    }

    private class MyCounter extends CountDownTimer {
        MyCounter(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onFinish() {
            startActivity(new Intent(SplashScreen.this, LoginActivity.class));
            SplashScreen.this.finish();
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        }

        @Override
        public void onTick(long millisUntilFinished) {

        }
    }

    @Override
    protected void onResume() {
        super.onResume();

    }
}
