package com.ternaknesia.sobaternak.activity;
/*        Mohon untuk tidak melakukan hal-hal yang melanggar hukum.
        Anda tidak berhak melakukan cracking terhadap system kami tanpa ada persetujuan.
        Terimakasih*/

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.ternaknesia.sobaternak.Connection.PostRequest;
import com.ternaknesia.sobaternak.Interface.EndpointAPI;
import com.ternaknesia.sobaternak.Interface.RequestComplete;
import com.ternaknesia.sobaternak.Manager.PrefManager;
import com.ternaknesia.sobaternak.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.FormBody;
import okhttp3.RequestBody;

/**
 * A login screen that offers login via loc/password.
 */
public class EditKandangActivity extends AppCompatActivity implements RequestComplete {
    private EditText mName;
    private EditText mLoc;
    private EditText mAnkk;
    private EditText mDesc;
    private EditText mCapacity;

    private RelativeLayout mProgressView;
    private View mLoginFormView;
    private RelativeLayout formSignUp;
    private Button btnSubmit;
    private PrefManager prefManager;
    private PostRequest postRequest;
    private String id_kandang = "";
    private String instance_id = "";
    private String type_id = "";
    private String animal_type = "";

    private RadioGroup radioGroup;
    //private String[] tempRId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_kandang);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Edit Kandang");
        prefManager = new PrefManager(this);

        init();

        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if (extras == null) {
                id_kandang = null;
                instance_id = null;
            } else {
                id_kandang = extras.getString("id");
                instance_id = extras.getString("instance_id");
                mName.setText(extras.getString("name"));
                mLoc.setText(extras.getString("lokasi"));
                mAnkk.setText(extras.getString("anak_kandang"));
                mDesc.setText(extras.getString("desc"));
                mCapacity.setText(extras.getString("capacity"));
                type_id = extras.getString("type_id");
            }
        } else {
            id_kandang = (String) savedInstanceState.getSerializable("id");
            instance_id = (String) savedInstanceState.getSerializable("instance_id");
            mName.setText((String) savedInstanceState.getSerializable("name"));
            mLoc.setText((String) savedInstanceState.getSerializable("lokasi"));
            mAnkk.setText((String) savedInstanceState.getSerializable("anak_kandang"));
            mDesc.setText((String) savedInstanceState.getSerializable("desc"));
            mCapacity.setText((String) savedInstanceState.getSerializable("capacity"));
            type_id = (String) savedInstanceState.getSerializable("type_id");
        }


        RequestBody _data = new FormBody.Builder()
                .build();
        postRequest = new PostRequest(this);
        postRequest.SendPostData(EndpointAPI.listTypes, prefManager.getToken(), _data, 2);
        postRequest.execute();


        btnSubmit.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                btnSubmit.setEnabled(false);
                attemptLogin();
            }
        });

        mLoginFormView = findViewById(R.id.form1);
        mProgressView = findViewById(R.id.loading);
    }

    private void init() {
        formSignUp = findViewById(R.id.formRegister);
        mName = findViewById(R.id.name);
        mLoc = findViewById(R.id.address);
        mAnkk = findViewById(R.id.anakk);
        mDesc = findViewById(R.id.desc);
        btnSubmit = findViewById(R.id.btn_submit);
        mCapacity = findViewById(R.id.capacity);
        LinearLayout layout = findViewById(R.id.linear_rgroup);
        radioGroup = new RadioGroup(this);
        layout.addView(radioGroup);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    private void attemptLogin() {
        // Reset errors.
        mName.setError(null);
        mLoc.setError(null);
        mAnkk.setError(null);
        mDesc.setError(null);

        // Store values at the time of the login attempt.
        String loc = mLoc.getText().toString();
        String name = mName.getText().toString();
        String ankk = mAnkk.getText().toString();
        String desc = mDesc.getText().toString();

        boolean cancel = false;
        View focusView = null;

        if (TextUtils.isEmpty(loc)) {
            mLoc.setError(getString(R.string.error_field_required));
            focusView = mLoc;
            cancel = true;
            btnSubmit.setEnabled(true);
        }

        if (TextUtils.isEmpty(name)) {
            mName.setError(getString(R.string.error_field_required));
            focusView = mName;
            cancel = true;
            btnSubmit.setEnabled(true);
        }

        if (TextUtils.isEmpty(ankk)) {
            mAnkk.setError(getString(R.string.error_field_required));
            focusView = mAnkk;
            cancel = true;
            btnSubmit.setEnabled(true);
        }


        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
            btnSubmit.setEnabled(true);
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            int selected = radioGroup.getCheckedRadioButtonId();
            //Log.e("FGX", String.valueOf(selected));
            //Log.e("FGZ", String.valueOf(tempRId.length));
            if (selected > 0 && !animal_type.equalsIgnoreCase("")) {
                showProgress(true);

                //Log.e("reg2 id_kandang : ", id_kandang);
                RequestBody data = new FormBody.Builder()
                        .add("id", id_kandang)
                        .add("instance_id", instance_id)
                        .add("name", name)
                        .add("lokasi", loc)
                        .add("desc", desc)
                        .add("anak_kandang", ankk)
                        .add("animal_types_id", animal_type)
                        .add("capacity", mCapacity.getText().toString())
                        .build();
                postRequest = new PostRequest(this);
                postRequest.SendPostData(EndpointAPI.editKandang, prefManager.getToken(), data, 1);
                postRequest.execute();
            } else {
                Toast.makeText(getBaseContext(), "Pilih tipe hewan.", Toast.LENGTH_LONG).show();
                btnSubmit.setEnabled(true);
            }
        }
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

        formSignUp.setVisibility(show ? View.GONE : View.VISIBLE);
        formSignUp.animate().setDuration(shortAnimTime).alpha(
                show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                formSignUp.setVisibility(show ? View.GONE : View.VISIBLE);
            }
        });

        mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            }
        });

        mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
        mProgressView.animate().setDuration(shortAnimTime).alpha(
                show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            }
        });
    }

    @Override
    public void onRequestComplete(JSONObject jsonObject, Integer REQUEST_CODE) {
        if (REQUEST_CODE == 1) {
            try {
                if (jsonObject.getString("response").equalsIgnoreCase("success")) {
                    Toast.makeText(getBaseContext(), "Berhasil mengubah kandang.", Toast.LENGTH_LONG).show();
                    showProgress(false);
                    setResult(4000);
                    this.finish();
                } else {
                    btnSubmit.setEnabled(true);
                    Toast.makeText(getBaseContext(), "Kesalahan server / Periksa koneksi Anda.", Toast.LENGTH_LONG).show();
                    showProgress(false);
                }
            } catch (JSONException e) {
                /*e.printStackTrace();*/
                btnSubmit.setEnabled(true);
                Toast.makeText(getBaseContext(), "Kesalahan server / Periksa koneksi Anda.", Toast.LENGTH_LONG).show();
                showProgress(false);
            }
        } else if (REQUEST_CODE == 2) {
            try {
                if (jsonObject.getString("response").equalsIgnoreCase("success")) {
                    JSONArray c = jsonObject.getJSONArray("result");
                    //tempRId = new String[c.length()];
                    for (int j = 0; j < c.length(); j++) {
                        JSONObject item = c.getJSONObject(j);
                        //content.add(new DataKandangProyek(id, instance_id, loc, name, desc, anakk, count));
                        //Log.e("kandang ke ", id);
                        String _name = item.getString("name");
                        String _id = item.getString("id");
                        addRadioButton(_id, _name, j == 0);
                        //tempRId[j] = _id;
                    }


                    for (int t = 0; t < radioGroup.getChildCount(); t++){
                        RadioButton v = (RadioButton) radioGroup.getChildAt(t);
                        //Log.e("JKX", String.valueOf(t));
                        //Log.e("JKI", String.valueOf(v.getId()));
                        //Log.e("JKT", type_id);
                        if(v.getId() == Integer.parseInt(type_id)){
                            //Log.e("JKR", "set true");
                            v.setChecked(true);
                        }
                    }

                } else {
                    Toast.makeText(getBaseContext(), "Kesalahan server / Periksa koneksi Anda.", Toast.LENGTH_LONG).show();
                }
            } catch (JSONException e) {
                //showProgress(false);
                Toast.makeText(getBaseContext(), "Kesalahan server / Periksa koneksi Anda.", Toast.LENGTH_LONG).show();
            }
        }
    }

    private void addRadioButton(String _id, String name, boolean ischecked) {
        RadioButton radioButtonView = new RadioButton(this);
        radioButtonView.setText(name);
        //radioButtonView.setChecked(ischecked);
        radioButtonView.setId(Integer.parseInt(_id));
        radioButtonView.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    animal_type = _id;
                }
            }
        });
        //radioButtonView.setOnClickListener(this);
        radioGroup.addView(radioButtonView);
    }

    @Override
    public boolean onSupportNavigateUp() {
        this.finish();
        return super.onSupportNavigateUp();
    }

}
