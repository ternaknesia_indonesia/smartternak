package com.ternaknesia.sobaternak.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.Toast;

import com.ternaknesia.sobaternak.Fragment.NavAccount;
import com.ternaknesia.sobaternak.Fragment.NavFeeds;
import com.ternaknesia.sobaternak.Fragment.NavHome;
import com.ternaknesia.sobaternak.Fragment.NavOrder;
import com.ternaknesia.sobaternak.R;

import static com.ternaknesia.sobaternak.Service.MyFirebaseMessagingService.OPEN_ORDER;

public class MainHome_Dev extends AppCompatActivity {

    private boolean doubleBackToExitPressedOnce = false;

    private final Fragment fragmentHome= new NavHome();
    private final Fragment fragmentFeed= new NavFeeds();
    private final Fragment fragmentSell= new NavOrder();
    private final Fragment fragmentAccount= new NavAccount();
    private Fragment active = fragmentHome;
    private FragmentManager fragmentManager = getSupportFragmentManager();


    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navHome:
                    fragmentManager.beginTransaction().hide(active).show(fragmentHome).commit();
                    active = fragmentHome;
                    return true;
                case R.id.navFeeds:
                    fragmentManager.beginTransaction().hide(active).show(fragmentFeed).commit();
                    active = fragmentFeed;
                    return true;
                case R.id.navSell:
                    fragmentManager.beginTransaction().hide(active).show(fragmentSell).commit();
                    active = fragmentSell;
                    return true;
                case R.id.navAccount:
                    fragmentManager.beginTransaction().hide(active).show(fragmentAccount).commit();
                    active = fragmentAccount;
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_dev);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        BottomNavigationView bottomBar = findViewById(R.id.bottomBar);
        bottomBar.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        fragmentManager.beginTransaction().add(R.id.contentContainer, fragmentAccount, "4").hide(fragmentAccount).commit();
        fragmentManager.beginTransaction().add(R.id.contentContainer, fragmentSell, "3").hide(fragmentSell).commit();
        fragmentManager.beginTransaction().add(R.id.contentContainer, fragmentFeed, "2").hide(fragmentFeed).commit();
        fragmentManager.beginTransaction().add(R.id.contentContainer, fragmentHome, "1").commit();
        processIntent(getIntent());
    }

    @Override
    protected void onNewIntent(Intent intent) {
        processIntent(intent);
    }

    private void processIntent(Intent intent) {
        if (intent.getAction() != null && intent.getAction().equalsIgnoreCase(OPEN_ORDER)) {
            fragmentManager.beginTransaction().hide(active).show(fragmentSell).commit();
            active = fragmentSell;
        }
    }

    @Override
    public void onBackPressed() {

        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Tekan kembali untuk keluar", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(() -> doubleBackToExitPressedOnce = false, 2000);

    }

}
