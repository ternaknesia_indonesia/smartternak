package com.ternaknesia.sobaternak.activity;
/*        Mohon untuk tidak melakukan hal-hal yang melanggar hukum.
        Anda tidak berhak melakukan cracking terhadap system kami tanpa ada persetujuan.
        Terimakasih*/

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.ternaknesia.sobaternak.Connection.PostRequest;
import com.ternaknesia.sobaternak.Interface.EndpointAPI;
import com.ternaknesia.sobaternak.Interface.RequestComplete;
import com.ternaknesia.sobaternak.Manager.PrefManager;
import com.ternaknesia.sobaternak.R;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.FormBody;
import okhttp3.RequestBody;

/**
 * A login screen that offers login via address/password.
 */
public class Register2Activity extends AppCompatActivity implements RequestComplete {
    private EditText mAddress;
    private EditText mName;
    private EditText mCode;

    private RelativeLayout mProgressView;
    private View mLoginFormView;
    private RelativeLayout formSignUp;
    private Button btnRegister;
    private PrefManager prefManager;
    private PostRequest postRequest;
    private String token = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register2);
        prefManager = new PrefManager(this);

        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if (extras == null) {
                token = "";
            } else {
                token = extras.getString("token");
            }
        } else {
            token = (String) savedInstanceState.getSerializable("token");
        }

        init();

        btnRegister.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                btnRegister.setEnabled(false);
                attemptLogin();
            }
        });

        mLoginFormView = findViewById(R.id.form1);
        mProgressView = findViewById(R.id.loading);

/*        mCode.setFilters(new InputFilter[] {
                new InputFilter.AllCaps()
        });*/
        mCode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable arg) {

                String s = arg.toString();
                if(!s.equals(s.toUpperCase())){
                    s = s.toUpperCase();
                    mCode.setText(s);
                    mCode.setSelection(mCode.getText().length());

                }

            }
        });

    }

    private void init() {
        formSignUp = findViewById(R.id.formRegister);
        mAddress = findViewById(R.id.address);
        mName = findViewById(R.id.name);
        btnRegister = findViewById(R.id.btn_register);
        mCode = findViewById(R.id.code);
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    private void attemptLogin() {
        // Reset errors.
        mAddress.setError(null);
        mName.setError(null);
        mCode.setError(null);

        // Store values at the time of the login attempt.
        String address = mAddress.getText().toString();
        String name = mName.getText().toString();
        String code = mCode.getText().toString();


        boolean cancel = false;
        View focusView = null;

        // Check for a valid address address.
        if (TextUtils.isEmpty(address)) {
            mAddress.setError(getString(R.string.error_field_required));
            focusView = mAddress;
            cancel = true;
            btnRegister.setEnabled(true);
        }

        if (TextUtils.isEmpty(name)) {
            mName.setError(getString(R.string.error_field_required));
            focusView = mName;
            cancel = true;
            btnRegister.setEnabled(true);
        }

        if (TextUtils.isEmpty(code)) {
            mCode.setError(getString(R.string.error_field_required));
            focusView = mCode;
            cancel = true;
            btnRegister.setEnabled(true);
        }

        if (code.length() != 3) {
            mCode.setError("Code harus berisi tiga huruf.");
            focusView = mCode;
            cancel = true;
            btnRegister.setEnabled(true);
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
            btnRegister.setEnabled(true);
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            showProgress(true);
            //Log.e("reg2 token : ", token);
            RequestBody data = new FormBody.Builder()
                    .add("instance_type_id", "1")
                    .add("name", name)
                    .add("address", address)
                    .add("instance_code", code)
                    .build();
            postRequest = new PostRequest(this);
            postRequest.SendPostData(EndpointAPI.RegistInstance, token, data, 2);
            postRequest.execute();
//            if(status.equalsIgnoreCase("success"))
            //Toast.makeText(getBaseContext(), "Berhasil mendaftar. Silakan aktivasi akun Anda dengan link yang telah kami kirimkan ke address Anda.", Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

        formSignUp.setVisibility(show ? View.GONE : View.VISIBLE);
        formSignUp.animate().setDuration(shortAnimTime).alpha(
                show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                formSignUp.setVisibility(show ? View.GONE : View.VISIBLE);
            }
        });

        mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            }
        });

        mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
        mProgressView.animate().setDuration(shortAnimTime).alpha(
                show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            }
        });
    }

    @Override
    public void onRequestComplete(JSONObject jsonObject, Integer REQUEST_CODE) {
        try {
            if (jsonObject.getString("response").equalsIgnoreCase("success")) {
                if(prefManager.isLog()){
                    Toast.makeText(getBaseContext(), "Berhasil mendaftar sebagai peternak.", Toast.LENGTH_SHORT).show();
                    Toast.makeText(getBaseContext(), "Berhasil login.", Toast.LENGTH_LONG).show();
                    prefManager.setLog(true);
                    Intent d = new Intent(this, MainHome.class);
                    startActivity(d);
                    this.finish();
                } else {
                    Toast.makeText(getBaseContext(), "Silakan masuk dengan akun yang telah didaftarkan", Toast.LENGTH_LONG).show();
                    showProgress(false);
                    Intent d = new Intent(this, LoginActivity.class);
                    startActivity(d);
                    this.finish();
                }
            } else {
                String msg = jsonObject.getString("result");
                //Log.e("reg2", "log 1");
                btnRegister.setEnabled(true);
                Toast.makeText(getBaseContext(), msg, Toast.LENGTH_LONG).show();
                showProgress(false);
            }
        } catch (JSONException e) {
            /*e.printStackTrace();*/
            btnRegister.setEnabled(true);
            Toast.makeText(getBaseContext(), "Kesalahan server / Periksa koneksi Anda.", Toast.LENGTH_LONG).show();
            showProgress(false);
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        this.finish();
        return super.onSupportNavigateUp();
    }

    @Override
    public void onBackPressed() {
        Back();
        //super.onBackPressed();
    }

    private void Back (){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Apakah Anda ingin membatalkan pendaftaran peternak?")
                .setCancelable(false)
                .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
/*                        Toast.makeText(getBaseContext(), "Silakan masuk dengan akun yang telah didaftarkan dan lengkapi data Anda.", Toast.LENGTH_LONG).show();
                        Intent l = new Intent(Register2Activity.this, LoginActivity.class);
                        startActivity(l);*/
                        finish();
                    }
                })
                .setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }
}
