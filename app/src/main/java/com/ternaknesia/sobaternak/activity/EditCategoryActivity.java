package com.ternaknesia.sobaternak.activity;
/*        Mohon untuk tidak melakukan hal-hal yang melanggar hukum.
        Anda tidak berhak melakukan cracking terhadap system kami tanpa ada persetujuan.
        Terimakasih*/

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ternaknesia.sobaternak.Connection.PostRequest;
import com.ternaknesia.sobaternak.Interface.EndpointAPI;
import com.ternaknesia.sobaternak.Interface.RequestComplete;
import com.ternaknesia.sobaternak.Manager.PrefManager;
import com.ternaknesia.sobaternak.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.FormBody;
import okhttp3.RequestBody;

/**
 * A login screen that offers login via loc/password.
 */
public class EditCategoryActivity extends AppCompatActivity implements RequestComplete {
    private TextView mId;
    private EditText mGrade;
    private EditText mMinW;
    private EditText mMaxW;

    private RelativeLayout mProgressView;
    private View mLoginFormView;
    private RelativeLayout formSignUp;
    private Button btnSubmit;
    private PrefManager prefManager;
    private PostRequest postRequest;
    private String id = "";
    private String grade = "";
    private String max_weight = "";
    private String min_weight = "";
    private LinearLayout linearKandang;
    private final String TAG = EditCategoryActivity.class.getSimpleName();
    private boolean isUpdate = false;

    //private String[] tempRId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_category);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        prefManager = new PrefManager(this);

        init();

        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if (extras == null) {
                id = "";
                grade = "";
                min_weight = "";
                max_weight = "";
            } else {
                id = extras.getString("id");
                grade = extras.getString("kandang_id");
                min_weight = extras.getString("min_weight");
                max_weight = extras.getString("max_weight");
            }
        } else {
            id = (String) savedInstanceState.getSerializable("id");
            grade = (String) savedInstanceState.getSerializable("kandang_id");
            min_weight = (String) savedInstanceState.getSerializable("min_weight");
            max_weight = (String) savedInstanceState.getSerializable("max_weight");
        }

        if (id.equalsIgnoreCase("")) getSupportActionBar().setTitle("Tambah Kategori");
        else getSupportActionBar().setTitle("Edit Kategori");

        isUpdate = !id.equalsIgnoreCase("");

        mId.setText(grade);
        mGrade.setText(grade);
        mMinW.setText(min_weight);
        //mTahun.setText(tahun);
        mMaxW.setText(max_weight);

        btnSubmit.setOnClickListener(view -> {
            btnSubmit.setEnabled(false);
            attemptSubmit();
        });

        mLoginFormView = findViewById(R.id.form1);
        mProgressView = findViewById(R.id.loading);
    }

    private void init() {
        formSignUp = findViewById(R.id.formRegister);
        mId = findViewById(R.id.idG);
        mGrade = findViewById(R.id.grade);
        mMinW = findViewById(R.id.min_w);
        mMaxW = findViewById(R.id.max_w);
        btnSubmit = findViewById(R.id.btn_submit);
        linearKandang = findViewById(R.id.linear_kandang);

    }

    private void attemptSubmit() {
        // Reset errors.
        mMinW.setError(null);
        mMaxW.setError(null);
        mGrade.setError(null);

        // Store values at the time of the login attempt.
        String minw_ = mMinW.getText().toString();
        String maxw_ = mMaxW.getText().toString();
        String grade_ = mGrade.getText().toString();

        boolean cancel = false;
        View focusView = null;

        if (TextUtils.isEmpty(minw_)) {
            mMinW.setError(getString(R.string.error_field_required));
            focusView = mMinW;
            cancel = true;
            btnSubmit.setEnabled(true);
        }

        if (TextUtils.isEmpty(maxw_)) {
            mMaxW.setError(getString(R.string.error_field_required));
            focusView = mMaxW;
            cancel = true;
            btnSubmit.setEnabled(true);
        }

        if (TextUtils.isEmpty(grade_)) {
            mGrade.setError(getString(R.string.error_field_required));
            focusView = mGrade;
            cancel = true;
            btnSubmit.setEnabled(true);
        }


        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
            btnSubmit.setEnabled(true);
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.


            //Log.e("reg2 grade : ", grade);
            RequestBody data = new FormBody.Builder()
                    .add("id", id)
                    .add("instance_id", prefManager.getSingleDataProfile("instance_id"))
                    .add("max_weight", maxw_)
                    .add("min_weight", minw_)
                    .add("name", grade_)
                    .add("animal_types_id", String.valueOf(1))
                    .add("is_active", "1")
                    .build();

            postRequest = new PostRequest(this);
            postRequest.SendPostData(EndpointAPI.editCategory, prefManager.getToken(), data, 1);
            postRequest.execute();
        }
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

        formSignUp.setVisibility(show ? View.GONE : View.VISIBLE);
        formSignUp.animate().setDuration(shortAnimTime).alpha(
                show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                formSignUp.setVisibility(show ? View.GONE : View.VISIBLE);
            }
        });

        mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            }
        });

        mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
        mProgressView.animate().setDuration(shortAnimTime).alpha(
                show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            }
        });
    }

    @Override
    public void onRequestComplete(JSONObject jsonObject, Integer REQUEST_CODE) {
        Log.e("vgf", jsonObject.toString());
        if (REQUEST_CODE == 1) {
            try {
                if (jsonObject.getString("response").equalsIgnoreCase("success")) {
                    if (isUpdate) {
                        Toast.makeText(getBaseContext(), "Berhasil mengubah Kategori.", Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(getBaseContext(), "Berhasil menambah Kategori.", Toast.LENGTH_LONG).show();
                    }
                    showProgress(false);
//                    setResultAllOrder(7000);
                    finish();
                    SetCategoryActivity.isReload = true;
                } else {
                    btnSubmit.setEnabled(true);
                    Toast.makeText(getBaseContext(), "Kesalahan server / Periksa koneksi Anda.", Toast.LENGTH_LONG).show();
                    showProgress(false);
                }
            } catch (JSONException e) {
                /*e.printStackTrace();*/
                btnSubmit.setEnabled(true);
                Toast.makeText(getBaseContext(), "Kesalahan server / Periksa koneksi Anda.", Toast.LENGTH_LONG).show();
                showProgress(false);
            }
        } else if (REQUEST_CODE == 2) {
            try {
                if (jsonObject.getString("response").equalsIgnoreCase("success")) {
                    JSONArray kandangs = jsonObject.getJSONArray("result");


                } else {
                    Toast.makeText(getBaseContext(), "Kesalahan server / Periksa koneksi Anda.", Toast.LENGTH_LONG).show();
                }
            } catch (JSONException e) {
                //showProgress(false);
                Toast.makeText(getBaseContext(), "Kesalahan server / Periksa koneksi Anda.", Toast.LENGTH_LONG).show();
            }
        }
    }


    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return super.onSupportNavigateUp();
    }
}
