package com.ternaknesia.sobaternak.activity;


import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.ternaknesia.sobaternak.Adapter.KandangProyekAdapter;
import com.ternaknesia.sobaternak.Adapter.SubPeternakAdapter;
import com.ternaknesia.sobaternak.Connection.PostRequest;
import com.ternaknesia.sobaternak.DataModel.DataSubPeternak;
import com.ternaknesia.sobaternak.Interface.EndpointAPI;
import com.ternaknesia.sobaternak.Interface.RequestComplete;
import com.ternaknesia.sobaternak.Manager.PrefManager;
import com.ternaknesia.sobaternak.R;
import com.ternaknesia.sobaternak.Utils.Footer;
import com.ternaknesia.sobaternak.Utils.Item;
import com.ternaknesia.sobaternak.Utils.JSONGetter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.FormBody;
import okhttp3.RequestBody;


public class SubPeternakActivity extends AppCompatActivity implements RequestComplete {
    PostRequest postRequest;
    PrefManager prefManager;
    private static final String TAG = "KandangActivity";
    private LineChart mLineChart;
    RecyclerView mRecyclerView;
    ArrayList<Item> content = new ArrayList<>();
    private boolean hasMore = false;
    private String next_page = "";
    private Boolean isLoadMore;
    private TextView jumlahSubPeternak;
    private TextView jumlahKandang;
    private TextView capacity;
    private TextView jumlahHewan;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub_peternak);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Peternak Mitra");
        prefManager = new PrefManager(this);

        init();

        isLoadMore = false;
        mRecyclerView = findViewById(R.id.mRecyclerView);

        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 1);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setNestedScrollingEnabled(false);
        mRecyclerView.setFocusable(false);

        content = new ArrayList<>();

        mRecyclerView.setAdapter(new KandangProyekAdapter(content, this));
        mRecyclerView.getLayoutManager().scrollToPosition(0);

        getData(EndpointAPI.subInstances, 1);
        getData(EndpointAPI.myInstancesData, 2);
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    private void getData(String url, Integer codeRequest) {
        String instance_id = prefManager.getSingleDataProfile("instance_id");
        RequestBody data = new FormBody.Builder()
                .add("instance_id", instance_id)
                .build();

        postRequest = new PostRequest(SubPeternakActivity.this);
        postRequest.SendPostData(url, prefManager.getToken(), data, codeRequest);
        postRequest.execute();
    }

    private void init() {
        jumlahSubPeternak = findViewById(R.id.jumlahSubPeternak);
        jumlahKandang = findViewById(R.id.jumlahKandang);
        capacity = findViewById(R.id.capacity);
        jumlahHewan = findViewById(R.id.jumlahHewan);
    }

    @Override
    public void onRequestComplete(JSONObject jsonObject, Integer REQUEST_CODE) {
        if(REQUEST_CODE == 1) {
            try {
                if (jsonObject.getString("response").equalsIgnoreCase("success")) {
                    JSONObject result = jsonObject.getJSONObject("result");
                    JSONArray data = result.getJSONArray("data");
                    next_page = result.getString("next_page_url");
                    ArrayList<Item> item = new ArrayList<>(data.length());
                    for (int f = 0; f < data.length(); f++) {
                        JSONObject ix = data.getJSONObject(f);
                        item.add(new DataSubPeternak(ix));
                    }

                    hasMore = !next_page.equalsIgnoreCase("null");
                    if (isLoadMore) {
                        deleteItem(content.size() - 1);
                        content.addAll(item);
                        mRecyclerView.getAdapter().notifyDataSetChanged();
                    } else {
                        content = item;
                        isLoadMore = hasMore;
                        mRecyclerView.setAdapter(new SubPeternakAdapter(content, this));
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else if (REQUEST_CODE == 2) {
            try {
                if (jsonObject.getString("response").equalsIgnoreCase("success")) {
                    JSONObject result = jsonObject.getJSONObject("result");
                    jumlahSubPeternak.setText(JSONGetter.getString(result,"sub_peternak", "0"));
                    jumlahKandang.setText(result.getString("kandang") + " Kandang (Mitra: " + JSONGetter.getString(result,"kandang_sub", "0") + " Kandang)");
                    capacity.setText(result.getString("capacity") + " Ekor (Mitra: " + JSONGetter.getString(result,"capacity_sub", "0") + " Ekor)");
                    jumlahHewan.setText(result.getString("animal") + " (Mitra: " + JSONGetter.getString(result, "animal_sub", "-") + ")");
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void deleteItem(int pos) {
        if(content.get(pos) instanceof Footer) {
            content.remove(pos);
            mRecyclerView.getAdapter().notifyDataSetChanged();
        }
    }
}

