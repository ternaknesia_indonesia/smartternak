package com.ternaknesia.sobaternak.activity;


import android.app.AlertDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.ternaknesia.sobaternak.Adapter.StockAdapter;
import com.ternaknesia.sobaternak.Connection.GetRequest;
import com.ternaknesia.sobaternak.Connection.PostRequest;
import com.ternaknesia.sobaternak.Interface.EndpointAPI;
import com.ternaknesia.sobaternak.Interface.RequestComplete;
import com.ternaknesia.sobaternak.Manager.PrefManager;
import com.ternaknesia.sobaternak.DataModel.StockProduct;
import com.ternaknesia.sobaternak.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.FormBody;
import okhttp3.RequestBody;

public class StockActivity extends AppCompatActivity implements StockAdapter.OnButtonClickedListener, RequestComplete {

    private RecyclerView listKambing, listSapi, listDomba;
    private StockAdapter adapterKambing, adapterSapi, adapterDomba;
    private Button action;

    //private SPUser spUser;
    private PrefManager prefManager;

    private boolean state1, state2, state3;

    private LinearLayout layoutKambing, layoutSapi, layoutDomba;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stock);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Kelola Stok");

        // Inflate the layout for this fragment
        //spUser = new SPUser(this);
        prefManager = new PrefManager(this);

        state1 = false;
        state2 = false;
        state3 = false;

        initView();
        initContent();
        getData();
    }

    private void getData() {
        adapterDomba.removeData();
        adapterSapi.removeData();
        adapterKambing.removeData();

        GetRequest getRequest = new GetRequest(this);
        getRequest.SendGetData(EndpointAPI.getMyMarket, prefManager.getToken(), 1);
        getRequest.execute();
    }

    private void initContent() {
        adapterKambing = new StockAdapter(this, 1);
        adapterSapi = new StockAdapter(this, 2);
        adapterDomba = new StockAdapter(this, 3);
        RecyclerView.LayoutManager recyclerLayoutManager = new LinearLayoutManager(this);
        listKambing.setLayoutManager(recyclerLayoutManager);
        listKambing.setAdapter(adapterKambing);
        recyclerLayoutManager = new LinearLayoutManager(this);
        listSapi.setLayoutManager(recyclerLayoutManager);
        listSapi.setAdapter(adapterSapi);
        recyclerLayoutManager = new LinearLayoutManager(this);
        listDomba.setLayoutManager(recyclerLayoutManager);
        listDomba.setAdapter(adapterDomba);


        layoutKambing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listKambing.scrollToPosition(0);
                listKambing.setVisibility(View.VISIBLE);
                listSapi.setVisibility(View.GONE);
                listDomba.setVisibility(View.GONE);
            }
        });

        layoutSapi.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                listSapi.scrollToPosition(0);
                listSapi.setVisibility(View.VISIBLE);
                listKambing.setVisibility(View.GONE);
                listDomba.setVisibility(View.GONE);

            }
        });

        layoutDomba.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view) {
                listDomba.scrollToPosition(0);
                listDomba.setVisibility(View.VISIBLE);
                listKambing.setVisibility(View.GONE);
                listSapi.setVisibility(View.GONE);
            }
        });
        action.setOnClickListener(new View.OnClickListener()

        {
            @Override
            public void onClick(View view) {
                Toast.makeText(getBaseContext(), "Test", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void initView() {
        listKambing = findViewById(R.id.recycler_view1);
        listSapi = findViewById(R.id.recycler_view2);
        listDomba = findViewById(R.id.recycler_view3);
        action = findViewById(R.id.action_button);
        layoutKambing = findViewById(R.id.category1);
        layoutSapi = findViewById(R.id.category2);
        layoutDomba = findViewById(R.id.category3);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    @Override
    public void onChangeClicked(final StockProduct productItemClass) {

        AlertDialog.Builder mBuilder = new AlertDialog.Builder(this);
        View mView = getLayoutInflater().inflate(R.layout.change_stock_layout, null);

        Button subtract, add, save;
        final EditText currentStock;

        currentStock = mView.findViewById(R.id.current_stock);
        currentStock.setText(String.valueOf(productItemClass.getStock()));
        subtract = mView.findViewById(R.id.subtract);
        add = mView.findViewById(R.id.add);
        save = mView.findViewById(R.id.save_change);

        mBuilder.setView(mView);
        final AlertDialog dialog = mBuilder.create();
        dialog.show();

        subtract.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                currentStock.setText(String.valueOf(Integer.valueOf(currentStock.getText().toString()) - 1));
                if (Integer.valueOf(currentStock.getText().toString()) <= 0)
                    currentStock.setText("0");
            }
        });
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                currentStock.setText(String.valueOf(Integer.valueOf(currentStock.getText().toString()) + 1));
            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateProduct(productItemClass, Integer.valueOf(currentStock.getText().toString()));
                dialog.dismiss();
            }
        });


    }

    private void setView() {
        if (adapterKambing.isEmpty()) layoutKambing.setVisibility(View.GONE);
        if (adapterSapi.isEmpty()) layoutSapi.setVisibility(View.GONE);
        if (adapterDomba.isEmpty()) layoutDomba.setVisibility(View.GONE);
    }

    private void updateProduct(StockProduct productItemClass, Integer stock) {
        RequestBody data = new FormBody.Builder()
                .add("product_id", String.valueOf(productItemClass.getProduct_id()))
                .add("market_id", String.valueOf(productItemClass.getMarket_id()))
                .add("category_id", String.valueOf(productItemClass.getCategory_id()))
                .add("nama", productItemClass.getNama())
                .add("stok", String.valueOf(stock))
                .build();
        PostRequest postRequest = new PostRequest(this);
        postRequest.SendPostData(EndpointAPI.updateProduct, prefManager.getToken(), data, 2);
        postRequest.execute();
    }


    @Override
    public void onRequestComplete(JSONObject jsonObject, Integer REQUEST_CODE) {
        if (REQUEST_CODE == 1) {
            try {
                if (jsonObject.getString("response").equalsIgnoreCase("success")) {
                    JSONArray data = jsonObject.getJSONObject("result").getJSONArray("products");
                    adapterKambing.addData(data);
                    adapterSapi.addData(data);
                    adapterDomba.addData(data);
                    setView();
                } else {
                    Toast.makeText(getBaseContext(), "Gagal mengambil data", Toast.LENGTH_LONG).show();
                }
            } catch (JSONException e) {
                //e.printStackTrace();
                Toast.makeText(getBaseContext(), "Akun Anda bukan akun peternak mitra.", Toast.LENGTH_LONG).show();
            }
        } else if (REQUEST_CODE == 2) {
            try {
                if (jsonObject.getString("response").equalsIgnoreCase("success")) {
                    Toast.makeText(getBaseContext(), "Berhasil mengubah item", Toast.LENGTH_LONG).show();
                    getData();
                } else {
                    Toast.makeText(getBaseContext(), "Gagal mengubah item", Toast.LENGTH_LONG).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
