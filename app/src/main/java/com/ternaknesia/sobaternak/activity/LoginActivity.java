package com.ternaknesia.sobaternak.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.ternaknesia.sobaternak.Connection.PostRequest;
import com.ternaknesia.sobaternak.Interface.EndpointAPI;
import com.ternaknesia.sobaternak.Interface.RequestComplete;
import com.ternaknesia.sobaternak.Manager.PrefManager;
import com.ternaknesia.sobaternak.R;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.FormBody;
import okhttp3.RequestBody;

public class LoginActivity extends AppCompatActivity implements RequestComplete {

    private EditText email, password;
    private Button loginButton;
    //private TextInputLayout emailLayout, passwordLayout;
    //private RelativeLayout loadingScreen;
    private PrefManager prefManager;
    private PostRequest postRequest, postRequest1;
    private TextView daftar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        prefManager = new PrefManager(this);
        if (prefManager.isLog()) {
            startActivity(new Intent(this, MainHome_Dev.class));
            this.finish();
        }

        setContentView(R.layout.activity_login);

        email = findViewById(R.id.email);
        password = findViewById(R.id.password);
        loginButton = findViewById(R.id.email_sign_in_button);
        daftar = findViewById(R.id.tvDaftar);
        //emailLayout = findViewById(R.id.email_layout);
        //passwordLayout = findViewById(R.id.password_layout);
        //loadingScreen = findViewById(R.id.loading);


        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (email.getText().toString().isEmpty() || password.getText().toString().isEmpty())
                    Toast.makeText(LoginActivity.this, "Email dan password tidak boleh kosong.", Toast.LENGTH_LONG).show();
                else {
                    loginNow();
                    //requestLogin(email.getText().toString(), password.getText().toString());
                }
            }
        });

        email.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                checkReady();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        password.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                checkReady();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        daftar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent s = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(s);
                finish();
            }
        });

    }

    private void checkReady() {
        loginButton.setEnabled((password.getText().toString().length() > 5 && !email.getText().toString().equalsIgnoreCase("")));
    }

    private void loginNow() {
        loginButton.setEnabled(false);
        RequestBody data = new FormBody.Builder()
                .add("email", email.getText().toString())
                .add("password", password.getText().toString())
                .add("id_firebase", prefManager.getIDFB())
                .add("is_farmer", "true")
                .build();
        postRequest = new PostRequest(this);
        postRequest.SendPostData(EndpointAPI.Login, "", data, 1);
        postRequest.execute();
    }

   /* private void requestLogin(String email, String password) {
        //loadingScreen.setVisibility(View.VISIBLE);
        String token = FirebaseInstanceId.getInstance().getToken();
        Log.d("K;aa;", "requestLogin: " + token);
        JSONObject obj = new JSONObject();
        try {
            obj.accumulate("email", email);
            obj.accumulate("password", password);
            obj.accumulate("id_firebase", token);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.d("Ternaknesia", "loginVolley: " +obj.toString());

        JsonObjectRequest req = new JsonObjectRequest(Request.Method.POST,
                getResources().getString(R.string.baseUrl)+"/api/login", obj, response -> {
            Log.d("Ternaknesiafs", response.toString());
            try {

                Log.d("26 maret", "check: ");
                Log.d("maret", response.getJSONObject("result").getString("token"));
                Log.d("maret", response.getString("response"));

                if(response.getString("response").equals("success"))
                {
                    spUser = new SPUser(LoginActivity.this);
                    spUser.saveSPBoolean(SPUser.SP_SUDAH_LOGIN, true);
                    spUser.saveSPString(SPUser.SP_TOKEN, response.getJSONObject("result").getString("token"));
                    //loadingScreen.setVisibility(View.GONE);
                    startActivity(new Intent(LoginActivity.this, TernakActivity.class));
                    finish();
                }
                else if(response.getString("message").equals("Invalid Password"))
                {
                    Toast.makeText(this, "Password yang anda masukkan salah", Toast.LENGTH_SHORT).show();

                }




            } catch (JSONException e) {
                //loadingScreen.setVisibility(View.GONE);
                Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }
        }, error -> {
            error.printStackTrace();
            //loadingScreen.setVisibility(View.GONE);
            Toast.makeText(this, error.getMessage(), Toast.LENGTH_SHORT).show();
            Log.d("Ternaknesia", "loginVolley: " + error.getMessage());
            Log.d("Ternaknesia", "loginVolley: " + error.networkResponse);
        });
        req.setRetryPolicy(new DefaultRetryPolicy(0,0,0));
        RequestSingleton.getInstance(this).addToRequestQueue(req);

    }*/

    @Override
    public void onRequestComplete(JSONObject jsonObject, Integer REQUEST_CODE) {
        Log.d(LoginActivity.class.getSimpleName(), "onRequestComplete login: " + jsonObject.toString());
        switch (REQUEST_CODE) {
            case 1:
                try {
                    if (jsonObject.getString("response").equalsIgnoreCase("success")) {
                        JSONObject c = jsonObject.getJSONObject("result");
                        prefManager.setToken(c.getString("token"));

                        RequestBody data1 = new FormBody.Builder().build();
                        postRequest1 = new PostRequest(this);
                        postRequest1.SendPostData(EndpointAPI.DetailProfile, c.getString("token"), data1, 2);
                        postRequest1.execute();
                        //Log.e("do second", "getdetail");
                    } else if (jsonObject.getString("response").equalsIgnoreCase("error")) {
                        //showProgress(false);
                        Toast.makeText(getBaseContext(), "Pastikan email dan password yang Anda masukkan benar.", Toast.LENGTH_LONG).show();
                        loginButton.setEnabled(true);
                    } else {
                        //showProgress(false);
                        Toast.makeText(getBaseContext(), "Kesalahan server / Periksa koneksi Anda.", Toast.LENGTH_LONG).show();
                        loginButton.setEnabled(true);
                    }
                } catch (JSONException e) {
                    //showProgress(false);
                    Toast.makeText(getBaseContext(), "Kesalahan server / Periksa koneksi Anda.", Toast.LENGTH_LONG).show();
                    loginButton.setEnabled(true);
                }
                break;
            case 2:
                try {
                    if (jsonObject.getString("response").equalsIgnoreCase("success")) {
                        prefManager.setDataProfile(jsonObject.toString());
                        prefManager.setLog(true);
                        if (prefManager.getSingleDataProfile("is_peternak").equalsIgnoreCase("false")) {
                            //Toast.makeText(getBaseContext(), "Akun Anda bukan akun peternak.", Toast.LENGTH_LONG).show();
                            AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
                            builder.setMessage("Akun Anda bukan akun peternak. Apakah Anda ingin mendaftar sebagai peternak?")
                                    .setCancelable(false)
                                    .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            Intent l = new Intent(LoginActivity.this, Register2Activity.class);
                                            Bundle x = new Bundle();
                                            x.putString("token", prefManager.getToken());
                                            l.putExtras(x);
                                            startActivity(l);
                                            finish();
                                        }
                                    })
                                    .setNegativeButton("Batal", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.cancel();
                                        }
                                    });
                            AlertDialog alert = builder.create();
                            alert.show();
                        } else {
                            Toast.makeText(getBaseContext(), "Berhasil login.", Toast.LENGTH_LONG).show();
                            //prefManager.setLog(true);
                            //showProgress(false);
                            Intent d = new Intent(this, MainHome_Dev.class);
                            startActivity(d);

                            //if(prefManager.getFPrev()){
                            this.finish();
                        /*}else {
                            Intent d = new Intent(this, GameHome.class);
                            GameHome.fa.finish();
                            this.finish();
                            startActivity(d);
                        }*/
                        }
                    }
                } catch (JSONException e) {
                    /*e.printStackTrace();*/
                }
                break;
        }
    }
}
