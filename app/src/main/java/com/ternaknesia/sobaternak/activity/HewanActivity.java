package com.ternaknesia.sobaternak.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.ternaknesia.sobaternak.Adapter.HewanAdapter;
import com.ternaknesia.sobaternak.Connection.PostRequest;
import com.ternaknesia.sobaternak.DataModel.DataHewan;
import com.ternaknesia.sobaternak.Interface.EndpointAPI;
import com.ternaknesia.sobaternak.Interface.RequestComplete;
import com.ternaknesia.sobaternak.Manager.PrefManager;
import com.ternaknesia.sobaternak.R;
import com.ternaknesia.sobaternak.Utils.Footer;
import com.ternaknesia.sobaternak.Utils.Item;
import com.ternaknesia.sobaternak.Utils.JSONGetter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.FormBody;
import okhttp3.RequestBody;


public class HewanActivity extends AppCompatActivity implements RequestComplete {
    PostRequest postRequest;
    PrefManager prefManager;
    RecyclerView recyclerView;
    ArrayList<Item> arrayList = new ArrayList<>();
    LinearLayout beliHewan;
    LinearLayout jualHewan;
    private boolean hasMore = false;
    private String next_page = "";
    private Boolean isLoadMore;
    private EditText searchBar;
    private Spinner kandang;
    private ArrayList<Integer> kandangsMap;
    private Integer kandang_id;
    private Spinner statusType;
    private ArrayList<String> status_options;
    private String status;
    private Button cari;
    private LinearLayout not_found;
    private TextView search_res_text;
    private NestedScrollView nestedScrollView;
    //public static Activity hewanActivity;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //hewanActivity = this;
        setContentView(R.layout.activity_list_hewan);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Daftar Ternak");
        prefManager = new PrefManager(this);

        beliHewan = findViewById(R.id.beli_hewan);
        jualHewan = findViewById(R.id.jual_hewan);
        searchBar = findViewById(R.id.searchBar);
        kandang = findViewById(R.id.kandang);
        statusType = findViewById(R.id.statusType);
        cari = findViewById(R.id.cari);
        not_found = findViewById(R.id.not_found);
        search_res_text = findViewById(R.id.search_res_text);

        status_options = new ArrayList<>();
        status_options.add("Semua Status");
        status_options.add("Mati");
        status_options.add("Hidup");
        status_options.add("Terjual");
        status_options.add("Terkirim");
        ArrayAdapter<String> statusAdapter = new ArrayAdapter<String>(this, R.layout.simple_spinner_item, status_options);
        statusType.setAdapter(statusAdapter);

        beliHewan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent a = new Intent(HewanActivity.this, BeliHewanActivity.class);
                startActivity(a);
            }
        });

        jualHewan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent a = new Intent(HewanActivity.this, JualHewanActivity.class);
                Bundle extras = new Bundle();
                a.putExtras(extras);
                startActivityForResult(a, 5000);
            }
        });

        //this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        isLoadMore = false;
        nestedScrollView = findViewById(R.id.nested1);
        recyclerView = findViewById(R.id.r_list_hewan);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setNestedScrollingEnabled(false);
        //recyclerView.setFocusable(false);

        nestedScrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                //Log.e("masuk AA", next_page);
                if (v.getChildAt(v.getChildCount() - 1) != null) {
                    //Log.e("masuk BB", next_page);
                    if ((scrollY >= (v.getChildAt(v.getChildCount() - 1).getMeasuredHeight() - v.getMeasuredHeight())) &&
                            scrollY > oldScrollY) {
                        //Log.e("masuk C1", String.valueOf(hasMore));
                        //Log.e("masuk C2", String.valueOf(!hasFooter()));
                        if (hasMore && (!hasFooter())) {
                            LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                            //position starts at 0
                            //Log.e("masuk C3", String.valueOf(layoutManager.findLastCompletelyVisibleItemPosition()));
                            //Log.e("masuk C4", String.valueOf(layoutManager.getItemCount()));
                            if (layoutManager.findLastCompletelyVisibleItemPosition() == layoutManager.getItemCount() - 1) {
                                //Log.e("masuk CC", next_page);
                        /*asyncTask = new BackgroundTask();
                        asyncTask.execute((Void[]) null);*/
                                arrayList.add(new Footer());
                                recyclerView.getAdapter().notifyDataSetChanged();
                                getData(next_page);
                            }
                        }
                    }
                }
            }
        });

        searchBar.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    arrayList = new ArrayList<>();
                    hasMore = false;
                    isLoadMore = false;
                    next_page = "";
                    recyclerView.getAdapter().notifyDataSetChanged();

                    getData(EndpointAPI.myAnimal);
                    return true;
                }
                return false;
            }
        });

        cari.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                arrayList = new ArrayList<>();
                hasMore = false;
                isLoadMore = false;
                next_page = "";
                recyclerView.getAdapter().notifyDataSetChanged();

                getData(EndpointAPI.myAnimal);
            }
        });

        getData(EndpointAPI.myAnimal);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 5000) {
            arrayList = new ArrayList<>();
            hasMore = false;
            isLoadMore = false;
            next_page = "";
            recyclerView.getAdapter().notifyDataSetChanged();
            getData(EndpointAPI.myAnimal);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    @Override
    public void onRequestComplete(JSONObject jsonObject, Integer REQUEST_CODE) {
        try {
            if (jsonObject.getString("response").equalsIgnoreCase("success")) {
                JSONObject result = jsonObject.getJSONObject("result");
                JSONArray data = result.getJSONArray("data");
                next_page = result.getString("next_page_url");
                ArrayList<Item> item = new ArrayList<>(data.length());
                for (int f = 0; f < data.length(); f++) {
                    JSONObject ix = data.getJSONObject(f);
                    item.add(new DataHewan(ix));
                }

                hasMore = !next_page.equalsIgnoreCase("null");
                if (isLoadMore) {
                    deleteItem(arrayList.size() - 1);
                    arrayList.addAll(item);
                    recyclerView.getAdapter().notifyDataSetChanged();
                } else {
                    arrayList = item;
                    isLoadMore = hasMore;
                    recyclerView.setAdapter(new HewanAdapter(arrayList, this));
                }

                if (arrayList.size() == 0) {
                    recyclerView.setVisibility(View.GONE);
                    not_found.setVisibility(View.VISIBLE);
                    search_res_text.setVisibility(View.GONE);
                } else {
                    recyclerView.setVisibility(View.VISIBLE);
                    not_found.setVisibility(View.GONE);
                    search_res_text.setText("Ditemukan " + JSONGetter.getString(result, "total", "0") + " hewan.");
                    search_res_text.setVisibility(View.VISIBLE);
                }

                ArrayList<String> kandangsLabel = new ArrayList<String>();
                kandangsLabel.add("Semua Kandang");

                kandangsMap = new ArrayList<Integer>();
                kandangsMap.add(0);

                JSONArray kandangs = result.getJSONArray("kandang");
                for (int f = 0; f < kandangs.length(); f++) {
                    JSONObject ix = kandangs.getJSONObject(f);
                    Integer id = ix.getInt("id");
                    String name = ix.getString("name");
                    kandangsMap.add(id);
                    kandangsLabel.add(name);
                }

                ArrayAdapter<String> kandangAdapter = new ArrayAdapter<String>(this, R.layout.simple_spinner_item, kandangsLabel);
                //dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                kandang.setAdapter(kandangAdapter);

                kandang.setSelection(kandangsMap.indexOf(kandang_id));
                if (!status.equalsIgnoreCase("0")) {
                    statusType.setSelection(status_options.indexOf(status));
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void deleteItem(int pos) {
        if (arrayList.get(pos) instanceof Footer) {
            arrayList.remove(pos);
            recyclerView.getAdapter().notifyDataSetChanged();
        }
    }

    private void getData(String url) {
        kandang_id = 0;
        try {
            kandang_id = kandangsMap.get(kandang.getSelectedItemPosition());
        } catch (Exception e) {
            e.printStackTrace();
        }

        Integer sel_stat = statusType.getSelectedItemPosition();
        status = sel_stat > 0 ? status_options.get(sel_stat).toLowerCase() : "0";

        RequestBody data = new FormBody.Builder()
                .add("q", searchBar.getText().toString())
                .add("kandang", String.valueOf(kandang_id))
                .add("status", status)
                .build();

        postRequest = new PostRequest(HewanActivity.this);
        postRequest.SendPostData(url, prefManager.getToken(), data, 1);
        postRequest.execute();
    }

    private boolean hasFooter() {
        return arrayList.get(arrayList.size() - 1) instanceof Footer;
    }
}
