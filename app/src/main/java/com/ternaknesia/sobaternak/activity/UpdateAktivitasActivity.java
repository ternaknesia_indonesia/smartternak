package com.ternaknesia.sobaternak.activity;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.ternaknesia.sobaternak.Connection.PostRequest;
import com.ternaknesia.sobaternak.Interface.EndpointAPI;
import com.ternaknesia.sobaternak.Interface.RequestComplete;
import com.ternaknesia.sobaternak.Manager.PrefManager;
import com.ternaknesia.sobaternak.R;
import com.ternaknesia.sobaternak.Utils.CameraUtils;
import com.ternaknesia.sobaternak.Utils.ImageConverter;
import com.ternaknesia.sobaternak.Utils.ImageFilePath;
import com.ternaknesia.sobaternak.Utils.ImageZoomView.PhotoView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;


public class UpdateAktivitasActivity extends AppCompatActivity implements RequestComplete {
    private RelativeLayout pilih_bukti;
    private ImageView foto_bukti;
    private static int PERMISSION_ALL = 1;
    private EditText caption;
    private Button update;

    private PostRequest postRequest;
    private PrefManager prefManager;
    private String[] PERMISSIONS = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA};
    private String pathImg = "";
    private static final int CAMERA_REQUEST_CODE = 0;
    private static final int GALLERY_REQUEST_CODE = 1;
    private Uri FileUriOutput = null;//Uri to capture image
    private AlertDialog dialog;
    private File fileCompressed = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_update_act);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Update Aktivitas");

        init();
        prefManager = new PrefManager(this);

        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                RequestBody requestBody = new MultipartBody.Builder()
                        .setType(MultipartBody.FORM)
                        .addFormDataPart("image", "image_cap",
                                RequestBody.create(MediaType.parse("image/jpg"), fileCompressed))
                        .addFormDataPart("caption", caption.getText().toString())
                        .build();

                postRequest = new PostRequest(UpdateAktivitasActivity.this);
                postRequest.SendPostData(EndpointAPI.updateAct, prefManager.getToken(), requestBody, 1);
                postRequest.execute();

            }
        });

        pilih_bukti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                File f = new File(Environment.getExternalStorageDirectory().toString(), "SmartTernak/Image/cache.jpg");
                if (f.exists())
                    if (f.delete()) Log.e("success", "ok");

                AlertDialog.Builder builder = new AlertDialog.Builder(UpdateAktivitasActivity.this);
                builder.setMessage("Pilih foto")
                        .setCancelable(true)
                        .setNegativeButton("KAMERA", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                                if (!hasPermissions(UpdateAktivitasActivity.this, PERMISSIONS)) {
                                    ActivityCompat.requestPermissions(UpdateAktivitasActivity.this, PERMISSIONS, PERMISSION_ALL);
                                } else {
                                    StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
                                    StrictMode.setVmPolicy(builder.build());
                                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);//Start intent with Action_Image_Capture
                                    FileUriOutput = CameraUtils.getOutputMediaFileUri(UpdateAktivitasActivity.this);//get FileUriOutput from CameraUtils
                                    intent.putExtra(MediaStore.EXTRA_OUTPUT, FileUriOutput);//Send FileUriOutput with intent
                                    startActivityForResult(intent, CAMERA_REQUEST_CODE);//start activity for result with CAMERA_REQUEST_CODE
                                }
                            }
                        })
                        .setPositiveButton("GALERI", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                                if (!hasPermissions(UpdateAktivitasActivity.this, PERMISSIONS)) {
                                    ActivityCompat.requestPermissions(UpdateAktivitasActivity.this, PERMISSIONS, PERMISSION_ALL);
                                } else {

                                    Intent pickPhoto = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                    startActivityForResult(pickPhoto, GALLERY_REQUEST_CODE);//one can be replaced with any action code
                                }
                            }
                        });
                dialog = builder.create();
                dialog.show();
            }
        });


        pilih_bukti.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                AlertDialog.Builder imageDialog = new AlertDialog.Builder(UpdateAktivitasActivity.this);
                LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);

                assert inflater != null;
                View layout = inflater.inflate(R.layout.popup, null);

                PhotoView image = layout.findViewById(R.id.imageView);

                Glide.with(getBaseContext())
                        .load(pathImg)
                        .into(image);

                imageDialog.setView(layout);
                imageDialog.create();
                imageDialog.show();
                return false;
            }
        });

    }

    private void init() {
        caption = findViewById(R.id.caption);
        pilih_bukti = findViewById(R.id.pilih_bukti_bayar);
        foto_bukti = findViewById(R.id.foto_bukti);
        update = findViewById(R.id.btn_post);
    }

    private static boolean hasPermissions(Context context, String... permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK && (data != null || FileUriOutput != null)) {
            File a = null;
            long capacity = 0;

            if (requestCode == CAMERA_REQUEST_CODE) {
                a = new File(FileUriOutput.getPath());
                //String realPath = ImageFilePath.getPath(InvestDetailOrderActivity.this, data.getData());
                //a = new File(realPath);
            } else if (requestCode == GALLERY_REQUEST_CODE) {
                String realPath = ImageFilePath.getPath(UpdateAktivitasActivity.this, data.getData());
                a = new File(realPath);
            }

            if (a != null) {

                pathImg = a.getAbsolutePath();
                Glide.with(getBaseContext())
                        .load(a.getAbsolutePath())
                        .asBitmap()
                        .centerCrop()
                        .into(new BitmapImageViewTarget(foto_bukti) {
                            @Override
                            protected void setResource(Bitmap resource) {
                                RoundedBitmapDrawable rounded =
                                        RoundedBitmapDrawableFactory.create(getResources(), resource);
                                rounded.setCircular(true);
                                Bitmap b = ImageConverter.getRoundedCornerBitmap(resource, 10);
                                foto_bukti.setImageBitmap(b);
                            }
                        });

                try {
                    long length = a.length();
                    length = length / 1024;
                    capacity = length;
                } catch (Exception ignored) {

                }

                BitmapFactory.Options bmOptions = new BitmapFactory.Options();
                Bitmap bitmap = BitmapFactory.decodeFile(a.getAbsolutePath(), bmOptions);
                bitmap = resize(bitmap, 800, 800);

                ///Bitmap thumbnail = BitmapFactory.decodeFile(imagePath);
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                assert bitmap != null;
                int percentage = 95 - (((int) capacity / 1000) * 5);
                bitmap.compress(Bitmap.CompressFormat.JPEG, percentage, bytes);
                fileCompressed = saveCompressed(bytes);

                //Log.e("cek0", "File saved");
                //cropped = bitmap;
                //ktp.setImageBitmap(bitmap);
            } else {
                //Log.e("cek1", "File is null");
            }
        } else {
            //Log.e("cek2", "File Uri is null");
        }
    }

    @Override
    public void onRequestComplete(JSONObject jsonObject, Integer REQUEST_CODE) {
        try {
            if (jsonObject.getString("response").equalsIgnoreCase("success")) {
                Toast.makeText(this, "Berhasil menambah transaksi", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, "Gagal menambah transaksi", Toast.LENGTH_SHORT).show();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static File saveCompressed(ByteArrayOutputStream bmpB) {
        File mediaStorageDir = new File(Environment.getExternalStorageDirectory().toString(), "SmartTernak");
        File mediaStorageDir1 = new File(Environment.getExternalStorageDirectory().toString(), "SmartTernak/Image");
        //If File is not present create directory
        if (!mediaStorageDir.exists()) {
            if (mediaStorageDir.mkdir())
                if (!mediaStorageDir1.exists()) {
                    if (mediaStorageDir1.mkdir())
                        Log.e("Success", "Ok");
                }
        } else {
            if (!mediaStorageDir1.exists()) {
                if (mediaStorageDir1.mkdir())
                    Log.e("Success", "Ok");
            }
        }

        File f = new File(Environment.getExternalStorageDirectory().toString(), "SmartTernak/Image" + File.separator + "cache.jpg");

        byte[] bitmapdata = bmpB.toByteArray();
        FileOutputStream fos;
        try {
            if (f.createNewFile()) {
                fos = new FileOutputStream(f, false);
                fos.write(bitmapdata);
                fos.flush();
                fos.close();
            }
        } catch (IOException e) {
            //e.printStackTrace();
        }
        return f;
    }

    private static Bitmap resize(Bitmap image, int maxWidth, int maxHeight) {
        if (maxHeight > 0 && maxWidth > 0) {
            int width = image.getWidth();
            int height = image.getHeight();
            float ratioBitmap = (float) width / (float) height;
            float ratioMax = (float) maxWidth / (float) maxHeight;

            int finalWidth = maxWidth;
            int finalHeight = maxHeight;
            if (ratioMax > ratioBitmap) {
                finalWidth = (int) ((float) maxHeight * ratioBitmap);
            } else {
                finalHeight = (int) ((float) maxWidth / ratioBitmap);
            }
            image = Bitmap.createScaledBitmap(image, finalWidth, finalHeight, true);
            return image;
        } else {
            return image;
        }
    }
}
