package com.ternaknesia.sobaternak.activity;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.ternaknesia.sobaternak.Connection.PostRequest;
import com.ternaknesia.sobaternak.Interface.EndpointAPI;
import com.ternaknesia.sobaternak.Interface.RequestComplete;
import com.ternaknesia.sobaternak.Manager.PrefManager;
import com.ternaknesia.sobaternak.R;
import com.ternaknesia.sobaternak.Utils.CameraUtils;
import com.ternaknesia.sobaternak.Utils.DateFormatter;
import com.ternaknesia.sobaternak.Utils.ImageConverter;
import com.ternaknesia.sobaternak.Utils.ImageFilePath;
import com.ternaknesia.sobaternak.Utils.ImageZoomView.PhotoView;
import com.ternaknesia.sobaternak.Utils.JSONGetter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;


public class EditHewanActivity extends AppCompatActivity implements RequestComplete {
    private static final int DATE_DIALOG_ID = 0;
    private PostRequest postRequest;
    private PostRequest postRequestKandang;
    private PrefManager prefManager;
    private RelativeLayout pilih_foto_hewan;
    private ImageView foto_hewan;
    private static int PERMISSION_ALL = 1;
    private String[] PERMISSIONS = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA};
    private static final int CAMERA_REQUEST_CODE = 0;
    private static final int GALLERY_REQUEST_CODE = 1;
    private Uri FileUriOutput = null;//Uri to capture image
    private String pathImg = "";
    private Integer id;
    private Integer instance_id;
    private TextView instance;
    private Integer project_id;
    private Spinner project;
    private Integer kandang_id;
    private Spinner kandang;
    private Spinner statusType;
    private Integer animal_type_id;
    private Integer animal_count;
    private Spinner animalType;
    private String jenis_kelamin;
    private Spinner genderType;
    private LinearLayout pemesan_cont;
    private EditText pemesan;
    private TextView code_tag;
    private EditText buy_date;
    private EditText harvest_date;
    private EditText harvest_age;
    private EditText weight;
    private EditText height;
    private EditText buy_price_unit;
    private EditText buy_price_kg;
    private LinearLayout sell_price_unit_cont;
    private EditText sell_price_unit;
    private LinearLayout sell_price_kg_cont;
    private EditText sell_price_kg;
    private String status;
    private AlertDialog dialog;
    private Button tambah;
    private Button previous;
    private int pYear;
    private int pMonth;
    private int pDay;
    private File fileCompressed = null;
    private ArrayList<Integer> instancesMap;
    private ArrayList<Integer> projectsMap;
    private ArrayList<Integer> kandangsMap;
    private Integer index_animals;
    private String animal_string;
    private JSONArray animals;
    private TextView title;
    private Boolean is_batch = false;
    private Boolean is_update = false;
    private ArrayList<String> animal_options;
    private ArrayList<String> status_options;
    private ArrayList<String> gender_options;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_add_hewan);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Data Hewan");

        init();

        animal_options = new ArrayList<>();
        animal_options.add("Pilih Jenis Hewan");
        animal_options.add("Kambing");
        animal_options.add("Sapi");
        animal_options.add("Domba");
        ArrayAdapter<String> animalAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, animal_options);
        animalType.setAdapter(animalAdapter);

        status_options = new ArrayList<>();
        status_options.add("Mati");
        status_options.add("Hidup");
        status_options.add("Terjual");
        status_options.add("Terkirim");
        ArrayAdapter<String> statusAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, status_options);
        statusType.setAdapter(statusAdapter);

        gender_options = new ArrayList<>();
        gender_options.add("Pilih Jenis Kelamin Hewan");
        gender_options.add("Jantan");
        gender_options.add("Betina");
        ArrayAdapter<String> genderAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, gender_options);
        genderType.setAdapter(genderAdapter);

        prefManager = new PrefManager(this);
        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if (extras == null) {
                instance_id = null;
            } else {
                instance_id = extras.getInt("instance_id");
                try {
                    id = extras.getInt("id");
                    if(id != null && id != 0) {
                        title.setText("Ubah data hewan");
                        index_animals = 0;
                        is_batch = false;
                        is_update = true;
                    } else {
                        index_animals = extras.getInt("index_animals");
                        animal_string = extras.getString("animals");
                        try {
                            animals = new JSONArray(animal_string);
                            is_batch = true;
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                } catch (NullPointerException e) {
                    e.printStackTrace();
                    title.setText("Tambah data hewan baru");
                    is_batch = false;
                }
            }

        } else {
            instance_id = (Integer) savedInstanceState.getSerializable("instance_id");
        }

        RequestBody data = new FormBody.Builder()
                .add("id", String.valueOf(id))
                .add("instance_id", String.valueOf(instance_id))
                .build();

        postRequestKandang = new PostRequest(EditHewanActivity.this);
        postRequestKandang.SendPostData(EndpointAPI.getAddHewan, prefManager.getToken(), data, 1);
        postRequestKandang.execute();

        tambah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            if (!code_tag.getText().toString().equalsIgnoreCase("")
                    && !buy_date.getText().toString().equalsIgnoreCase("")) {

                project_id = projectsMap.get(project.getSelectedItemPosition());
                animal_type_id = animalType.getSelectedItemPosition();
                kandang_id = kandangsMap.get(kandang.getSelectedItemPosition());
                String buy = "";
                try {
                    buy = new DateFormatter().convertDate(buy_date.getText().toString(), "dd/MM/yyyy", "yyyy-MM-dd");
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }

                String harvest = harvest_date.getText().toString();
                if(harvest != "") {
                    try {
                        harvest = new DateFormatter().convertDate(harvest_date.getText().toString(), "dd/MM/yyyy", "yyyy-MM-dd");
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    }
                }

                jenis_kelamin = genderType.getSelectedItemPosition() != 0 ? genderType.getSelectedItem().toString().toLowerCase() : "";
                status = status_options.get(statusType.getSelectedItemPosition()).toLowerCase();

                RequestBody data;
                if(fileCompressed != null) {
                    data = new MultipartBody.Builder()
                            .setType(MultipartBody.FORM)
                            .addFormDataPart("index_animals", String.valueOf(index_animals))
                            .addFormDataPart("id", String.valueOf(id))
                            .addFormDataPart("instance_id", String.valueOf(instance_id))
                            .addFormDataPart("project_id", String.valueOf(project_id))
                            .addFormDataPart("animal_type_id", String.valueOf(animal_type_id))
                            .addFormDataPart("jenis_kelamin", jenis_kelamin)
                            .addFormDataPart("kandang_id", String.valueOf(kandang_id))
                            .addFormDataPart("code_tag", code_tag.getText().toString())
                            .addFormDataPart("pemesan", pemesan.getText().toString())
                            .addFormDataPart("buy_date", buy)
                            .addFormDataPart("harvest_date", harvest)
                            .addFormDataPart("harvest_age", harvest_age.getText().toString())
                            .addFormDataPart("weight", weight.getText().toString())
                            .addFormDataPart("height", height.getText().toString())
                            .addFormDataPart("buy_price_unit", buy_price_unit.getText().toString())
                            .addFormDataPart("buy_price_kg", buy_price_kg.getText().toString())
                            .addFormDataPart("sell_price_unit", sell_price_unit.getText().toString())
                            .addFormDataPart("sell_price_kg", sell_price_kg.getText().toString())
                            .addFormDataPart("status", status)
                            .addFormDataPart("image", "image_cap",
                                    RequestBody.create(MediaType.parse("image/jpg"), fileCompressed))
                            .build();
                } else {
                    data = new MultipartBody.Builder()
                            .setType(MultipartBody.FORM)
                            .addFormDataPart("index_animals", String.valueOf(index_animals))
                            .addFormDataPart("id", String.valueOf(id))
                            .addFormDataPart("instance_id", String.valueOf(instance_id))
                            .addFormDataPart("project_id", String.valueOf(project_id))
                            .addFormDataPart("animal_type_id", String.valueOf(animal_type_id))
                            .addFormDataPart("jenis_kelamin", jenis_kelamin)
                            .addFormDataPart("kandang_id", String.valueOf(kandang_id))
                            .addFormDataPart("code_tag", code_tag.getText().toString())
                            .addFormDataPart("pemesan", pemesan.getText().toString())
                            .addFormDataPart("buy_date", buy)
                            .addFormDataPart("harvest_date", harvest)
                            .addFormDataPart("harvest_age", harvest_age.getText().toString())
                            .addFormDataPart("weight", weight.getText().toString())
                            .addFormDataPart("height", height.getText().toString())
                            .addFormDataPart("buy_price_unit", buy_price_unit.getText().toString())
                            .addFormDataPart("buy_price_kg", buy_price_kg.getText().toString())
                            .addFormDataPart("sell_price_unit", sell_price_unit.getText().toString())
                            .addFormDataPart("sell_price_kg", sell_price_kg.getText().toString())
                            .addFormDataPart("status", status)
                            .build();
                }

                postRequest = new PostRequest(EditHewanActivity.this);
                postRequest.SendPostData(EndpointAPI.updateAnimal, prefManager.getToken(), data, 2);
                postRequest.execute();
            } else {
                Toast.makeText(getBaseContext(), "Mohon melengkapi semua isian.", Toast.LENGTH_SHORT).show();
            }
            }
        });

        previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(index_animals > 0) {
                    index_animals--;
                    setAnimalForm();
                }
            }
        });

        pilih_foto_hewan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                File f = new File(Environment.getExternalStorageDirectory().toString(), "SmartTernak/Image/cache.jpg");
                if (f.exists())
                    if (f.delete()) Log.e("success", "ok");

                AlertDialog.Builder builder = new AlertDialog.Builder(EditHewanActivity.this);
                builder.setMessage("Pilih foto")
                        .setCancelable(true)
                        .setNegativeButton("KAMERA", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                                if (!hasPermissions(EditHewanActivity.this, PERMISSIONS)) {
                                    ActivityCompat.requestPermissions(EditHewanActivity.this, PERMISSIONS, PERMISSION_ALL);
                                } else {
                                    StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
                                    StrictMode.setVmPolicy(builder.build());
                                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);//Start intent with Action_Image_Capture
                                    FileUriOutput = CameraUtils.getOutputMediaFileUri(EditHewanActivity.this);//get FileUriOutput from CameraUtils
                                    intent.putExtra(MediaStore.EXTRA_OUTPUT, FileUriOutput);//Send FileUriOutput with intent
                                    startActivityForResult(intent, CAMERA_REQUEST_CODE);//start activity for result with CAMERA_REQUEST_CODE
                                }
                            }
                        })
                        .setPositiveButton("GALERI", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                                if (!hasPermissions(EditHewanActivity.this, PERMISSIONS)) {
                                    ActivityCompat.requestPermissions(EditHewanActivity.this, PERMISSIONS, PERMISSION_ALL);
                                } else {

                                    Intent pickPhoto = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                    startActivityForResult(pickPhoto, GALLERY_REQUEST_CODE);//one can be replaced with any action code
                                }
                            }
                        });
                dialog = builder.create();
                dialog.show();
            }
        });


        pilih_foto_hewan.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                AlertDialog.Builder imageDialog = new AlertDialog.Builder(EditHewanActivity.this);
                LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);

                assert inflater != null;
                View layout = inflater.inflate(R.layout.popup, null);

                PhotoView image = layout.findViewById(R.id.imageView);

                Glide.with(getBaseContext())
                        .load(pathImg)
                        .into(image);

                imageDialog.setView(layout);
                imageDialog.create();
                imageDialog.show();
                return false;
            }
        });

        weight.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                changeText(true, false);
                changeText(false, false);
                return false;
            }
        });

        buy_price_unit.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                changeText(true, false);
                return false;
            }
        });

        buy_price_kg.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                changeText(true, true);
                return false;
            }
        });

        sell_price_unit.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                changeText(false, false);
                return false;
            }
        });

        sell_price_kg.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                changeText(false, true);
                return false;
            }
        });
    }

    private void changeText(boolean is_buy, boolean is_kg) {
        Integer w = getInteger(weight);
        Integer unit = is_buy ? getInteger(buy_price_unit) : getInteger(sell_price_unit);
        Integer kg = is_buy ? getInteger(buy_price_kg) : getInteger(sell_price_kg);

        if (w > 0) {
            if ((unit > 0 && kg > 0) || (unit > 0 && kg == 0) && !is_kg) {
                kg = unit / w;
                if (is_buy) {
                    buy_price_kg.setText(String.valueOf(kg));
                } else {
                    sell_price_kg.setText(String.valueOf(kg));
                }
            } else if (unit == 0 && kg > 0 || is_kg) {
                unit = kg * w;
                if(is_buy) {
                    buy_price_unit.setText(String.valueOf(unit));
                } else {
                    sell_price_unit.setText(String.valueOf(unit));
                }
            }
        }
    }

    private Integer getInteger(EditText source) {
        try {
            return Integer.parseInt(source.getText().toString());
        } catch (Exception e) {
            return 0;
        }
    }

    private int indexOf(String str, ArrayList<String> list) {
        if (str != null) {
            for (int i = 0; i < list.size(); i++) {
                if (str.equalsIgnoreCase(list.get(i))) {
                    return i;
                }
            }
        }
        return 0;
    }

    private void setAnimalForm() {
        try {
            if(animals.length() > 1) {
                is_batch = true;
            }
            JSONObject animal = animals.getJSONObject(index_animals);
            id = JSONGetter.getInt(animal, "id", null);
            instance_id = JSONGetter.getInt(animal, "instance_id", null);
            instance.setText(JSONGetter.getString(animal,"instance_label", ""));
            project.setSelection(projectsMap.indexOf(JSONGetter.getInt(animal, "project_id", 0)));
            animalType.setSelection(JSONGetter.getInt(animal, "animal_type_id", 0));
            jenis_kelamin = JSONGetter.getString(animal,"jenis_kelamin", "");
            genderType.setSelection(indexOf(jenis_kelamin, gender_options));
            kandang.setSelection(kandangsMap.indexOf(JSONGetter.getInt(animal, "kandang_id", 0)));
            status = JSONGetter.getString(animal,"status", "");
            statusType.setSelection(indexOf(status, status_options));
            code_tag.setText("");
            code_tag.append(JSONGetter.getString(animal,"code_tag", ""));
            pemesan.setText(JSONGetter.getString(animal,"pemesan", ""));
            buy_date.setText(JSONGetter.getString(animal, "buy_date", ""));
            harvest_date.setText(JSONGetter.getString(animal, "harvest_date", ""));
            Integer ha_val = JSONGetter.getInt(animal, "harvest_age", 0);
            if (ha_val > 0) {
                harvest_age.setText(String.valueOf(ha_val));
            } else {
                harvest_age.setText("");
            }
            buy_price_unit.setText(JSONGetter.getString(animal, "buy_price_unit", ""));
            buy_price_kg.setText(JSONGetter.getString(animal, "buy_price_kg", ""));
            sell_price_unit.setText(JSONGetter.getString(animal, "sell_price_unit", ""));
            sell_price_kg.setText(JSONGetter.getString(animal, "sell_price_kg", ""));
            Integer height_val = JSONGetter.getInt(animal, "height", 0);
            if (height_val > 0) {
                height.setText(String.valueOf(height_val));
            } else {
                height.setText("");
            }
            Integer weight_val = JSONGetter.getInt(animal, "weight", 0);
            if (weight_val > 0) {
                weight.setText(String.valueOf(weight_val));
            } else {
                weight.setText("");
            }
            code_tag.requestFocus();
            if(is_batch) {
                pemesan_cont.setVisibility(View.GONE);
                sell_price_unit_cont.setVisibility(View.GONE);
                sell_price_kg_cont.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            is_batch = false;
        }
        if(index_animals > 0) {
            previous.setEnabled(true);
            previous.setBackground(getResources().getDrawable(R.color.red_x));
        } else {
            previous.setEnabled(false);
            previous.setBackground(getResources().getDrawable(R.color.gray400));
        }

        if(is_batch == false) {
            previous.setVisibility(View.GONE);
        } else {
            title.setText("Hewan " + (index_animals + 1) + " dari " + animals.length() + " hewan baru");
            project.setEnabled(false);
            animalType.setEnabled(false);
            statusType.setEnabled(false);
            buy_date.setEnabled(false);
            previous.setVisibility(View.VISIBLE);
        }
    }

    private void GetDate() {
        final Calendar c = Calendar.getInstance();
        pYear = c.get(Calendar.YEAR);
        pMonth = c.get(Calendar.MONTH);
        pDay = c.get(Calendar.DAY_OF_MONTH);
        showDialog(DATE_DIALOG_ID);
    }

    private String generateTag() {
        return (10000 + instance_id) + "." + (1000 + animal_count + index_animals);
    }

    public Dialog onCreateDialog(int id) {
        switch (id) {
            case DATE_DIALOG_ID:
                DatePickerDialog.OnDateSetListener pDateSetListener = new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        pYear = year;
                        pMonth = monthOfYear;
                        pDay = dayOfMonth;
                        //__date_of = String.valueOf(new StringBuilder().append(pDay).append("-").append(pMonth + 1).append("-").append(pYear));
                        //setDate(new DateFormatter().newDateFrom(__date_of, "dd-MM-yyyy"));
                    }
                };
                DatePickerDialog dialog = new DatePickerDialog(this, pDateSetListener, pYear, pMonth, pDay);

                //Date newDate = new DateFormatter().newDateFrom(qurban_date, "yyyy-MM-dd HH:mm:ss");
                Date newDate = Calendar.getInstance().getTime();

                /*if (newDate != null) {
                    dialog.getDatePicker().setMinDate(newDate.getTime() - (1000 * 60 * 60 * 24 * day_min));
                    dialog.getDatePicker().setMaxDate(newDate.getTime() + (1000 * 60 * 60 * 24 * day_max));
                }*/

                if (newDate != null) {
                    dialog.getDatePicker().updateDate(newDate.getYear(), newDate.getMonth(), newDate.getDay());
                }

                return dialog;
            default:
                return null;
        }
    }

    private void init() {
        title = findViewById(R.id.title);
        instance = findViewById(R.id.instance);
        project = findViewById(R.id.project);
        animalType = findViewById(R.id.animalType);
        genderType = findViewById(R.id.genderType);
        kandang = findViewById(R.id.kandang);
        code_tag = findViewById(R.id.code_tag);
        pemesan_cont = findViewById(R.id.pemesan_cont);
        pemesan = findViewById(R.id.pemesan);
        buy_date = findViewById(R.id.buy_date);
        harvest_date = findViewById(R.id.harvest_date);
        harvest_age = findViewById(R.id.harvest_age);
        buy_price_unit = findViewById(R.id.buy_price_unit);
        buy_price_kg = findViewById(R.id.buy_price_kg);
        sell_price_unit_cont = findViewById(R.id.sell_price_unit_cont);
        sell_price_unit = findViewById(R.id.sell_price_unit);
        sell_price_kg_cont = findViewById(R.id.sell_price_kg_cont);
        sell_price_kg = findViewById(R.id.sell_price_kg);
        weight = findViewById(R.id.weight);
        height = findViewById(R.id.height);
        statusType = findViewById(R.id.statusType);
        pilih_foto_hewan = findViewById(R.id.pilih_foto_hewan);
        foto_hewan = findViewById(R.id.foto_hewan);
        tambah = findViewById(R.id.btn_tambah);
        previous = findViewById(R.id.btn_previous);
    }

    public static boolean hasPermissions(Context context, String... permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK && (data != null || FileUriOutput != null)) {
            File a = null;
            long capacity = 0;

            if (requestCode == CAMERA_REQUEST_CODE) {
                a = new File(FileUriOutput.getPath());
                //String realPath = ImageFilePath.getPath(InvestDetailOrderActivity.this, data.getData());
                //a = new File(realPath);
            } else if (requestCode == GALLERY_REQUEST_CODE) {
                String realPath = ImageFilePath.getPath(EditHewanActivity.this, data.getData());
                a = new File(realPath);
            }

            if (a != null) {

                pathImg = a.getAbsolutePath();
                Glide.with(getBaseContext())
                        .load(a.getAbsolutePath())
                        .asBitmap()
                        .centerCrop()
                        .into(new BitmapImageViewTarget(foto_hewan) {
                            @Override
                            protected void setResource(Bitmap resource) {
                                RoundedBitmapDrawable rounded =
                                        RoundedBitmapDrawableFactory.create(getResources(), resource);
                                rounded.setCircular(true);
                                Bitmap b = ImageConverter.getRoundedCornerBitmap(resource, 10);
                                foto_hewan.setImageBitmap(b);
                            }
                        });

                try {
                    long length = a.length();
                    length = length / 1024;
                    capacity = length;
                } catch (Exception ignored) {

                }

                BitmapFactory.Options bmOptions = new BitmapFactory.Options();
                Bitmap bitmap = BitmapFactory.decodeFile(a.getAbsolutePath(), bmOptions);
                bitmap = resize(bitmap, 800, 800);

                ///Bitmap thumbnail = BitmapFactory.decodeFile(imagePath);
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                assert bitmap != null;
                int percentage = 95 - (((int) capacity / 1000) * 5);
                bitmap.compress(Bitmap.CompressFormat.JPEG, percentage, bytes);
                fileCompressed = saveCompressed(bytes);

                //Log.e("cek0", "File saved");
                //cropped = bitmap;
                //ktp.setImageBitmap(bitmap);
            } else {
                //Log.e("cek1", "File is null");
            }
        } else {
            //Log.e("cek2", "File Uri is null");
        }
    }

    @Override
    public void onRequestComplete(JSONObject jsonObject, Integer REQUEST_CODE) {
        if (REQUEST_CODE == 2) {
            try {
                if (jsonObject.getString("response").equalsIgnoreCase("success")) {
                    if(!is_update) {
                        Toast.makeText(this, "Berhasil menambah transaksi", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(this, "Data berhasil diubah", Toast.LENGTH_SHORT).show();
                    }

                    if(!is_batch) {
                        finish();
                        //HewanActivity.hewanActivity.recreate();
                        setResult(5000);
                    } else {
                        JSONArray result = jsonObject.getJSONArray("result");
                        JSONObject data = result.getJSONObject(0);
                        Integer idx = data.getInt("index_animals");
                        animals.put(idx, data);

                        animal_string = animals.toString();
                        if(index_animals < animals.length() - 1) {
                            index_animals++;
                            setAnimalForm();
                        } else {
                            finish();
                            //HewanActivity.hewanActivity.recreate();
                            setResult(5000);
                        }
                    }
                } else {
                    Toast.makeText(this, "Gagal menambah transaksi", Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if(REQUEST_CODE == 1) {
            try {
                ArrayList<String> kandangsLabel = new ArrayList<String>();
                kandangsLabel.add("Pilih Kandang");

                ArrayList<String> projectsLabel = new ArrayList<String>();
                projectsLabel.add("Pilih Project");

                ArrayList<String> instancesLabel = new ArrayList<String>();
                instancesLabel.add("Pilih Peternak");

                kandangsMap = new ArrayList<Integer>();
                kandangsMap.add(0);

                projectsMap = new ArrayList<Integer>();
                projectsMap.add(0);

                if (jsonObject.getString("response").equalsIgnoreCase("success")) {
                    JSONObject result = jsonObject.getJSONObject("result");
                    JSONArray kandangs = result.getJSONArray("kandang");
                    JSONArray projects = result.getJSONArray("projects");
                    if(!is_batch) {
                        animals = result.getJSONArray("animal");
                    }
                    for (int f = 0; f < kandangs.length(); f++){
                        JSONObject ix = kandangs.getJSONObject(f);
                        Integer id = ix.getInt("id");
                        String name = ix.getString("name");
                        kandangsMap.add(id);
                        kandangsLabel.add(name);
                    }

                    for (int f = 0; f < projects.length(); f++){
                        JSONObject ix = projects.getJSONObject(f);
                        Integer id = ix.getInt("id");
                        String name = ix.getString("name");
                        projectsMap.add(id);
                        projectsLabel.add(name);
                    }

                    ArrayAdapter<String> kandangAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, kandangsLabel);
                    kandang.setAdapter(kandangAdapter);

                    ArrayAdapter<String> projectAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, projectsLabel);
                    project.setAdapter(projectAdapter);

                    setAnimalForm();
                } else {
                    Toast.makeText(this, "Terjadi kesalahan server", Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public static File saveCompressed(ByteArrayOutputStream bmpB) {
        File mediaStorageDir = new File(Environment.getExternalStorageDirectory().toString(), "SmartTernak");
        File mediaStorageDir1 = new File(Environment.getExternalStorageDirectory().toString(), "SmartTernak/Image");
        //If File is not present create directory
        if (!mediaStorageDir.exists()) {
            if (mediaStorageDir.mkdir())
                if (!mediaStorageDir1.exists()) {
                    if (mediaStorageDir1.mkdir())
                        Log.e("Success", "Ok");
                }
        } else {
            if (!mediaStorageDir1.exists()) {
                if (mediaStorageDir1.mkdir())
                    Log.e("Success", "Ok");
            }
        }

        File f = new File(Environment.getExternalStorageDirectory().toString(), "SmartTernak/Image" + File.separator + "cache.jpg");

        byte[] bitmapdata = bmpB.toByteArray();
        FileOutputStream fos;
        try {
            if (f.createNewFile()) {
                fos = new FileOutputStream(f, false);
                fos.write(bitmapdata);
                fos.flush();
                fos.close();
            }
        } catch (IOException e) {
            //e.printStackTrace();
        }
        return f;
    }

    private static Bitmap resize(Bitmap image, int maxWidth, int maxHeight) {
        if (maxHeight > 0 && maxWidth > 0) {
            int width = image.getWidth();
            int height = image.getHeight();
            float ratioBitmap = (float) width / (float) height;
            float ratioMax = (float) maxWidth / (float) maxHeight;

            int finalWidth = maxWidth;
            int finalHeight = maxHeight;
            if (ratioMax > ratioBitmap) {
                finalWidth = (int) ((float) maxHeight * ratioBitmap);
            } else {
                finalHeight = (int) ((float) maxWidth / ratioBitmap);
            }
            image = Bitmap.createScaledBitmap(image, finalWidth, finalHeight, true);
            return image;
        } else {
            return image;
        }
    }
}
