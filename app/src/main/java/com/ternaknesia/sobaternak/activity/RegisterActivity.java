package com.ternaknesia.sobaternak.activity;
/*        Mohon untuk tidak melakukan hal-hal yang melanggar hukum.
        Anda tidak berhak melakukan cracking terhadap system kami tanpa ada persetujuan.
        Terimakasih*/

import android.annotation.TargetApi;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;

import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.ternaknesia.sobaternak.Connection.PostRequest;
import com.ternaknesia.sobaternak.Interface.EndpointAPI;
import com.ternaknesia.sobaternak.Interface.RequestComplete;
import com.ternaknesia.sobaternak.Manager.PrefManager;
import com.ternaknesia.sobaternak.R;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.FormBody;
import okhttp3.RequestBody;

/**
 * A login screen that offers login via email/password.
 */
public class RegisterActivity extends AppCompatActivity implements RequestComplete {

    private final String TAG = RegisterActivity.class.getSimpleName();

    private EditText mEmailView;
    private EditText mPasswordView;
    private EditText mCPasswordView;
    private EditText mName;
    private EditText mTelp;

    Button btnRegister, btnLogin;
    PrefManager prefManager;
    PostRequest postRequest, postRequest1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        prefManager = new PrefManager(this);

        init();

        btnLogin.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent d = new Intent(RegisterActivity.this, LoginActivity.class);
                startActivity(d);
                finish();
            }
        });


        btnRegister.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                btnRegister.setEnabled(false);
                btnLogin.setEnabled(false);
                attemptLogin();
            }
        });

    }

    private void init() {
        mEmailView = findViewById(R.id.email);
        mName = findViewById(R.id.name);
        mPasswordView = findViewById(R.id.password);
        mCPasswordView = findViewById(R.id.repeat_password);
        mTelp = findViewById(R.id.phone);
        btnRegister = findViewById(R.id.btn_register);
        btnLogin = findViewById(R.id.btn_login);
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }


    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }


    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptLogin() {
        // Reset errors.
        mEmailView.setError(null);
        mPasswordView.setError(null);
        mCPasswordView.setError(null);
        mName.setError(null);
        mTelp.setError(null);

        // Store values at the time of the login attempt.
        String email = mEmailView.getText().toString();
        String password = mPasswordView.getText().toString();
        String cpassword = mCPasswordView.getText().toString();
        String name = mName.getText().toString();
        String telp = mTelp.getText().toString();


        boolean cancel = false;
        View focusView = null;

        if (TextUtils.isEmpty(name)) {
            mName.setError(getString(R.string.error_field_required));
            focusView = mName;
            cancel = true;
            btnRegister.setEnabled(true);
        }

        if (TextUtils.isEmpty(telp)) {
            mTelp.setError(getString(R.string.error_field_required));
            focusView = mTelp;
            cancel = true;
            btnRegister.setEnabled(true);
        }

        // Check for a valid password, if the user entered one.
        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
            btnRegister.setEnabled(true);
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            mEmailView.setError(getString(R.string.error_field_required));
            focusView = mEmailView;
            cancel = true;
            btnRegister.setEnabled(true);
        } else if (!isEmailValid(email)) {
            mEmailView.setError(getString(R.string.error_invalid_email));
            focusView = mEmailView;
            cancel = true;
            btnRegister.setEnabled(true);
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
            btnRegister.setEnabled(true);
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            showProgress(true);

            RequestBody data = new FormBody.Builder()
                    .add("name", name)
                    .add("email", email)
                    .add("telp", telp)
                    .add("password", password)
                    .add("c_password", cpassword)
                    .add("id_firebase", prefManager.getIDFB())
                    .build();
            postRequest = new PostRequest(this);
            postRequest.SendPostData(EndpointAPI.Signup, "", data, 1);
            postRequest.execute();

        }
    }

    private boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        return email.contains("@");
    }

    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() > 5;
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

    }

    String _token = "";

    @Override
    public void onRequestComplete(JSONObject jsonObject, Integer REQUEST_CODE) {
        Log.d(TAG, "onRequestComplete response : " + jsonObject.toString());
        switch (REQUEST_CODE) {
            case 1:
                try {
                    if (jsonObject.getString("response").equalsIgnoreCase("success")) {
                        JSONObject c = jsonObject.getJSONObject("result");
                        prefManager.setToken(c.getString("token"));

                        _token = c.getString("token");

                        RequestBody data1 = new FormBody.Builder().build();
                        postRequest1 = new PostRequest(this);
                        postRequest1.SendPostData(EndpointAPI.DetailProfile, _token, data1, 2);
                        postRequest1.execute();
                    } else {
                        btnRegister.setEnabled(true);
                        btnLogin.setEnabled(true);
                        Toast.makeText(getBaseContext(), "Kesalahan server / Periksa koneksi Anda.", Toast.LENGTH_LONG).show();
                        showProgress(false);
                    }
                } catch (JSONException e) {
                    /*e.printStackTrace();*/
                    btnRegister.setEnabled(true);
                    btnLogin.setEnabled(true);
                    Toast.makeText(getBaseContext(), "Kesalahan server / Periksa koneksi Anda.", Toast.LENGTH_LONG).show();
                    showProgress(false);
                }
                break;
            case 2:
                try {
                    if (jsonObject.getString("response").equalsIgnoreCase("success")) {
                        //prefManager.setDataProfile(jsonObject.toString());
                        //Toast.makeText(getBaseContext(), "Berhasil mendaftar. Silakan masuk dengan akun yang telah didaftarkan", Toast.LENGTH_LONG).show();
                        //prefManager.setLog(true);
                        Toast.makeText(getBaseContext(), "Berhasil mendaftar akun baru.", Toast.LENGTH_LONG).show();
                        showProgress(false);
                        //if(prefManager.getFPrev()){
                        //MainActivity.fa.recreate();
                        Intent d = new Intent(this, Register2Activity.class);
                        Bundle x = new Bundle();
                        x.putString("token", _token);
                        d.putExtras(x);
                        startActivity(d);
                        this.finish();
/*                        }else {
                            GameHome.fa.finish();
                            Intent d = new Intent(this, GameHome.class);
                            startActivity(d);
                            this.finish();
                        }*/
                    } else {
                        btnRegister.setEnabled(true);
                        btnLogin.setEnabled(true);
                        Toast.makeText(getBaseContext(), "Kesalahan server / Periksa koneksi Anda.", Toast.LENGTH_LONG).show();
                        showProgress(false);
                    }
                } catch (JSONException e) {
                    /*e.printStackTrace();*/
                    btnRegister.setEnabled(true);
                    btnLogin.setEnabled(true);
                    Toast.makeText(getBaseContext(), "Kesalahan server / Periksa koneksi Anda.", Toast.LENGTH_LONG).show();
                    showProgress(false);
                }
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        this.finish();
        return super.onSupportNavigateUp();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }
}
