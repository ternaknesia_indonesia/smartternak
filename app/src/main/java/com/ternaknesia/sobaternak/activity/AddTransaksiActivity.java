package com.ternaknesia.sobaternak.activity;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.ternaknesia.sobaternak.Adapter.CAdapterCategory;
import com.ternaknesia.sobaternak.Connection.PostRequest;
import com.ternaknesia.sobaternak.Interface.EndpointAPI;
import com.ternaknesia.sobaternak.Interface.RequestComplete;
import com.ternaknesia.sobaternak.Manager.PrefManager;
import com.ternaknesia.sobaternak.R;
import com.ternaknesia.sobaternak.Utils.CameraUtils;
import com.ternaknesia.sobaternak.Utils.DateFormatter;
import com.ternaknesia.sobaternak.Utils.ImageZoomView.PhotoView;
import com.ternaknesia.sobaternak.Utils.MyDatePicker;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import okhttp3.FormBody;
import okhttp3.RequestBody;


public class AddTransaksiActivity extends AppCompatActivity implements RequestComplete, MyDatePicker.OnDateSet {
    private static final int DATE_DIALOG_ID = 0;
    private PostRequest postRequest, p1, p2;
    private PrefManager prefManager;
    private RelativeLayout pilih_bukti;
    private ImageView foto_bukti;
    private static int PERMISSION_ALL = 1;
    private String[] PERMISSIONS = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA};
    private static final int CAMERA_REQUEST_CODE = 0;
    private static final int GALLERY_REQUEST_CODE = 1;
    private Uri FileUriOutput = null;//Uri to capture image
    private String pathImg = "";
    private String project_id;
    private AlertDialog dialog;
    private EditText title;
    private EditText desc;
    private TextView dateTextView;
    private EditText value;
    private LinearLayout id_click_date;
    private Spinner sJenis;
    private Spinner sDetail;
    private Button tambah;
    private String category = "0";
    private int pYear;
    private int pMonth;
    private int pDay;
    String[] jenis_list = {"Pemasukan", "Pengeluaran"};
    String[] jenis_id = {"2", "1"};
    String[] detail_list1 = {"Ternak", "Pakan", "Obat", "Lain-lain"};
    String[] detail_list2 = {"Untung", "Investasi", "Lain-lain"};
    ArrayAdapter<String> jns = null;
    private int position_selected = 0;
    String[] spinnerName;
    String[] spinnerId;
    MyDatePicker myDatePicker;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_transaksi);
        prefManager = new PrefManager(this);
        myDatePicker = new MyDatePicker(this);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Tambah Transaksi");

        init();
        initSpinner();
        setCurrentDate();
        id_click_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //GetDate();
                myDatePicker.SetDatePicker("dd/MM/yyyy", dateTextView.getText().toString(),1);
                myDatePicker.show();
            }
        });

        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if (extras == null) {
                project_id = null;
            } else {
                project_id = extras.getString("project_id");
            }
        } else {
            project_id = (String) savedInstanceState.getSerializable("project_id");
        }


        tambah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!sJenis.isSelected() && !sDetail.isSelected() && category.equalsIgnoreCase("0")){
                    Toast.makeText(getBaseContext(), "Pilih jenis dan detail transaksi.", Toast.LENGTH_SHORT).show();
                }else {
                    if (!title.getText().toString().equalsIgnoreCase("") && !value.getText().toString().equalsIgnoreCase("")
                            && !dateTextView.getText().toString().equalsIgnoreCase("")) {

                        View v1 = sDetail.getSelectedView();
                        TextView idf = v1.findViewById(R.id.id);
                        category = idf.getText().toString();

                        /*if (sJenis.getPosition() == 0)
                            category = 2;
                        else category = 1;*/

                        RequestBody data = new FormBody.Builder()
                                .add("project_id", project_id)
                                .add("transaction_date", new DateFormatter().convertDate(dateTextView.getText().toString(), "dd/MM/yyyy", "yyyy-MM-dd"))
                                .add("title", title.getText().toString())
                                .add("value", value.getText().toString())
                                .add("category", String.valueOf(category))
                                .add("desc", desc.getText().toString())
                                .build();

                        postRequest = new PostRequest(AddTransaksiActivity.this);
                        postRequest.SendPostData(EndpointAPI.addTransc, prefManager.getToken(), data, 1);
                        postRequest.execute();
                    } else {
                        Toast.makeText(getBaseContext(), "Mohon melengkapi semua isian.", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        pilih_bukti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                File f = new File(Environment.getExternalStorageDirectory().toString(), "Ternaknesia/Image/cache.jpg");
                if (f.exists())
                    if (f.delete()) Log.e("success", "ok");

                AlertDialog.Builder builder = new AlertDialog.Builder(AddTransaksiActivity.this);
                builder.setMessage("Pilih foto")
                        .setCancelable(true)
                        .setNegativeButton("KAMERA", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                                if (!hasPermissions(AddTransaksiActivity.this, PERMISSIONS)) {
                                    ActivityCompat.requestPermissions(AddTransaksiActivity.this, PERMISSIONS, PERMISSION_ALL);
                                } else {
                                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);//Start intent with Action_Image_Capture
                                    FileUriOutput = CameraUtils.getOutputMediaFileUri(getBaseContext());//get FileUriOutput from CameraUtils
                                    intent.putExtra(MediaStore.EXTRA_OUTPUT, FileUriOutput);//Send FileUriOutput with intent
                                    startActivityForResult(intent, CAMERA_REQUEST_CODE);//start activity for result with CAMERA_REQUEST_CODE
                                }
                            }
                        })
                        .setPositiveButton("GALERI", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                                if (!hasPermissions(AddTransaksiActivity.this, PERMISSIONS)) {
                                    ActivityCompat.requestPermissions(AddTransaksiActivity.this, PERMISSIONS, PERMISSION_ALL);
                                } else {
                                    Intent pickPhoto = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                    startActivityForResult(pickPhoto, GALLERY_REQUEST_CODE);//one can be replaced with any action code
                                }
                            }
                        });
                dialog = builder.create();
                dialog.show();
            }
        });


        pilih_bukti.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                AlertDialog.Builder imageDialog = new AlertDialog.Builder(AddTransaksiActivity.this);
                LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);

                assert inflater != null;
                View layout = inflater.inflate(R.layout.popup, null);

                PhotoView image = layout.findViewById(R.id.imageView);

                Glide.with(getBaseContext())
                        .load(pathImg)
                        .into(image);

                imageDialog.setView(layout);
                imageDialog.create();
                imageDialog.show();
                return false;
            }
        });

    }

    private void setCurrentDate() {
        Calendar c = Calendar.getInstance();
        int pYear = c.get(Calendar.YEAR);
        int pMonth = c.get(Calendar.MONTH);
        int pDay = c.get(Calendar.DAY_OF_MONTH);
        String __date_of = String.valueOf(new StringBuilder().append(String.format(Locale.getDefault(), "%02d", pDay)).append("/").append(pMonth + 1).append("/").append(pYear));
        dateTextView.setText(__date_of);
    }


    private void initSpinner() {

        CAdapterCategory mCustomAdapter = new CAdapterCategory(this, jenis_id, jenis_list);
        sJenis.setAdapter(mCustomAdapter);

        sJenis.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                TextView id_category = view.findViewById(R.id.id);
                setListDetail(Integer.parseInt(id_category.getText().toString()));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        sDetail.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                TextView id_category = view.findViewById(R.id.id);
                category = id_category.getText().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //setListDetail(1);

    }

    private void setListDetail(int position) {
        //Log.e("cekj", String.valueOf(position));
        RequestBody data = new FormBody.Builder()
                .add("parent_id", String.valueOf(position))
                .build();
        postRequest = new PostRequest(this);
        postRequest.SendPostData(EndpointAPI.cat_Trans, prefManager.getToken(), data, 2);
        postRequest.execute();
    }

    private void GetDate() {
        Calendar c = Calendar.getInstance();
        pYear = c.get(Calendar.YEAR);
        pMonth = c.get(Calendar.MONTH);
        pDay = c.get(Calendar.DAY_OF_MONTH);
        showDialog(DATE_DIALOG_ID);
    }

    public Dialog onCreateDialog(int id) {
        switch (id) {
            case DATE_DIALOG_ID:
                DatePickerDialog.OnDateSetListener pDateSetListener = new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        //pYear = year;
                        //pMonth = monthOfYear;
                        //pDay = dayOfMonth;
                        String __date_of = String.valueOf(new StringBuilder().append(dayOfMonth).append("/").append(monthOfYear + 1).append("/").append(year));
                        dateTextView.setText(__date_of);
                        //setDate(new DateFormatter().newDateFrom(__date_of, "dd-MM-yyyy"));
                    }
                };
                DatePickerDialog dialog = new DatePickerDialog(AddTransaksiActivity.this, pDateSetListener, pYear, pMonth, pDay);

                //Date newDate = new DateFormatter().newDateFrom(qurban_date, "yyyy-MM-dd HH:mm:ss");
                Date newDate = Calendar.getInstance().getTime();

                /*if (newDate != null) {
                    dialog.getDatePicker().setMinDate(newDate.getTime() - (1000 * 60 * 60 * 24 * day_min));
                    dialog.getDatePicker().setMaxDate(newDate.getTime() + (1000 * 60 * 60 * 24 * day_max));
                }*/

                /*if (newDate != null) {
                    dialog.getDatePicker().updateDate(newDate.getYear(), newDate.getMonth(), newDate.getDay());
                }*/

                return dialog;
            default:
                return null;
        }
    }

    private void init() {
        //mSpinner = findViewById(R.id.spinnerD);
        title = findViewById(R.id.title_transaksi);
        desc = findViewById(R.id.desc_transaksi);
        dateTextView = findViewById(R.id.date_transaksi);
        value = findViewById(R.id.value_transaksi);
        pilih_bukti = findViewById(R.id.pilih_bukti_bayar);
        foto_bukti = findViewById(R.id.foto_bukti);
        tambah = findViewById(R.id.btn_tambah);
        sJenis = findViewById(R.id.spinnerJ);
        sDetail = findViewById(R.id.spinnerD);
        id_click_date = findViewById(R.id.id_click_date);
    }

    public static boolean hasPermissions(Context context, String... permissions) {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    @Override
    public void onRequestComplete(JSONObject jsonObject, Integer REQUEST_CODE) {
        if(REQUEST_CODE == 1) {
            try {
                if (jsonObject.getString("response").equalsIgnoreCase("success")) {
                    Toast.makeText(this, "Berhasil menambah transaksi", Toast.LENGTH_SHORT).show();
                    DetailKeuanganActivity.dkeu.recreate();
                } else {
                    Toast.makeText(this, "Gagal menambah transaksi", Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else if (REQUEST_CODE == 2){
            try {
                if (jsonObject.getString("response").equalsIgnoreCase("success")) {
                    //Log.e("ckk", jsonObject.toString());
                    JSONArray res = jsonObject.getJSONArray("result");
                    spinnerId = new String[res.length()];
                    spinnerName = new String[res.length()];

                    for(int y = 0; y < res.length(); y++) {
                        JSONObject item = res.getJSONObject(y);
                        int id = item.getInt("id");
                        String value = item.getString("name");
                        spinnerId[y] = String.valueOf(id);
                        spinnerName[y] = value;
                    }

                    CAdapterCategory mCustomAdapter = new CAdapterCategory(this, spinnerId, spinnerName);
                    sDetail.setAdapter(mCustomAdapter);

/*                    ab = new ArrayAdapter<String>(getBaseContext(), R.layout.item_spinner, detail_list2);
                    sDetail.setText(ab.getItem(0));
                    sDetail.setAdapter(ab);*/
                } else {
                    Toast.makeText(this, "Error", Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onDateSet(String date, int iDate, int iMonth, int iYear, Integer CODE) {
        //Log.e("datep", String.valueOf(iDate));
        if (CODE == 1){
            dateTextView.setText(date);
        }
    }
}
