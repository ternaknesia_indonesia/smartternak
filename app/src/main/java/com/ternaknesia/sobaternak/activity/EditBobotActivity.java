package com.ternaknesia.sobaternak.activity;
/*        Mohon untuk tidak melakukan hal-hal yang melanggar hukum.
        Anda tidak berhak melakukan cracking terhadap system kami tanpa ada persetujuan.
        Terimakasih*/

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.ternaknesia.sobaternak.Connection.PostRequest;
import com.ternaknesia.sobaternak.Interface.EndpointAPI;
import com.ternaknesia.sobaternak.Interface.RequestComplete;
import com.ternaknesia.sobaternak.Manager.PrefManager;
import com.ternaknesia.sobaternak.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.FormBody;
import okhttp3.RequestBody;

/**
 * A login screen that offers login via loc/password.
 */
public class EditBobotActivity extends AppCompatActivity implements RequestComplete {
    private TextView mId;
    private EditText mWeight;
    private Spinner mTahun;
    private EditText mBulan;
    private EditText mChecked_tags;

    private RelativeLayout mProgressView;
    private View mLoginFormView;
    private RelativeLayout formSignUp;
    private Button btnSubmit;
    private PrefManager prefManager;
    private PostRequest postRequest;
    private String id = "";
    private String id_kandang = "";
    private String weight = "";
    private String tahun = "";
    private String bulan = "";
    private String checked_tags = "";
    private LinearLayout linearKandang;

    private ArrayList<Integer> tahunMap;

    private RadioGroup radioGroup;
    private Spinner kandang;
    private ArrayList<Integer> kandangsMap;
    //private String[] tempRId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_bobot);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Edit Bobot");
        prefManager = new PrefManager(this);

        init();

        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if (extras == null) {
                id = "";
                id_kandang = "";
                tahun = "";
                bulan = "";
                weight = "";
                checked_tags = "";
            } else {
                id = extras.getString("id");
                id_kandang = extras.getString("kandang_id");
                tahun = extras.getString("tahun");
                bulan = extras.getString("bulan");
                weight = extras.getString("weight");
                checked_tags = extras.getString("checked_tags");
            }
        } else {
            id = (String) savedInstanceState.getSerializable("id");
            id_kandang = (String) savedInstanceState.getSerializable("kandang_id");
            tahun = (String) savedInstanceState.getSerializable("tahun");
            bulan = (String) savedInstanceState.getSerializable("bulan");
            weight = (String) savedInstanceState.getSerializable("weight");
            checked_tags = (String) savedInstanceState.getSerializable("checked_tags");
        }

        mId.setText(id_kandang);
        mBulan.setText(bulan);
        //mTahun.setText(tahun);
        mWeight.setText(weight);
        mChecked_tags.setText(checked_tags);

        ArrayList<String> kandangsLabel = new ArrayList<String>();
        tahunMap = new ArrayList<Integer>();

        for (int f = 2018; f >= 2015; f--) {
            String name = String.valueOf(f);
            kandangsLabel.add(name);
            tahunMap.add(f);
        }

        ArrayAdapter<String> kandangAdapter = new ArrayAdapter<String>(this, R.layout.simple_spinner_item, kandangsLabel);
        mTahun.setAdapter(kandangAdapter);
        if(!tahun.equalsIgnoreCase("")) mTahun.setSelection(tahunMap.indexOf(Integer.parseInt(tahun)));

        btnSubmit.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                btnSubmit.setEnabled(false);
                attemptLogin();
            }
        });

        if(id.equalsIgnoreCase("")) {
            RequestBody data = new FormBody.Builder()
                    .build();
            postRequest = new PostRequest(this);
            postRequest.SendPostData(EndpointAPI.listKandang, prefManager.getToken(), data, 2);
            postRequest.execute();

        } else linearKandang.setVisibility(View.GONE);

        mLoginFormView = findViewById(R.id.form1);
        mProgressView = findViewById(R.id.loading);
    }

    private void init() {
        formSignUp = findViewById(R.id.formRegister);
        mId = findViewById(R.id.id_kandang);
        mTahun = findViewById(R.id.tahun2);
        mBulan = findViewById(R.id.bulan);
        mWeight = findViewById(R.id.weight);
        mChecked_tags = findViewById(R.id.checked_tags);
        btnSubmit = findViewById(R.id.btn_submit);
        kandang = findViewById(R.id.kandang);
        linearKandang = findViewById(R.id.linear_kandang);

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    private void attemptLogin() {
        // Reset errors.
        mBulan.setError(null);
        mWeight.setError(null);
        mChecked_tags.setError(null);
        try {
            tahun = String.valueOf(tahunMap.get(mTahun.getSelectedItemPosition()));
        } catch (Exception e) {
            //e.printStackTrace();
        }

        if(id.equalsIgnoreCase(""))
            try {
                id_kandang = String.valueOf(kandangsMap.get(kandang.getSelectedItemPosition()));
            } catch (Exception e) {
                //e.printStackTrace();
            }

        // Store values at the time of the login attempt.
        String bulan_ = mBulan.getText().toString();
        String weight_ = mWeight.getText().toString();
        String tags_ = mChecked_tags.getText().toString();

        boolean cancel = false;
        View focusView = null;

        if (TextUtils.isEmpty(bulan_)) {
            mBulan.setError(getString(R.string.error_field_required));
            focusView = mBulan;
            cancel = true;
            btnSubmit.setEnabled(true);
        }

        if (TextUtils.isEmpty(tags_)) {
            mWeight.setError(getString(R.string.error_field_required));
            focusView = mWeight;
            cancel = true;
            btnSubmit.setEnabled(true);
        }

        if (TextUtils.isEmpty(weight_)) {
            mWeight.setError(getString(R.string.error_field_required));
            focusView = mWeight;
            cancel = true;
            btnSubmit.setEnabled(true);
        }


        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
            btnSubmit.setEnabled(true);
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.


                //Log.e("reg2 id_kandang : ", id_kandang);
                RequestBody data = new FormBody.Builder()
                        .add("id", id)
                        .add("kandang_id", id_kandang)
                        .add("instance_id", prefManager.getSingleDataProfile("instance_id"))
                        .add("weight", weight_)
                        .add("tahun", tahun)
                        .add("bulan", bulan_)
                        .add("checked_tags", tags_)
                        .build();

                postRequest = new PostRequest(this);
                postRequest.SendPostData(EndpointAPI.EditBobot, prefManager.getToken(), data, 1);
                postRequest.execute();
        }
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

        formSignUp.setVisibility(show ? View.GONE : View.VISIBLE);
        formSignUp.animate().setDuration(shortAnimTime).alpha(
                show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                formSignUp.setVisibility(show ? View.GONE : View.VISIBLE);
            }
        });

        mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            }
        });

        mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
        mProgressView.animate().setDuration(shortAnimTime).alpha(
                show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            }
        });
    }

    @Override
    public void onRequestComplete(JSONObject jsonObject, Integer REQUEST_CODE) {
        //Log.e("vgf", jsonObject.toString());
        if (REQUEST_CODE == 1) {
            try {
                if (jsonObject.getString("response").equalsIgnoreCase("success")) {
                    Toast.makeText(getBaseContext(), "Berhasil mengubah bobot.", Toast.LENGTH_LONG).show();
                    showProgress(false);
                    setResult(6000);
                    this.finish();
                } else {
                    btnSubmit.setEnabled(true);
                    Toast.makeText(getBaseContext(), "Kesalahan server / Periksa koneksi Anda.", Toast.LENGTH_LONG).show();
                    showProgress(false);
                }
            } catch (JSONException e) {
                /*e.printStackTrace();*/
                btnSubmit.setEnabled(true);
                Toast.makeText(getBaseContext(), "Kesalahan server / Periksa koneksi Anda.", Toast.LENGTH_LONG).show();
                showProgress(false);
            }
        } else if (REQUEST_CODE == 2){
            try {
                if (jsonObject.getString("response").equalsIgnoreCase("success")) {
                    JSONArray kandangs = jsonObject.getJSONArray("result");

                    ArrayList<String> kandangsLabel = new ArrayList<String>();
                    kandangsLabel.add("Semua Kandang");

                    kandangsMap = new ArrayList<Integer>();
                    kandangsMap.add(0);

                    for (int f = 0; f < kandangs.length(); f++) {
                        JSONObject ix = kandangs.getJSONObject(f);
                        Integer id = ix.getInt("id");
                        String name = ix.getString("name");
                        kandangsMap.add(id);
                        kandangsLabel.add(name);
                    }

                    ArrayAdapter<String> kandangAdapter = new ArrayAdapter<String>(this, R.layout.simple_spinner_item, kandangsLabel);
                    //dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    kandang.setAdapter(kandangAdapter);
                } else {
                    Toast.makeText(getBaseContext(), "Kesalahan server / Periksa koneksi Anda.", Toast.LENGTH_LONG).show();
                }
            } catch (JSONException e) {
                //showProgress(false);
                Toast.makeText(getBaseContext(), "Kesalahan server / Periksa koneksi Anda.", Toast.LENGTH_LONG).show();
            }
        }
    }


    @Override
    public boolean onSupportNavigateUp() {
        this.finish();
        return super.onSupportNavigateUp();
    }

}
