package com.ternaknesia.sobaternak.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import com.ternaknesia.sobaternak.Adapter.ProjectAdapter;
import com.ternaknesia.sobaternak.Connection.PostRequest;
import com.ternaknesia.sobaternak.DataModel.DataProject;
import com.ternaknesia.sobaternak.Interface.EndpointAPI;
import com.ternaknesia.sobaternak.Interface.RequestComplete;
import com.ternaknesia.sobaternak.Manager.PrefManager;
import com.ternaknesia.sobaternak.R;
import com.ternaknesia.sobaternak.Utils.Item;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.FormBody;
import okhttp3.RequestBody;


public class KelolaKeuanganActivity extends AppCompatActivity implements RequestComplete{
    PostRequest postRequest;
    PrefManager prefManager;
    RecyclerView recyclerView;
    ArrayList<Item> arrayList = new ArrayList<>();



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kelola_list_proyek);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Kelola Keuangan");
        prefManager = new PrefManager(this);

        recyclerView = findViewById(R.id.r_list_proyek);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setFocusable(false);

        RequestBody data = new FormBody.Builder()
                .build();

        postRequest = new PostRequest(KelolaKeuanganActivity.this);
        postRequest.SendPostData(EndpointAPI.listProyek, prefManager.getToken(), data, 1);
        postRequest.execute();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    @Override
    public void onRequestComplete(JSONObject jsonObject, Integer REQUEST_CODE) {
        //Log.e("resp", jsonObject.toString());
        try {
            if (jsonObject.getString("response").equalsIgnoreCase("success")){
                JSONArray items = jsonObject.getJSONArray("result");
                for (int f = 0; f < items.length(); f++){
                    JSONObject ix = items.getJSONObject(f);
                    String id = ix.getString("id");
                    String instance_id = ix.getString("instance_id");
                    String name = ix.getString("name");
                    String period = ix.getString("period");
                    String image = ix.getString("feat_img");

                    arrayList.add(new DataProject(id, name, image, period, instance_id, "Alat sudah disediakan"));
                    recyclerView.setAdapter(new ProjectAdapter(arrayList, this));
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
