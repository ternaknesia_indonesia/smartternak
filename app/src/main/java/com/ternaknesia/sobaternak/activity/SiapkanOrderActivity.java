package com.ternaknesia.sobaternak.activity;

import android.app.Dialog;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ternaknesia.sobaternak.Adapter.HewanAdapter;
import com.ternaknesia.sobaternak.Connection.PostRequest;
import com.ternaknesia.sobaternak.DataModel.DataHewan;
import com.ternaknesia.sobaternak.DataModel.DataPendingOrder;
import com.ternaknesia.sobaternak.Interface.EndpointAPI;
import com.ternaknesia.sobaternak.Interface.RequestComplete;
import com.ternaknesia.sobaternak.Manager.PrefManager;
import com.ternaknesia.sobaternak.R;
import com.ternaknesia.sobaternak.Utils.Item;
import com.ternaknesia.sobaternak.Utils.JSONGetter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.FormBody;
import okhttp3.RequestBody;

public class SiapkanOrderActivity extends AppCompatActivity implements RequestComplete {

    @BindView(R.id.btn_save)
    Button btnSave;
    private PrefManager prefManager;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.appbar)
    AppBarLayout appbar;
    @BindView(R.id.qty)
    TextView tvQty;
    @BindView(R.id.info)
    LinearLayout info;
    @BindView(R.id.r_list_hewan)
    RecyclerView rListHewan;
    @BindView(R.id.nested1)
    NestedScrollView nested1;
    private EditText d_code_tag;
    private LinearLayout d_fail_container;
    private TextView d_fail;
    private LinearLayout d_info_container;
    private TextView d_info_hewan;
    private TextView d_instance;
    private TextView d_project;
    private TextView d_pemesan;
    private TextView d_buy_price;
    private TextView d_harvest_date;
    private LinearLayout d_sell_price;
    private EditText d_sell_price_unit;
    private EditText d_sell_price_kg;
    private Button d_add;
    private ArrayList<Item> sell_animal = new ArrayList<>();
    private DataHewan hewan;
    private DataPendingOrder item;
    private Integer qty;
    RequestBody data = new FormBody.Builder()
            .build();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_siapkan_order);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        item = getIntent().getParcelableExtra("item");
        qty = getIntent().getIntExtra("qty", 0);
        tvQty.setText(String.valueOf(qty));
        if (getSupportActionBar() != null) getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Objects.requireNonNull(getSupportActionBar()).setTitle("Siapkan Order");

        prefManager = new PrefManager(this);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        rListHewan.setLayoutManager(mLayoutManager);
        rListHewan.setItemAnimator(new DefaultItemAnimator());
        rListHewan.setNestedScrollingEnabled(false);
        rListHewan.setFocusable(false);
        rListHewan.setAdapter(new HewanAdapter(sell_animal, this));
    }

    @OnClick({R.id.btn_tambah, R.id.btn_save})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_tambah:
                onAddHewanClicked();
                break;
            case R.id.btn_save:
                if (rListHewan.getAdapter().getItemCount() == qty) {
                    StringBuilder tags = new StringBuilder();
                    for (int i = 0; i < sell_animal.size(); i++) {
                        DataHewan dataHewan = (DataHewan) sell_animal.get(i);
                        tags.append(dataHewan.getCodeTag()).append(";");
                    }
                    if (tags.length() > 0) tags.setLength(tags.length() - 1);
                    RequestBody data = new FormBody.Builder()
                            .add("sales_order_line_id", String.valueOf(item.getSales_order_line_id()))
                            .add("weight", "0;0;0")
                            .add("prepared", String.valueOf(item.getJumlah()))
                            .add("instance_id", hewan.getInstanceId())
                            .add("tags", tags.toString())
                            .build();
//                    StringBuilder param = new StringBuilder();
//                    param.append("sales_order_line_id = ").append(item.getSales_order_line_id())
//                            .append("weight = ").append("0;0;0")
//                            .append("prepared = ").append(item.getJumlah())
//                            .append("instance_id = ").append(hewan.getInstanceId())
//                            .append("tags = ").append(tags.toString());
//                    Log.d("Request Body", "onViewClicked: " + param.toString());
                    PostRequest postRequest = new PostRequest(this);
                    postRequest.SendPostData(EndpointAPI.prepare_Order, prefManager.getToken(), data, 2);
                    postRequest.execute();
                } else
                    Toast.makeText(this, "Hewan yang harus disiapkan : " + qty, Toast.LENGTH_SHORT).show();
                break;
        }
    }

    @Override
    public void onRequestComplete(JSONObject jsonObject, Integer REQUEST_CODE) {
        if (REQUEST_CODE == 2) {
            try {
                if (jsonObject.getString("response").equalsIgnoreCase("success")) {
                    Toast.makeText(this, "Berhasil konfirmasi", Toast.LENGTH_SHORT).show();
                    finish();
                    setResult(5000);
                } else {
                    Log.d("Error", "onRequestComplete: " + jsonObject.toString());
                    Toast.makeText(this, "Gagal Konfirmasi", Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
                Toast.makeText(this, "Ada kesalahan sistem", Toast.LENGTH_LONG).show();
            }
        } else if (REQUEST_CODE == 1) {
            try {
                if (jsonObject.getString("response").equalsIgnoreCase("success")) {
                    JSONObject result = jsonObject.getJSONObject("result");
                    JSONArray data = result.getJSONArray("data");
                    if (data.length() > 0) {
                        hewan = new DataHewan(data.getJSONObject(0));
                        d_info_hewan.setText(hewan.getJenisHewan() + " " + hewan.getJenisKelamin() + ", " + hewan.getStatus() + "\n" +
                                "Berat: " + hewan.getWeight() + ", Tinggi: " + hewan.getHeight());
                        d_instance.setText("Instansi: " + hewan.getInstance());
                        d_project.setText("Proyek: " + hewan.getProject());
                        d_pemesan.setText("Pemesan: " + hewan.getPemesan());
                        d_buy_price.setText("Harga Beli: " + hewan.getBuyPriceUnit() + hewan.getBuyPriceKg());
                        d_harvest_date.setText("Tanggal Panen: " + hewan.getHarvestDate() + " " + hewan.getHarvestAge());
                        Double vp_unit = hewan.getSellPriceUnitNum();
                        String p_unit = vp_unit > 0 ? String.format("%.0f", vp_unit) : "";
                        d_sell_price_unit.setText(String.valueOf(p_unit));
                        Double vp_kg = hewan.getSellPriceKgNum();
                        String p_kg = vp_kg > 0 ? String.format("%.0f", vp_kg) : "";
                        d_sell_price_kg.setText(String.valueOf(p_kg));
                        d_info_container.setVisibility(View.VISIBLE);
                        d_fail_container.setVisibility(View.GONE);
                        // CHANGE
                        if (!data.getJSONObject(0).getString("status").equalsIgnoreCase("hidup")) {
                            d_sell_price.setVisibility(View.GONE);
                            d_sell_price_unit.setEnabled(false);
                            d_sell_price_kg.setEnabled(false);
                            d_add.setEnabled(false);
                            d_add.setBackground(getResources().getDrawable(R.color.gray400));
                            d_fail.setText("Hewan ini sudah " + data.getJSONObject(0).getString("status"));
                            d_fail_container.setVisibility(View.VISIBLE);
                        } else {
                            d_sell_price.setVisibility(View.VISIBLE);
                            d_sell_price_unit.setEnabled(true);
                            d_sell_price_kg.setEnabled(true);
                            d_add.setEnabled(true);
                            d_add.setBackground(getResources().getDrawable(R.color.colorAccent));
                        }
                    } else {
                        showFailDialog("Kode Tagging hewan tidak ditemukan");
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void showFailDialog(String message) {
        d_fail.setText(message);
        d_fail_container.setVisibility(View.VISIBLE);
        d_info_container.setVisibility(View.GONE);
        d_sell_price.setVisibility(View.GONE);
    }

    private void findTag() {
        boolean isThere = false;
        for (int i = 0; i < sell_animal.size(); i++) {
            DataHewan dataHewan = (DataHewan) sell_animal.get(i);
            if (dataHewan.getCodeTag().equalsIgnoreCase(d_code_tag.getText().toString())) {
                isThere = true;
                break;
            }
        }
        if (isThere) {
            showFailDialog("Kode Tagging Hewan Sudah Dipakai");
        } else {
            RequestBody data = new FormBody.Builder()
                    .add("t", d_code_tag.getText().toString())
                    .build();

            PostRequest postRequest = new PostRequest(this);
            Log.d("Token", "findTag: " + prefManager.getToken());
            postRequest.SendPostData(EndpointAPI.findHewanByTag, prefManager.getToken(), data, 1);
            postRequest.execute();
        }
    }

    public void onAddHewanClicked() {
        final Dialog _dialog = new Dialog(this);
        _dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        _dialog.setContentView(R.layout.dialog_jual_hewan);
        d_code_tag = _dialog.findViewById(R.id.code_tag);
        Button d_search = _dialog.findViewById(R.id.btn_search);
        d_fail_container = _dialog.findViewById(R.id.info_fail);
        d_fail = _dialog.findViewById(R.id.fail);
        d_info_container = _dialog.findViewById(R.id.info_container);
        d_info_hewan = _dialog.findViewById(R.id.info_hewan);
        d_instance = _dialog.findViewById(R.id.instance);
        d_project = _dialog.findViewById(R.id.project);
        d_pemesan = _dialog.findViewById(R.id.pemesan);
        d_buy_price = _dialog.findViewById(R.id.buy_price);
        d_harvest_date = _dialog.findViewById(R.id.harvest_date);
        d_sell_price = _dialog.findViewById(R.id.sell_price);
        d_sell_price_unit = _dialog.findViewById(R.id.sell_price_unit);
        d_sell_price_kg = _dialog.findViewById(R.id.sell_price_kg);
        d_add = _dialog.findViewById(R.id.btn_tambah);

        d_code_tag.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    findTag();
                    return true;
                }
                return false;
            }
        });

        d_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                findTag();
            }
        });

        d_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hewan.setSellPriceUnitNum(Double.parseDouble(d_sell_price_unit.getText().toString()));
                hewan.setSellPriceKgNum(Double.parseDouble(d_sell_price_kg.getText().toString()));
                sell_animal.add(hewan);
                rListHewan.getAdapter().notifyDataSetChanged();
                _dialog.hide();
            }
        });

        d_sell_price_unit.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                changeText(false);
                return false;
            }
        });

        d_sell_price_kg.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                changeText(true);
                return false;
            }
        });

        _dialog.show();
    }

    private void changeText(boolean is_kg) {
        JSONObject data = hewan.getJSONData();
        Integer w = JSONGetter.getInt(data, "weight", 0);
        Integer unit = getInteger(d_sell_price_unit.getText().toString());
        Integer kg = getInteger(d_sell_price_kg.getText().toString());

        if (w > 0) {
            if ((unit > 0 && kg > 0) || (unit > 0 && kg == 0) && !is_kg) {
                kg = unit / w;
                d_sell_price_kg.setText(String.valueOf(kg));
            } else if (unit == 0 && kg > 0 || is_kg) {
                unit = kg * w;
                d_sell_price_unit.setText(String.valueOf(unit));
            }
        }
    }

    private Integer getInteger(String source) {
        try {
            return Integer.parseInt(source);
        } catch (Exception e) {
            return 0;
        }
    }
}
