package com.ternaknesia.sobaternak.activity;
/*        Mohon untuk tidak melakukan hal-hal yang melanggar hukum.
        Anda tidak berhak melakukan cracking terhadap system kami tanpa ada persetujuan.
        Terimakasih*/

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.ternaknesia.sobaternak.BuildConfig;
import com.ternaknesia.sobaternak.R;

public class TentangAppsActivity extends AppCompatActivity {
    int countClick = 0;
    TextView version;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tentang_apps);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if(getSupportActionBar()!=null)getSupportActionBar().setTitle("Tentang Aplikasi");
        if(getSupportActionBar()!=null)getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        ImageView logo = findViewById(R.id.logo);
        logo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (countClick >= 2) {
                    //Intent a = new Intent(TentangAppsActivity.this, GameHome.class);
                    //startActivity(a);
                    return;
                }
                countClick = countClick + 1;
                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        countClick = 0;
                    }
                }, 3000);
            }
        });

        TextView link = findViewById(R.id.textLink);
        link.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://ternaknesia.com"));
                startActivity(browserIntent);
            }
        });

        version = findViewById(R.id.version_apps);
        version.setText(BuildConfig.VERSION_NAME);

    }

    @Override
    public boolean onSupportNavigateUp() {
        this.finish();
        return super.onSupportNavigateUp();
    }
}
