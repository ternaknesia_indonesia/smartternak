package com.ternaknesia.sobaternak.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.ternaknesia.sobaternak.R;

public class MenuTernak extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_ternak);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle("Menu Ternak");
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        findViewById(R.id.ternak).setOnClickListener(v -> openActivity(new HewanActivity()));
        findViewById(R.id.bobot).setOnClickListener(v -> openActivity(new BobotActivity()));
        findViewById(R.id.set_category).setOnClickListener(v -> openActivity(new SetCategoryActivity()));

    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return super.onSupportNavigateUp();
    }

    private void openActivity(Activity activity) {
        startActivity(new Intent(MenuTernak.this, activity.getClass()));
    }

}
