package com.ternaknesia.sobaternak.Fragment;


import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.ternaknesia.sobaternak.Adapter.ReadyOrderAdapter;
import com.ternaknesia.sobaternak.Connection.GetRequest;
import com.ternaknesia.sobaternak.Connection.PostRequest;
import com.ternaknesia.sobaternak.DataModel.DataPendingOrder;
import com.ternaknesia.sobaternak.DataModel.DataProcessOrder;
import com.ternaknesia.sobaternak.DataModel.DataReadyOrder;
import com.ternaknesia.sobaternak.Interface.EndpointAPI;
import com.ternaknesia.sobaternak.Interface.OnClickInterface;
import com.ternaknesia.sobaternak.Interface.RequestComplete;
import com.ternaknesia.sobaternak.Manager.PrefManager;
import com.ternaknesia.sobaternak.R;
import com.ternaknesia.sobaternak.Utils.Footer;
import com.ternaknesia.sobaternak.Utils.Item;
import com.ternaknesia.sobaternak.SQLiteDB.DBDriver;
import com.ternaknesia.sobaternak.SQLiteDB.Driver;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import okhttp3.FormBody;
import okhttp3.RequestBody;

/**
 * A simple {@link Fragment} subclass.
 */
public class ReadyOrderFragment extends Fragment implements OnClickInterface, RequestComplete, FragmentLifecycle {

    //widget
    private RecyclerView recyclerView;
    private boolean isCreated = false;
    private PrefManager prefManager;
    private List<Item> results;
    private boolean hasMore = false;
    private String next_page = "";
    private Boolean isLoadMore;
    private GetRequest connection;
    private EditText searchView;
    private ImageView imageView;


    //recyclerview
    //private InProgressProductAdapter recyclerAdapter;
    private RecyclerView.LayoutManager recyclerLayoutManager;

    public ReadyOrderFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_order_ready, container, false);
        prefManager = new PrefManager(Objects.requireNonNull(getActivity()));
        db = new DBDriver(getContext());
        isLoadMore = false;
        recyclerView = view.findViewById(R.id.recycler_view);
        searchView = view.findViewById(R.id.searchBar);
        imageView = view.findViewById(R.id.imageView);

        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (hasMore && (!hasFooter())) {
                    LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                    //position starts at 0
                    if (layoutManager.findLastCompletelyVisibleItemPosition() == layoutManager.getItemCount() - 2) {
                        /*asyncTask = new BackgroundTask();
                        asyncTask.execute((Void[]) null);*/
                        results.add(new Footer());
                        recyclerView.getAdapter().notifyDataSetChanged();
                        getData(next_page, searchView.getText().toString(), true);
                        //Log.e("url", next_page);
                        //Log.e("load", "load more");
                    }
                }
            }
        });

        searchView.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                //Log.e("cek", query);
                results = new ArrayList<>();
                hasMore = false;
                isLoadMore = false;
                next_page = "";
                Objects.requireNonNull(recyclerView.getAdapter()).notifyDataSetChanged();

                getData(EndpointAPI.getReady_Order, searchView.getText().toString(), false);
                return true;
            }
            return false;
        });

        imageView.setOnClickListener(v -> {
            results = new ArrayList<>();
            hasMore = false;
            isLoadMore = false;
            next_page = "";
            Objects.requireNonNull(recyclerView.getAdapter()).notifyDataSetChanged();

            getData(EndpointAPI.getReady_Order, searchView.getText().toString(), false);
        });

        refreshPage(true);
        return view;
    }

    private void getData(String url, String query, boolean is_paging) {
        connection = new GetRequest(this);
        if (is_paging)
            connection.SendGetData(url + "&q=" + query, prefManager.getToken(), 1);
        else connection.SendGetData(url + "?q=" + query, prefManager.getToken(), 1);
        connection.execute();
    }

    private boolean hasFooter() {
        return results.get(results.size() - 1) instanceof Footer;
    }

    private void deleteItem(int pos) {
        results.remove(pos);
        recyclerView.getAdapter().notifyDataSetChanged();
    }

    @Override
    public void onRequestComplete(JSONObject jsonObject, Integer REQUEST_CODE) {
        if (REQUEST_CODE == 1) {
            ArrayList<Item> item_order_arr;
            try {
                if (jsonObject.getString("response").equalsIgnoreCase("success")) {
                    JSONObject res = jsonObject.getJSONObject("result");
                    JSONArray data = res.getJSONArray("data");
                    next_page = res.getString("next_page_url");
                    //Log.e("cek1", jsonObject.toString());
                    item_order_arr = new ArrayList<>(data.length());

                    for (int d = 0; d < data.length(); d++) {
                        JSONObject item_order = data.getJSONObject(d);

                        String invoice = item_order.getString("no_invoice");
                        int id_transaksi = item_order.getInt("sales_order_id");
                        int id_item_transaksi = item_order.getInt("sales_order_line_id");
                        int terkirim = item_order.getInt("terkirim");
                        String driverphone = item_order.getString("courier_phone");
                        String item = item_order.getString("nama");
                        int market_id = item_order.getInt("market_id");
                        int jumlah = item_order.getInt("jumlah");
                        //Log.e("cek2", "oke");
                        JSONObject lines = item_order.getJSONObject("sales_order");
                        String user_telp = lines.getString("telp");
                        String name = lines.getString("name");
                        String catatan = lines.getString("catatan");
                        String tanggal = lines.getString("tanggal");

                        int is_disalurkan = lines.getInt("is_disalurkan");
                        String alamat;
                        if (is_disalurkan == 1) {
                            alamat = "Disalurkan";
                        } else {
                            alamat = lines.getString("alamat_kirim");
                            if (!lines.isNull("kecamatan") && !lines.isNull("kota")) {
                                String kec = lines.getJSONObject("kecamatan").getString("name");
                                String kot = lines.getJSONObject("kota").getString("name");
                                alamat = alamat + "\n" + kec + ", " + kot;
                            }
                        }

                        item_order_arr.add(new DataReadyOrder(invoice, id_transaksi, id_item_transaksi, market_id, name, jumlah, terkirim, catatan, driverphone, user_telp,
                                item, alamat, tanggal));
                        //Log.e("cek3", "oke");
                    }

                    hasMore = !next_page.equalsIgnoreCase("null");
                    if (isLoadMore) {
                        //Log.e("cek1", "true...");
                        deleteItem(results.size() - 1);
                        results.addAll(item_order_arr);
                        recyclerView.getAdapter().notifyDataSetChanged();
                    } else {
                        //Log.e("cek1", "first");
                        results = item_order_arr;
                        isLoadMore = hasMore;
                        recyclerView.setAdapter(new ReadyOrderAdapter(results, this, getActivity()));
                    }
                } else {
                    Toast.makeText(getContext(), "Gagal mengambil data", Toast.LENGTH_LONG).show();
                }
            } catch (JSONException e) {
                //e.printStackTrace();
                //Log.e("apa", "oke");
            }
        } else if (REQUEST_CODE == 2) {
            //Log.e("kirim", "ok1");
            try {
                if (jsonObject.getString("response").equalsIgnoreCase("success")) {
                    Toast.makeText(getContext(), "Berhasil memulai pengiriman.", Toast.LENGTH_LONG).show();
                    refreshPage(true);
                    //deleteItem(_pos);

                } else {
                    Toast.makeText(getContext(), "Gagal memulai pengiriman", Toast.LENGTH_LONG).show();
                }
            } catch (JSONException e) {
                //e.printStackTrace();
                Toast.makeText(getContext(), "Kesalahan sistem", Toast.LENGTH_LONG).show();
            }
        }
    }


    private DBDriver db;
    private List<Driver> dlist;

    private void addDriverList(Spinner spinner) {
        String[] driverName = new String[db.getDriverCount()];
        String[] driverPhoneNumber = new String[db.getDriverCount()];
        dlist = db.getAllDrivers();
        if (db.getDriverCount() == 0) {
            Toast.makeText(getContext(), "Harap mengisi data armada terlebih dahulu", Toast.LENGTH_SHORT).show();
        }


        for (int i = 0; i < db.getDriverCount(); i++) {
            driverName[i] = dlist.get(i).getNama();
            driverPhoneNumber[i] = dlist.get(i).getTelepon();
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_dropdown_item, driverName);
        spinner.setAdapter(adapter);
    }


    @Override
    public void onConfirmOrderClicked(DataPendingOrder item, int position) {

    }

    int _pos;

    @Override
    public void onSendOrderClicked(final DataReadyOrder item, int position) {
        _pos = position;
        final Dialog _dialog = new Dialog(getContext());
        _dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        _dialog.setContentView(R.layout.dialog_delivery);
        final Button send = _dialog.findViewById(R.id.ok_kirim);
        final Button cancel = _dialog.findViewById(R.id.cancel);
        final Spinner spinner = _dialog.findViewById(R.id.driver);
        final EditText jml_ekor = _dialog.findViewById(R.id.jml_ekor);
        final EditText note = _dialog.findViewById(R.id.note_courier);
        addDriverList(spinner);

        jml_ekor.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (jml_ekor.getText().toString().equalsIgnoreCase(""))
                    jml_ekor.setText("0");

                int j = Integer.parseInt(jml_ekor.getText().toString());

                if (j > item.getJumlah()) {
                    jml_ekor.setText(String.valueOf(item.getJumlah()));
                }
            }
        });

        send.setOnClickListener(v -> {
            if (dlist.size() != 0) {
                //Log.e("kirim", String.valueOf(item.getSales_order_line_id()));
                //Log.e("kirim", String.valueOf(item.getMarket_id()));

                send.setEnabled(false);
                Driver sender = dlist.get(spinner.getSelectedItemPosition());
                //Log.e("kirim", sender.getNama());
                //Log.e("kirim", sender.getTelepon());

                RequestBody data = new FormBody.Builder()
                        .add("sales_order_line_id", String.valueOf(item.getSales_order_line_id()))
                        .add("market_id", String.valueOf(item.getMarket_id()))
                        .add("terkirim", "0")
                        .add("courier_name", sender.getNama())
                        .add("courier_phone", sender.getTelepon())
                        .add("courier_carry", jml_ekor.getText().toString())
                        .add("courier_note", note.getText().toString())
                        .build();
                PostRequest postRequest = new PostRequest(ReadyOrderFragment.this);
                postRequest.SendPostData(EndpointAPI.sendOrder, prefManager.getToken(), data, 2);
                postRequest.execute();
                _dialog.cancel();
            } else {
                Toast.makeText(getContext(), "Harap mengisi data armada terlebih dahulu", Toast.LENGTH_SHORT).show();
            }

        });

        cancel.setOnClickListener(v -> _dialog.cancel());
        _dialog.show();
    }

    @Override
    public void onCompleteOrderClicked(DataProcessOrder item, int position) {

    }

    @Override
    public void refreshPage(boolean isNew) {
        getData(EndpointAPI.getReady_Order, "", false);
    }
}
