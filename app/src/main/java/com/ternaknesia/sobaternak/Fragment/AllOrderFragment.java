package com.ternaknesia.sobaternak.Fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.ternaknesia.sobaternak.Adapter.AllOrderAdapter;
import com.ternaknesia.sobaternak.DataModel.DataAllOrder;
import com.ternaknesia.sobaternak.DataModel.SalesOrder.Data;
import com.ternaknesia.sobaternak.DataModel.SalesOrder.ResponseOrder;
import com.ternaknesia.sobaternak.DataModel.SalesOrder.ResultAllOrder;
import com.ternaknesia.sobaternak.Manager.PrefManager;
import com.ternaknesia.sobaternak.R;
import com.ternaknesia.sobaternak.Service.retro.ApiClient;
import com.ternaknesia.sobaternak.Service.retro.modul.SalesOrderService;
import com.ternaknesia.sobaternak.Utils.Footer;
import com.ternaknesia.sobaternak.Utils.Item;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AllOrderFragment extends Fragment implements FragmentLifecycle, SwipeRefreshLayout.OnRefreshListener {

    @BindView(R.id.searchBar)
    EditText searchBar;
    @BindView(R.id.imageView)
    ImageView imageView;
    @BindView(R.id.c_search)
    CardView cSearch;
    @BindView(R.id.my_rec1)
    RecyclerView recyclerView;
    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;
    Unbinder unbinder;

    SalesOrderService service = ApiClient.getClient().create(SalesOrderService.class);
    private PrefManager prefManager;
    private List<Item> results = new ArrayList<>();
    private boolean hasMore = false;
    private Integer page = 1;
    private Boolean isLoadMore = false;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_order_all, container, false);
        unbinder = ButterKnife.bind(this, view);

        prefManager = new PrefManager(Objects.requireNonNull(getContext()));
        swipeRefreshLayout.setOnRefreshListener(this);

        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                if (Objects.requireNonNull(layoutManager).findLastCompletelyVisibleItemPosition() == layoutManager.getItemCount() - 2) {
                    Log.d("Pos", "onScrolled: " + (hasMore) + "-" + !hasFooter() + " -> " + page);
                    if (hasMore && (!hasFooter())) {
                        results.add(new Footer());
                        Objects.requireNonNull(recyclerView.getAdapter()).notifyDataSetChanged();
                        getData(page, searchBar.getText().toString());
                        //Log.e("url", next_page);
                        Log.e("load", "load more");
                    }
                }
                if (layoutManager.findFirstCompletelyVisibleItemPosition() == 0) {
                    swipeRefreshLayout.setEnabled(true);
//                    swipeRefreshLayout.setRefreshing(true);
                } else {
                    swipeRefreshLayout.setEnabled(false);
                }
            }
        });

        searchBar.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                results = new ArrayList<>();
                hasMore = false;
                isLoadMore = false;
                Objects.requireNonNull(recyclerView.getAdapter()).notifyDataSetChanged();

                page = 1;
                getData(page, searchBar.getText().toString());
                return true;
            }
            return false;
        });

        imageView.setOnClickListener(v -> {
            results = new ArrayList<>();
            hasMore = false;
            isLoadMore = false;
            page = 1;
            Objects.requireNonNull(recyclerView.getAdapter()).notifyDataSetChanged();

            getData(page, "");
        });

        refreshPage(false);
        return view;
    }

    private void getData(int page, String query) {
        Call<ResponseOrder> getAllData = service.getAllOrderList("Bearer " + prefManager.getToken(), page, query);
        getAllData.enqueue(new Callback<ResponseOrder>() {
            @Override
            public void onResponse(@NonNull Call<ResponseOrder> call, @NonNull Response<ResponseOrder> response) {
                swipeRefreshLayout.setRefreshing(false);
                if (response.code() == 201) {
                    if (page == 1) {
                        results.clear();
                        isLoadMore = false;
                    }
                    ResultAllOrder resultAllOrder = Objects.requireNonNull(response.body()).getResultAllOrder();
                    ArrayList<Item> item_order_arr = new ArrayList<>();
                    for (Data data : resultAllOrder.getData()) {
                        String alamat;
                        if (data.getIsDisalurkan() == 1) {
                            alamat = "Disalurkan";
                        } else {
                            alamat = data.getAlamatKirim();
                            if (data.getKecamatan() != null && data.getKota() != null) {
                                String kec = data.getKecamatan().getName();
                                String kot = data.getKota().getName();
                                alamat = alamat + "\n" + kec + ", " + kot;
                            }
                        }

                        item_order_arr.add(new DataAllOrder(data.getSalesOrderId(), data.getNoInvoice(), data.getName(),
                                data.getTerbayar(), data.getCatatan(), data.getTelp(), alamat, data.getWaktuKirim(), data.getTanggal(),
                                data.getStatus(), data.getDeliveryStatus(), data.getLines(), data.getDiskon(), data.getGrandTotal(), Integer.parseInt(data.getMinDownpayment()),
                                data.getKodeUnik()));
                    }
                    if (AllOrderFragment.this.page < resultAllOrder.getLastPage()) {
                        AllOrderFragment.this.page++;
                        hasMore = true;
                    } else hasMore = false;
                    if (isLoadMore) {
                        deleteItem(results.size() - 1);
                        results.addAll(item_order_arr);
                        Objects.requireNonNull(recyclerView.getAdapter()).notifyDataSetChanged();
                    } else {
                        results = item_order_arr;
                        isLoadMore = hasMore;
                        recyclerView.setAdapter(new AllOrderAdapter(results, this, getActivity()));
                    }
                } else if (response.code() == 200) {
                    Toast.makeText(getContext(), "Unauthorized", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getContext(), response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseOrder> call, @NonNull Throwable t) {
                swipeRefreshLayout.setRefreshing(false);
                t.printStackTrace();
                call.cancel();
                Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private boolean hasFooter() {
        return results.get(results.size() - 1) instanceof Footer;
    }

    private void deleteItem(int pos) {
        results.remove(pos);
        Objects.requireNonNull(recyclerView.getAdapter()).notifyDataSetChanged();
    }

    @Override
    public void refreshPage(boolean isNew) {
        page = 1;
        getData(page, "");
    }

    @Override
    public void onRefresh() {
        page = 1;
        getData(page, "");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.btn_filter5)
    public void onViewClicked() {
    }
}
