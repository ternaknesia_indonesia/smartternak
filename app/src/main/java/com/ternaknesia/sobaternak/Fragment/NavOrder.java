package com.ternaknesia.sobaternak.Fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ternaknesia.sobaternak.R;
import com.ternaknesia.sobaternak.Service.MyFirebaseMessagingService;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class NavOrder extends Fragment {

    private final String TAG = NavOrder.class.getSimpleName();
    private FragmentManager fragmentManager;
    private ViewPagerAdapter adapter;


    private BroadcastReceiver receiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            onResume();
            Log.d(TAG, "onReceive: is called");
            Log.d(TAG, "onReceive data: " + intent.getStringExtra(MyFirebaseMessagingService.REQUEST_DATA));
            adapter.notifyDataSetChanged();
        }
    };

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.nav_order, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        TabLayout tabLayout = view.findViewById(R.id.tabs);
        ViewPager viewPager = view.findViewById(R.id.viewpager);

        fragmentManager = Objects.requireNonNull(getActivity()).getSupportFragmentManager();
        setupViewPager(viewPager);
//        viewPager.setOffscreenPageLimit(4);
        tabLayout.setupWithViewPager(viewPager);
    }

    private void setupViewPager(ViewPager viewPager) {
        adapter = new ViewPagerAdapter(fragmentManager);
        Fragment fAll = new AllOrderFragment();
        Fragment fPending = new PendingOrderFragment();
        Fragment fReady = new ReadyOrderFragment();
        Fragment fProcess = new ProcessOrderFragment();
        Fragment fFinish = new FinishOrderFragment();

        adapter.addFragment(fAll, "Semua");
        adapter.addFragment(fPending, "Pending");
        adapter.addFragment(fReady, "Siap");
        adapter.addFragment(fProcess, "Proses");
        adapter.addFragment(fFinish, "Selesai");

        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(pageChangeListener);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public int getItemPosition(@NonNull Object object) {
            return POSITION_NONE;
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    private ViewPager.OnPageChangeListener pageChangeListener = new ViewPager.OnPageChangeListener() {

        int currentPosition = 0;

        @Override
        public void onPageSelected(int newPosition) {
            currentPosition = newPosition;
            FragmentLifecycle fragment = (FragmentLifecycle) adapter.getItem(currentPosition);
            fragment.refreshPage(true);
        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {
        }

        public void onPageScrollStateChanged(int arg0) {
        }
    };

    @Override
    public void onStart() {
        super.onStart();
        LocalBroadcastManager.getInstance(Objects.requireNonNull(getActivity())).registerReceiver((receiver),
                new IntentFilter(MyFirebaseMessagingService.REQUEST_RELOAD));
    }

    @Override
    public void onStop() {
        super.onStop();
        LocalBroadcastManager.getInstance(Objects.requireNonNull(getActivity())).unregisterReceiver(receiver);
    }

}
