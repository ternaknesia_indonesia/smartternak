package com.ternaknesia.sobaternak.Fragment;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;

import com.ternaknesia.sobaternak.Adapter.FinishOrderAdapter;
import com.ternaknesia.sobaternak.DataModel.sales_order.FinishResponse;
import com.ternaknesia.sobaternak.Manager.PrefManager;
import com.ternaknesia.sobaternak.R;
import com.ternaknesia.sobaternak.Service.retro.ApiClient;
import com.ternaknesia.sobaternak.Service.retro.modul.SalesOrderService;

import java.util.ArrayList;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.support.constraint.Constraints.TAG;

public class FinishOrderFragment extends Fragment implements FragmentLifecycle {

    @BindView(R.id.searchBar)
    EditText searchView;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    Unbinder unbinder;
    private PrefManager prefManager;
    private SalesOrderService service;
    private FinishOrderAdapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private int page = 1;

    private boolean isHasNext = true;

    public FinishOrderFragment() {
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_order_finish, container, false);
        unbinder = ButterKnife.bind(this, view);

        prefManager = new PrefManager(Objects.requireNonNull(getContext()));

        service = ApiClient.getClient().create(SalesOrderService.class);

        layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setNestedScrollingEnabled(false);

        adapter = new FinishOrderAdapter(new ArrayList<>());
        recyclerView.setAdapter(adapter);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (!recyclerView.canScrollVertically(1) && isHasNext && !adapter.isLoading()) {
                    recyclerView.post(() -> {
                        adapter.addLoading();
                        refreshPage(false);
                    });
                }
            }
        });

        searchView.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                refreshPage(true);
                return true;
            }
            return false;
        });

        refreshPage(true);
        return view;
    }

    @Override
    public void refreshPage(boolean isNew) {
        if(isNew)page=1;
        getFinishOrderList(isNew);
    }

    void getFinishOrderList(boolean isNew) {
        Call<FinishResponse> getFinishOrderList = service.getFinishOrderList("Bearer " + prefManager.getToken(), page, searchView.getText().toString());
        getFinishOrderList.enqueue(new Callback<FinishResponse>() {
            @Override
            public void onResponse(Call<FinishResponse> call, Response<FinishResponse> response) {
                if (response.code() == 201) {
                    if (isNew) adapter.getAll().clear();
                    else adapter.removeLoading();
                    adapter.getAll().addAll(response.body().getResult().getData());
                    adapter.notifyDataSetChanged();
                    page++;
                    if (response.body().getResult().getCurrentPage().equals(response.body().getResult().getLastPage()))
                        isHasNext = false;
                } else {
                    Log.e(TAG, "onResponse: " + response.message());
                }
            }

            @Override
            public void onFailure(Call<FinishResponse> call, Throwable t) {
                call.cancel();
                Log.e(TAG, "onResponse: " + t.getMessage());
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.imageView, R.id.recycler_view})
    public void onViewClicked(View view) {
        if (view.getId() == R.id.imageView) {
            refreshPage(true);
        }
    }
}
