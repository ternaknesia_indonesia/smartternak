package com.ternaknesia.sobaternak.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ternaknesia.sobaternak.Adapter.PendingOrderAdapter;
import com.ternaknesia.sobaternak.DataModel.DataPendingOrder;
import com.ternaknesia.sobaternak.DataModel.DataProcessOrder;
import com.ternaknesia.sobaternak.DataModel.DataReadyOrder;
import com.ternaknesia.sobaternak.DataModel.SalesOrder.DataPending;
import com.ternaknesia.sobaternak.DataModel.SalesOrder.ResponsePendingOrder;
import com.ternaknesia.sobaternak.DataModel.SalesOrder.ResultPendingOrder;
import com.ternaknesia.sobaternak.DataModel.SalesOrder.SalesOrder;
import com.ternaknesia.sobaternak.Interface.OnClickInterface;
import com.ternaknesia.sobaternak.Manager.PrefManager;
import com.ternaknesia.sobaternak.R;
import com.ternaknesia.sobaternak.Service.retro.ApiClient;
import com.ternaknesia.sobaternak.Service.retro.modul.SalesOrderService;
import com.ternaknesia.sobaternak.Utils.Footer;
import com.ternaknesia.sobaternak.Utils.Item;
import com.ternaknesia.sobaternak.activity.SiapkanOrderActivity;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PendingOrderFragment extends Fragment implements OnClickInterface, FragmentLifecycle, SwipeRefreshLayout.OnRefreshListener {
    @BindView(R.id.searchBar)
    EditText searchView;
    @BindView(R.id.imageView)
    ImageView imageView;
    @BindView(R.id.c_search)
    CardView cSearch;
    @BindView(R.id.my_rec)
    RecyclerView recyclerView;
    @BindView(R.id.btn_filter)
    ImageButton filter;
    @BindView(R.id.btm)
    LinearLayout btm;
    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;
    Unbinder unbinder;

    SalesOrderService service = ApiClient.getClient().create(SalesOrderService.class);
    private PrefManager prefManager;
    private List<Item> results=new ArrayList<>();
    private boolean hasMore = false;
    private Boolean isLoadMore;
    private int page = 1;

    public PendingOrderFragment() {}

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_order_pending, container, false);
        unbinder = ButterKnife.bind(this, view);
        prefManager = new PrefManager(Objects.requireNonNull(getContext()));

        isLoadMore = false;

        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                if (Objects.requireNonNull(layoutManager).findLastCompletelyVisibleItemPosition() == layoutManager.getItemCount() - 2) {
                    Log.d("Pos", "onScrolled: " + (hasMore) + "-" + !hasFooter() + " -> " + page);
                    if (hasMore && (!hasFooter())) {
                        results.add(new Footer());
                        Objects.requireNonNull(recyclerView.getAdapter()).notifyDataSetChanged();
                        getData(page, searchView.getText().toString());
                        //Log.e("url", next_page);
                        Log.e("load", "load more");
                    }
                }
                if (layoutManager.findFirstCompletelyVisibleItemPosition() == 0) {
                    swipeRefreshLayout.setEnabled(true);
//                    swipeRefreshLayout.setRefreshing(true);
                } else {
                    swipeRefreshLayout.setEnabled(false);
                }
            }
        });

        searchView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    results = new ArrayList<>();
                    hasMore = false;
                    isLoadMore = false;
                    Objects.requireNonNull(recyclerView.getAdapter()).notifyDataSetChanged();

                    page = 1;
                    getData(page, searchView.getText().toString());
                    return true;
                }
                return false;
            }
        });

        imageView.setOnClickListener(v -> {
            results = new ArrayList<>();
            hasMore = false;
            isLoadMore = false;
            page = 1;
            Objects.requireNonNull(recyclerView.getAdapter()).notifyDataSetChanged();

            getData(page, "");
        });

        filter.setOnClickListener(v -> {

        });

        Log.d("Token", "onCreateView: " + prefManager.getToken());
        refreshPage(false);
        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

    }

    private void getData(int page, String query) {
        Call<ResponsePendingOrder> getAllData = service.getPendingOrderList("Bearer " + prefManager.getToken(), page, query);
        getAllData.enqueue(new Callback<ResponsePendingOrder>() {
            @Override
            public void onResponse(@NonNull Call<ResponsePendingOrder> call, @NonNull Response<ResponsePendingOrder> response) {
                swipeRefreshLayout.setRefreshing(false);
                if (response.code() == 201) {
                    if (page == 1) {
                        results.clear();
                        isLoadMore = false;
                    }
                    ResultPendingOrder resultPending = Objects.requireNonNull(response.body()).getResult();
                    ArrayList<Item> item_order_arr = new ArrayList<>();
                    for (DataPending data : resultPending.getData()) {
                        SalesOrder salesOrder = data.getSalesOrder();
                        String alamat;
                        if (data.getIsDisalurkan() == 1) {
                            alamat = "Disalurkan";
                        } else {
                            alamat = data.getAlamatKirim();
                            if (salesOrder.getKecamatan() != null && salesOrder.getKota() != null) {
                                String kec = salesOrder.getKecamatan().getName();
                                String kot = salesOrder.getKota().getName();
                                alamat = alamat + "\n" + kec + ", " + kot;
                            }
                        }

                        item_order_arr.add(new DataPendingOrder(data.getNoInvoice(), data.getSoid(), data.getSalesOrderId(), data.getSalesOrderLineId(), data.getMarketId(), data.getName(), data.getJumlah(), data.getTerkirim(), data.getCatatan(), data.getCourierPhone(), salesOrder.getTelp(),
                                data.getNama(), alamat, data.getWaktuKirim()));
                    }
                    if (PendingOrderFragment.this.page < resultPending.getLastPage()) {
                        PendingOrderFragment.this.page++;
                        hasMore = true;
                    } else hasMore = false;
                    if (isLoadMore) {
                        deleteItem(results.size() - 1);
                        results.addAll(item_order_arr);
                        Objects.requireNonNull(recyclerView.getAdapter()).notifyDataSetChanged();
                    } else {
                        results = item_order_arr;
                        isLoadMore = hasMore;
                        recyclerView.setAdapter(new PendingOrderAdapter(results, PendingOrderFragment.this, getActivity()));
                    }
                } else if (response.code() == 200) {
                    Toast.makeText(getContext(), "Unauthorized", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getContext(), response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponsePendingOrder> call, @NonNull Throwable t) {
                swipeRefreshLayout.setRefreshing(false);
                t.printStackTrace();
                call.cancel();
                Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private boolean hasFooter() {
        return results.get(results.size() - 1) instanceof Footer;
    }


    private void deleteItem(int pos) {
        results.remove(pos);
        Objects.requireNonNull(recyclerView.getAdapter()).notifyDataSetChanged();
    }

    @Override
    public void onConfirmOrderClicked(DataPendingOrder item, int position) {
        Intent intent = new Intent(getActivity(), SiapkanOrderActivity.class);
        intent.putExtra("item", item);
        intent.putExtra("qty", item.getJumlah());
        startActivity(intent);
    }

    @Override
    public void onSendOrderClicked(DataReadyOrder item, int position) {

    }

    @Override
    public void onCompleteOrderClicked(DataProcessOrder item, int position) {

    }


    @Override
    public void refreshPage(boolean isNew) {
        page = 1;
        getData(page, "");
    }

    @Override
    public void onRefresh() {
        page = 1;
        getData(page, "");
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.btn_filter)
    public void onViewClicked() {
    }
}
