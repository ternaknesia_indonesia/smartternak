package com.ternaknesia.sobaternak.Fragment;

public interface FragmentLifecycle {
    void refreshPage(boolean isNew);
}