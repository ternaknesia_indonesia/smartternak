package com.ternaknesia.sobaternak.Fragment;
/*        Mohon untuk tidak melakukan hal-hal yang melanggar hukum.
        Anda tidak berhak melakukan cracking terhadap system kami tanpa ada persetujuan.
        Terimakasih*/

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.ternaknesia.sobaternak.activity.ContactUsActivity;
import com.ternaknesia.sobaternak.activity.LoginActivity;
import com.ternaknesia.sobaternak.activity.TentangAppsActivity;
import com.ternaknesia.sobaternak.Connection.PostRequest;
import com.ternaknesia.sobaternak.Interface.EndpointAPI;
import com.ternaknesia.sobaternak.Interface.RequestComplete;
import com.ternaknesia.sobaternak.Manager.PrefManager;
import com.ternaknesia.sobaternak.R;
import com.ternaknesia.sobaternak.Utils.RoundedCornersTransformation;

import org.json.JSONObject;

public class NavAccount extends Fragment implements RequestComplete {
    PrefManager prefManager;
    PostRequest postRequest = null;
    RelativeLayout content;
    private TextView nav_user, nav_email;
    private ImageView nav_avatar;
    private LinearLayout btHubKami, btSyarat, btTentangApps, btShareApps, btRateApps, btLogout;
    private Toolbar toolbar;

    //private File actualImage;
    //private File compressedImage;

    public static NavAccount newInstance() {
        NavAccount fragment = new NavAccount();

        return fragment;
    }


    public NavAccount() {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.nav_account, container, false);
        prefManager = new PrefManager(getContext());
        init(rootView);

        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);

        nav_user.setText(prefManager.getSingleDataProfile("name"));
        nav_email.setText(prefManager.getSingleDataProfile("email"));

        Glide.with(getContext())
                .load(EndpointAPI.HostStorage + prefManager.getSingleDataProfile("avatar"))
                .placeholder(R.drawable.placeholder_avatar)
                .centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .skipMemoryCache(true)
                .bitmapTransform(new RoundedCornersTransformation(getContext(), 10, 0))
                .into(nav_avatar);


/*        btShareApps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                String shareBodyText = "Halo sobat, Saya ingin mengajak Anda untuk mewujudkan peternakan milenial melalui Ternaknesia. " +
                        "Yuk unduh aplikasinya di Play Store : https://play.google.com/store/apps/details?id=com.ternaknesia.app" +
                        "\nSalam peternakan milenial.";
                sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "Ternaknesia");
                sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBodyText);
                startActivity(Intent.createChooser(sharingIntent, "Bagikan aplikasi Ternaknesia"));
            }
        });*/

        btRateApps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.ternaknesia.sobaternak"));
                startActivity(browserIntent);
            }
        });

        btHubKami.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent a = new Intent(getContext(), ContactUsActivity.class);
                startActivity(a);
            }
        });

/*        btSyarat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Intent a = new Intent(getContext(), KetentuanActivity.class);
                //startActivity(a);

            }
        });*/

        btTentangApps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent a = new Intent(getContext(), TentangAppsActivity.class);
                startActivity(a);
            }
        });

        btLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setMessage("Anda yakin akan keluar dari akun Anda?")
                        .setCancelable(false)
                        .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                prefManager.logout();
                                Intent intent = new Intent(getContext(), LoginActivity.class);
                                getActivity().finish();
                                startActivity(intent);
                            }
                        })
                        .setNegativeButton("Batal", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();
            }
        });

        return rootView;
    }

    private void init(View rootView) {
        nav_user = rootView.findViewById(R.id.name);
        nav_email = rootView.findViewById(R.id.email);
        nav_avatar = rootView.findViewById(R.id.profile_image);
        btHubKami = rootView.findViewById(R.id.btHubKami);
        btSyarat = rootView.findViewById(R.id.btSyarat);
        btTentangApps = rootView.findViewById(R.id.btTentangApps);
        btShareApps = rootView.findViewById(R.id.btShareApp);
        btRateApps = rootView.findViewById(R.id.btRateUs);
        btLogout = rootView.findViewById(R.id.btLogout);
        toolbar = rootView.findViewById(R.id.toolbar);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_fragment_account, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int idMenu = item.getItemId();
        switch (idMenu) {
            case R.id.menu_edit_account:
/*                Intent e = new Intent(getContext(), EditProfileActivity.class);
                getContext().startActivity(e);*/
                //Intent intent = new Intent(getActivity(), EditProfileActivity.class);
                //startActivityForResult(intent, 101);
                break;
            case R.id.menu_chpass_account:
                //Intent c = new Intent(getContext(), ChPassActivity.class);
                //getContext().startActivity(c);
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRequestComplete(JSONObject jsonObject, Integer CODE_REQUEST) {

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if ((requestCode == 101) && (resultCode == Activity.RESULT_OK)) {
            // recreate your fragment here

            FragmentTransaction ft = getFragmentManager().beginTransaction();
            ft.detach(this).attach(this).commit();
        }
    }
}

