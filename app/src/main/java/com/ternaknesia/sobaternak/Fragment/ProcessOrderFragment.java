package com.ternaknesia.sobaternak.Fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.ternaknesia.sobaternak.Adapter.ProcessOrderAdapter;
import com.ternaknesia.sobaternak.Connection.GetRequest;
import com.ternaknesia.sobaternak.Connection.PostRequest;
import com.ternaknesia.sobaternak.DataModel.DataPendingOrder;
import com.ternaknesia.sobaternak.DataModel.DataProcessOrder;
import com.ternaknesia.sobaternak.DataModel.DataReadyOrder;
import com.ternaknesia.sobaternak.Interface.EndpointAPI;
import com.ternaknesia.sobaternak.Interface.OnClickInterface;
import com.ternaknesia.sobaternak.Interface.RequestComplete;
import com.ternaknesia.sobaternak.Manager.PrefManager;
import com.ternaknesia.sobaternak.R;
import com.ternaknesia.sobaternak.Utils.Footer;
import com.ternaknesia.sobaternak.Utils.Item;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import okhttp3.FormBody;
import okhttp3.RequestBody;


/**
 * A simple {@link Fragment} subclass.
 */
public class ProcessOrderFragment extends Fragment implements RequestComplete, OnClickInterface, FragmentLifecycle{
    private RecyclerView recyclerView;
    private PrefManager prefManager;
    private Context mContext;
    private List<Item> results;
    private boolean hasMore = false;
    private String next_page = "";
    private Boolean isLoadMore;
    private GetRequest connection;
    private EditText searchView;
    private ImageView imageView;


    public ProcessOrderFragment() {
        // Required empty public constructor
        this.mContext = getActivity();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_order_process, container, false);


        prefManager = new PrefManager(getContext());

        isLoadMore = false;

        recyclerView = view.findViewById(R.id.my_rec);
        searchView = view.findViewById(R.id.searchBar);
        imageView = view.findViewById(R.id.imageView);

        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (hasMore && (!hasFooter())) {
                    LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                    //position starts at 0
                    if (layoutManager.findLastCompletelyVisibleItemPosition() == layoutManager.getItemCount() - 2) {
                        /*asyncTask = new BackgroundTask();
                        asyncTask.execute((Void[]) null);*/
                        results.add(new Footer());
                        recyclerView.getAdapter().notifyDataSetChanged();
                        getData(next_page, searchView.getText().toString(), true);
                        //Log.e("url", next_page);
                        //Log.e("load", "load more");
                    }
                }
            }
        });

        searchView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    //Log.e("cek", query);
                    results = new ArrayList<>();
                    hasMore = false;
                    isLoadMore = false;
                    next_page = "";
                    recyclerView.getAdapter().notifyDataSetChanged();

                    getData(EndpointAPI.getProcess_Order, searchView.getText().toString(), false);
                    return true;
                }
                return false;
            }
        });

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                results = new ArrayList<>();
                hasMore = false;
                isLoadMore = false;
                next_page = "";
                recyclerView.getAdapter().notifyDataSetChanged();

                getData(EndpointAPI.getProcess_Order, searchView.getText().toString(), false);
            }
        });

        refreshPage(true);
        return view;
    }


    private void getData(String url, String query, boolean is_paging) {
        connection = new GetRequest(this);
        if(is_paging)
            connection.SendGetData(url + "&q=" + query, prefManager.getToken(), 1);
        else connection.SendGetData(url + "?q=" + query, prefManager.getToken(), 1);
        connection.execute();
    }

    private boolean hasFooter() {
        return results.size() > 0 && results.get(results.size() - 1) instanceof Footer;
    }

    private void deleteItem(int pos) {
        results.remove(pos);
        recyclerView.getAdapter().notifyDataSetChanged();
    }

    @Override
    public void onRequestComplete(JSONObject jsonObject, Integer REQUEST_CODE) {
        //Log.e("cek", jsonObject.toString());
        if (REQUEST_CODE == 1) {
            ArrayList<Item> item_order_arr;
            try {
                if (jsonObject.getString("response").equalsIgnoreCase("success")) {
                    JSONObject res = jsonObject.getJSONObject("result");
                    JSONArray data = res.getJSONArray("data");
                    //Log.e("cek1", "oke");
                    item_order_arr = new ArrayList<>();

                    for (int d = 0; d < data.length(); d++) {
                        JSONObject item_order = data.getJSONObject(d);

                        String invoice = item_order.getString("no_invoice");
                        int id_transaksi = item_order.getInt("sales_order_id");
                        int id_item_transaksi = item_order.getInt("sales_order_line_id");
                        int terkirim = item_order.getInt("terkirim");
                        String courier_name = item_order.getString("courier_name");
                        String driverphone = item_order.getString("courier_phone");
                        String user_telp = item_order.getJSONObject("sales_order").getString("telp");
                        String item = item_order.getString("nama");
                        int market_id = item_order.getInt("market_id");
                        int jumlah = item_order.getInt("jumlah");
                        int carry = item_order.getInt("courier_carry");
                        String courier_note = item_order.getString("courier_note");
                        //Log.e("cek2", "oke");
                        JSONObject lines = item_order.getJSONObject("sales_order");
                        String name = lines.getString("name");
                        String catatan = lines.getString("catatan");
                        String tanggal = lines.getString("waktu_kirim");

                        int is_disalurkan = lines.getInt("is_disalurkan");
                        String alamat;
                        if (is_disalurkan == 1) {
                            alamat = "Disalurkan";
                        } else {
                            alamat = lines.getString("alamat_kirim");
                            if(!lines.isNull("kecamatan") && !lines.isNull("kota")){
                                String kec = lines.getJSONObject("kecamatan").getString("name");
                                String kot = lines.getJSONObject("kota").getString("name");
                                alamat = alamat + "\n" + kec + ", " + kot;
                            }
                        }

                        item_order_arr.add(new DataProcessOrder(invoice, id_transaksi, id_item_transaksi, market_id, name, jumlah, terkirim, catatan, courier_name, driverphone, user_telp,
                                item, alamat, tanggal, carry, courier_note));
                    }

                    hasMore = !next_page.equalsIgnoreCase("null");
                    if (isLoadMore) {
                        //Log.e("cek1", "true...");
                        if (results.size()>0)deleteItem(results.size() - 1);
                        results.addAll(item_order_arr);
                        recyclerView.getAdapter().notifyDataSetChanged();
                    } else {
                        //Log.e("cek1", "first");
                        results = item_order_arr;
                        isLoadMore = hasMore;
                        recyclerView.setAdapter(new ProcessOrderAdapter(results, this, getActivity()));
                    }
                } else {
                    Toast.makeText(getContext(), "Gagal mengambil data", Toast.LENGTH_LONG).show();
                }
            } catch (JSONException e) {
                //e.printStackTrace();
            }
        } else if (REQUEST_CODE == 2) {
            try {
                if (jsonObject.getString("response").equalsIgnoreCase("success")) {
                    refreshPage(true);
                    Toast.makeText(getContext(), "Berhasil konfirmasi.", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getContext(), "Gagal konfirmasi", Toast.LENGTH_LONG).show();
                }
            } catch (JSONException e) {
                //e.printStackTrace();
                Toast.makeText(getContext(), "Kesalahan sistem", Toast.LENGTH_LONG).show();
            }
        }
    }

    private void sendItem(DataProcessOrder item, AlertDialog dialog, String s) {
        int terkirim_ = item.getTerkirim();
        int carry = item.getCarry();
        int jml = item.getJumlah();

        int done = terkirim_ + Integer.parseInt(s);

        RequestBody data = new FormBody.Builder()
                .add("sales_order_line_id", String.valueOf(item.getSales_order_line_id()))
                .add("terkirim", String.valueOf(done))
                .add("courier_name", item.getCourier_name())
                .add("courier_phone", item.getDriver_phone())
                .add("courier_carry", String.valueOf(item.getCarry()))
                .add("courier_note", item.getCourier_note())
                .build();
        PostRequest postRequest = new PostRequest(this);
        postRequest.SendPostData(EndpointAPI.sendOrder, prefManager.getToken(), data, 2);
        postRequest.execute();

        /*isLoadMore = false;
        results.clear();
        recyclerView.getAdapter().notifyDataSetChanged();
        getData(EndpointAPI.getProcess_Order);*/

        dialog.cancel();

    }

    @Override
    public void onConfirmOrderClicked(DataPendingOrder item, int position) {

    }

    @Override
    public void onSendOrderClicked(DataReadyOrder item, int position) {

    }

    @Override
    public void onCompleteOrderClicked(final DataProcessOrder item, int position) {
        AlertDialog.Builder mBuilder = new AlertDialog.Builder(getContext());
        View mView = getLayoutInflater().inflate(R.layout.confirm_layout, null);

        Button addAll, save;
        final TextView belumTerkirim;
        final EditText terkirim;

        addAll = mView.findViewById(R.id.add_all);
        save = mView.findViewById(R.id.save);
        belumTerkirim = mView.findViewById(R.id.belum_terkirim);
        terkirim = mView.findViewById(R.id.terkirim);

        belumTerkirim.setText(String.valueOf(item.getJumlah() - item.getTerkirim()));
        mBuilder.setView(mView);
        final AlertDialog dialog = mBuilder.create();
        dialog.show();

        addAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                terkirim.setText(belumTerkirim.getText().toString());
            }
        });


        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (terkirim.getText().toString().isEmpty())
                    Toast.makeText(getContext(), "Harap isi jumlah yang sudah dikonfirmasi", Toast.LENGTH_SHORT).show();
                else if (Integer.parseInt(terkirim.getText().toString()) > item.getCarry()) {
                    Toast.makeText(getContext(), "Melebihi jumlah yang dikirim", Toast.LENGTH_SHORT).show();
                } else {
                    //if(item.getType()==0)
                    sendItem(item, dialog, terkirim.getText().toString());
                    //else sendItemOffline(item, dialog, terkirim.getText().toString());
                }
            }
        });
    }

    @Override
    public void refreshPage(boolean isNew) {
        prefManager = new PrefManager(getContext());
        getData(EndpointAPI.getProcess_Order, "", false);
    }
}
