package com.ternaknesia.sobaternak.Fragment;

import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ternaknesia.sobaternak.Adapter.AllNewsAdapter;
import com.ternaknesia.sobaternak.Adapter.TopNewsAdapter;
import com.ternaknesia.sobaternak.Connection.GetRequest;
import com.ternaknesia.sobaternak.DataModel.DataNews;
import com.ternaknesia.sobaternak.Interface.EndpointAPI;
import com.ternaknesia.sobaternak.Interface.RequestComplete;
import com.ternaknesia.sobaternak.Manager.PrefManager;
import com.ternaknesia.sobaternak.R;
import com.ternaknesia.sobaternak.Utils.CheckConnection;
import com.ternaknesia.sobaternak.Utils.Footer;
import com.ternaknesia.sobaternak.Utils.Item;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class NavFeeds extends Fragment implements RequestComplete, SwipeRefreshLayout.OnRefreshListener{
    RequestComplete mContext;
    PrefManager prefManager;
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private String next_page = "null";

    public NavFeeds() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static NavFeeds newInstance(String param1, String param2) {
        NavFeeds fragment = new NavFeeds();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    ArrayList<Item> contentAdding = null;
    ArrayList<Item> contentAdding2 = null;
    private boolean hasMore = false;
    GetRequest connection, connection2;
    Boolean isLoadMore;
    AllNewsAdapter a;

    NestedScrollView mNestedScrollView;
    RecyclerView mRecyclerView1;
    RecyclerView mRecyclerView2;
    SwipeRefreshLayout swipeRefreshLayout;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.nav_feeds, container, false);
        mContext = this;

        mNestedScrollView = rootView.findViewById(R.id.nestedScroll);
        mRecyclerView1 = rootView.findViewById(R.id.my_recycler_view_1);
        mRecyclerView2 = rootView.findViewById(R.id.my_recycler_view_2);
        swipeRefreshLayout = rootView.findViewById(R.id.swipe_refresh_layout);


        isLoadMore = false;
        prefManager = new PrefManager(getContext());

        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getContext(), 2);
        mRecyclerView1.setLayoutManager(mLayoutManager);
        mRecyclerView1.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView1.setNestedScrollingEnabled(false);

        RecyclerView.LayoutManager mLayoutManager2 = new GridLayoutManager(getContext(), 1);
        mRecyclerView2.setLayoutManager(mLayoutManager2);
        mRecyclerView2.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView2.setNestedScrollingEnabled(false);

        mNestedScrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                //Log.e("masuk AA", next_page);
                if (v.getChildAt(v.getChildCount() - 1) != null) {
                    //Log.e("masuk BB", next_page);
                    if ((scrollY >= (v.getChildAt(v.getChildCount() - 1).getMeasuredHeight() - v.getMeasuredHeight())) &&
                            scrollY > oldScrollY) {
                        //Log.e("masuk CC", next_page);
                        //Log.e("masuk C1", String.valueOf(hasMore));
                        //Log.e("masuk C2", String.valueOf(!hasFooter()));
                        if (hasMore && (!hasFooter())) {
                            LinearLayoutManager layoutManager = (LinearLayoutManager) mRecyclerView2.getLayoutManager();
                            //position starts at 0
                            //Log.e("masuk C3", String.valueOf(layoutManager.findLastCompletelyVisibleItemPosition()));
                            //Log.e("masuk C4", String.valueOf(layoutManager.getItemCount()));
                            if (layoutManager.findLastCompletelyVisibleItemPosition() == layoutManager.getItemCount() - 1) {
                                contentAdding2.add(new Footer());
                                mRecyclerView2.getAdapter().notifyDataSetChanged();
                                connection2 = new GetRequest(mContext);
                                //Log.e("masuk 1", next_page);
                                connection2.SendGetData(next_page,"", 2);
                                connection2.execute();
                            }
                        }
                    }
                }
            }
        });

        swipeRefreshLayout.setOnRefreshListener(this);

        swipeRefreshLayout.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        swipeRefreshLayout.setRefreshing(true);
                                        getDataNow();
                                    }
                                }
        );

/*        contentNews = new ArrayList<>(4);
        contentNews.add(new DataNews("4", "Harga Daging Ayam Melonjak, Begini Penjelasan Peternak Jawa Barat", R.string.news4, "Tempo.co", "19/01/2018", R.drawable.news_04));
        contentNews.add(new DataNews("1", "Ternak Kambing, Petani di Riau Bisa Beli Rumah", R.string.news1, "Liputan6.com", "11/11/2017", R.drawable.news_01));
        contentNews.add(new DataNews("2", "Kementan Bakal Cabut Izin Usaha Peternak yang Pakai Antibiotik", R.string.news2, "Liputan6.com", "08/11/2017", R.drawable.news_02));
        contentNews.add(new DataNews("3", "Kambing Capai 500 Ekor, Jokowi Bakal Datangi 'Guru' Peternak ke Purworejo", R.string.news3, "Okezone", "24/09/2017", R.drawable.news_03));
        mRecyclerView1.setAdapter(new TopNewsAdapter(contentNews, getContext()));*/

        return rootView;
    }

    private boolean hasFooter() {
        return contentAdding2.get(contentAdding2.size() - 1) instanceof Footer;
    }

    private void getDataNow() {
        boolean internetAvailable = new CheckConnection().isInternetAvailable(getActivity());

        if (!prefManager.getDataTopNews().equalsIgnoreCase("") && prefManager.getDataTopNews() != null) {
            JSONObject a, b;
            try {
                a = new JSONObject(prefManager.getDataTopNews());
                //JSONObject b = a.getJSONObject("result");
                JSONArray c = a.getJSONArray("result");
                int lenArray = c.length();
                contentAdding = new ArrayList<>(lenArray);
                for (int i = 0; i < lenArray; i++) {
                    JSONObject dat = c.getJSONObject(i);
                    int id = dat.getInt("id");
                    int views = dat.getInt("views");
                    String title = dat.getString("title");
                    String image = EndpointAPI.HostStorage + dat.getString("image");


                    JSONObject author = dat.getJSONObject("author");
                    String author_name = author.getString("name");

                    contentAdding.add(new DataNews(String.valueOf(id), title, image, author_name, String.valueOf(views)));
                }

                b = new JSONObject(prefManager.getDataAllNews());
                JSONObject c2 = b.getJSONObject("result");
                JSONArray data = c2.getJSONArray("data");
                int lenArray2 = data.length();
                contentAdding2 = new ArrayList<>(lenArray2);
                for (int i = 0; i < lenArray2; i++) {
                    JSONObject dat = data.getJSONObject(i);
                    int id = dat.getInt("id");
                    int views = dat.getInt("views");
                    String title = dat.getString("title");
                    String image = EndpointAPI.HostStorage + dat.getString("image");


                    JSONObject author = dat.getJSONObject("author");
                    String author_name = author.getString("name");

                    contentAdding2.add(new DataNews(String.valueOf(id), title, image, author_name, String.valueOf(views)));
                }

                mRecyclerView1.setAdapter(new TopNewsAdapter(contentAdding, getContext()));
                mRecyclerView1.getLayoutManager().scrollToPosition(0);

                mRecyclerView2.setAdapter(new AllNewsAdapter(contentAdding2, getContext()));
                mRecyclerView2.getLayoutManager().scrollToPosition(0);

            } catch (JSONException e) {
                //e.printStackTrace();
                prefManager.setDataTopNews("");
            }
            if (internetAvailable) {
                clearItem();

                connection = new GetRequest(this);
                connection.SendGetData(EndpointAPI.ListTopNews,"", 1);
                connection.execute();

                connection2 = new GetRequest(this);
                connection2.SendGetData(EndpointAPI.ListAllNews,"", 2);
                connection2.execute();
            } else {
                /*AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setMessage("Tidak ada koneksi internet, mohon periksa kembali.")
                        .setCancelable(false)
                        .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();*/
                swipeRefreshLayout.setRefreshing(false);
            }
        } else {
            if (internetAvailable) {
                clearItem();

                connection = new GetRequest(this);
                connection.SendGetData(EndpointAPI.ListTopNews,"", 1);
                connection.execute();

                connection2 = new GetRequest(this);
                connection2.SendGetData(EndpointAPI.ListAllNews,"", 2);
                connection2.execute();
            } else {
                /*AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setMessage("Tidak ada koneksi internet, mohon periksa kembali.")
                        .setCancelable(false)
                        .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();*/
                swipeRefreshLayout.setRefreshing(false);
            }
        }
    }

    @Override
    public void onRequestComplete(JSONObject jsonObject, Integer REQUEST_CODE) {
        if (REQUEST_CODE == 1) {
            try {
                if (!jsonObject.toString().equalsIgnoreCase(prefManager.getDataTopNews())) {
                    prefManager.setDataTopNews(jsonObject.toString());

                    JSONArray c = jsonObject.getJSONArray("result");
                    int lenArray = c.length();
                    contentAdding = new ArrayList<>(lenArray);
                    for (int i = 0; i < lenArray; i++) {
                        JSONObject dat = c.getJSONObject(i);
                        int id = dat.getInt("id");
                        int views = dat.getInt("views");
                        String title = dat.getString("title");
                        String image = EndpointAPI.HostStorage + dat.getString("image");

                        JSONObject author = dat.getJSONObject("author");
                        String author_name = author.getString("name");

                        contentAdding.add(new DataNews(String.valueOf(id), title, image, author_name, String.valueOf(views)));
                    }
                    mRecyclerView1.setAdapter(new TopNewsAdapter(contentAdding, getContext()));
                    mRecyclerView1.getLayoutManager().scrollToPosition(0);
                    swipeRefreshLayout.setRefreshing(false);
                } else {
                    swipeRefreshLayout.setRefreshing(false);
                }
            } catch (JSONException e) {
                swipeRefreshLayout.setRefreshing(false);
                //e.printStackTrace();
            }
        } else if (REQUEST_CODE == 2) {
            //Log.e("masuk data", jsonObject.toString());
            ArrayList<Item> _contentAdding;
            int current_page;
            try {
                String author_name;
                JSONObject c = jsonObject.getJSONObject("result");
                next_page = c.getString("next_page_url");
                current_page = c.getInt("current_page");

                //if (!jsonObject.toString().equalsIgnoreCase(prefManager.getDataAllNews())) {
                JSONArray data = c.getJSONArray("data");
                int lenArray = data.length();
                _contentAdding = new ArrayList<>();
                for (int i = 0; i < lenArray; i++) {
                    JSONObject dat = data.getJSONObject(i);
                    int id = dat.getInt("id");
                    int views = dat.getInt("views");
                    String title = dat.getString("title");
                    String image = EndpointAPI.HostStorage + dat.getString("image");

                    JSONObject author = dat.getJSONObject("author");

                    if(author.has("name") && author.getString("name") != null)author_name = author.getString("name");
                    else author_name = "Anonim";

                    _contentAdding.add(new DataNews(String.valueOf(id), title, image, author_name, String.valueOf(views)));
                }
                //Log.e("cek1", "error");
                hasMore = !next_page.equalsIgnoreCase("null");
                if (isLoadMore) {
                    //Log.e("masuk 4", "true");
                    if (contentAdding2.size() > 0)
                    deleteItem(contentAdding2.size() - 1);

                    contentAdding2.addAll(_contentAdding);
                    mRecyclerView2.getAdapter().notifyDataSetChanged();
                } else {
                    //Log.e("masuk 3", String.valueOf(isLoadMore));
                    contentAdding2 = _contentAdding;
                    isLoadMore = hasMore;

                    a = new AllNewsAdapter(contentAdding2, getActivity());
                    mRecyclerView2.setAdapter(a);
                }


                //Log.e("masuk 2", next_page);
                //mRecyclerView2.setAdapter(new AllNewsAdapter(contentAdding2, getContext()));
                //mRecyclerView2.getLayoutManager().scrollToPosition(0);
                swipeRefreshLayout.setRefreshing(false);
                if (current_page == 1)
                    prefManager.setDataAllNews(jsonObject.toString());

                //} else {
                /*    hasMore = !next_page.equalsIgnoreCase("null");
                    if(isLoadMore){
                        Log.e("masuk 4", String.valueOf(isLoadMore));
                        //deleteItem(contentAdding2.size() - 1);
                        //contentAdding2.addAll(contentAdding2);
                        //mRecyclerView2.getAdapter().notifyDataSetChanged();
                    }else {
                        Log.e("masuk 3", String.valueOf(isLoadMore));
                        isLoadMore = hasMore;
                        mRecyclerView2.setAdapter(new AllNewsAdapter(contentAdding2, getActivity()));
                    }
                    swipeRefreshLayout.setRefreshing(false);
                }*/
            } catch (JSONException e) {
                //Log.e("cek", "error");
                swipeRefreshLayout.setRefreshing(false);
                //Toast.makeText(getContext(), "Kesalahan server", Toast.LENGTH_SHORT).show();
                if(contentAdding2!=null)
                    if (contentAdding2.size()>0)
                    deleteItem(contentAdding2.size() - 1);
                //e.printStackTrace();
            }
        }

    }

    public void deleteItem(int pos) {
        contentAdding2.remove(pos);
        mRecyclerView2.getAdapter().notifyDataSetChanged();
    }

    public void clearItem() {
        LinearLayoutManager layoutManager = (LinearLayoutManager) mRecyclerView2.getLayoutManager();
        int mSize = layoutManager.getItemCount() - 1, i;
        if (mSize != 0 && mRecyclerView2.getAdapter() != null) {
            //int sizeAdapter = contentAdding2.size();
            //Log.e("masuk", String.valueOf(mSize));
            //Log.e("masuk", String.valueOf(sizeAdapter));
            for (i = mSize; i > 0; i--) {
                contentAdding2.remove(i);
            }
            mRecyclerView2.getAdapter().notifyDataSetChanged();
        }
    }

    @Override
    public void onRefresh() {
        getDataNow();
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
