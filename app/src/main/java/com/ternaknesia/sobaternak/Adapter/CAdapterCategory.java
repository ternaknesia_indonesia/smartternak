package com.ternaknesia.sobaternak.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.ternaknesia.sobaternak.R;

/**
 * Created by abdalla on 10/29/17.
 */

public class CAdapterCategory extends ArrayAdapter<String> {

    String[] spinnerId;
    String[] spinnerName;
    Context mContext;

    public CAdapterCategory(@NonNull Context context, String[] id, String[] name) {
        super(context, R.layout.item_spinner);
        this.spinnerId = id;
        this.spinnerName = name;
        this.mContext = context;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return getView(position, convertView, parent);
    }

    @Override
    public int getCount() {
        return spinnerId.length;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        ViewHolder mViewHolder = new ViewHolder();
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) mContext.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.item_spinner, parent, false);
            mViewHolder.mName = convertView.findViewById(R.id.name);
            mViewHolder.mId = convertView.findViewById(R.id.id);
            convertView.setTag(mViewHolder);
        } else {
            mViewHolder = (ViewHolder) convertView.getTag();
        }
        mViewHolder.mName.setText(spinnerName[position]);
        mViewHolder.mId.setText(spinnerId[position]);

        return convertView;
    }

    private static class ViewHolder {
        TextView mName;
        TextView mId;
    }
}