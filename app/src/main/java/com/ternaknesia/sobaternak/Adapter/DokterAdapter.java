package com.ternaknesia.sobaternak.Adapter;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.ternaknesia.sobaternak.DataModel.DataDokter;
import com.ternaknesia.sobaternak.R;
import com.ternaknesia.sobaternak.Utils.Item;

import java.util.List;


public class DokterAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<Item> data;
    private Context mContext;

    private static final int TYPE_CONTENT = 0;
    private static final int TYPE_FOOTER = 1;

    public DokterAdapter(@NonNull List<Item> data, Context context) {
        this.data = data;
        this.mContext = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_dokter, parent, false);
        return new PaletteViewHolder(row);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof PaletteViewHolder) {
            final DataDokter contentSet = (DataDokter) data.get(position);
            PaletteViewHolder paletteViewHolder = (PaletteViewHolder) holder;
            paletteViewHolder.getId().setText(contentSet.getId());
            paletteViewHolder.getName().setText(contentSet.getName());
            paletteViewHolder.getAddress().setText(contentSet.getAddress());
            paletteViewHolder.getWork_time().setText(contentSet.getWork_time());
            paletteViewHolder.getNote().setText(contentSet.getNote());

            paletteViewHolder.getBtnWA().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        String toNumber = contentSet.getPhone(); // Replace with mobile phone number without +Sign or leading zeros.

                        toNumber = toNumber.replaceAll("\\D+", "");
                        if (toNumber.contains(" "))
                            toNumber = toNumber.replaceAll(" ", "");
                        if (toNumber.contains("-"))
                            toNumber = toNumber.replaceAll("-", "");
                        if (toNumber.charAt(0) == '0')
                            toNumber = "+62" + toNumber.substring(1, toNumber.length());

                        //Log.e("cek", toNumber);

                        Intent sendIntent = new Intent(Intent.ACTION_SENDTO, Uri.parse("smsto:" + "" + toNumber + "?body=" + ""));

                        sendIntent.setPackage("com.whatsapp");
                        mContext.startActivity(sendIntent);
                    } catch (Exception e) {
                        e.printStackTrace();
                        Toast.makeText(mContext, "Anda tidak memiliki aplikasi WhatsApp", Toast.LENGTH_LONG).show();

                    }
                }
            });

            paletteViewHolder.getBtnCall().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse("tel:" + contentSet.getPhone()));

                    if (ActivityCompat.checkSelfPermission(mContext,
                            Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                        Toast.makeText(mContext, "Harap mengizinkan panggilan telepon", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    mContext.startActivity(callIntent);
                }
            });

            paletteViewHolder.getBtnSMS().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent smsIntent = new Intent(Intent.ACTION_SENDTO, Uri.parse("smsto:" + contentSet.getPhone()));
                    mContext.startActivity(smsIntent);
                }
            });
        }

        //FOOTER: nothing to do
    }

    @Override
    public int getItemCount() {
        return data.size();
    }


    private class PaletteViewHolder extends RecyclerView.ViewHolder {
        private TextView id;
        private TextView name;
        private TextView address;
        private TextView work_time;
        private TextView note;
        private ImageView btnWA, btnCall, btnSMS;


        PaletteViewHolder(View itemView) {
            super(itemView);
            id = itemView.findViewById(R.id.id);
            name = itemView.findViewById(R.id.name);
            address = itemView.findViewById(R.id.address);
            work_time = itemView.findViewById(R.id.work_time);
            note = itemView.findViewById(R.id.note);
            btnWA = itemView.findViewById(R.id.imageButtonWA);
            btnCall = itemView.findViewById(R.id.imageButtonPhone);
            btnSMS = itemView.findViewById(R.id.imageButtonSMS);

        }

        ImageView getBtnWA() {
            return btnWA;
        }

        ImageView getBtnCall() {
            return btnCall;
        }

        ImageView getBtnSMS() {
            return btnSMS;
        }

        TextView getNote() {
            return note;
        }

        TextView getId() {
            return id;
        }

        TextView getName() {
            return name;
        }

        TextView getAddress() {
            return address;
        }

        TextView getWork_time() {
            return work_time;
        }
    }
}