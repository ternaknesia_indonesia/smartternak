package com.ternaknesia.sobaternak.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.ternaknesia.sobaternak.R;
import com.ternaknesia.sobaternak.Utils.ImageZoomView.PhotoView;

import org.json.JSONArray;
import org.json.JSONException;

public class ImageViewerAdapter extends PagerAdapter {
    private Context mContext;
    private JSONArray imgs;
    private boolean is_single;
    //private int [] myRes = {R.drawable.i1, R.drawable.i2, R.drawable.i3};

    public ImageViewerAdapter(Context context, JSONArray imgs, boolean is_single_){
        this.mContext = context;
        this.imgs = imgs;
        this.is_single = is_single_;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        LayoutInflater layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.item_imageview, container, false);
        PhotoView imageView = view.findViewById(R.id.item_viewer);
        TextView count_img = view.findViewById(R.id.count_imgs);

        try {
            Glide.with(mContext)
                    .load(imgs.getString(position))
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .skipMemoryCache(true)
                    .into(imageView);
        } catch (JSONException e) {
            //e.printStackTrace();
        }

        String a = String.valueOf(position+1) + "/" + String.valueOf(imgs.length());
        count_img.setText(a);

        if(is_single) count_img.setVisibility(View.GONE);

        container.addView(view);
        return view;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((RelativeLayout) object);
    }

    @Override
    public int getCount() {
        return imgs.length();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }
}
