package com.ternaknesia.sobaternak.Adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.ternaknesia.sobaternak.DataModel.DataFeed;
import com.ternaknesia.sobaternak.Interface.EndpointAPI;
import com.ternaknesia.sobaternak.R;
import com.ternaknesia.sobaternak.Utils.DateFormatter;
import com.ternaknesia.sobaternak.Utils.Footer;
import com.ternaknesia.sobaternak.Utils.ImageViewer;
import com.ternaknesia.sobaternak.Utils.Item;
import com.ternaknesia.sobaternak.Utils.RoundedCornersTransformation;

import java.util.List;

public class PostFeedAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<Item> data;
    private Context context;


    private static final int TYPE_CONTENT = 0;
    private static final int TYPE_FOOTER = 1;

    public PostFeedAdapter(@NonNull List<Item> data, Context context) {
        this.data = data;
        this.context = context;
    }

    @Override
    public int getItemViewType(int position) {
        if (data.get(position) instanceof DataFeed) {
            return TYPE_CONTENT;
        } else if (data.get(position) instanceof Footer) {
            return TYPE_FOOTER;
        } else {
            throw new RuntimeException("ItemViewType unknown");
        }
    }

/*    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_all_news, parent, false);
        return new PaletteViewHolder(row);
    }*/

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_CONTENT) {
            View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_feed_status, parent, false);
            return new PaletteViewHolder(row);
        } else {
            View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.progressbar_loadmore, parent, false);
            return new FooterViewHolder(row);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof PaletteViewHolder) {
            DataFeed contentSet = (DataFeed) data.get(position);

            PaletteViewHolder paletteViewHolder = (PaletteViewHolder) holder;

            paletteViewHolder.getUser().setText(contentSet.getmUser());
            paletteViewHolder.getDate().setText(new DateFormatter().convertDate(contentSet.getmDate(), "dd MMMM yyyy HH:mm"));
            paletteViewHolder.getCaption().setText(contentSet.getmCaption());


            if(contentSet.getmImage().equalsIgnoreCase("") || contentSet.getmImage().equalsIgnoreCase("null"))
                paletteViewHolder.getImg_post().setVisibility(View.GONE);
            else Glide.with(context)
                    .load(EndpointAPI.HostStorage + contentSet.getmImage())
                    .centerCrop()
                    .placeholder(R.drawable.placeholder_home)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .skipMemoryCache(true)
                    .bitmapTransform(new RoundedCornersTransformation(context, 10, 0))
                    .into(paletteViewHolder.getImg_post());

            Glide.with(context)
                    .load(contentSet.getmAvatar())
                    .fitCenter()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .skipMemoryCache(true)
                    .bitmapTransform(new RoundedCornersTransformation(context, 10, 0))
                    .into(paletteViewHolder.getAvatar());

            paletteViewHolder.getImg_post().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent a = new Intent(context, ImageViewer.class);
                    Bundle extras = new Bundle();
                    extras.putString("imgs", EndpointAPI.HostStorage + contentSet.getmImage());
                    extras.putInt("position", 0);
                    a.putExtras(extras);
                    context.startActivity(a);
                }
            });

        }
        //FOOTER: nothing to do
    }

    @Override
    public int getItemCount() {
        return data.size();
    }



    private class PaletteViewHolder extends RecyclerView.ViewHolder {
        private ImageView avatar, img_post;
        private TextView caption, id, user, date ;

        PaletteViewHolder(View itemView) {
            super(itemView);
            //id = itemView.findViewById(R.id.news_id);
            user = itemView.findViewById(R.id.username_post);
            avatar = itemView.findViewById(R.id.avatar_post);
            caption = itemView.findViewById(R.id.caption_post);
            date = itemView.findViewById(R.id.date_post);
            img_post = itemView.findViewById(R.id.image_post);

/*            Typeface segoe_light = Typeface.createFromAsset(context.getAssets(), "fonts/varelaround.ttf");
            caption.setTypeface(segoe_light);*/

/*            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(context, ReadNews.class);
                    Bundle extras = new Bundle();
                    //extras.putParcelable("bitmap", image);
                    extras.putString("news_id", id.getText().toString());

                    i.putExtras(extras);
                    context.startActivity(i);
                    //Toast.makeText(context, resImg.getText().toString(), Toast.LENGTH_LONG).show();
                }
            });*/
        }

        public ImageView getAvatar() {
            return avatar;
        }

        public ImageView getImg_post() {
            return img_post;
        }

        public TextView getCaption() {
            return caption;
        }

        public TextView getId() {
            return id;
        }

        public TextView getUser() {
            return user;
        }

        public TextView getDate() {
            return date;
        }
    }


    public void clear() {
        final int size = data.size();
        data.clear();
        notifyItemRangeRemoved(0, size);
    }

    public static class FooterViewHolder extends RecyclerView.ViewHolder {

        ProgressBar progressBar;

        FooterViewHolder(View itemView) {
            super(itemView);
            progressBar = (ProgressBar) itemView.findViewById(R.id.progressBar1);
        }
    }
}