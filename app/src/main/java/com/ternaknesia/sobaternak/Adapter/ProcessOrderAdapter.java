package com.ternaknesia.sobaternak.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.ternaknesia.sobaternak.DataModel.DataProcessOrder;
import com.ternaknesia.sobaternak.Interface.OnClickInterface;
import com.ternaknesia.sobaternak.Interface.RequestComplete;
import com.ternaknesia.sobaternak.Manager.PrefManager;
import com.ternaknesia.sobaternak.R;
import com.ternaknesia.sobaternak.Utils.Footer;
import com.ternaknesia.sobaternak.Utils.Item;

import org.json.JSONObject;

import java.util.List;
import java.util.Locale;


public class ProcessOrderAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements RequestComplete{
    private List<Item> data;
    private PrefManager prefManager;
    private OnClickInterface confirmButtonOrder;

    private static final int TYPE_CONTENT = 0;
    private static final int TYPE_FOOTER = 1;

    public ProcessOrderAdapter(@NonNull List<Item> data, OnClickInterface confirmButtonOrder1, Context context) {
        this.data = data;
        this.prefManager = new PrefManager(context);
        this.confirmButtonOrder = confirmButtonOrder1;
    }

    @Override
    public int getItemViewType(int position) {
        if (data.get(position) instanceof DataProcessOrder) {
            //Log.e("ok", "content2");
            return TYPE_CONTENT;
        } else if (data.get(position) instanceof Footer) {
            return TYPE_FOOTER;
        } else {
            throw new RuntimeException("ItemViewType unknown");
        }
    }

/*    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_all_news, parent, false);
        return new PaletteViewHolder(row);
    }*/

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_CONTENT) {
            View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_process_product, parent, false);
            return new PaletteViewHolder(row);
        } else {
            View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.progressbar_loadmore, parent, false);
            return new FooterViewHolder(row);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof PaletteViewHolder) {
            final DataProcessOrder contentSet = (DataProcessOrder) data.get(position);

            PaletteViewHolder paletteViewHolder = (PaletteViewHolder) holder;
            //ImageView imageView = paletteViewHolder.getImg();
            //String url = contentSet.getmTextLinkImg();
            /*Glide.with(context)
                    .load(url)
                    .placeholder(R.drawable.news_placeholder)
                    .fitCenter()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .skipMemoryCache(true)
                    .bitmapTransform(new RoundedCornersTransformation(context, 10, 0))
                    .into(imageView);*/

            paletteViewHolder.getInvoice().setText(contentSet.getInvoice());
            paletteViewHolder.getOrder_id().setText(String.valueOf(contentSet.getSales_order_line_id()));
            paletteViewHolder.getItems().setText(Html.fromHtml(contentSet.getItem()));
            paletteViewHolder.getUser_name().setText(contentSet.getPemesan());
            paletteViewHolder.getAddress().setText(contentSet.getDestination());
            paletteViewHolder.getSend_time().setText(contentSet.getSendTime());
            paletteViewHolder.getNote().setText(contentSet.getCatatan());
            if(contentSet.getTerkirim()<0)
                paletteViewHolder.getTerkirim().setText(String.format(Locale.getDefault(), "0/%d Terkirim", contentSet.getJumlah()));
            else
                paletteViewHolder.getTerkirim().setText(String.format(Locale.getDefault(), "%d/%d Terkirim", contentSet.getTerkirim(), contentSet.getJumlah()));


            paletteViewHolder.getKonfirmasi().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    confirmButtonOrder.onCompleteOrderClicked(contentSet, position);
                }
            });
            //Log.e("name", contentSet.getPemesan());
        }
        //FOOTER: nothing to do
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public void onRequestComplete(JSONObject jsonObject, Integer REQUEST_CODE) {

    }


    private class PaletteViewHolder extends RecyclerView.ViewHolder {
        private ImageView img;
        private TextView title, id, price, market_id, user_name, items, address, send_time, terkirim, note, invoice, order_id;
        private Button konfirmasi;

        PaletteViewHolder(View itemView) {
            super(itemView);
            user_name = itemView.findViewById(R.id.user_name);
            id = itemView.findViewById(R.id.id);
            market_id = itemView.findViewById(R.id.market_id);
            items = itemView.findViewById(R.id.items);
            address = itemView.findViewById(R.id.address);
            send_time = itemView.findViewById(R.id.send_time);
            terkirim = itemView.findViewById(R.id.terkirim);
            konfirmasi = itemView.findViewById(R.id.confirm);
            note = itemView.findViewById(R.id.note);
            invoice = itemView.findViewById(R.id.invoice);
            order_id = itemView.findViewById(R.id.order_id);

        }

        public TextView getInvoice() {
            return invoice;
        }

        public TextView getOrder_id() {
            return order_id;
        }

        public TextView getNote() {
            return note;
        }

        public Button getKonfirmasi() {
            return konfirmasi;
        }

        public TextView getTerkirim() {
            return terkirim;
        }

        private ImageView getImg() {
            return img;
        }

        private TextView getTitle() {
            return title;
        }

        private TextView getId() {
            return id;
        }

        private TextView getPrice() {
            return price;
        }

        private TextView getMarket_id() {
            return market_id;
        }

        public TextView getUser_name() {
            return user_name;
        }

        public TextView getItems() {
            return items;
        }

        public TextView getAddress() {
            return address;
        }

        public TextView getSend_time() {
            return send_time;
        }
    }


    public void clear() {
        final int size = data.size();
        data.clear();
        notifyItemRangeRemoved(0, size);
    }

    public static class FooterViewHolder extends RecyclerView.ViewHolder {

        ProgressBar progressBar;

        FooterViewHolder(View itemView) {
            super(itemView);
            progressBar = (ProgressBar) itemView.findViewById(R.id.progressBar1);
        }
    }
}