package com.ternaknesia.sobaternak.Adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.ternaknesia.sobaternak.activity.ReadNews;
import com.ternaknesia.sobaternak.DataModel.DataNews;
import com.ternaknesia.sobaternak.R;
import com.ternaknesia.sobaternak.Utils.Item;
import com.ternaknesia.sobaternak.Utils.RoundedCornersTransformation;

import java.util.List;


public class TopNewsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<Item> data;
    private Context context;


    private static final int TYPE_CONTENT = 0;
    private static final int TYPE_FOOTER = 1;

    public TopNewsAdapter(@NonNull List<Item> data, Context context) {
        this.data = data;
        this.context = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_news, parent, false);
        return new PaletteViewHolder(row);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof PaletteViewHolder) {
            DataNews contentSet = (DataNews) data.get(position);

            PaletteViewHolder paletteViewHolder = (PaletteViewHolder) holder;
            ImageView imageView = paletteViewHolder.getImg();
            String url = contentSet.getLinkImg();
            Glide.with(context)
                    .load(url)
                    .placeholder(R.drawable.news_placeholder)
                    .fitCenter()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .skipMemoryCache(true)
                    .bitmapTransform(new RoundedCornersTransformation(context, 13, 0))
                    .into(imageView);
            paletteViewHolder.getNewsTitle().setText(contentSet.getNewsTitle());
            paletteViewHolder.getId().setText(contentSet.getNewsId());
            paletteViewHolder.getAuthor().setText(contentSet.getmTextAuthor());
            paletteViewHolder.getViews().setText(contentSet.getmViews());
            paletteViewHolder.getResImg().setText(String.valueOf(contentSet.getLinkImg()));
        }
        //FOOTER: nothing to do
    }

    @Override
    public int getItemCount() {
        return data.size();
    }



    private class PaletteViewHolder extends RecyclerView.ViewHolder {
        private ImageView img;
        private TextView title, id, author, tgl, resImg, content, views;

        PaletteViewHolder(View itemView) {
            super(itemView);
            img = itemView.findViewById(R.id.imgNews);
            id = itemView.findViewById(R.id.news_id);
            title = itemView.findViewById(R.id.txtTitleNews);
            author = itemView.findViewById(R.id.txtAuthor);
            tgl = itemView.findViewById(R.id.txtDate);
            views = itemView.findViewById(R.id.countViews);

            resImg = itemView.findViewById(R.id.news_img);
            content = itemView.findViewById(R.id.news_content);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(context, ReadNews.class);
                    Bundle extras = new Bundle();
                    //extras.putParcelable("bitmap", image);
                    extras.putString("blog_id", id.getText().toString());

                    i.putExtras(extras);
                    context.startActivity(i);
                    //Toast.makeText(context, resImg.getText().toString(), Toast.LENGTH_LONG).show();
                }
            });
        }

        TextView getViews() {
            return views;
        }

        TextView getId(){
            return  id;
        }

        TextView getNewsTitle() {
            return title;
        }

        TextView getAuthor() {
            return author;
        }

        ImageView getImg(){return img;}

        TextView getResImg(){return resImg;}
    }
}