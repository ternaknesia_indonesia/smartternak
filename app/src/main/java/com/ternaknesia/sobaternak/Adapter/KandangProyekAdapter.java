package com.ternaknesia.sobaternak.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.ternaknesia.sobaternak.activity.EditKandangActivity;
import com.ternaknesia.sobaternak.DataModel.DataKandangProyek;
import com.ternaknesia.sobaternak.R;
import com.ternaknesia.sobaternak.Utils.Item;

import java.util.List;


public class KandangProyekAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<Item> data;
    private Context context;

    private static final int TYPE_CONTENT = 0;
    private static final int TYPE_FOOTER = 1;

    public KandangProyekAdapter(@NonNull List<Item> data, Context context) {
        this.data = data;
        this.context = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_kandang_proyek, parent, false);
        return new PaletteViewHolder(row);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof PaletteViewHolder) {
            DataKandangProyek contentSet = (DataKandangProyek) data.get(position);

            PaletteViewHolder paletteViewHolder = (PaletteViewHolder) holder;
            /*ImageView imageView = paletteViewHolder.getImg();
            String url = contentSet.getLinkImg();
            Glide.with(context)
                    .load(url)
                    .placeholder(R.drawable.placeholder)
                    .fitCenter()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .skipMemoryCache(true)
                    .into(imageView);*/

            paletteViewHolder.getName().setText(contentSet.getName());
            paletteViewHolder.getId().setText(contentSet.getId());
            paletteViewHolder.getInstance_id().setText(contentSet.getmInstanceId());
            paletteViewHolder.getLoc().setText(contentSet.getLoc());
            paletteViewHolder.getCount().setText(contentSet.getCount());
            paletteViewHolder.getAnakk().setText(contentSet.getAnakK());
            paletteViewHolder.getDesc().setText(contentSet.getDesc());
            paletteViewHolder.getCapacity().setText(contentSet.getmCapacity());
            paletteViewHolder.getType_id().setText(contentSet.getmIdType());
            paletteViewHolder.getType_name().setText(contentSet.getmNameType());

        }

        //FOOTER: nothing to do
    }

    @Override
    public int getItemCount() {
        return data.size();
    }


    private class PaletteViewHolder extends RecyclerView.ViewHolder {
        private TextView desc;
        private TextView anakk;
        private TextView count;
        private TextView name;
        private TextView loc;
        private TextView id;
        private TextView instance_id;
        private TextView capacity;
        private TextView type_id;
        private TextView type_name;
        private ImageView img_ops;

        PaletteViewHolder(View itemView) {
            super(itemView);
            desc = itemView.findViewById(R.id.txtDesc);
            id = itemView.findViewById(R.id.txtKId);
            name = itemView.findViewById(R.id.txtKName);
            count = itemView.findViewById(R.id.txtKCount);
            anakk = itemView.findViewById(R.id.txtKAnak);
            loc = itemView.findViewById(R.id.txtKAddress);
            img_ops = itemView.findViewById(R.id.ops);
            capacity = itemView.findViewById(R.id.capacity);
            type_id = itemView.findViewById(R.id.type_id);
            type_name = itemView.findViewById(R.id.type_name);
            instance_id = itemView.findViewById(R.id.txtKInstanceId);

            img_ops.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showMenu(v);
                }
            });
        }

        public TextView getType_id() {
            return type_id;
        }

        public TextView getType_name() {
            return type_name;
        }

        public TextView getInstance_id() {
            return instance_id;
        }

        public TextView getDesc() {
            return desc;
        }

        public TextView getAnakk() {
            return anakk;
        }

        public TextView getCount() {
            return count;
        }

        public TextView getName() {
            return name;
        }

        public TextView getLoc() {
            return loc;
        }

        public TextView getId() {
            return id;
        }

        public TextView getCapacity() {
            return capacity;
        }

        void showMenu(View v) {
            PopupMenu popup = new PopupMenu(context, v);
            popup.inflate(R.menu.item_kandang_menu);
            ((Activity)context).invalidateOptionsMenu();
            popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                @Override
                public boolean onMenuItemClick(MenuItem item) {
                    if (item.getItemId() == R.id.item_edit) {
                        Intent i = new Intent(context, EditKandangActivity.class);
                        Bundle extras = new Bundle();
                        extras.putString("id", id.getText().toString());
                        extras.putString("instance_id", instance_id.getText().toString());
                        extras.putString("name", name.getText().toString());
                        extras.putString("desc", desc.getText().toString());
                        extras.putString("anak_kandang", anakk.getText().toString());
                        extras.putString("lokasi", loc.getText().toString());
                        extras.putString("capacity", capacity.getText().toString());
                        extras.putString("type_id", type_id.getText().toString());

                        i.putExtras(extras);
                        ((Activity) context).startActivityForResult(i, 4000);
                    } else if (item.getItemId() == R.id.item_hapus){
                        Toast.makeText(context, "Sementara fitur belum tersedia", Toast.LENGTH_LONG).show();
                    }
                    return item.isChecked();
                }
            });
            popup.show();
        }
    }
}