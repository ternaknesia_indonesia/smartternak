package com.ternaknesia.sobaternak.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ternaknesia.sobaternak.DataModel.DataSubPeternak;
import com.ternaknesia.sobaternak.R;
import com.ternaknesia.sobaternak.Utils.Item;

import java.util.List;


public class SubPeternakAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<Item> data;
    private Context context;

    private static final int TYPE_CONTENT = 0;
    private static final int TYPE_FOOTER = 1;

    public SubPeternakAdapter(@NonNull List<Item> data, Context context) {
        this.data = data;
        this.context = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_sub_peternak, parent, false);
        return new PaletteViewHolder(row);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof PaletteViewHolder) {
            DataSubPeternak contentSet = (DataSubPeternak) data.get(position);

            PaletteViewHolder paletteViewHolder = (PaletteViewHolder) holder;

            paletteViewHolder.getId().setText(contentSet.getId());
            paletteViewHolder.getPeternak().setText(contentSet.getName() + " [" + contentSet.getInstanceCode() + "]");
            paletteViewHolder.getAddress().setText(contentSet.getAddress());
            paletteViewHolder.getCount().setText("" + contentSet.getCount());
        }

        //FOOTER: nothing to do
    }

    @Override
    public int getItemCount() {
        return data.size();
    }



    private class PaletteViewHolder extends RecyclerView.ViewHolder {
        private ImageView img;
        private TextView id;
        private TextView peternak;
        private TextView address;
        private TextView count;

        PaletteViewHolder(View itemView) {
            super(itemView);
            img = itemView.findViewById(R.id.imgPeternak);
            id = itemView.findViewById(R.id.txtId);
            peternak = itemView.findViewById(R.id.txtPeternak);
            address = itemView.findViewById(R.id.txtAddress);
            count = itemView.findViewById(R.id.txtCount);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
/*                    Intent i = new Intent(context, DetailProjectActivity.class);
                    Bundle extras = new Bundle();
                    //extras.putParcelable("bitmap", image);
                    extras.putString("project_id", project_id.getText().toString());
                    extras.putString("project_name", project_name.getText().toString());
                    extras.putString("project_remain", remain_slot.getText().toString());
                    extras.putString("project_price", price.getText().toString());
                    i.putExtras(extras);
                    context.startActivity(i);*/
                }
            });
        }

        ImageView getImg() {return img;}

        TextView getId() {
            return id;
        }

        TextView getPeternak() {
            return peternak;
        }

        TextView getAddress() { return address; }

        TextView getCount() { return count; }
    }
}