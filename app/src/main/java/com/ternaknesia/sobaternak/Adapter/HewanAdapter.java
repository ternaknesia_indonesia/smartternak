package com.ternaknesia.sobaternak.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.ternaknesia.sobaternak.activity.EditHewanActivity;
import com.ternaknesia.sobaternak.DataModel.DataHewan;
import com.ternaknesia.sobaternak.R;
import com.ternaknesia.sobaternak.Utils.Footer;
import com.ternaknesia.sobaternak.Utils.Item;

import java.util.List;
import java.util.Locale;


public class HewanAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<Item> data;
    private Context mContext;

    private static final int TYPE_CONTENT = 0;
    private static final int TYPE_FOOTER = 1;

    public HewanAdapter(@NonNull List<Item> data, Context context) {
        this.data = data;
        this.mContext = context;
    }

    @Override
    public int getItemViewType(int position) {
        if (data.get(position) instanceof DataHewan) {
            return TYPE_CONTENT;
        } else if (data.get(position) instanceof Footer) {
            return TYPE_FOOTER;
        } else {
            throw new RuntimeException("ItemViewType unknown");
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_CONTENT) {
            View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_hewan, parent, false);
            return new HewanAdapter.PaletteViewHolder(row);
        } else {
            View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.progressbar_loadmore, parent, false);
            return new HewanAdapter.FooterViewHolder(row);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof PaletteViewHolder) {
            DataHewan contentSet = (DataHewan) data.get(position);
            final PaletteViewHolder paletteViewHolder = (PaletteViewHolder) holder;

            paletteViewHolder.getId().setText(contentSet.getId());
            paletteViewHolder.getInstanceId().setText(contentSet.getInstanceId());
            paletteViewHolder.getCodeTag().setText(contentSet.getCodeTag());
            paletteViewHolder.getJenisKelamin().setText(
                    contentSet.getJenisHewan() + " " + contentSet.getJenisKelamin() + ", " + contentSet.getStatus() + ", " + contentSet.getKandang()
            );
            paletteViewHolder.getInstance().setText(contentSet.getInstance());
            paletteViewHolder.getProject().setText(contentSet.getProject());
            paletteViewHolder.getPemesan().setText(contentSet.getPemesan());
            paletteViewHolder.getHeight().setText(contentSet.getHeight());
            paletteViewHolder.getWeight().setText(contentSet.getWeight());
            paletteViewHolder.getHarvestDate().setText(
                    contentSet.getHarvestDate() + " " + contentSet.getHarvestAge()
            );
            if(contentSet.getSellPriceUnitNum() > 0){
                paletteViewHolder.getPrice().setText(String.format(Locale.getDefault(), "Rp. %,.0f", contentSet.getSellPriceUnitNum()).replaceAll(",", "."));
            } else {
                paletteViewHolder.getPrice().setText("-");
            }
        }

        //FOOTER: nothing to do
    }

    @Override
    public int getItemCount() {
        return data.size();
    }


    private class PaletteViewHolder extends RecyclerView.ViewHolder {
        private TextView id;
        private TextView instance_id;
        private TextView code_tag;
        private TextView jenis_kelamin;
        private TextView instance;
        private TextView project;
        private TextView pemesan;
        private TextView weight;
        private TextView height;
        private TextView harvest_date;
        private TextView price;

        PaletteViewHolder(View itemView) {
            super(itemView);
            id = itemView.findViewById(R.id.id);
            instance_id = itemView.findViewById(R.id.instance_id);
            code_tag = itemView.findViewById(R.id.code_tag);
            jenis_kelamin = itemView.findViewById(R.id.jenis_kelamin);
            instance = itemView.findViewById(R.id.instance);
            project = itemView.findViewById(R.id.project);
            pemesan = itemView.findViewById(R.id.pemesan);
            weight = itemView.findViewById(R.id.weight);
            height = itemView.findViewById(R.id.height);
            harvest_date = itemView.findViewById(R.id.harvest_date);
            price = itemView.findViewById(R.id.price);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent n = new Intent(mContext, EditHewanActivity.class);
                    Bundle extras = new Bundle();
                    extras.putInt("id", Integer.parseInt(id.getText().toString()));
                    extras.putInt("instance_id", Integer.parseInt(instance_id.getText().toString()));
                    n.putExtras(extras);
                    ((Activity)mContext).startActivityForResult(n, 5000);
                }
            });
        }

        public TextView getPrice() {
            return price;
        }

        public TextView getId() {
            return id;
        }

        public TextView getInstanceId() {
            return instance_id;
        }

        public TextView getCodeTag() {
            return code_tag;
        }

        public TextView getJenisKelamin() {
            return jenis_kelamin;
        }

        public TextView getInstance() {
            return instance;
        }

        public TextView getProject() { return project; }

        public TextView getPemesan() {
            return pemesan;
        }

        public TextView getWeight() { return weight; }

        public TextView getHeight() { return height; }

        public TextView getHarvestDate() {
            return harvest_date;
        }
    }

    private static class FooterViewHolder extends RecyclerView.ViewHolder {

        ProgressBar progressBar;

        FooterViewHolder(View itemView) {
            super(itemView);
            progressBar = (ProgressBar) itemView.findViewById(R.id.progressBar1);
        }
    }
}