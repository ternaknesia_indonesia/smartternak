package com.ternaknesia.sobaternak.Adapter;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.ternaknesia.sobaternak.DataModel.DataAllOrder;
import com.ternaknesia.sobaternak.DataModel.SalesOrder.ResponseOrder;
import com.ternaknesia.sobaternak.Interface.OnClickInterface;
import com.ternaknesia.sobaternak.Interface.RequestComplete;
import com.ternaknesia.sobaternak.R;
import com.ternaknesia.sobaternak.Utils.DateFormatter;
import com.ternaknesia.sobaternak.Utils.Footer;
import com.ternaknesia.sobaternak.Utils.Item;
import com.ternaknesia.sobaternak.activity.DetailOrderActivity;

import org.json.JSONObject;

import java.util.List;

import retrofit2.Callback;
import timber.log.Timber;


public class AllOrderAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements RequestComplete {
    private List<Item> data;
    private OnClickInterface confirmButtonOrder;
    private Context mContext;

    private static final int TYPE_CONTENT = 0;
    private static final int TYPE_FOOTER = 1;

    public AllOrderAdapter(@NonNull List<Item> data, Callback<ResponseOrder> confirm, Context mContext) {
        this.data = data;
        this.mContext = mContext;
    }

    @Override
    public int getItemViewType(int position) {
        if (data.get(position) instanceof DataAllOrder) {
            //Log.e("ok", "content2");
            return TYPE_CONTENT;
        } else if (data.get(position) instanceof Footer) {
            return TYPE_FOOTER;
        } else {
            throw new RuntimeException("ItemViewType unknown");
        }
    }

/*    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_all_news, parent, false);
        return new PaletteViewHolder(row);
    }*/

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == TYPE_CONTENT) {
            View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_all_orders, parent, false);
            return new PaletteViewHolder(row);
        } else {
            View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.progressbar_loadmore, parent, false);
            return new FooterViewHolder(row);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof PaletteViewHolder) {
            final DataAllOrder contentSet = (DataAllOrder) data.get(position);
            Timber.d("content set");
            Timber.d("content set -> " + contentSet.toString());

            PaletteViewHolder paletteViewHolder = (PaletteViewHolder) holder;
            //paletteViewHolder.getItems().setText(Html.fromHtml(contentSet.getItem()));
            paletteViewHolder.getInvoice().setText(contentSet.getInvoice());
            paletteViewHolder.getOrder_id().setText(String.valueOf(contentSet.getSales_order_id()));
            paletteViewHolder.getUser_name().setText(contentSet.getPemesan());
            paletteViewHolder.getAddress().setText(contentSet.getAlamat());
            String date_ok = new DateFormatter().convertDate(contentSet.getWaktuKirim(), "EEEE, dd MMMM yyyy");
            paletteViewHolder.getSend_time().setText(date_ok);
            //paletteViewHolder.getTerkirim().setText(String.valueOf(contentSet.getJumlah()));
            if (contentSet.getCatatan()==null)
                paletteViewHolder.getCatatan().setText("Tidak ada catatan.");
            else paletteViewHolder.getCatatan().setText(contentSet.getCatatan());

            paletteViewHolder.getBtnWA().setOnClickListener(v -> {
                try {
                    String toNumber = contentSet.getTelp(); // Replace with mobile phone number without +Sign or leading zeros.

                    toNumber = toNumber.replaceAll("\\D+", "");
                    if (toNumber.contains(" "))
                        toNumber = toNumber.replaceAll(" ", "");
                    if (toNumber.contains("-"))
                        toNumber = toNumber.replaceAll("-", "");
                    if (toNumber.charAt(0) == '0')
                        toNumber = "+62" + toNumber.substring(1, toNumber.length());

                    //Log.e("cek", toNumber);

                    Intent sendIntent = new Intent(Intent.ACTION_SENDTO, Uri.parse("smsto:" + "" + toNumber + "?body=" + ""));

                    sendIntent.setPackage("com.whatsapp");
                    mContext.startActivity(sendIntent);
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(mContext, "Anda tidak memiliki aplikasi WhatsApp", Toast.LENGTH_LONG).show();

                }
            });

            paletteViewHolder.getBtnCall().setOnClickListener(v -> {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + contentSet.getTelp()));

                if (ActivityCompat.checkSelfPermission(mContext,
                        Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(mContext, "Harap mengizinkan panggilan telepon", Toast.LENGTH_SHORT).show();
                    return;
                }
                mContext.startActivity(callIntent);
            });

            paletteViewHolder.getBtnSMS().setOnClickListener(v -> {
                Intent smsIntent = new Intent(Intent.ACTION_SENDTO, Uri.parse("smsto:" + contentSet.getTelp()));
                mContext.startActivity(smsIntent);
            });
            //Log.e("name", contentSet.getPemesan());

            holder.itemView.setOnClickListener(v -> {
                Intent intent = new Intent(mContext, DetailOrderActivity.class);
                intent.putExtra("data", contentSet);
                mContext.startActivity(intent);
            });

        }
        //FOOTER: nothing to do
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public void onRequestComplete(JSONObject jsonObject, Integer REQUEST_CODE) {

    }


    private class PaletteViewHolder extends RecyclerView.ViewHolder {
        private ImageView btnWA, btnCall, btnSMS;
        private TextView title, id, price, market_id, user_name, items, address, send_time, terkirim, catatan, invoice, order_id;

        PaletteViewHolder(View itemView) {
            super(itemView);
            user_name = itemView.findViewById(R.id.user_name);
            id = itemView.findViewById(R.id.id);
            invoice = itemView.findViewById(R.id.invoice);
            market_id = itemView.findViewById(R.id.market_id);
            items = itemView.findViewById(R.id.items);
            address = itemView.findViewById(R.id.address);
            send_time = itemView.findViewById(R.id.send_time);
            terkirim = itemView.findViewById(R.id.count_h);
            btnWA = itemView.findViewById(R.id.imageButtonWA);
            btnCall = itemView.findViewById(R.id.imageButtonPhone);
            btnSMS = itemView.findViewById(R.id.imageButtonSMS);
            catatan = itemView.findViewById(R.id.note);
            order_id = itemView.findViewById(R.id.order_id);


        }


        public TextView getOrder_id() {
            return order_id;
        }

        public TextView getInvoice() {
            return invoice;
        }

        public TextView getCatatan() {
            return catatan;
        }

        public ImageView getBtnWA() {
            return btnWA;
        }

        public ImageView getBtnCall() {
            return btnCall;
        }

        public ImageView getBtnSMS() {
            return btnSMS;
        }

        public TextView getTerkirim() {
            return terkirim;
        }

        private TextView getTitle() {
            return title;
        }

        private TextView getId() {
            return id;
        }

        private TextView getPrice() {
            return price;
        }

        private TextView getMarket_id() {
            return market_id;
        }

        public TextView getUser_name() {
            return user_name;
        }

        public TextView getItems() {
            return items;
        }

        public TextView getAddress() {
            return address;
        }

        public TextView getSend_time() {
            return send_time;
        }
    }


    public void clear() {
        final int size = data.size();
        data.clear();
        notifyItemRangeRemoved(0, size);
    }

    public static class FooterViewHolder extends RecyclerView.ViewHolder {

        ProgressBar progressBar;

        FooterViewHolder(View itemView) {
            super(itemView);
            progressBar = (ProgressBar) itemView.findViewById(R.id.progressBar1);
        }
    }
}