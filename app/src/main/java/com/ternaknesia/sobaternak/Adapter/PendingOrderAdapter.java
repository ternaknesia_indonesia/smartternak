package com.ternaknesia.sobaternak.Adapter;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.ternaknesia.sobaternak.DataModel.DataPendingOrder;
import com.ternaknesia.sobaternak.Interface.OnClickInterface;
import com.ternaknesia.sobaternak.Interface.RequestComplete;
import com.ternaknesia.sobaternak.R;
import com.ternaknesia.sobaternak.Utils.DateFormatter;
import com.ternaknesia.sobaternak.Utils.Footer;
import com.ternaknesia.sobaternak.Utils.Item;

import org.json.JSONObject;

import java.util.List;


public class PendingOrderAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{
    private List<Item> data;
    private OnClickInterface confirmButtonOrder;
    private Context mContext;

    private static final int TYPE_CONTENT = 0;
    private static final int TYPE_FOOTER = 1;

    public PendingOrderAdapter(@NonNull List<Item> data, OnClickInterface confirm, Context mContext) {
        this.data = data;
        this.confirmButtonOrder = confirm;
        this.mContext = mContext;
    }

    @Override
    public int getItemViewType(int position) {
        if (data.get(position) instanceof DataPendingOrder) {
            //Log.e("ok", "content2");
            return TYPE_CONTENT;
        } else if (data.get(position) instanceof Footer) {
            return TYPE_FOOTER;
        } else {
            throw new RuntimeException("ItemViewType unknown");
        }
    }

/*    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_all_news, parent, false);
        return new PaletteViewHolder(row);
    }*/

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_CONTENT) {
            View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_pending_product, parent, false);
            return new PaletteViewHolder(row);
        } else {
            View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.progressbar_loadmore, parent, false);
            return new FooterViewHolder(row);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof PaletteViewHolder) {
            final DataPendingOrder contentSet = (DataPendingOrder) data.get(position);

            PaletteViewHolder paletteViewHolder = (PaletteViewHolder) holder;
            paletteViewHolder.getInvoice().setText(String.valueOf(contentSet.getInvoice()));
            paletteViewHolder.getOrder_id().setText(String.valueOf(contentSet.getSales_order_id()));
            paletteViewHolder.getItems().setText(Html.fromHtml(contentSet.getItem()));
            paletteViewHolder.getUser_name().setText(contentSet.getPemesan());
            paletteViewHolder.getAddress().setText(contentSet.getDestination());
            String date_ok = new DateFormatter().convertDate(contentSet.getSendTime(), "EEEE, dd MMMM yyyy");
            paletteViewHolder.getSend_time().setText(date_ok);
            paletteViewHolder.getTerkirim().setText(String.valueOf(contentSet.getJumlah()));
            if(contentSet.getCatatan().equalsIgnoreCase("null"))
                paletteViewHolder.getCatatan().setText("Tidak ada catatan.");
            else paletteViewHolder.getCatatan().setText(contentSet.getCatatan());

            paletteViewHolder.getKonfirmasi().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    confirmButtonOrder.onConfirmOrderClicked(contentSet, position);
                }
            });

            paletteViewHolder.getBtnWA().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        String toNumber = contentSet.getUser_telp(); // Replace with mobile phone number without +Sign or leading zeros.

                        toNumber = toNumber.replaceAll("\\D+", "");
                        if (toNumber.contains(" "))
                            toNumber = toNumber.replaceAll(" ", "");
                        if (toNumber.contains("-"))
                            toNumber = toNumber.replaceAll("-", "");
                        if (toNumber.charAt(0) == '0')
                            toNumber = "+62" + toNumber.substring(1, toNumber.length());

                        Intent sendIntent = new Intent(Intent.ACTION_SENDTO, Uri.parse("smsto:" + "" + toNumber + "?body=" + ""));

                        sendIntent.setPackage("com.whatsapp");
                        mContext.startActivity(sendIntent);
                    } catch (Exception e) {
                        e.printStackTrace();
                        Toast.makeText(mContext, "Anda tidak memiliki aplikasi WhatsApp", Toast.LENGTH_LONG).show();

                    }
                }
            });

            paletteViewHolder.getBtnCall().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse("tel:" + contentSet.getUser_telp()));

                    if (ActivityCompat.checkSelfPermission(mContext,
                            Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                        Toast.makeText(mContext, "Harap mengizinkan panggilan telepon", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    mContext.startActivity(callIntent);
                }
            });

            paletteViewHolder.getBtnSMS().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent smsIntent = new Intent(Intent.ACTION_SENDTO, Uri.parse("smsto:" + contentSet.getUser_telp()));
                    mContext.startActivity(smsIntent);
                }
            });
            //Log.e("name", contentSet.getPemesan());
        }
        //FOOTER: nothing to do
    }

    @Override
    public int getItemCount() {
        return data.size();
    }


    private class PaletteViewHolder extends RecyclerView.ViewHolder {
        private ImageView btnWA, btnCall, btnSMS;
        private TextView title, id, price, market_id, user_name, items, address, send_time, terkirim, catatan, invoice, order_id;
        private Button konfirmasi;

        PaletteViewHolder(View itemView) {
            super(itemView);
            user_name = itemView.findViewById(R.id.user_name);
            id = itemView.findViewById(R.id.id);
            market_id = itemView.findViewById(R.id.market_id);
            items = itemView.findViewById(R.id.items);
            address = itemView.findViewById(R.id.address);
            send_time = itemView.findViewById(R.id.send_time);
            terkirim = itemView.findViewById(R.id.count_h);
            konfirmasi = itemView.findViewById(R.id.confirm);
            btnWA = itemView.findViewById(R.id.imageButtonWA);
            btnCall = itemView.findViewById(R.id.imageButtonPhone);
            btnSMS = itemView.findViewById(R.id.imageButtonSMS);
            catatan = itemView.findViewById(R.id.note);
            invoice = itemView.findViewById(R.id.invoice);
            order_id = itemView.findViewById(R.id.order_id);

        }

        public TextView getInvoice() {
            return invoice;
        }

        public TextView getOrder_id() {
            return order_id;
        }

        public TextView getCatatan() {
            return catatan;
        }

        public ImageView getBtnWA() {
            return btnWA;
        }

        public ImageView getBtnCall() {
            return btnCall;
        }

        public ImageView getBtnSMS() {
            return btnSMS;
        }

        public Button getKonfirmasi() {
            return konfirmasi;
        }

        public TextView getTerkirim() {
            return terkirim;
        }

        private TextView getTitle() {
            return title;
        }

        private TextView getId() {
            return id;
        }

        private TextView getPrice() {
            return price;
        }

        private TextView getMarket_id() {
            return market_id;
        }

        public TextView getUser_name() {
            return user_name;
        }

        public TextView getItems() {
            return items;
        }

        public TextView getAddress() {
            return address;
        }

        public TextView getSend_time() {
            return send_time;
        }
    }


    public void clear() {
        final int size = data.size();
        data.clear();
        notifyItemRangeRemoved(0, size);
    }

    public static class FooterViewHolder extends RecyclerView.ViewHolder {

        ProgressBar progressBar;

        FooterViewHolder(View itemView) {
            super(itemView);
            progressBar = (ProgressBar) itemView.findViewById(R.id.progressBar1);
        }
    }
}