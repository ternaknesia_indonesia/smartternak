package com.ternaknesia.sobaternak.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.ternaknesia.sobaternak.activity.EditCategoryActivity;
import com.ternaknesia.sobaternak.DataModel.DataCategory;
import com.ternaknesia.sobaternak.R;
import com.ternaknesia.sobaternak.Utils.Footer;
import com.ternaknesia.sobaternak.Utils.Item;

import java.util.List;


public class SetCatAdapter extends RecyclerView.Adapter<SetCatAdapter.PaletteViewHolder> {

    private List<Item> data;
    private Context mContext;

    private static final int TYPE_CONTENT = 0;
    private static final int TYPE_FOOTER = 1;

    public SetCatAdapter(@NonNull List<Item> data, Context mContext) {
        this.data = data;
        this.mContext = mContext;
    }

    @Override
    public int getItemViewType(int position) {
        if (data.get(position) instanceof DataCategory) {
            return TYPE_CONTENT;
        } else if (data.get(position) instanceof Footer) {
            return TYPE_FOOTER;
        } else {
            throw new RuntimeException("ItemViewType unknown");
        }
    }

    @NonNull
    @Override
    public SetCatAdapter.PaletteViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_cat, parent,
                false);
        return new PaletteViewHolder(row);
    }

    @Override
    public void onBindViewHolder(@NonNull PaletteViewHolder holder, int position) {
        holder.bind((DataCategory) data.get(position));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class PaletteViewHolder extends RecyclerView.ViewHolder {
        private TextView id;
        private TextView name;
        private TextView range;
        private Button edit;

        PaletteViewHolder(View itemView) {
            super(itemView);
            id = itemView.findViewById(R.id.id);
            name = itemView.findViewById(R.id.name);
            range = itemView.findViewById(R.id.range);
            edit = itemView.findViewById(R.id.btn_edit);

        }

        void bind(DataCategory data) {
            Log.d(SetCatAdapter.class.getSimpleName(), "bind: kandang_id " + data.getName());
            id.setText(data.getId());
            name.setText(data.getName());
            range.setText(data.getRange());
            edit.setOnClickListener(v -> {
                Intent intent = new Intent(mContext, EditCategoryActivity.class);
                Bundle extras = new Bundle();
                extras.putString("id", data.getId());
                extras.putString("kandang_id", data.getName());
                extras.putString("min_weight", data.getMin_w());
                extras.putString("max_weight", data.getMax_w());
                intent.putExtras(extras);
                ((Activity) mContext).startActivityForResult(intent, 7000);
            });
        }
    }

}