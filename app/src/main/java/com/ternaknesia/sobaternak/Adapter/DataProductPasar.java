package com.ternaknesia.sobaternak.Adapter;

import com.ternaknesia.sobaternak.Utils.Item;

/**
 * Created by A MADRISH on 9/15/2016
 */
public class DataProductPasar extends Item {

    private String mTextId;
    private String mTextName;
    private String mTextRemain;
    private String mTextPrice;
    private String mTextLinkImg;
    private String mTextRoi;
    private String mTextStatus;
    private int mProgress;

    public DataProductPasar(String ProjectId, String ProjectName, String RemainSlot, String ProjectPrice, String Roi, int progress, String link_img, String status){
        mTextId = ProjectId;
        mTextName = ProjectName;
        mTextRemain = RemainSlot;
        mTextPrice = ProjectPrice;
        mTextLinkImg = link_img;
        mTextRoi = Roi;
        mProgress = progress;
        mTextStatus = status;
    }

    public String getProjectName() {
        return mTextName;
    }
    public String getRemainSlot() {
        return mTextRemain;
    }
    public String getProjectPrice() {
        int price = Integer.parseInt(mTextPrice);
        return String.format("Rp. %,d", price).replaceAll(",", ".");
    }
    public String getProjectId() {
        return mTextId;
    }
    public String getmTextRoi() {return mTextRoi;}
    public String getLinkImg() {
        return mTextLinkImg;
    }
    public int getmProgress () {
        return mProgress;
    }

    public String getmTextStatus() {
        return mTextStatus;
    }
}