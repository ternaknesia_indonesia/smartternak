package com.ternaknesia.sobaternak.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.ternaknesia.sobaternak.activity.EditBobotActivity;
import com.ternaknesia.sobaternak.DataModel.DataBobot;
import com.ternaknesia.sobaternak.R;
import com.ternaknesia.sobaternak.Utils.Footer;
import com.ternaknesia.sobaternak.Utils.Item;

import java.util.List;


public class BobotAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<Item> data;
    private Context mContext;

    private static final int TYPE_CONTENT = 0;
    private static final int TYPE_FOOTER = 1;

    public BobotAdapter(@NonNull List<Item> data, Context mContext) {
        this.data = data;
        this.mContext = mContext;
    }

    @Override
    public int getItemViewType(int position) {
        if (data.get(position) instanceof DataBobot) {
            return TYPE_CONTENT;
        } else if (data.get(position) instanceof Footer) {
            return TYPE_FOOTER;
        } else {
            throw new RuntimeException("ItemViewType unknown");
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_CONTENT) {
            View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_bobot, parent, false);
            return new BobotAdapter.PaletteViewHolder(row);
        } else {
            View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.progressbar_loadmore, parent, false);
            return new BobotAdapter.FooterViewHolder(row);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof PaletteViewHolder) {
            DataBobot contentSet = (DataBobot) data.get(position);
            final PaletteViewHolder p = (PaletteViewHolder) holder;

            p.getId().setText(contentSet.getId());
            p.getKandang_id().setText(contentSet.getKandang_id());

            p.getKandang_name().setText(contentSet.getKandang_name());
            p.getKandang_anak().setText(contentSet.getKandang_anak());
            p.getKandang_lokasi().setText(contentSet.getKandang_lokasi());

            p.getSampel().setText(contentSet.getSample());
            p.getTahun().setText(contentSet.getTahun());
            p.getBulan().setText(contentSet.getBulan());

            p.getBerat().setText(contentSet.getWeight());
        }

        //FOOTER: nothing to do
    }

    @Override
    public int getItemCount() {
        return data.size();
    }


    private class PaletteViewHolder extends RecyclerView.ViewHolder {
        private TextView kandang_id;
        private TextView instance_id;
        private TextView kandang_name;
        private TextView kandang_anak;
        private TextView kandang_lokasi;
        private TextView tahun;
        private TextView bulan;
        private TextView berat;
        private TextView sampel;
        private ImageView img_ops;
        private TextView id;

        PaletteViewHolder(View itemView) {
            super(itemView);
            id = itemView.findViewById(R.id.id);
            kandang_id = itemView.findViewById(R.id.kandang_id);
            instance_id = itemView.findViewById(R.id.instance_id);
            kandang_name = itemView.findViewById(R.id.name_kandang);
            kandang_anak = itemView.findViewById(R.id.anak_kandang);
            kandang_lokasi = itemView.findViewById(R.id.lokasi_kandang);
            tahun = itemView.findViewById(R.id.tahun);
            bulan = itemView.findViewById(R.id.bulan);
            berat = itemView.findViewById(R.id.berat);
            sampel = itemView.findViewById(R.id.sample);
            img_ops = itemView.findViewById(R.id.ops2);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    /*Intent n = new Intent(mContext, EditHewanActivity.class);
                    Bundle extras = new Bundle();
                    extras.putInt("id", Integer.parseInt(id.getText().toString()));
                    extras.putInt("instance_id", Integer.parseInt(instance_id.getText().toString()));
                    n.putExtras(extras);
                    ((Activity)mContext).startActivityForResult(n, 5000);*/
                }
            });

            img_ops.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showMenu(v);
                }
            });
        }

        public TextView getSampel() {
            return sampel;
        }

        public TextView getId() {
            return id;
        }

        public TextView getKandang_id() {
            return kandang_id;
        }

        public TextView getInstance_id() {
            return instance_id;
        }

        public TextView getKandang_name() {
            return kandang_name;
        }

        public TextView getKandang_anak() {
            return kandang_anak;
        }

        public TextView getKandang_lokasi() {
            return kandang_lokasi;
        }

        public TextView getTahun() {
            return tahun;
        }

        public TextView getBulan() {
            return bulan;
        }

        public TextView getBerat() {
            return berat;
        }

        void showMenu(View v) {
            PopupMenu popup = new PopupMenu(mContext, v);
            popup.inflate(R.menu.item_kandang_menu);
            ((Activity)mContext).invalidateOptionsMenu();
            popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                @Override
                public boolean onMenuItemClick(MenuItem item) {
                    if (item.getItemId() == R.id.item_edit) {
                        Intent i = new Intent(mContext, EditBobotActivity.class);
                        Bundle extras = new Bundle();
                        extras.putString("id", id.getText().toString());
                        extras.putString("kandang_id", kandang_id.getText().toString());
                        extras.putString("weight", berat.getText().toString().replaceAll("\\D+", ""));
                        extras.putString("bulan", bulan.getText().toString());
                        extras.putString("tahun", tahun.getText().toString());
                        extras.putString("checked_tags", sampel.getText().toString());

                        i.putExtras(extras);
                        ((Activity) mContext).startActivityForResult(i, 6000);
                    } else if (item.getItemId() == R.id.item_hapus){
                        Toast.makeText(mContext, "Sementara fitur belum tersedia", Toast.LENGTH_LONG).show();
                    }
                    return item.isChecked();
                }
            });
            popup.show();
        }
    }

    private static class FooterViewHolder extends RecyclerView.ViewHolder {

        ProgressBar progressBar;

        FooterViewHolder(View itemView) {
            super(itemView);
            progressBar = (ProgressBar) itemView.findViewById(R.id.progressBar1);
        }
    }
}