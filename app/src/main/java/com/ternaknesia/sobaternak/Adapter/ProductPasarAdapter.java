package com.ternaknesia.sobaternak.Adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.ternaknesia.sobaternak.DataModel.DataProject;
import com.ternaknesia.sobaternak.R;
import com.ternaknesia.sobaternak.Utils.Item;
import com.ternaknesia.sobaternak.Utils.RoundedCornersTransformation;

import java.util.List;

public class ProductPasarAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<Item> data;
    private Context context;


    private static final int TYPE_CONTENT = 0;
    private static final int TYPE_FOOTER = 1;

    public ProductPasarAdapter(@NonNull List<Item> data, Context context) {
        this.data = data;
        this.context = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_product_pasar, parent, false);
        return new PaletteViewHolder(row);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof PaletteViewHolder) {
            DataProject contentSet = (DataProject) data.get(position);

            PaletteViewHolder paletteViewHolder = (PaletteViewHolder) holder;
            ImageView imageView = paletteViewHolder.getImg();
/*            String url = contentSet.getLinkImg();
            Glide.with(context)
                    .load(url)
                    .placeholder(R.drawable.placeholder_catalog)
                    .centerCrop()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .skipMemoryCache(true)
                    .bitmapTransform(new RoundedCornersTransformation(context, 15, 0))
                    .into(imageView);*/

/*            paletteViewHolder.getPrice().setText(contentSet.getProjectPrice());
            paletteViewHolder.getProject_name().setText(contentSet.getProjectName());
            paletteViewHolder.getProject_id().setText(contentSet.getProjectId());
            paletteViewHolder.getProject_roi().setText(contentSet.getmTextRoi());
            paletteViewHolder.getProgressBar().setProgress(contentSet.getmProgress());*/
            /*if(Integer.parseInt(contentSet.getRemainSlot())!=0){
                paletteViewHolder.getRemain_slot().setText(contentSet.getRemainSlot()+" Lembar");
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    //paletteViewHolder.getBack_ava().setBackground(context.getResources().getDrawable(R.drawable.back_available));
                }
                //paletteViewHolder.getStatus().setBackgroundColor(context.getResources().getColor(R.color.available));
            }else {
                paletteViewHolder.getRemain_slot().setText("Sold out");
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    //paletteViewHolder.getBack_ava().setBackground(context.getResources().getDrawable(R.drawable.back_soldout));
                }
                //paletteViewHolder.getStatus().setBackgroundColor(context.getResources().getColor(R.color.soldout));
            }

            if(contentSet.getmTextStatus().equalsIgnoreCase("comingsoon")){
                paletteViewHolder.getRemain_slot().setText("Coming soon");
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    //paletteViewHolder.getBack_ava().setBackground(context.getResources().getDrawable(R.drawable.back_available));
                }
            } else if (contentSet.getmTextStatus().equalsIgnoreCase("finished")){
                paletteViewHolder.getRemain_slot().setText("Selesai");
                //paletteViewHolder.getBack_ava().setBackground(context.getResources().getDrawable(R.drawable.back_finished));
            }*/
        }

        //FOOTER: nothing to do
    }

    @Override
    public int getItemCount() {
        return data.size();
    }



    private class PaletteViewHolder extends RecyclerView.ViewHolder {
        private ImageView img;
        private TextView remain_slot;
        private TextView price;
        private TextView project_name;
        private TextView project_roi;
        private TextView project_id;
        private LinearLayout back_ava;
        private ProgressBar progressBar;

        PaletteViewHolder(View itemView) {
            super(itemView);
            img = itemView.findViewById(R.id.coverImageView);
            project_id = itemView.findViewById(R.id.project_id);
            project_name = itemView.findViewById(R.id.project_name);
            remain_slot = itemView.findViewById(R.id.remainslot);
            price = itemView.findViewById(R.id.price);
            back_ava = itemView.findViewById(R.id.back_available);
            //project_roi = itemView.findViewById(R.id.desc_qurban);
            //progressBar = itemView.findViewById(R.id.progress_);
            progressBar.getProgressDrawable().setColorFilter(Color.parseColor("#00bfa5"), PorterDuff.Mode.SRC_IN);

            //Typeface segoe_light = Typeface.createFromAsset(context.getAssets(), "fonts/varelaround.ttf");
            //project_name.setTypeface(segoe_light);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    /*Intent i = new Intent(context, DetailProjectActivity.class);
                    Bundle extras = new Bundle();
                    //extras.putParcelable("bitmap", image);
                    extras.putString("project_id", project_id.getText().toString());
                    i.putExtras(extras);
                    context.startActivity(i);*/
                }
            });
            /*itemView.setOnLongClickListener(new View.OnLongClickListener() {

                @Override
                public boolean onLongClick(View v) {
                        ClipboardManager clip = (ClipboardManager) App.getContext().getSystemService(Context.CLIPBOARD_SERVICE);
                        clip.setText("Golongan Darah : " + remain_slot.getText().toString() + "\nJumlah Stok : " + project_name.getText().toString()
                                + "\nLokasi : " + price.getText().toString());
                        Toast.makeText(context, "Copied to clipboard!", Toast.LENGTH_SHORT).show();
                    return true;
                }
            });*/
        }

        TextView getRemain_slot() {
            return remain_slot;
        }

        TextView getPrice() {
            return price;
        }

        TextView getProject_name() {
            return project_name;
        }

        TextView getProject_id() {
            return project_id;
        }

        TextView getProject_roi() {return project_roi;}

        LinearLayout getBack_ava() {
            return back_ava;
        }

        ImageView getImg(){return img;}

        ProgressBar getProgressBar(){return progressBar;}
    }
}