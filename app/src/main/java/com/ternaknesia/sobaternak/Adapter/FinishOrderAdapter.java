package com.ternaknesia.sobaternak.Adapter;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.ternaknesia.sobaternak.DataModel.DataAllOrder;
import com.ternaknesia.sobaternak.DataModel.sales_order.Data;
import com.ternaknesia.sobaternak.R;
import com.ternaknesia.sobaternak.activity.DetailOrderActivity;

import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;


public class FinishOrderAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<Data> listData;
    private static final int TYPE_CONTENT = 0;
    private static final int TYPE_FOOTER = 1;
    private boolean isLoading = false;

    public FinishOrderAdapter(@NonNull List<Data> listData) {
        this.listData = listData;
    }

    public List<Data> getAll() {
        return listData;
    }

    @Override
    public int getItemViewType(int position) {
        return listData.get(position) == null ? TYPE_FOOTER : TYPE_CONTENT;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == TYPE_CONTENT) {
            View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_finish_product, parent, false);
            return new MyViewHolder(row);
        } else if (viewType == TYPE_FOOTER) {
            View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.progressbar_loadmore, parent, false);
            return new LoadingViewHolder(row);
        } else return null;
    }

    public void addLoading() {
        isLoading = true;
        listData.add(null);
        notifyItemInserted(listData.size() - 1);
    }

    public boolean isLoading() {
        return isLoading;
    }

    public void removeLoading() {
        isLoading = false;
        int position = listData.size() - 1;
        if (position != -1) {
            Data item = getItem(position);
            if (item != null) {
                listData.remove(position);
                notifyItemRemoved(position);
            }
        }
    }

    private Data getItem(int position) {
        return listData.get(position);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof MyViewHolder) {
            MyViewHolder myViewHolder = (MyViewHolder) holder;
            Data data = listData.get(position);
            myViewHolder.id.setText(data.getProductId() + "");
            myViewHolder.marketId.setText(data.getMarketId() + "");
            myViewHolder.invoice.setText(data.getNoInvoice());
            myViewHolder.items.setText(Html.fromHtml(data.getNama()));
            myViewHolder.orderId.setText(data.getSalesOrderId() + "");
            myViewHolder.userName.setText(data.getName());
            String alamat;
            if (data.getIsDisalurkan() == 1) {
                alamat = "Disalurkan";
            } else {
                alamat = data.getAlamatKirim();
                if (data.getSalesOrder().getKecamatan() != null && data.getSalesOrder().getKota() != null) {
                    String kec = data.getSalesOrder().getKecamatan().getName();
                    String kot = data.getSalesOrder().getKota().getName();
                    alamat = alamat + "\n" + kec + ", " + kot;
                }
            }
            myViewHolder.address.setText(alamat);
            myViewHolder.sendTime.setText(data.getTanggal());
            myViewHolder.note.setText(data.getCatatan() == null ? data.getCatatan() : "Tidak ada catatan");
            if (data.getTerkirim() < 0) {
                myViewHolder.terkirim.setText(String.format(Locale.getDefault(), "0/%d Terkirim", data.getJumlah()));
            } else {
                myViewHolder.terkirim.setText(String.format(Locale.getDefault(), "%d/%d Terkirim", data.getTerkirim(), data.getJumlah()));
            }

            DataAllOrder contentSet = new DataAllOrder(data.getSalesOrderId(), data.getNoInvoice(),
                    data.getName(), data.getTerbayar(), data.getCatatan(), data.getTelp(),
                    data.getAlamatKirim(), data.getWaktuKirim(), data.getTanggal(), data.getStatus(),
                    data.getDeliveryStatus(), null, data.getDiskon(), data.getGrandTotal(),
                    Integer.valueOf(data.getMinDownpayment()), data.getKodeUnik());

            holder.itemView.setOnClickListener(v -> {
                Intent intent = new Intent(holder.itemView.getContext(), DetailOrderActivity.class);
                intent.putExtra("data", contentSet);
                holder.itemView.getContext().startActivity(intent);
            });

        } else if (holder instanceof LoadingViewHolder) {
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        }
    }

    @Override
    public int getItemCount() {
        return listData.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.id)
        TextView id;
        @BindView(R.id.market_id)
        TextView marketId;
        @BindView(R.id.invoice)
        TextView invoice;
        @BindView(R.id.items)
        TextView items;
        @BindView(R.id.order_id)
        TextView orderId;
        @BindView(R.id.imageView6)
        ImageView imageView6;
        @BindView(R.id.user_name)
        TextView userName;
        @BindView(R.id.address)
        TextView address;
        @BindView(R.id.send_time)
        TextView sendTime;
        @BindView(R.id.note)
        TextView note;
        @BindView(R.id.terkirim)
        TextView terkirim;
        @BindView(R.id.view5)
        View view5;

        MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    private class LoadingViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public LoadingViewHolder(View view) {
            super(view);
            progressBar = view.findViewById(R.id.progressBar1);
        }
    }
}
