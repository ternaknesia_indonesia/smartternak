package com.ternaknesia.sobaternak.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.ternaknesia.sobaternak.DataModel.DataDriver;
import com.ternaknesia.sobaternak.R;
import com.ternaknesia.sobaternak.Utils.Footer;
import com.ternaknesia.sobaternak.Utils.Item;

import java.util.List;


public class DriverAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{
    private List<Item> data;
    private Context mContext;

    private static final int TYPE_CONTENT = 0;
    private static final int TYPE_FOOTER = 1;

    public DriverAdapter(@NonNull List<Item> data, Context mContext) {
        this.data = data;
        this.mContext = mContext;
    }

    @Override
    public int getItemViewType(int position) {
        if (data.get(position) instanceof DataDriver) {
            //Log.e("ok", "content2");
            return TYPE_CONTENT;
        } else if (data.get(position) instanceof Footer) {
            return TYPE_FOOTER;
        } else {
            throw new RuntimeException("ItemViewType unknown");
        }
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_CONTENT) {
            View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_driver, parent, false);
            return new PaletteViewHolder(row);
        } else {
            View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.progressbar_loadmore, parent, false);
            return new FooterViewHolder(row);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof PaletteViewHolder) {
            final DataDriver contentSet = (DataDriver) data.get(position);

            PaletteViewHolder paletteViewHolder = (PaletteViewHolder) holder;
            paletteViewHolder.getName().setText(Html.fromHtml(contentSet.getName()));
            paletteViewHolder.getPhone().setText(contentSet.getPhone());

        }
        //FOOTER: nothing to do
    }

    @Override
    public int getItemCount() {
        return data.size();
    }


    private class PaletteViewHolder extends RecyclerView.ViewHolder {
        private TextView name, phone;

        PaletteViewHolder(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.name);
            phone = itemView.findViewById(R.id.phone);

        }

        public TextView getName() {
            return name;
        }

        public TextView getPhone() {
            return phone;
        }
    }


    public void clear() {
        final int size = data.size();
        data.clear();
        notifyItemRangeRemoved(0, size);
    }

    public static class FooterViewHolder extends RecyclerView.ViewHolder {
        ProgressBar progressBar;
        FooterViewHolder(View itemView) {
            super(itemView);
            progressBar = (ProgressBar) itemView.findViewById(R.id.progressBar1);
        }
    }
}