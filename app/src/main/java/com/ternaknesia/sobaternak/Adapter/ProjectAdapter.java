package com.ternaknesia.sobaternak.Adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.ternaknesia.sobaternak.activity.DetailKeuanganActivity;
import com.ternaknesia.sobaternak.DataModel.DataProject;
import com.ternaknesia.sobaternak.Interface.EndpointAPI;
import com.ternaknesia.sobaternak.R;
import com.ternaknesia.sobaternak.Utils.ImageConverter;
import com.ternaknesia.sobaternak.Utils.Item;

import java.util.List;


public class ProjectAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<Item> data;
    private Context mContext;

    private static final int TYPE_CONTENT = 0;
    private static final int TYPE_FOOTER = 1;

    public ProjectAdapter(@NonNull List<Item> data, Context context) {
        this.data = data;
        this.mContext = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_project, parent, false);
        return new PaletteViewHolder(row);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof PaletteViewHolder) {
            DataProject contentSet = (DataProject) data.get(position);
            final PaletteViewHolder paletteViewHolder = (PaletteViewHolder) holder;

            paletteViewHolder.getId().setText(contentSet.getId());
            paletteViewHolder.getInstanceId().setText(contentSet.getInstanceId());
            paletteViewHolder.getName().setText(contentSet.getName());
            paletteViewHolder.getPeriode().setText(contentSet.getPeriode());

            Glide.with(mContext)
                    .load(EndpointAPI.HostStorage + contentSet.getImage())
                    .asBitmap()
                    .centerCrop()
                    .into(new BitmapImageViewTarget(paletteViewHolder.getImageView()) {
                        @Override
                        protected void setResource(Bitmap resource) {
                            RoundedBitmapDrawable rounded =
                                    RoundedBitmapDrawableFactory.create(mContext.getResources(), resource);
                            rounded.setCircular(true);
                            Bitmap b = ImageConverter.getRoundedCornerBitmap(resource, 10);
                            paletteViewHolder.getImageView().setImageBitmap(b);
                        }
                    });

        }

        //FOOTER: nothing to do
    }

    @Override
    public int getItemCount() {
        return data.size();
    }


    private class PaletteViewHolder extends RecyclerView.ViewHolder {
        private TextView id;
        private TextView instance_id;
        private TextView name;
        private TextView periode;
        private ImageView imageView;


        PaletteViewHolder(View itemView) {
            super(itemView);
            id = itemView.findViewById(R.id.project_id);
            instance_id = itemView.findViewById(R.id.instance_id);
            name = itemView.findViewById(R.id.code_tag);
            periode = itemView.findViewById(R.id.periode);
            imageView = itemView.findViewById(R.id.coverImageView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String id_ = id.getText().toString();
                    Intent n = new Intent(mContext, DetailKeuanganActivity.class);
                    Bundle extras = new Bundle();
                    extras.putString("project_id", id_);
                    extras.putString("instance_id", instance_id.getText().toString());
                    n.putExtras(extras);
                    mContext.startActivity(n);
                }
            });
        }

        TextView getId() {
            return id;
        }

        TextView getInstanceId() { return instance_id; }

        TextView getName() {
            return name;
        }

        ImageView getImageView() {
            return imageView;
        }

        TextView getPeriode() {
            return periode;
        }
    }
}