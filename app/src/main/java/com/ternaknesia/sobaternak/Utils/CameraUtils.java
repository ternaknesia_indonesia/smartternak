package com.ternaknesia.sobaternak.Utils;

/*
 * Created by A MADRISH on 1/29/2018
 */

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;


public class CameraUtils {

    //Get Uri Of captured Image
    public static Uri getOutputMediaFileUri(Context context) {
        File mediaStorageDir = new File(Environment.getExternalStorageDirectory().toString(),"SmartTernak");
        File mediaStorageDir1 = new File(Environment.getExternalStorageDirectory().toString(),"SmartTernak/Image");
        //If File is not present create directory
        if (!mediaStorageDir.exists()) {
            if (mediaStorageDir.mkdir())
                if (!mediaStorageDir1.exists()) {
                    if (mediaStorageDir1.mkdir())
                        Log.e("Success", "Ok");
                }
        }else {
            if (!mediaStorageDir1.exists()) {
                if (mediaStorageDir1.mkdir())
                    Log.e("Success", "Ok");
            }
        }

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());//Get Current timestamp
        File mediaFile = new File(mediaStorageDir1.getPath() + File.separator
                + "IMG_" + timeStamp + ".jpg");//create image path with system mill and image format
        return Uri.fromFile(mediaFile);

    }

    /*  Convert Captured image path into Bitmap to display over ImageView  */
    public static Bitmap convertImagePathToBitmap(String imagePath, boolean scaleBitmap) {
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        Bitmap bitmap = BitmapFactory.decodeFile(imagePath, bmOptions);//Decode image path

        //If you want to scale bitmap/reduce captured image size then send true
        if (scaleBitmap)
            return Bitmap.createScaledBitmap(bitmap, 500, 500, true);
        else
            //if you don't want to scale bitmap then send false
            return bitmap;
    }
}