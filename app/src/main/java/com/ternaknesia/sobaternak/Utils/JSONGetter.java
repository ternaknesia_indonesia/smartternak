package com.ternaknesia.sobaternak.Utils;

import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class JSONGetter {
    public static Integer getInt(JSONObject data, String name, Integer when_fail) {
        try {
            return data.getInt(name);
        } catch (Exception e) {
            return when_fail;
        }
    }

    public static Double getDouble(JSONObject data, String name, Double when_fail) {
        try {
            return data.getDouble(name);
        } catch (Exception e) {
            return when_fail;
        }
    }

    public static String getRupiahFormat(JSONObject data, String name, String when_fail) {
        try {
            DecimalFormat format = (DecimalFormat) DecimalFormat.getCurrencyInstance();
            DecimalFormatSymbols format_sym = new DecimalFormatSymbols();
            format_sym.setCurrencySymbol("Rp. ");
            format_sym.setMonetaryDecimalSeparator(',');
            format_sym.setGroupingSeparator('.');
            format.setDecimalFormatSymbols(format_sym);
            return format.format(data.getDouble(name));
        } catch (Exception e) {
            return when_fail;
        }
    }

    public static String getString(JSONObject data, String name, String when_fail) {
        try {
            String val = data.getString(name);
            return !val.equalsIgnoreCase( "null") && !val.equalsIgnoreCase("") ? val : when_fail;
        } catch (Exception e) {
            return when_fail;
        }
    }

    public static String getUpperCase(JSONObject data, String name, String when_fail) {
        try {
            String str = data.getString(name);
            return str.substring(0,1).toUpperCase() + str.substring(1);
        } catch (Exception e) {
            return when_fail;
        }
    }

    public static String getDate(JSONObject data, String name, String src_format, String dst_format, String when_fail) {
        try {
            String dt = data.getString(name);
            SimpleDateFormat src = new SimpleDateFormat(src_format);
            SimpleDateFormat dst = new SimpleDateFormat(dst_format);
            Date date = src.parse(dt);
            return dst.format(date);
        } catch (Exception e) {
            return when_fail;
        }
    }

    public static String getFromArrayListString(JSONObject data, String name, ArrayList<String> array, String when_fail) {
        try {
            return array.get(data.getInt(name));
        } catch (Exception e) {
            return when_fail;
        }
    }
}
