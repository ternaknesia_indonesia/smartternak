package com.ternaknesia.sobaternak.Utils;

/*
 * Created by A MADRISH on 1/29/2018
 */

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;


public class DateFormatter {

    public int getDateDiff(Date date1, Date date2) {
        int diffInDays = (int)( (date2.getTime() - date1.getTime())
                / (1000 * 60 * 60 * 24) );
        return Math.abs(diffInDays);
    }

    public String convertFromDate(Date date, String outFormat){
        SimpleDateFormat format = new SimpleDateFormat(outFormat, Locale.getDefault());
        return format.format(date);
    }

    public Date newDateFrom(String inputDate, String inputFormat){
        SimpleDateFormat format = new SimpleDateFormat(inputFormat, Locale.getDefault());
        Date newDate;
        try {
            newDate = format.parse(inputDate);
        } catch (ParseException e) {
            //e.printStackTrace();
            return null;
        }
        if (newDate != null) return newDate;
        else return null;
    }

    public Date newDateFromLong(long dateLong){
        return new Date(dateLong);
    }

    public String getCurrentTime(){
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).format(new Date());
    }

    public long getLongFrom(String inputDate, String inputFormat){
        SimpleDateFormat formatter = new SimpleDateFormat(inputFormat, Locale.getDefault());
        Date a = null;
        try {
            a = formatter.parse(inputDate);
        } catch (ParseException e) {
            //e.printStackTrace();
        }
        if (a != null) return a.getTime();
        else return 0;
    }

    //convert format
    public String convertDate(String inputDate, String outputFormat) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        Date newDate = null;
        try {
            newDate = format.parse(inputDate);
        } catch (ParseException e) {
            //e.printStackTrace();
        }
        format = new SimpleDateFormat(outputFormat, Locale.getDefault());
        return format.format(newDate);
    }


    public String convertDate(String inputDate, String inputFormat, String outputFormat) {
        SimpleDateFormat format = new SimpleDateFormat(inputFormat, Locale.getDefault());
        Date newDate = null;
        try {
            newDate = format.parse(inputDate);
        } catch (ParseException e) {
            //e.printStackTrace();
        }
        format = new SimpleDateFormat(outputFormat, Locale.getDefault());
        return format.format(newDate);
    }
}