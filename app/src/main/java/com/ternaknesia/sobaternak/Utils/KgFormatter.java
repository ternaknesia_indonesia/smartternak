package com.ternaknesia.sobaternak.Utils;

/**
 * Created by A MADRISH on 11/30/2017.
 */


import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.formatter.IValueFormatter;
import com.github.mikephil.charting.utils.ViewPortHandler;

import java.text.DecimalFormat;

public class KgFormatter implements IValueFormatter, IAxisValueFormatter {

    private DecimalFormat mFormat;

    public KgFormatter() {
        mFormat = new DecimalFormat("###,###,##0.0");
    }

    public KgFormatter(DecimalFormat format) {
        this.mFormat = format;
    }

    @Override
    public String getFormattedValue(float value, Entry entry, int dataSetIndex, ViewPortHandler viewPortHandler) {
        return mFormat.format(value) + " Kg";
    }

    @Override
    public String getFormattedValue(float value, AxisBase axis) {
        return mFormat.format(value) + " Kg";
    }

}