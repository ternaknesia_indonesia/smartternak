package com.ternaknesia.sobaternak.Utils;

/*
 * Created by A MADRISH on 1/29/2018
 */

import android.app.DatePickerDialog;
import android.content.Context;
import android.widget.DatePicker;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;


public class MyDatePicker {
    private OnDateSet mCallback;
    private Integer REQUEST_CODE;
    private String DATE_FORMAT;
    private int pDay;
    private int pMonth;
    private int pYear;
    private Context context;
    private DatePickerDialog.OnDateSetListener pDateSetListener;

    public MyDatePicker(Context context){
        this.context = context;
        this.mCallback = (OnDateSet) context;
    }

    public void SetDatePicker(String dateFormat, String currentDate, Integer CODE){
        this.REQUEST_CODE = CODE;
        this.DATE_FORMAT = dateFormat;

        if(currentDate.equalsIgnoreCase("")){
            Calendar c = Calendar.getInstance();
            pYear = c.get(Calendar.YEAR);
            pMonth = c.get(Calendar.MONTH);
            pDay = c.get(Calendar.DAY_OF_MONTH);
        } else {
            try {
                SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
                Date d = sdf.parse(currentDate);
                Calendar cal = Calendar.getInstance();
                cal.setTime(d);
                pYear = cal.get(Calendar.YEAR);
                pMonth = cal.get(Calendar.MONTH);
                pDay = cal.get(Calendar.DAY_OF_MONTH);
            } catch (Exception e) {
                //e.printStackTrace();
            }
        }

        pDateSetListener = new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                String _currentFormat = String.valueOf(new StringBuilder().append(String.format(Locale.getDefault(), "%02d", dayOfMonth)).append("-").append(monthOfYear + 1).append("-").append(year));
                String _resultFormat = new DateFormatter().convertDate(_currentFormat, "dd-MM-yyyy", DATE_FORMAT);
                mCallback.onDateSet(_resultFormat, dayOfMonth, monthOfYear, year, REQUEST_CODE);
            }
        };
    }

    public void show(){
        new DatePickerDialog(context, pDateSetListener, pYear, pMonth, pDay).show();
    }

    public interface OnDateSet {
        void onDateSet(String date, int iDate, int iMonth, int iYear, Integer CODE);
    }
}