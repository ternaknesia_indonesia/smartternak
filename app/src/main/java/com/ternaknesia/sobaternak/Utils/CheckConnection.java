package com.ternaknesia.sobaternak.Utils;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by A MADRISH on 11/13/2017
 */

public class CheckConnection {

    public boolean isInternetAvailable(Activity myActivity) {
        ConnectivityManager cm =
                (ConnectivityManager) myActivity.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }
}

