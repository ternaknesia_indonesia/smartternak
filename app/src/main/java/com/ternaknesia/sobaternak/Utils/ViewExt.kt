package com.ternaknesia.sobaternak.Utils

import android.content.Context
import android.content.res.Resources
import android.graphics.Typeface
import android.os.Build
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.Html
import android.util.Patterns
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.TextView


/**
 * Created by didik on 08/12/17.
 */
fun View.visible() {
    bringToFront()
    visibility = View.VISIBLE
}

fun View.invisible() {
    visibility = View.INVISIBLE
}

fun View.gone() {
    visibility = View.GONE
}

fun View.show(isShow: Boolean = true) = if (isShow) visible() else gone()

fun pxToDp(px: Float): Int {
    val metrics = Resources.getSystem().displayMetrics
    val dp = px / (metrics.densityDpi / 160f)
    return Math.round(dp)
}

fun dpToPx(dp: Float): Int {
    val metrics = Resources.getSystem().displayMetrics
    val px = dp * (metrics.densityDpi / 160f)
    return Math.round(px)
}

fun EditText.isEmpty(strInputName: String): Boolean {
    val str = text.toString()

    return when {
        str.isEmpty() -> {
            error = "$strInputName tidak boleh kosong"
            requestFocus()
            true
        }
        else -> false
    }
}

fun EditText.isEmailInvalid(): Boolean {
    val str = text.toString()

    return when {
        str.isEmpty() -> {
            error = "Email tidak boleh kosong"
            requestFocus()
            true
        }
        !Patterns.EMAIL_ADDRESS.matcher(str).matches() -> {
            error = "Alamat email tidak valid."
            requestFocus()
            true
        }
        else -> false
    }
}

fun EditText.isPasswordInvalid(): Boolean {
    val str = text.toString()

    return when {
        str.isEmpty() -> {
            error = "Password tidak boleh kosong"
            requestFocus()
            true
        }
        str.length < 6 -> {
            error = "Password harus setidaknya 6 karakter"
            requestFocus()
            true
        }
        else -> false
    }
}

fun EditText.isPasswordNotMatch(firstPass: EditText): Boolean {
    val str = text.toString()
    val pass = firstPass.text.toString()

    return when {
        str.isEmpty() -> {
            error = "Passord tidak boleh kosong"
            requestFocus()
            true
        }
        str.length < 6 -> {
            error = "Password harus setidaknya 6 karakter"
            requestFocus()
            true
        }
        str != pass -> {
            error = "Password tidak cocok"
            requestFocus()
            true
        }
        else -> false
    }
}

fun EditText.strText(): String = this.text.toString()

fun TextView.setHtml(text: String?) {
    text?.let {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            setText(Html.fromHtml(text, Html.FROM_HTML_MODE_LEGACY))
        } else {
            setText(Html.fromHtml(text))
        }
    }
}

fun <T : RecyclerView.ViewHolder>
        RecyclerView.vertical(ctx: Context,
                              adapter: RecyclerView.Adapter<T>, isNestedScroll: Boolean = false) {
    this.isNestedScrollingEnabled = isNestedScroll
    this.adapter = adapter
    this.layoutManager = LinearLayoutManager(ctx, LinearLayout.VERTICAL, false)
}


fun TextView.textWithSpecialFont(ctx: Context) {
    val varelaround = Typeface.createFromAsset(ctx.assets, "fonts/varelaround.ttf")
    typeface = varelaround
}

fun ViewGroup.LayoutParams.setViewMargins(con: Context, left: Int=0, top: Int=0, right: Int=0,
                                          bottom: Int=0, view: View) {

    val scale = con.resources.displayMetrics.density
    val pixelLeft = (left * scale + 0.5f).toInt()
    val pixelTop = (top * scale + 0.5f).toInt()
    val pixelRight = (right * scale + 0.5f).toInt()
    val pixelBottom = (bottom * scale + 0.5f).toInt()

    val s = this as ViewGroup.MarginLayoutParams
    s.setMargins(pixelLeft, pixelTop, pixelRight, pixelBottom)
    view.layoutParams = this
}