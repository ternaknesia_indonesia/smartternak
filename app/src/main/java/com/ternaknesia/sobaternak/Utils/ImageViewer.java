package com.ternaknesia.sobaternak.Utils;
/*        Mohon untuk tidak melakukan hal-hal yang melanggar hukum.
        Anda tidak berhak melakukan cracking terhadap system kami tanpa ada persetujuan.
        Terimakasih*/
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;


import com.ternaknesia.sobaternak.Adapter.ImageViewerAdapter;
import com.ternaknesia.sobaternak.R;

import org.json.JSONArray;
import org.json.JSONException;

public class ImageViewer extends AppCompatActivity {
    String imgs;
    int position_click = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.image_viewer_pager);
        ViewPager viewPager = findViewById(R.id.img_viewer);

        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if (extras == null) {
                imgs = null;
                position_click = 0;
            } else {
                imgs = extras.getString("imgs");
                position_click = extras.getInt("position");
            }
        } else {
            imgs = (String) savedInstanceState.getSerializable("imgs");
            position_click = (int) savedInstanceState.getSerializable("position");
        }

        //Log.e("cek1", imgs);

        try {
            JSONArray i = new JSONArray(imgs);
            ImageViewerAdapter imageViewerAdapter = new ImageViewerAdapter(this, i, false);
            viewPager.setAdapter(imageViewerAdapter);
            viewPager.setCurrentItem(position_click);
        } catch (JSONException e) {
            //e.printStackTrace();
            JSONArray j = new JSONArray().put(imgs);
            ImageViewerAdapter imageViewerAdapter = new ImageViewerAdapter(this, j, true);
            viewPager.setAdapter(imageViewerAdapter);
            viewPager.setCurrentItem(position_click);
        }
    }
}
