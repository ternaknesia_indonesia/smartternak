package com.ternaknesia.sobaternak.Service.retro;

public class ApiURL {
    public static final String SALES_ORDER_FINISH = "qurban/market/sales_order_lines/finish";
    public static final String SALES_ORDER_ALL = "qurban/market/sales_orders";
    public static final String SALES_ORDER_PENDING = "qurban/market/sales_order_lines/pending";
    public static final String SALES_ORDER_READY = "qurban/market/sales_order_lines/ready";
    public static final String SALES_ORDER_PROCESS = "qurban/market/sales_order_lines/process";
}