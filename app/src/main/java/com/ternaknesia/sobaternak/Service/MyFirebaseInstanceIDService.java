package com.ternaknesia.sobaternak.Service;
/*        Mohon untuk tidak melakukan hal-hal yang melanggar hukum.
        Anda tidak berhak melakukan cracking terhadap system kami tanpa ada persetujuan.
        Terimakasih*/
/*
 * Created by A MADRISH on 11/1/2017
 */

import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.google.firebase.messaging.FirebaseMessaging;
import com.ternaknesia.sobaternak.Application.Config;
import com.ternaknesia.sobaternak.Manager.PrefManager;

public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {
//    private static final String TAG = MyFirebaseInstanceIDService.class.getSimpleName();

    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        FirebaseMessaging.getInstance().subscribeToTopic("global");

        Log.d(MyFirebaseInstanceIDService.class.getSimpleName(), "onTokenRefresh: " + refreshedToken);

        // Saving reg id to shared preferences
        storeRegIdInPref(refreshedToken);

        // sending reg id to your server
        sendRegistrationToServer(refreshedToken);

        // Notify UI that registration has completed, so the progress indicator can be hidden.
        Intent registrationComplete = new Intent(Config.REGISTRATION_COMPLETE);
        registrationComplete.putExtra("token", refreshedToken);
        LocalBroadcastManager.getInstance(this).sendBroadcast(registrationComplete);
    }

    private void sendRegistrationToServer(final String token) {
        /*RequestBody data = new FormBody.Builder()
                .add("token", token)
                .build();

        PostRequest postToken = new PostRequest(getBaseContext(), EndpointAPI.SendTokenFirebase,"", data);
        try {
            postToken.execute().get();
        } catch (InterruptedException | ExecutionException e) {
            //e.printStackTrace();
        }*/
    }

    private void storeRegIdInPref(String token) {
        PrefManager prefManager = new PrefManager(this);
        prefManager.setIDFB(token);
    }
}