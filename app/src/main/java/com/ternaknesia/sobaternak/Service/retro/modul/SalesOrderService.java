package com.ternaknesia.sobaternak.Service.retro.modul;

import com.ternaknesia.sobaternak.DataModel.SalesOrder.ResponseOrder;
import com.ternaknesia.sobaternak.DataModel.SalesOrder.ResponsePendingOrder;
import com.ternaknesia.sobaternak.DataModel.sales_order.FinishResponse;
import com.ternaknesia.sobaternak.Service.retro.ApiURL;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;

public interface SalesOrderService {
    @GET(ApiURL.SALES_ORDER_ALL)
    Call<ResponseOrder> getAllOrderList(
            @Header("Authorization") String authorization,
            @Query("page") int page,
            @Query("q") String query
    );
    @GET(ApiURL.SALES_ORDER_PENDING)
    Call<ResponsePendingOrder> getPendingOrderList(
            @Header("Authorization") String authorization,
            @Query("page") int page,
            @Query("q") String query
    );
    @GET(ApiURL.SALES_ORDER_READY)
    Call<FinishResponse> getReadyOrderList(
            @Header("Authorization") String authorization,
            @Query("page") int page,
            @Query("q") String query
    );
    @GET(ApiURL.SALES_ORDER_PROCESS)
    Call<FinishResponse> getProcessOrderList(
            @Header("Authorization") String authorization,
            @Query("page") int page,
            @Query("q") String query
    );
    @GET(ApiURL.SALES_ORDER_FINISH)
    Call<FinishResponse> getFinishOrderList(
            @Header("Authorization") String authorization,
            @Query("page") int page,
            @Query("q") String query
    );
}
