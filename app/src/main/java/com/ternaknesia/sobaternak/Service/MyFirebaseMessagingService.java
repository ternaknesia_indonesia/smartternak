package com.ternaknesia.sobaternak.Service;
/*        Mohon untuk tidak melakukan hal-hal yang melanggar hukum.
        Anda tidak berhak melakukan cracking terhadap system kami tanpa ada persetujuan.
        Terimakasih*/
/*
 * Created by A MADRISH on 11/1/2017
 */

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.ternaknesia.sobaternak.activity.MainHome;
import com.ternaknesia.sobaternak.activity.MainHome_Dev;
import com.ternaknesia.sobaternak.Application.Config;
import com.ternaknesia.sobaternak.Manager.PrefManager;
import com.ternaknesia.sobaternak.Utils.NotificationUtils;

import org.json.JSONException;
import org.json.JSONObject;


public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = MyFirebaseMessagingService.class.getSimpleName();
    public static final String REQUEST_RELOAD = "request-reload";
    public static final String REQUEST_DATA = "request-data";
    public static final String OPEN_ORDER = "open-order";

    private NotificationUtils notificationUtils;
    private PrefManager prefManager;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        Log.d(TAG, "onMessageReceived from: " + remoteMessage.getFrom());
        Log.d(TAG, "onMessageReceived data: " + remoteMessage.getData().toString());
        Log.d(TAG, "onMessageReceived message type: " + remoteMessage.getMessageType());
        Log.d(TAG, "onMessageReceived message id: " + remoteMessage.getMessageId());
        Log.d(TAG, "onMessageReceived " + remoteMessage.toString());

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            //Log.e(TAG, "Notification Body: " + remoteMessage.getNotification().getBody());
            handleNotification(remoteMessage.getNotification().getBody());
            Log.d(TAG, "onMessageReceived notification: " + remoteMessage.getNotification().toString());
        }

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            //Log.e(TAG, "Data Payload: " + remoteMessage.getData().toString());

            broadcastLocal(remoteMessage.getData().toString());


            try {
                JSONObject json = new JSONObject(remoteMessage.getData().toString());
                JSONObject data = json.getJSONObject("data");
                String title = data.getString("title");
                String message = data.getString("message");
                String timeStamp = data.getString("timestamp");

                Intent intent = new Intent(this, MainHome_Dev.class);
                intent.putExtra("go-order", true);
                intent.setAction(OPEN_ORDER);

                if (title.equalsIgnoreCase("Ada Pesanan Baru"))
                    showNotificationMessage(this, title, message, timeStamp, intent);

                handleDataMessage(json);
            } catch (Exception e) {
                //Log.e(TAG, "Exception: " + e.getMessage());
            }
        }
    }

    private void handleNotification(String message) {
        //if (!NotificationUtils.isAppIsInBackground(getApplicationContext())) {
        // app is in foreground, broadcast the push message
        Intent pushNotification = new Intent(Config.PUSH_NOTIFICATION);
        pushNotification.putExtra("message", message);
        LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);

        // play notification sound
        NotificationUtils notificationUtils = new NotificationUtils(this);
        notificationUtils.playNotificationSound();
        //}else{
        // If the app is in background, firebase itself handles the notification
        //}
    }

    private void handleDataMessage(JSONObject json) {
        //Log.e(TAG, "push json: " + json.toString());
        prefManager = new PrefManager(this);
        String title = null, message = null, imageUrl = null, timestamp = null;
        boolean isBackground, deleteable = true;
        int type = 0;
        String id = "-";
        String activity = "com.ternaknesia.sobaternak.Activity.MainHome";
        String bundle = "";
        JSONObject payload;
        Intent resultIntent = null;

        try {
            JSONObject data = json.getJSONObject("data");
            if (data.has("title")) title = data.getString("title");
            if (data.has("message")) message = data.getString("message");
            if (data.has("is_background")) isBackground = data.getBoolean("is_background");
            if (data.has("deleteable")) deleteable = data.getBoolean("deleteable");
            if (data.has("type")) type = data.getInt("type");
            if (data.has("image")) imageUrl = data.getString("image");
            if (data.has("news_id")) id = data.getString("news_id");
            if (data.has("activity")) activity = data.getString("activity");
            if (data.has("data_bundle")) bundle = data.getString("data_bundle");
            if (data.has("timestamp")) timestamp = data.getString("timestamp");
            if (data.has("payload")) payload = data.getJSONObject("payload");

            //Log.e(TAG, "title: " + title);
            //Log.e(TAG, "message: " + message);
            //Log.e(TAG, "isBackground: " + iasBackground);
            //Log.e(TAG, "payload: " + payload.toString());
            //Log.e(TAG, "imageUrl: " + imageUrl);
            //Log.e(TAG, "timestamp: " + timestamp);
            //Log.e(TAG, "okeee3");

            if (type == 4) {//just small info
                prefManager.setDataMsg(json.toString());
                prefManager.setDelMsg(deleteable);
                resultIntent = new Intent(getApplicationContext(), MainHome.class);
                resultIntent.putExtra("message", message);
            } else if (type == 3) {//notif invest, buy qurban, aqiqah.
                resultIntent = new Intent(getApplicationContext(), MainHome.class);
                Bundle extras = new Bundle();
                //extras.putParcelable("bitmap", image);
                extras.putString("activity", activity);
                extras.putString("data_bundle", bundle);
                extras.putInt("type", type);
                resultIntent.putExtras(extras);
            } else if (type == 5) { //update
                resultIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.ternaknesia.smartternak"));
                //startActivity(browserIntent);
            }

            //Log.e(TAG, "okeee4");
            //Log.e(TAG, String.valueOf(NotificationUtils.isAppIsInBackground(getApplicationContext())));

            if (!NotificationUtils.isAppIsInBackground(getApplicationContext())) {
                //Log.e(TAG, "okeee41");
                // app is in foreground, broadcast the push message
                Intent pushNotification = new Intent(Config.PUSH_NOTIFICATION);
                pushNotification.putExtra("message", message);
                LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);

                // play notification sound
                NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
                notificationUtils.playNotificationSound();
            } else {
                //Log.e(TAG, "okeee42");
                // app is in background, show the notification in notification tray
                if (resultIntent != null) {
                    //Log.e(TAG, "okeee5");
                    // check for image attachment
                    if (TextUtils.isEmpty(imageUrl)) {
                        //Log.e(TAG, "okeee6");
                        showNotificationMessage(getApplicationContext(), title, message, timestamp, resultIntent);
                    } else {
                        // image is present, show notification with image
                        showNotificationMessageWithBigImage(getApplicationContext(), title, message, timestamp, resultIntent, imageUrl);
                    }
                }
            }
        } catch (JSONException e) {
            //Log.e(TAG, "Json Exception: " + e.getMessage());
            //Log.e("cek", "okeee2");
        } catch (Exception e) {
            //Log.e(TAG, "Exception: " + e.getMessage());
        }
    }

    /**
     * Showing notification with text only
     */
    private void showNotificationMessage(Context context, String title, String message,
                                         String timeStamp, Intent intent) {
        notificationUtils = new NotificationUtils(context);
//        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        notificationUtils.showNotificationMessage(title, message, timeStamp, intent);
    }

    /**
     * Showing notification with text and image
     */
    private void showNotificationMessageWithBigImage(Context context, String title, String message,
                                                     String timeStamp, Intent intent, String imageUrl) {
        notificationUtils = new NotificationUtils(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, message, timeStamp, intent, imageUrl);
    }

    private void broadcastLocal(String strJson) {
        Intent intent = new Intent(REQUEST_RELOAD);
        intent.putExtra(REQUEST_DATA, strJson);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

}