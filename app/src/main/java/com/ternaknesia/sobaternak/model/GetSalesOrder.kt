package com.ternaknesia.sobaternak.model

import com.google.gson.annotations.SerializedName

data class GetSalesOrder(

	@field:SerializedName("result")
	val result: Result? = null,

	@field:SerializedName("response")
	val response: String? = null
)