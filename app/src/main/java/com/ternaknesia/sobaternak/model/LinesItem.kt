package com.ternaknesia.sobaternak.model

import com.google.gson.annotations.SerializedName

data class LinesItem(

	@field:SerializedName("prepared")
	val prepared: Int? = null,

	@field:SerializedName("market_id")
	val marketId: Int? = null,

	@field:SerializedName("weight")
	val weight: String? = null,

	@field:SerializedName("created_at")
	val createdAt: String? = null,

	@field:SerializedName("courier_note")
	val courierNote: String? = null,

	@field:SerializedName("terkirim")
	val terkirim: Int? = null,

	@field:SerializedName("deleted_at")
	val deletedAt: String? = null,

	@field:SerializedName("courier_phone")
	val courierPhone: String? = null,

	@field:SerializedName("tags")
	val tags: String? = null,

	@field:SerializedName("courier_carry")
	val courierCarry: Int? = null,

	@field:SerializedName("courier_name")
	val courierName: String? = null,

	@field:SerializedName("nama")
	val nama: String? = null,

	@field:SerializedName("harga")
	val harga: Int? = null,

	@field:SerializedName("jumlah")
	val jumlah: Int? = null,

	@field:SerializedName("updated_at")
	val updatedAt: String? = null,

	@field:SerializedName("user_id")
	val userId: Int? = null,

	@field:SerializedName("sales_order_line_id")
	val salesOrderLineId: Int? = null,

	@field:SerializedName("subtotal")
	val subtotal: Int? = null,

	@field:SerializedName("sales_order_id")
	val salesOrderId: Int? = null,

	@field:SerializedName("product_id")
	val productId: String? = null
)