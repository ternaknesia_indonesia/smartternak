package com.ternaknesia.sobaternak.model

import com.google.gson.annotations.SerializedName

data class Provinsi(

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("id")
	val id: String? = null
)