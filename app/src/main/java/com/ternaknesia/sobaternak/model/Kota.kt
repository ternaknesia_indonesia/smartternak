package com.ternaknesia.sobaternak.model

import com.google.gson.annotations.SerializedName

data class Kota(

	@field:SerializedName("provinsi_id")
	val provinsiId: String? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("id")
	val id: String? = null
)