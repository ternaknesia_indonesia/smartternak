package com.ternaknesia.sobaternak.model

import com.google.gson.annotations.SerializedName

data class Kecamatan(

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("id")
	val id: String? = null,

	@field:SerializedName("kota_id")
	val kotaId: String? = null
)