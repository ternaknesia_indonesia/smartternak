package com.ternaknesia.sobaternak.model

import com.google.gson.annotations.SerializedName

data class DataItem(

	@field:SerializedName("provinsi")
	val provinsi: Provinsi? = null,

	@field:SerializedName("cancel_message")
	val cancelMessage: String? = null,

	@field:SerializedName("review_id")
	val reviewId: String? = null,

	@field:SerializedName("soid")
	val soid: String? = null,

	@field:SerializedName("telp")
	val telp: String? = null,

	@field:SerializedName("kota_kirim_id")
	val kotaKirimId: String? = null,

	@field:SerializedName("downpayment_percent")
	val downpaymentPercent: String? = null,

	@field:SerializedName("created_at")
	val createdAt: String? = null,

	@field:SerializedName("ref_code")
	val refCode: String? = null,

	@field:SerializedName("no_invoice")
	val noInvoice: String? = null,

	@field:SerializedName("min_downpayment")
	val minDownpayment: String? = null,

	@field:SerializedName("ongkos_kirim")
	val ongkosKirim: Int? = null,

	@field:SerializedName("batas_pembayaran")
	val batasPembayaran: String? = null,

	@field:SerializedName("updated_at")
	val updatedAt: String? = null,

	@field:SerializedName("bank_id")
	val bankId: String? = null,

	@field:SerializedName("sales_order_id")
	val salesOrderId: Int? = null,

	@field:SerializedName("no_so")
	val noSo: String? = null,

	@field:SerializedName("grand_total")
	val grandTotal: Int? = null,

	@field:SerializedName("alamat_kirim")
	val alamatKirim: String? = null,

	@field:SerializedName("biaya_transaksi")
	val biayaTransaksi: Int? = null,

	@field:SerializedName("lines")
	val lines: List<LinesItem>? = null,

	@field:SerializedName("email")
	val email: String? = null,

	@field:SerializedName("diskon")
	val diskon: Int? = null,

	@field:SerializedName("kode_unik")
	val kodeUnik: Int? = null,

	@field:SerializedName("address")
	val address: String? = null,

	@field:SerializedName("kota")
	val kota: Kota? = null,

	@field:SerializedName("kecamatan_kirim_id")
	val kecamatanKirimId: String? = null,

	@field:SerializedName("is_preorder")
	val isPreorder: Int? = null,

	@field:SerializedName("kode_pos")
	val kodePos: String? = null,

	@field:SerializedName("market_id")
	val marketId: Int? = null,

	@field:SerializedName("catatan")
	val catatan: String? = null,

	@field:SerializedName("waktu_kirim")
	val waktuKirim: String? = null,

	@field:SerializedName("promo_id")
	val promoId: Int? = null,

	@field:SerializedName("deleted_at")
	val deletedAt: String? = null,

	@field:SerializedName("is_invoiced")
	val isInvoiced: Int? = null,

	@field:SerializedName("metode_pengiriman")
	val metodePengiriman: String? = null,

	@field:SerializedName("terbayar")
	val terbayar: Int? = null,

	@field:SerializedName("user_id")
	val userId: Int? = null,

	@field:SerializedName("is_disalurkan")
	val isDisalurkan: Int? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("kecamatan")
	val kecamatan: Kecamatan? = null,

	@field:SerializedName("tanggal")
	val tanggal: String? = null,

	@field:SerializedName("provinsi_kirim_id")
	val provinsiKirimId: String? = null,

	@field:SerializedName("desa_kirim_id")
	val desaKirimId: String? = null,

	@field:SerializedName("no_akun")
	val noAkun: String? = null,

	@field:SerializedName("delivery_status")
	val deliveryStatus: String? = null,

	@field:SerializedName("status")
	val status: String? = null
)