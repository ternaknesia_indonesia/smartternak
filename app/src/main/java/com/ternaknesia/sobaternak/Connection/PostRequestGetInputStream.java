package com.ternaknesia.sobaternak.Connection;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;

import com.ternaknesia.sobaternak.Interface.GetInputStreamComplete;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;

/**
 * Created by A MADRISH on 10/28/2017
 */


public class PostRequestGetInputStream extends AsyncTask<String, Void, Bitmap> {
    private String mUrl;
    private RequestBody mData;
    private String mToken;
    private GetInputStreamComplete mCallback;
    private Integer REQUEST_CODE;
    private Integer REQUEST_TIMEOUT = 10;

    private OkHttpClient client = new OkHttpClient.Builder()
            .connectTimeout(REQUEST_TIMEOUT, TimeUnit.SECONDS)
            .build();

    public PostRequestGetInputStream(GetInputStreamComplete context) {
        this.mCallback = context;
    }

    public void SendPostData(String url, String token, RequestBody dat, Integer CODE_REQUEST) {
        this.mToken = token;
        this.mUrl = url;
        this.mData = dat;
        this.REQUEST_CODE = CODE_REQUEST;

    }


    protected Bitmap doInBackground(String... urls) {
        try {
            Request request = new Request.Builder()
                    .url(mUrl)
                    .post(mData)
                    .addHeader("Authorization", "Bearer " + mToken)
                    .build();
            Response response = client.newCall(request).execute();

            //Log.e("do back", "pass");
            //return response.body().byteStream();
            ResponseBody in = response.body();
            assert in != null;
            InputStream inputStream = in.byteStream();
            BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream);
            return BitmapFactory.decodeStream(bufferedInputStream);
        } catch (Exception e) {
            //Log.e("do back", "pass err");
            return null;
        }
    }

    protected void onPostExecute(Bitmap bitmap) {
        //Log.e("contentdata", inputStream.toString());
        try {
            //Log.e("valid json", "pass");
            mCallback.onGetInputStreamComplete(bitmap, REQUEST_CODE);
        } catch (Exception e) {
            /*impossible*/
        }
    }
}