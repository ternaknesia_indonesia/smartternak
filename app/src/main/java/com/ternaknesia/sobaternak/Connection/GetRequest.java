package com.ternaknesia.sobaternak.Connection;

import android.os.AsyncTask;

import com.ternaknesia.sobaternak.Interface.RequestComplete;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by A MADRISH on 10/28/2017
 */



public class GetRequest extends AsyncTask<String, Void, String>{
    private String mUrl;
    private RequestComplete mCallback;
    private Integer REQUEST_CODE;
    private Integer REQUEST_TIMEOUT = 10;
    private String mToken;

    private OkHttpClient client = new OkHttpClient.Builder()
            .connectTimeout(REQUEST_TIMEOUT, TimeUnit.SECONDS)
            .build();

    public GetRequest(RequestComplete context){
        this.mCallback = context;
    }

    public void SendGetData(String url, String token,Integer CODE_REQUEST){
        this.mUrl = url;
        this.mToken = token;
        this.REQUEST_CODE = CODE_REQUEST;
    }

    protected String doInBackground(String... urls) {
        try {
            Request request = new Request.Builder()
                    .url(mUrl)
                    .addHeader("Authorization", "Bearer " + mToken)
                    .build();
            Response response = client.newCall(request).execute();

            //Log.e("do back", "pass");
            return response.body().string();
        } catch (Exception e) {
            //Log.e("do back", "pass err");
            return "null";
        }
    }

    protected void onPostExecute(String getResponse) {
        //Log.e("contentdata", getResponse);
        if (isJSONValid(getResponse)) {
            try {
                //Log.e("valid json", "pass");
                JSONObject _return = new JSONObject(getResponse);
                mCallback.onRequestComplete(_return, REQUEST_CODE);
            } catch (JSONException e) {
                /*impossible*/
            }
        }else {
            try {
                //Log.e("invalid json", "pass");
                JSONObject _return = new JSONObject("{\"response\" : \"invalid\"}");
                mCallback.onRequestComplete(_return, REQUEST_CODE);
            } catch (JSONException e) {
                /*impossible*/
            }
        }
    }

    private boolean isJSONValid(String test) {
        try {
            new JSONObject(test);
        } catch (JSONException ex) {
            // edited, to include @Arthur's comment
            // e.g. in case JSONArray is valid as well...
            try {
                new JSONArray(test);
            } catch (JSONException ex1) {
                return false;
            }
        }
        return true;
    }
}