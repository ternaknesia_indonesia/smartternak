package com.ternaknesia.sobaternak.DataModel;

import com.ternaknesia.sobaternak.Utils.Item;

public class DataDokter extends Item{
    private String id;
    private String name;
    private String phone;
    private String address;
    private String work_time;
    private String note;

    public DataDokter(String id, String name, String phone, String address, String work_time, String note) {
        this.id = id;
        this.name = name;
        this.phone = phone;
        this.address = address;
        this.work_time = work_time;
        this.note = note;
    }

    public String getNote() {
        return note;
    }

    public String getName() {
        return name;
    }

    public String getPhone() {
        return phone;
    }

    public String getId() {
        return id;
    }

    public String getAddress() {
        return address;
    }

    public String getWork_time() {
        return work_time;
    }
}
