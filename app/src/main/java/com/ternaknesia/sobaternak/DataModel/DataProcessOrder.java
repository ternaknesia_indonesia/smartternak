package com.ternaknesia.sobaternak.DataModel;

import com.ternaknesia.sobaternak.Utils.Item;

/**
 * Created by Abyan Dafa on 27/05/2018.
 */

public class DataProcessOrder extends Item{
    private int sales_order_id, sales_order_line_id;
    private int terkirim;
    private String catatan, driver_phone;
    private String item;
    private int jumlah;
    private String destination;
    private String sendTime;
    private boolean isChecked;
    private int market_id;
    private String pemesan;
    private String user_telp;
    private int type;
    private int carry;
    private String courier_note;
    private String courier_name;
    private String invoice;

    public DataProcessOrder(String invoice, int sales_order_id, int sales_order_line_id, int market_id, String name, int jumlah, int terkirim, String catatan, String courier_name, String driver_phone, String user_telp, String item, String destination, String sendTime, int carry, String courier_note) {
        this.invoice = invoice;
        this.sales_order_id = sales_order_id;
        this.sales_order_line_id = sales_order_line_id;
        this.market_id = market_id;
        this.pemesan = name;
        this.jumlah = jumlah;
        this.terkirim = terkirim;
        this.catatan = catatan;
        this.driver_phone = driver_phone;
        this.item = item;
        this.destination = destination;
        this.sendTime = sendTime;
        this.courier_name = courier_name;
        this.user_telp = user_telp;
        this.carry = carry;
        this.courier_note = courier_note;
    }

    public String getInvoice() {
        return invoice;
    }

    public int getCarry() {
        return carry;
    }
    public String getCourier_name() {
        return courier_name;
    }

    public String getCourier_note() {
        return courier_note;
    }

    public String getUser_telp() {
        return user_telp;
    }

    public int getSales_order_id() {
        return sales_order_id;
    }

    public int getSales_order_line_id() {
        return sales_order_line_id;
    }

    public int getTerkirim() {
        return terkirim;
    }

    public String getCatatan() {
        return catatan;
    }

    public String getDriver_phone() {
        return driver_phone;
    }

    public String getItem() {
        return item;
    }

    public int getJumlah() {
        return jumlah;
    }

    public String getDestination() {
        return destination;
    }

    public String getSendTime() {
        return sendTime;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public int getMarket_id() {
        return market_id;
    }

    public String getPemesan() {
        return pemesan;
    }

    public int getType() {
        return type;
    }
}
