package com.ternaknesia.sobaternak.DataModel;

import com.ternaknesia.sobaternak.Utils.Item;

public class DataProject extends Item{
    private String id;
    private String name;
    private String image;
    private String periode;
    private String instance_id;
    private String note;

    public DataProject(String id, String name, String image, String periode, String instance_id, String note) {
        this.id = id;
        this.name = name;
        this.image = image;
        this.periode = periode;
        this.instance_id = instance_id;
        this.note = note;
    }

    public String getNote() {
        return note;
    }

    public String getName() {
        return name;
    }

    public String getImage() {
        return image;
    }

    public String getId() {
        return id;
    }

    public String getInstanceId() { return instance_id; }

    public String getPeriode() {
        return periode;
    }
}
