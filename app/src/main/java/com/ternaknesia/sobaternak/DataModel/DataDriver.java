package com.ternaknesia.sobaternak.DataModel;

import com.ternaknesia.sobaternak.Utils.Item;

public class DataDriver extends Item{
    private String name;
    private String phone;

    public DataDriver(String name, String phone) {
        this.name = name;
        this.phone = phone;
    }

    public String getName() {
        return name;
    }

    public String getPhone() {
        return phone;
    }
}
