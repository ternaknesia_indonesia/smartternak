package com.ternaknesia.sobaternak.DataModel.SalesOrder;


import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Kecamatan implements Parcelable {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("kota_id")
    @Expose
    private String kotaId;
    @SerializedName("name")
    @Expose
    private String name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getKotaId() {
        return kotaId;
    }

    public void setKotaId(String kotaId) {
        this.kotaId = kotaId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.kotaId);
        dest.writeString(this.name);
    }

    public Kecamatan() {
    }

    protected Kecamatan(Parcel in) {
        this.id = in.readString();
        this.kotaId = in.readString();
        this.name = in.readString();
    }

    public static final Parcelable.Creator<Kecamatan> CREATOR = new Parcelable.Creator<Kecamatan>() {
        @Override
        public Kecamatan createFromParcel(Parcel source) {
            return new Kecamatan(source);
        }

        @Override
        public Kecamatan[] newArray(int size) {
            return new Kecamatan[size];
        }
    };
}