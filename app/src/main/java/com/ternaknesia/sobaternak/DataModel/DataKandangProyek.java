package com.ternaknesia.sobaternak.DataModel;

import com.ternaknesia.sobaternak.Utils.Item;

/**
 * Created by A MADRISH on 9/15/2016
 */
public class DataKandangProyek extends Item {

    private String mTextId;
    private String mInstanceId;
    private String mTextName;
    private String mTextLinkImg;
    private String mTextAnakK;
    private String mTextCount;
    private String mTextType;
    private String mCapacity;
    private String mIdType;
    private String mNameType;


    public DataKandangProyek(String Id, String instanceId, String loc, String Name, String desc, String anak_k, String count, String capacity, String type, String type_name){

        mTextId = Id;
        mInstanceId = instanceId;
        mTextName = Name;
        mTextType = loc;
        mTextLinkImg = desc;
        mTextAnakK = anak_k;
        mTextCount = count;
        mCapacity = capacity;
        mIdType = type;
        mNameType = type_name;
    }

    public String getmNameType() {
        return mNameType;
    }

    public String getmIdType() {
        return mIdType;
    }

    public String getmCapacity() {
        return mCapacity;
    }

    public String getmInstanceId() {
        return mInstanceId;
    }

    public String getName() {
        return mTextName;
    }
    public String getDesc() {
        return mTextLinkImg;
    }
    public String getAnakK() {return mTextAnakK;}
    public String getId() {
        return mTextId;
    }
    public String getCount() {
        return mTextCount;
    }
    public String getLoc() {return mTextType;}

}