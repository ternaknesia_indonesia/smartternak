package com.ternaknesia.sobaternak.DataModel.sales_order;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FinishResponse {

    @SerializedName("response")
    @Expose
    private String response;
    @SerializedName("result")
    @Expose
    private Result result;

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

}
