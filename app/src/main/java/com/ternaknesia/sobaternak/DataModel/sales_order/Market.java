package com.ternaknesia.sobaternak.DataModel.sales_order;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Market implements Parcelable {

    @SerializedName("market_id")
    @Expose
    private Integer marketId;
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("nama_toko")
    @Expose
    private String namaToko;
    @SerializedName("deskripsi")
    @Expose
    private String deskripsi;
    @SerializedName("telp")
    @Expose
    private String telp;
    @SerializedName("bid")
    @Expose
    private Integer bid;
    @SerializedName("is_special")
    @Expose
    private Integer isSpecial;
    @SerializedName("is_offline")
    @Expose
    private Integer isOffline;
    @SerializedName("published")
    @Expose
    private Integer published;
    @SerializedName("feat_image")
    @Expose
    private String featImage;
    @SerializedName("provinsi_id")
    @Expose
    private String provinsiId;
    @SerializedName("kota_id")
    @Expose
    private String kotaId;
    @SerializedName("kecamatan_id")
    @Expose
    private String kecamatanId;
    @SerializedName("desa_id")
    @Expose
    private Integer desaId;
    @SerializedName("alamat")
    @Expose
    private String alamat;
    @SerializedName("kode_pos")
    @Expose
    private String kodePos;
    @SerializedName("location")
    @Expose
    private Integer location;
    @SerializedName("slug")
    @Expose
    private String slug;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;

    public Integer getMarketId() {
        return marketId;
    }

    public void setMarketId(Integer marketId) {
        this.marketId = marketId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getNamaToko() {
        return namaToko;
    }

    public void setNamaToko(String namaToko) {
        this.namaToko = namaToko;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public String getTelp() {
        return telp;
    }

    public void setTelp(String telp) {
        this.telp = telp;
    }

    public Integer getBid() {
        return bid;
    }

    public void setBid(Integer bid) {
        this.bid = bid;
    }

    public Integer getIsSpecial() {
        return isSpecial;
    }

    public void setIsSpecial(Integer isSpecial) {
        this.isSpecial = isSpecial;
    }

    public Integer getIsOffline() {
        return isOffline;
    }

    public void setIsOffline(Integer isOffline) {
        this.isOffline = isOffline;
    }

    public Integer getPublished() {
        return published;
    }

    public void setPublished(Integer published) {
        this.published = published;
    }

    public String getFeatImage() {
        return featImage;
    }

    public void setFeatImage(String featImage) {
        this.featImage = featImage;
    }

    public String getProvinsiId() {
        return provinsiId;
    }

    public void setProvinsiId(String provinsiId) {
        this.provinsiId = provinsiId;
    }

    public String getKotaId() {
        return kotaId;
    }

    public void setKotaId(String kotaId) {
        this.kotaId = kotaId;
    }

    public String getKecamatanId() {
        return kecamatanId;
    }

    public void setKecamatanId(String kecamatanId) {
        this.kecamatanId = kecamatanId;
    }

    public Integer getDesaId() {
        return desaId;
    }

    public void setDesaId(Integer desaId) {
        this.desaId = desaId;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getKodePos() {
        return kodePos;
    }

    public void setKodePos(String kodePos) {
        this.kodePos = kodePos;
    }

    public Integer getLocation() {
        return location;
    }

    public void setLocation(Integer location) {
        this.location = location;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.marketId);
        dest.writeValue(this.userId);
        dest.writeString(this.namaToko);
        dest.writeString(this.deskripsi);
        dest.writeString(this.telp);
        dest.writeValue(this.bid);
        dest.writeValue(this.isSpecial);
        dest.writeValue(this.isOffline);
        dest.writeValue(this.published);
        dest.writeString(this.featImage);
        dest.writeString(this.provinsiId);
        dest.writeString(this.kotaId);
        dest.writeString(this.kecamatanId);
        dest.writeValue(this.desaId);
        dest.writeString(this.alamat);
        dest.writeString(this.kodePos);
        dest.writeValue(this.location);
        dest.writeString(this.slug);
        dest.writeString(this.createdAt);
        dest.writeString(this.updatedAt);
    }

    public Market() {
    }

    protected Market(Parcel in) {
        this.marketId = (Integer) in.readValue(Integer.class.getClassLoader());
        this.userId = (Integer) in.readValue(Integer.class.getClassLoader());
        this.namaToko = in.readString();
        this.deskripsi = in.readString();
        this.telp = in.readString();
        this.bid = (Integer) in.readValue(Integer.class.getClassLoader());
        this.isSpecial = (Integer) in.readValue(Integer.class.getClassLoader());
        this.isOffline = (Integer) in.readValue(Integer.class.getClassLoader());
        this.published = (Integer) in.readValue(Integer.class.getClassLoader());
        this.featImage = in.readString();
        this.provinsiId = in.readString();
        this.kotaId = in.readString();
        this.kecamatanId = in.readString();
        this.desaId = (Integer) in.readValue(Integer.class.getClassLoader());
        this.alamat = in.readString();
        this.kodePos = in.readString();
        this.location = (Integer) in.readValue(Integer.class.getClassLoader());
        this.slug = in.readString();
        this.createdAt = in.readString();
        this.updatedAt = in.readString();
    }

    public static final Parcelable.Creator<Market> CREATOR = new Parcelable.Creator<Market>() {
        @Override
        public Market createFromParcel(Parcel source) {
            return new Market(source);
        }

        @Override
        public Market[] newArray(int size) {
            return new Market[size];
        }
    };
}