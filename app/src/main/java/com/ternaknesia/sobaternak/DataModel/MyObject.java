package com.ternaknesia.sobaternak.DataModel;

/**
 * Created by A MADRISH on 2/21/2018
 */

public class MyObject {

    private String id, name, count;

    public MyObject(String name, String id, String count) {
        this.name = name;
        this.id = id;
        this.count = count;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCount() {
        return count;
    }

}