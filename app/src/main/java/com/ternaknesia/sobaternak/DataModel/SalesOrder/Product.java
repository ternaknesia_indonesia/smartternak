package com.ternaknesia.sobaternak.DataModel.SalesOrder;


import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Product {

    @SerializedName("product_id")
    @Expose
    private Integer productId;
    @SerializedName("market_id")
    @Expose
    private Integer marketId;
    @SerializedName("category_id")
    @Expose
    private Integer categoryId;
    @SerializedName("nama")
    @Expose
    private String nama;
    @SerializedName("deskripsi")
    @Expose
    private String deskripsi;
    @SerializedName("peternak_id")
    @Expose
    private Integer peternakId;
    @SerializedName("penyaluran")
    @Expose
    private String penyaluran;
    @SerializedName("lembaga_donasi_id")
    @Expose
    private Integer lembagaDonasiId;
    @SerializedName("dibutuhkan")
    @Expose
    private Integer dibutuhkan;
    @SerializedName("terkumpul")
    @Expose
    private Integer terkumpul;
    @SerializedName("due_date")
    @Expose
    private String dueDate;
    @SerializedName("berat_min")
    @Expose
    private Integer beratMin;
    @SerializedName("berat_max")
    @Expose
    private Integer beratMax;
    @SerializedName("tinggi_min")
    @Expose
    private Integer tinggiMin;
    @SerializedName("tinggi_max")
    @Expose
    private Integer tinggiMax;
    @SerializedName("beef")
    @Expose
    private Integer beef;
    @SerializedName("harga_asli")
    @Expose
    private Integer hargaAsli;
    @SerializedName("harga")
    @Expose
    private Integer harga;
    @SerializedName("stok")
    @Expose
    private Integer stok;
    @SerializedName("is_special")
    @Expose
    private Integer isSpecial;
    @SerializedName("is_unlimited")
    @Expose
    private Integer isUnlimited;
    @SerializedName("prod_image")
    @Expose
    private List<String> prodImage = null;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public Integer getMarketId() {
        return marketId;
    }

    public void setMarketId(Integer marketId) {
        this.marketId = marketId;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public Integer getPeternakId() {
        return peternakId;
    }

    public void setPeternakId(Integer peternakId) {
        this.peternakId = peternakId;
    }

    public String getPenyaluran() {
        return penyaluran;
    }

    public void setPenyaluran(String penyaluran) {
        this.penyaluran = penyaluran;
    }

    public Integer getLembagaDonasiId() {
        return lembagaDonasiId;
    }

    public void setLembagaDonasiId(Integer lembagaDonasiId) {
        this.lembagaDonasiId = lembagaDonasiId;
    }

    public Integer getDibutuhkan() {
        return dibutuhkan;
    }

    public void setDibutuhkan(Integer dibutuhkan) {
        this.dibutuhkan = dibutuhkan;
    }

    public Integer getTerkumpul() {
        return terkumpul;
    }

    public void setTerkumpul(Integer terkumpul) {
        this.terkumpul = terkumpul;
    }

    public String getDueDate() {
        return dueDate;
    }

    public void setDueDate(String dueDate) {
        this.dueDate = dueDate;
    }

    public Integer getBeratMin() {
        return beratMin;
    }

    public void setBeratMin(Integer beratMin) {
        this.beratMin = beratMin;
    }

    public Integer getBeratMax() {
        return beratMax;
    }

    public void setBeratMax(Integer beratMax) {
        this.beratMax = beratMax;
    }

    public Integer getTinggiMin() {
        return tinggiMin;
    }

    public void setTinggiMin(Integer tinggiMin) {
        this.tinggiMin = tinggiMin;
    }

    public Integer getTinggiMax() {
        return tinggiMax;
    }

    public void setTinggiMax(Integer tinggiMax) {
        this.tinggiMax = tinggiMax;
    }

    public Integer getBeef() {
        return beef;
    }

    public void setBeef(Integer beef) {
        this.beef = beef;
    }

    public Integer getHargaAsli() {
        return hargaAsli;
    }

    public void setHargaAsli(Integer hargaAsli) {
        this.hargaAsli = hargaAsli;
    }

    public Integer getHarga() {
        return harga;
    }

    public void setHarga(Integer harga) {
        this.harga = harga;
    }

    public Integer getStok() {
        return stok;
    }

    public void setStok(Integer stok) {
        this.stok = stok;
    }

    public Integer getIsSpecial() {
        return isSpecial;
    }

    public void setIsSpecial(Integer isSpecial) {
        this.isSpecial = isSpecial;
    }

    public Integer getIsUnlimited() {
        return isUnlimited;
    }

    public void setIsUnlimited(Integer isUnlimited) {
        this.isUnlimited = isUnlimited;
    }

    public List<String> getProdImage() {
        return prodImage;
    }

    public void setProdImage(List<String> prodImage) {
        this.prodImage = prodImage;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

}