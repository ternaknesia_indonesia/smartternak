package com.ternaknesia.sobaternak.DataModel;

import com.ternaknesia.sobaternak.Utils.Item;
import com.ternaknesia.sobaternak.Utils.JSONGetter;

import org.json.JSONObject;

public class DataCategory extends Item {
    private String id;
    private String instance_id;
    private String name;
    private String animal_type;
    private String is_active;
    private String min_w;
    private String max_w;
    private String range;
    private JSONObject data;

    public DataCategory(JSONObject data) {
        this.data = data;
        this.id = JSONGetter.getString(data, "id", "0");
        this.instance_id = JSONGetter.getString(data, "instance_id", "");
        this.name = JSONGetter.getString(data, "name", "");
        this.animal_type = JSONGetter.getString(data, "animal_types_id", "1");
        this.is_active = JSONGetter.getString(data, "is_active", "0");
        this.min_w = JSONGetter.getString(data, "min_weight", "0");
        this.max_w = JSONGetter.getString(data, "max_weight", "0");
        range = min_w + "Kg - " + max_w + "Kg";

    }

    public String getRange() {
        return range;
    }

    public String getInstance_id() {
        return instance_id;
    }

    public JSONObject getData() {
        return data;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getAnimal_type() {
        return animal_type;
    }

    public String getIs_active() {
        return is_active;
    }

    public String getMin_w() {
        return min_w;
    }

    public String getMax_w() {
        return max_w;
    }

    @Override
    public String toString() {
        return "DataCategory{" +
                "id='" + id + '\'' +
                ", instance_id='" + instance_id + '\'' +
                ", name='" + name + '\'' +
                ", animal_type='" + animal_type + '\'' +
                ", is_active='" + is_active + '\'' +
                ", min_w='" + min_w + '\'' +
                ", max_w='" + max_w + '\'' +
                ", range='" + range + '\'' +
                ", data=" + data +
                '}';
    }
}
