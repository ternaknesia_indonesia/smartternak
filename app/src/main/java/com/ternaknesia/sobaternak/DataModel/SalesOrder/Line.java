package com.ternaknesia.sobaternak.DataModel.SalesOrder;


import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Line implements Parcelable {

    @SerializedName("sales_order_line_id")
    @Expose
    private Integer salesOrderLineId;
    @SerializedName("sales_order_id")
    @Expose
    private Integer salesOrderId;
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("market_id")
    @Expose
    private Integer marketId;
    @SerializedName("product_id")
    @Expose
    private String productId;
    @SerializedName("nama")
    @Expose
    private String nama;
    @SerializedName("harga")
    @Expose
    private Integer harga;
    @SerializedName("jumlah")
    @Expose
    private Integer jumlah;
    @SerializedName("subtotal")
    @Expose
    private Integer subtotal;
    @SerializedName("prepared")
    @Expose
    private Integer prepared;
    @SerializedName("terkirim")
    @Expose
    private Integer terkirim;
    @SerializedName("tags")
    @Expose
    private String tags;
    @SerializedName("weight")
    @Expose
    private String weight;
    @SerializedName("courier_name")
    @Expose
    private String courierName;
    @SerializedName("courier_phone")
    @Expose
    private String courierPhone;
    @SerializedName("courier_carry")
    @Expose
    private Integer courierCarry;
    @SerializedName("courier_note")
    @Expose
    private String courierNote;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("deleted_at")
    @Expose
    private String deletedAt;

    public Integer getSalesOrderLineId() {
        return salesOrderLineId;
    }

    public void setSalesOrderLineId(Integer salesOrderLineId) {
        this.salesOrderLineId = salesOrderLineId;
    }

    public Integer getSalesOrderId() {
        return salesOrderId;
    }

    public void setSalesOrderId(Integer salesOrderId) {
        this.salesOrderId = salesOrderId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getMarketId() {
        return marketId;
    }

    public void setMarketId(Integer marketId) {
        this.marketId = marketId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public Integer getHarga() {
        return harga;
    }

    public void setHarga(Integer harga) {
        this.harga = harga;
    }

    public Integer getJumlah() {
        return jumlah;
    }

    public void setJumlah(Integer jumlah) {
        this.jumlah = jumlah;
    }

    public Integer getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(Integer subtotal) {
        this.subtotal = subtotal;
    }

    public Integer getPrepared() {
        return prepared;
    }

    public void setPrepared(Integer prepared) {
        this.prepared = prepared;
    }

    public Integer getTerkirim() {
        return terkirim;
    }

    public void setTerkirim(Integer terkirim) {
        this.terkirim = terkirim;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getCourierName() {
        return courierName;
    }

    public void setCourierName(String courierName) {
        this.courierName = courierName;
    }

    public String getCourierPhone() {
        return courierPhone;
    }

    public void setCourierPhone(String courierPhone) {
        this.courierPhone = courierPhone;
    }

    public Integer getCourierCarry() {
        return courierCarry;
    }

    public void setCourierCarry(Integer courierCarry) {
        this.courierCarry = courierCarry;
    }

    public String getCourierNote() {
        return courierNote;
    }

    public void setCourierNote(String courierNote) {
        this.courierNote = courierNote;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(String deletedAt) {
        this.deletedAt = deletedAt;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.salesOrderLineId);
        dest.writeValue(this.salesOrderId);
        dest.writeValue(this.userId);
        dest.writeValue(this.marketId);
        dest.writeString(this.productId);
        dest.writeString(this.nama);
        dest.writeValue(this.harga);
        dest.writeValue(this.jumlah);
        dest.writeValue(this.subtotal);
        dest.writeValue(this.prepared);
        dest.writeValue(this.terkirim);
        dest.writeString(this.tags);
        dest.writeString(this.weight);
        dest.writeString(this.courierName);
        dest.writeString(this.courierPhone);
        dest.writeValue(this.courierCarry);
        dest.writeString(this.courierNote);
        dest.writeString(this.createdAt);
        dest.writeString(this.updatedAt);
        dest.writeString(this.deletedAt);
    }

    public Line() {
    }

    protected Line(Parcel in) {
        this.salesOrderLineId = (Integer) in.readValue(Integer.class.getClassLoader());
        this.salesOrderId = (Integer) in.readValue(Integer.class.getClassLoader());
        this.userId = (Integer) in.readValue(Integer.class.getClassLoader());
        this.marketId = (Integer) in.readValue(Integer.class.getClassLoader());
        this.productId = in.readString();
        this.nama = in.readString();
        this.harga = (Integer) in.readValue(Integer.class.getClassLoader());
        this.jumlah = (Integer) in.readValue(Integer.class.getClassLoader());
        this.subtotal = (Integer) in.readValue(Integer.class.getClassLoader());
        this.prepared = (Integer) in.readValue(Integer.class.getClassLoader());
        this.terkirim = (Integer) in.readValue(Integer.class.getClassLoader());
        this.tags = in.readString();
        this.weight = in.readString();
        this.courierName = in.readString();
        this.courierPhone = in.readString();
        this.courierCarry = (Integer) in.readValue(Integer.class.getClassLoader());
        this.courierNote = in.readString();
        this.createdAt = in.readString();
        this.updatedAt = in.readString();
        this.deletedAt = in.readString();
    }

    public static final Parcelable.Creator<Line> CREATOR = new Parcelable.Creator<Line>() {
        @Override
        public Line createFromParcel(Parcel source) {
            return new Line(source);
        }

        @Override
        public Line[] newArray(int size) {
            return new Line[size];
        }
    };
}
