package com.ternaknesia.sobaternak.DataModel.SalesOrder;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SalesOrder {

    @SerializedName("sales_order_id")
    @Expose
    private Integer salesOrderId;
    @SerializedName("soid")
    @Expose
    private String soid;
    @SerializedName("tanggal")
    @Expose
    private String tanggal;
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("market_id")
    @Expose
    private Integer marketId;
    @SerializedName("no_so")
    @Expose
    private String noSo;
    @SerializedName("no_invoice")
    @Expose
    private String noInvoice;
    @SerializedName("bank_id")
    @Expose
    private String bankId;
    @SerializedName("no_akun")
    @Expose
    private String noAkun;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("telp")
    @Expose
    private String telp;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("name_qurban")
    @Expose
    private String nameQurban;
    @SerializedName("promo_id")
    @Expose
    private Integer promoId;
    @SerializedName("diskon")
    @Expose
    private Integer diskon;
    @SerializedName("grand_total")
    @Expose
    private Integer grandTotal;
    @SerializedName("min_downpayment")
    @Expose
    private String minDownpayment;
    @SerializedName("downpayment_percent")
    @Expose
    private String downpaymentPercent;
    @SerializedName("ongkos_kirim")
    @Expose
    private Integer ongkosKirim;
    @SerializedName("terbayar")
    @Expose
    private Integer terbayar;
    @SerializedName("kode_unik")
    @Expose
    private Integer kodeUnik;
    @SerializedName("waktu_kirim")
    @Expose
    private String waktuKirim;
    @SerializedName("batas_pembayaran")
    @Expose
    private String batasPembayaran;
    @SerializedName("is_disalurkan")
    @Expose
    private Integer isDisalurkan;
    @SerializedName("metode_pengiriman")
    @Expose
    private String metodePengiriman;
    @SerializedName("provinsi_kirim_id")
    @Expose
    private String provinsiKirimId;
    @SerializedName("kota_kirim_id")
    @Expose
    private String kotaKirimId;
    @SerializedName("kecamatan_kirim_id")
    @Expose
    private String kecamatanKirimId;
    @SerializedName("desa_kirim_id")
    @Expose
    private Integer desaKirimId;
    @SerializedName("alamat_kirim")
    @Expose
    private String alamatKirim;
    @SerializedName("kode_pos")
    @Expose
    private String kodePos;
    @SerializedName("catatan")
    @Expose
    private String catatan;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("delivery_status")
    @Expose
    private String deliveryStatus;
    @SerializedName("cancel_message")
    @Expose
    private String cancelMessage;
    @SerializedName("review_id")
    @Expose
    private Integer reviewId;
    @SerializedName("is_preorder")
    @Expose
    private Integer isPreorder;
    @SerializedName("is_invoiced")
    @Expose
    private Integer isInvoiced;
    @SerializedName("biaya_transaksi")
    @Expose
    private Integer biayaTransaksi;
    @SerializedName("ref_code")
    @Expose
    private String refCode;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("deleted_at")
    @Expose
    private String deletedAt;
    @SerializedName("provinsi")
    @Expose
    private Provinsi provinsi;
    @SerializedName("kota")
    @Expose
    private Kota kota;
    @SerializedName("kecamatan")
    @Expose
    private Kecamatan kecamatan;

    public Integer getSalesOrderId() {
        return salesOrderId;
    }

    public void setSalesOrderId(Integer salesOrderId) {
        this.salesOrderId = salesOrderId;
    }

    public String getSoid() {
        return soid;
    }

    public void setSoid(String soid) {
        this.soid = soid;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getMarketId() {
        return marketId;
    }

    public void setMarketId(Integer marketId) {
        this.marketId = marketId;
    }

    public String getNoSo() {
        return noSo;
    }

    public void setNoSo(String noSo) {
        this.noSo = noSo;
    }

    public String getNoInvoice() {
        return noInvoice;
    }

    public void setNoInvoice(String noInvoice) {
        this.noInvoice = noInvoice;
    }

    public String getBankId() {
        return bankId;
    }

    public void setBankId(String bankId) {
        this.bankId = bankId;
    }

    public String getNoAkun() {
        return noAkun;
    }

    public void setNoAkun(String noAkun) {
        this.noAkun = noAkun;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTelp() {
        return telp;
    }

    public void setTelp(String telp) {
        this.telp = telp;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getNameQurban() {
        return nameQurban;
    }

    public void setNameQurban(String nameQurban) {
        this.nameQurban = nameQurban;
    }

    public Integer getPromoId() {
        return promoId;
    }

    public void setPromoId(Integer promoId) {
        this.promoId = promoId;
    }

    public Integer getDiskon() {
        return diskon;
    }

    public void setDiskon(Integer diskon) {
        this.diskon = diskon;
    }

    public Integer getGrandTotal() {
        return grandTotal;
    }

    public void setGrandTotal(Integer grandTotal) {
        this.grandTotal = grandTotal;
    }

    public String getMinDownpayment() {
        return minDownpayment;
    }

    public void setMinDownpayment(String minDownpayment) {
        this.minDownpayment = minDownpayment;
    }

    public String getDownpaymentPercent() {
        return downpaymentPercent;
    }

    public void setDownpaymentPercent(String downpaymentPercent) {
        this.downpaymentPercent = downpaymentPercent;
    }

    public Integer getOngkosKirim() {
        return ongkosKirim;
    }

    public void setOngkosKirim(Integer ongkosKirim) {
        this.ongkosKirim = ongkosKirim;
    }

    public Integer getTerbayar() {
        return terbayar;
    }

    public void setTerbayar(Integer terbayar) {
        this.terbayar = terbayar;
    }

    public Integer getKodeUnik() {
        return kodeUnik;
    }

    public void setKodeUnik(Integer kodeUnik) {
        this.kodeUnik = kodeUnik;
    }

    public String getWaktuKirim() {
        return waktuKirim;
    }

    public void setWaktuKirim(String waktuKirim) {
        this.waktuKirim = waktuKirim;
    }

    public String getBatasPembayaran() {
        return batasPembayaran;
    }

    public void setBatasPembayaran(String batasPembayaran) {
        this.batasPembayaran = batasPembayaran;
    }

    public Integer getIsDisalurkan() {
        return isDisalurkan;
    }

    public void setIsDisalurkan(Integer isDisalurkan) {
        this.isDisalurkan = isDisalurkan;
    }

    public String getMetodePengiriman() {
        return metodePengiriman;
    }

    public void setMetodePengiriman(String metodePengiriman) {
        this.metodePengiriman = metodePengiriman;
    }

    public String getProvinsiKirimId() {
        return provinsiKirimId;
    }

    public void setProvinsiKirimId(String provinsiKirimId) {
        this.provinsiKirimId = provinsiKirimId;
    }

    public String getKotaKirimId() {
        return kotaKirimId;
    }

    public void setKotaKirimId(String kotaKirimId) {
        this.kotaKirimId = kotaKirimId;
    }

    public String getKecamatanKirimId() {
        return kecamatanKirimId;
    }

    public void setKecamatanKirimId(String kecamatanKirimId) {
        this.kecamatanKirimId = kecamatanKirimId;
    }

    public Integer getDesaKirimId() {
        return desaKirimId;
    }

    public void setDesaKirimId(Integer desaKirimId) {
        this.desaKirimId = desaKirimId;
    }

    public String getAlamatKirim() {
        return alamatKirim;
    }

    public void setAlamatKirim(String alamatKirim) {
        this.alamatKirim = alamatKirim;
    }

    public String getKodePos() {
        return kodePos;
    }

    public void setKodePos(String kodePos) {
        this.kodePos = kodePos;
    }

    public String getCatatan() {
        return catatan;
    }

    public void setCatatan(String catatan) {
        this.catatan = catatan;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDeliveryStatus() {
        return deliveryStatus;
    }

    public void setDeliveryStatus(String deliveryStatus) {
        this.deliveryStatus = deliveryStatus;
    }

    public String getCancelMessage() {
        return cancelMessage;
    }

    public void setCancelMessage(String cancelMessage) {
        this.cancelMessage = cancelMessage;
    }

    public Integer getReviewId() {
        return reviewId;
    }

    public void setReviewId(Integer reviewId) {
        this.reviewId = reviewId;
    }

    public Integer getIsPreorder() {
        return isPreorder;
    }

    public void setIsPreorder(Integer isPreorder) {
        this.isPreorder = isPreorder;
    }

    public Integer getIsInvoiced() {
        return isInvoiced;
    }

    public void setIsInvoiced(Integer isInvoiced) {
        this.isInvoiced = isInvoiced;
    }

    public Integer getBiayaTransaksi() {
        return biayaTransaksi;
    }

    public void setBiayaTransaksi(Integer biayaTransaksi) {
        this.biayaTransaksi = biayaTransaksi;
    }

    public String getRefCode() {
        return refCode;
    }

    public void setRefCode(String refCode) {
        this.refCode = refCode;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(String deletedAt) {
        this.deletedAt = deletedAt;
    }

    public Provinsi getProvinsi() {
        return provinsi;
    }

    public void setProvinsi(Provinsi provinsi) {
        this.provinsi = provinsi;
    }

    public Kota getKota() {
        return kota;
    }

    public void setKota(Kota kota) {
        this.kota = kota;
    }

    public Kecamatan getKecamatan() {
        return kecamatan;
    }

    public void setKecamatan(Kecamatan kecamatan) {
        this.kecamatan = kecamatan;
    }

}