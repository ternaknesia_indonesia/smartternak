package com.ternaknesia.sobaternak.DataModel.SalesOrder;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ResponseOrder {

    @SerializedName("response")
    @Expose
    private String response;
    @SerializedName("result")
    @Expose
    private ResultAllOrder resultAllOrder;

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public ResultAllOrder getResultAllOrder() {
        return resultAllOrder;
    }

    public void setResultAllOrder(ResultAllOrder resultAllOrder) {
        this.resultAllOrder = resultAllOrder;
    }

}