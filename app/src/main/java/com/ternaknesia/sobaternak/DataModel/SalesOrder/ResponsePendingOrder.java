package com.ternaknesia.sobaternak.DataModel.SalesOrder;


        import com.google.gson.annotations.Expose;
        import com.google.gson.annotations.SerializedName;

public class ResponsePendingOrder {

    @SerializedName("response")
    @Expose
    private String response;
    @SerializedName("result")
    @Expose
    private ResultPendingOrder result;

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public ResultPendingOrder getResult() {
        return result;
    }

    public void setResult(ResultPendingOrder result) {
        this.result = result;
    }

}
