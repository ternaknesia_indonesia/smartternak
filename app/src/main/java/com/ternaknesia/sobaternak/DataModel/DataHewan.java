package com.ternaknesia.sobaternak.DataModel;

import android.util.Log;

import com.ternaknesia.sobaternak.Utils.Item;
import com.ternaknesia.sobaternak.Utils.JSONGetter;

import org.json.JSONObject;

import java.util.ArrayList;

public class DataHewan extends Item{
    private String id;
    private String instance_id;
    private String project_id;
    private String kandang_id;
    private String kandang;
    private String animal_type_id;
    private String code_tag;
    private String jenis_hewan;
    private String jenis_kelamin;
    private String pemesan_text;
    private String pemesan;
    private String desc;
    private String instance;
    private String project;
    private String buy_date;
    private String buy_date_text;
    private String birth_date;
    private String birth_date_text;
    private String harvest_date;
    private String harvest_date_text;
    private String harvest_age;
    private String weight;
    private String height;
    private String buy_price_unit;
    private String buy_price_kg;
    private String sell_price_unit;
    private Double sell_price_unit_num;
    private String sell_price_kg;
    private Double sell_price_kg_num;
    private String status;
    private ArrayList<String> animalType;
    private JSONObject data;

    public DataHewan(JSONObject data) {
        this.data = data;
        animalType = new ArrayList<>();
        animalType.add("Jenis hewan tidak diketahui");
        animalType.add("Kambing");
        animalType.add("Sapi");
        animalType.add("Domba");

        this.id = JSONGetter.getString(data, "id", "0");
        this.instance_id = JSONGetter.getString(data, "instance_id", "");
        this.project_id = JSONGetter.getString(data, "project_id", "");
        this.kandang_id = JSONGetter.getString(data, "kandang_id", "");
        this.animal_type_id = JSONGetter.getString(data, "animal_type_id", "");
        this.code_tag = JSONGetter.getString(data, "code_tag", "XXXXX.XXX");
        this.jenis_hewan = JSONGetter.getFromArrayListString(data, "animal_type_id", animalType, animalType.get(0));
        this.jenis_kelamin = JSONGetter.getUpperCase(data, "jenis_kelamin", "Jenis kelamin tidak diketahui");
        this.pemesan_text = JSONGetter.getString(data, "pemesan", "Belum dipesan");
        this.desc = JSONGetter.getString(data, "desc", "");
        this.instance = getInstanceJSON(data, "Instansi tidak diketahui");
        this.project = JSONGetter.getString(data, "project","Belum masuk project");
        this.buy_date_text = JSONGetter.getDate(data, "buy_date",
                "yyyy-MM-dd HH:mm:ss", "dd/MM/yyyy", "Tanggal beli tidak diketahui");
        this.buy_date = JSONGetter.getString(data, "buy_date", "");
        this.birth_date_text = JSONGetter.getDate(data, "birth_date",
                "yyyy-MM-dd HH:mm:ss", "dd/MM/yyyy", "Tanggal lahir tidak diketahui");
        this.birth_date = JSONGetter.getString(data, "birth_date", "");
        this.harvest_date_text = JSONGetter.getDate(data, "harvest_date",
                "yyyy-MM-dd HH:mm:ss", "dd/MM/yyyy", "Tanggal panen tidak diketahui");
        this.harvest_date = JSONGetter.getString(data, "harvest_date","");
        this.harvest_age = "(" + JSONGetter.getString(data, "harvest_age", "-") + " bulan)";
        this.weight = JSONGetter.getString(data, "weight", "-") + " kg";
        this.height = JSONGetter.getString(data, "height", "-") + " cm";
        this.buy_price_unit = JSONGetter.getRupiahFormat(data, "buy_price_unit", "-") + " per Ekor";
        this.buy_price_kg = JSONGetter.getRupiahFormat(data, "buy_price_kg", "-") + " per Kg";
        this.sell_price_unit = JSONGetter.getRupiahFormat(data, "sell_price_unit", "-") + " per Ekor";
        this.sell_price_unit_num = JSONGetter.getDouble(data, "sell_price_unit", 0.0);
        this.sell_price_kg = JSONGetter.getRupiahFormat(data, "sell_price_kg", "-") + " per Kg";
        this.sell_price_kg_num = JSONGetter.getDouble(data, "sell_price_kg", 0.0);
        this.status = JSONGetter.getUpperCase(data, "status", "Status tidak diketahui");
        this.kandang = JSONGetter.getString(data, "kandang", "Kandang tidak diketahui");
    }

    public String getId() { return id; }

    public String getInstanceId() { return instance_id; }

    public String getCodeTag() { return code_tag; }

    public String getAnimalTypeId() { return animal_type_id; }

    public String getJenisHewan() { return jenis_hewan; }

    public String getJenisKelamin() { return jenis_kelamin; }

    public String getPemesan() { return pemesan_text; }

    public void setPemesan(String pemesan) {
        this.pemesan = pemesan;
        this.putData("pemesan", pemesan);
    }

    public String getInstance() { return instance; }

    public String getProject() { return project; }

    public String getKandang() { return kandang; }

    public String getBuyDate() { return buy_date_text; }

    public String getHarvestDate() { return harvest_date_text; }

    public String getHarvestAge() { return harvest_age; }

    public String getWeight() { return weight; }

    public String getHeight() { return height; }

    public String getBuyPriceUnit() { return buy_price_unit; }

    public String getBuyPriceKg() { return buy_price_kg; }

    public String getSellPriceUnit() { return sell_price_unit; }

    public void setSellPriceUnitNum(Double sell_price_unit_num) {
        this.sell_price_unit_num = sell_price_unit_num;
        this.sell_price_unit = JSONGetter.getRupiahFormat(data, "sell_price_unit", "-") + " per Ekor";
        this.putData("sell_price_unit", sell_price_unit_num);
    }

    public Double getSellPriceUnitNum() { return sell_price_unit_num; }

    public String getSellPriceKg() { return sell_price_kg; }

    public void setSellPriceKgNum(Double sell_price_kg_num) {
        this.sell_price_kg_num = sell_price_kg_num;
        this.sell_price_kg = JSONGetter.getRupiahFormat(data, "sell_price_kg", "-") + " per Kg";
        this.putData("sell_price_kg", sell_price_kg_num);
    }

    public Double getSellPriceKgNum() { return sell_price_kg_num; }

    public String getStatus() { return status; }

    public JSONObject getJSONData() { return data; }

    private String getInstanceJSON(JSONObject data, String when_fail) {
        try {
            JSONObject inst = data.getJSONObject("instance");
            return inst.getString("name");
        } catch (Exception e) {
            return when_fail;
        }
    }

    private void putData(String name, String value) {
        try {
            this.data.put(name, value);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void putData(String name, Double value) {
        this.putData(name, String.format("%.0f", value));
    }
}
