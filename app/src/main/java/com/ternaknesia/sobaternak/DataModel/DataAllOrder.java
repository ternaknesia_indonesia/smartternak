package com.ternaknesia.sobaternak.DataModel;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import com.ternaknesia.sobaternak.DataModel.SalesOrder.Data;
import com.ternaknesia.sobaternak.DataModel.SalesOrder.Line;
import com.ternaknesia.sobaternak.Utils.Item;

import java.util.List;

/**
 * Created by Abyan Dafa on 27/05/2018.
 */

public class DataAllOrder extends Item implements Parcelable {
    private int sales_order_id;
    private String catatan, invoice;
    private String telp;
    private int terbayar;
    private String pemesan;
    private String alamat;
    private String waktuKirim;
    private String tanggalPesan;
    private String status;
    private String deliveryStatus;
    private List<Line> products;
    private int diskon;
    private int grandTotal;
    private int uangMuka;
    private int kodeUnik;

    public DataAllOrder(int sales_order_id, String invoice, String name, int terbayar,
                        String catatan, String telp, String alamat, String waktuKirim,
                        String tanggalPesan, String status, String deliveryStatus,
                        List<Line> products, int diskon, int grandTotal, int uangMuka, int kodeUnik) {
        this.sales_order_id = sales_order_id;
        this.invoice = invoice;
        this.pemesan = name;
        this.terbayar = terbayar;
        this.catatan = catatan;
        this.telp = telp;
        this.alamat = alamat;
        this.waktuKirim = waktuKirim;
        this.tanggalPesan = tanggalPesan;
        this.status = status;
        this.deliveryStatus = deliveryStatus;
        this.products = products;
        this.diskon = diskon;
        this.grandTotal = grandTotal;
        this.uangMuka = uangMuka;
        this.kodeUnik = kodeUnik;
    }

    public int getSales_order_id() {
        return sales_order_id;
    }

    public String getCatatan() {
        return catatan;
    }

    public String getInvoice() {
        return invoice;
    }

    public String getTelp() {
        return telp;
    }

    public int getTerbayar() {
        return terbayar;
    }

    public String getPemesan() {
        return pemesan;
    }

    public String getAlamat() {
        return alamat;
    }

    public String getWaktuKirim() {
        return waktuKirim;
    }

    public String getTanggalPesan() {
        return tanggalPesan;
    }

    public void setTanggalPesan(String tanggalPesan) {
        this.tanggalPesan = tanggalPesan;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDeliveryStatus() {
        return deliveryStatus;
    }

    public void setDeliveryStatus(String deliveryStatus) {
        this.deliveryStatus = deliveryStatus;
    }

    public List<Line> getProducts() {
        return products;
    }

    public void setProducts(List<Line> products) {
        this.products = products;
    }

    public int getDiskon() {
        return diskon;
    }

    public void setDiskon(int diskon) {
        this.diskon = diskon;
    }

    public int getGrandTotal() {
        return grandTotal;
    }

    public void setGrandTotal(int grandTotal) {
        this.grandTotal = grandTotal;
    }

    public int getUangMuka() {
        return uangMuka;
    }

    public void setUangMuka(int uangMuka) {
        this.uangMuka = uangMuka;
    }

    public int getKodeUnik() {
        return kodeUnik;
    }

    public void setKodeUnik(int kodeUnik) {
        this.kodeUnik = kodeUnik;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.sales_order_id);
        dest.writeString(this.catatan);
        dest.writeString(this.invoice);
        dest.writeString(this.telp);
        dest.writeInt(this.terbayar);
        dest.writeString(this.pemesan);
        dest.writeString(this.alamat);
        dest.writeString(this.waktuKirim);
        dest.writeString(this.tanggalPesan);
        dest.writeString(this.status);
        dest.writeString(this.deliveryStatus);
        dest.writeTypedList(this.products);
        dest.writeInt(this.diskon);
        dest.writeInt(this.grandTotal);
        dest.writeInt(this.uangMuka);
        dest.writeInt(this.kodeUnik);
    }

    private DataAllOrder(Parcel in) {
        this.sales_order_id = in.readInt();
        this.catatan = in.readString();
        this.invoice = in.readString();
        this.telp = in.readString();
        this.terbayar = in.readInt();
        this.pemesan = in.readString();
        this.alamat = in.readString();
        this.waktuKirim = in.readString();
        this.tanggalPesan = in.readString();
        this.status = in.readString();
        this.deliveryStatus = in.readString();
        this.products = in.createTypedArrayList(Line.CREATOR);
        this.diskon = in.readInt();
        this.grandTotal = in.readInt();
        this.uangMuka = in.readInt();
        this.kodeUnik = in.readInt();
    }

    public static final Parcelable.Creator<DataAllOrder> CREATOR = new Parcelable.Creator<DataAllOrder>() {
        @Override
        public DataAllOrder createFromParcel(Parcel source) {
            return new DataAllOrder(source);
        }

        @Override
        public DataAllOrder[] newArray(int size) {
            return new DataAllOrder[size];
        }
    };

    @Override
    public String toString() {
        return "DataAllOrder{" +
                "sales_order_id=" + sales_order_id +
                ", catatan='" + catatan + '\'' +
                ", invoice='" + invoice + '\'' +
                ", telp='" + telp + '\'' +
                ", terbayar=" + terbayar +
                ", pemesan='" + pemesan + '\'' +
                ", alamat='" + alamat + '\'' +
                ", waktuKirim='" + waktuKirim + '\'' +
                ", tanggalPesan='" + tanggalPesan + '\'' +
                ", status='" + status + '\'' +
                ", deliveryStatus='" + deliveryStatus + '\'' +
                ", products=" + products +
                ", diskon=" + diskon +
                ", grandTotal=" + grandTotal +
                ", uangMuka=" + uangMuka +
                ", kodeUnik=" + kodeUnik +
                '}';
    }
}
