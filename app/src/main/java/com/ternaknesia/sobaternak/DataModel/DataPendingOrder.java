package com.ternaknesia.sobaternak.DataModel;

import android.os.Parcel;
import android.os.Parcelable;

import com.ternaknesia.sobaternak.Utils.Item;

/**
 * Created by Abyan Dafa on 27/05/2018.
 */

public class DataPendingOrder extends Item implements Parcelable {
    private int sales_order_id, sales_order_line_id;
    private int terkirim;
    private String catatan, driver_phone;
    private String item;
    private int jumlah;
    private String destination;
    private String sendTime;
    private boolean isChecked;
    private int market_id;
    private String pemesan;
    private String user_telp;
    private String soid;
    private String invoice;

    public DataPendingOrder(String invoice, String soid, int sales_order_id, int sales_order_line_id, int market_id, String name, int jumlah, int terkirim, String catatan, String driver_phone, String user_telp, String item, String destination, String sendTime) {
        this.invoice = invoice;
        this.sales_order_id = sales_order_id;
        this.sales_order_line_id = sales_order_line_id;
        this.market_id = market_id;
        this.pemesan = name;
        this.jumlah = jumlah;
        this.terkirim = terkirim;
        this.catatan = catatan;
        this.driver_phone = driver_phone;
        this.item = item;
        this.destination = destination;
        this.sendTime = sendTime;
        this.isChecked = isChecked;
        this.user_telp = user_telp;
        this.soid = soid;
    }

    public String getInvoice() {
        return invoice;
    }

    public String getSoid() {
        return soid;
    }

    public String getUser_telp() {
        return user_telp;
    }

    public int getSales_order_id() {
        return sales_order_id;
    }

    public int getSales_order_line_id() {
        return sales_order_line_id;
    }

    public int getTerkirim() {
        return terkirim;
    }

    public String getCatatan() {
        return catatan;
    }

    public String getDriver_phone() {
        return driver_phone;
    }

    public String getItem() {
        return item;
    }

    public int getJumlah() {
        return jumlah;
    }

    public String getDestination() {
        return destination;
    }

    public String getSendTime() {
        return sendTime;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public int getMarket_id() {
        return market_id;
    }

    public String getPemesan() {
        return pemesan;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.sales_order_id);
        dest.writeInt(this.sales_order_line_id);
        dest.writeInt(this.terkirim);
        dest.writeString(this.catatan);
        dest.writeString(this.driver_phone);
        dest.writeString(this.item);
        dest.writeInt(this.jumlah);
        dest.writeString(this.destination);
        dest.writeString(this.sendTime);
        dest.writeByte(this.isChecked ? (byte) 1 : (byte) 0);
        dest.writeInt(this.market_id);
        dest.writeString(this.pemesan);
        dest.writeString(this.user_telp);
        dest.writeString(this.soid);
        dest.writeString(this.invoice);
    }

    protected DataPendingOrder(Parcel in) {
        this.sales_order_id = in.readInt();
        this.sales_order_line_id = in.readInt();
        this.terkirim = in.readInt();
        this.catatan = in.readString();
        this.driver_phone = in.readString();
        this.item = in.readString();
        this.jumlah = in.readInt();
        this.destination = in.readString();
        this.sendTime = in.readString();
        this.isChecked = in.readByte() != 0;
        this.market_id = in.readInt();
        this.pemesan = in.readString();
        this.user_telp = in.readString();
        this.soid = in.readString();
        this.invoice = in.readString();
    }

    public static final Parcelable.Creator<DataPendingOrder> CREATOR = new Parcelable.Creator<DataPendingOrder>() {
        @Override
        public DataPendingOrder createFromParcel(Parcel source) {
            return new DataPendingOrder(source);
        }

        @Override
        public DataPendingOrder[] newArray(int size) {
            return new DataPendingOrder[size];
        }
    };
}
