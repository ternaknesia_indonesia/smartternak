package com.ternaknesia.sobaternak.DataModel;

import android.os.Parcel;
import android.os.Parcelable;

import com.ternaknesia.sobaternak.Utils.Item;
import com.ternaknesia.sobaternak.Utils.JSONGetter;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class DataBobot extends Item implements Parcelable {
    private String id;
    private String instance_id;
    private String kandang_id;
    private String kandang_name;
    private String kandang_anak;
    private String kandang_lokasi;
    private String sample;
    private String weight;
    private String tahun;
    private String bulan;
    private JSONObject data;

    public DataBobot(JSONObject data) {
        this.data = data;
        this.id = JSONGetter.getString(data, "id", "0");
        this.instance_id = JSONGetter.getString(data, "instance_id", "");
        this.kandang_id = JSONGetter.getString(data, "kandang_id", "");
        this.weight = JSONGetter.getString(data, "weight", "-") + " Kg";
        this.bulan = JSONGetter.getString(data, "bulan", "");
        this.tahun = JSONGetter.getString(data, "tahun", "");
        this.sample = JSONGetter.getString(data, "checked_tags", "");

        try {
            JSONObject d = data.getJSONObject("kandang");
            this.kandang_name = JSONGetter.getString(d, "name", "Belum masuk project");
            this.kandang_lokasi = JSONGetter.getString(d, "lokasi", "Belum masuk project");
            this.kandang_anak = JSONGetter.getString(d, "anak_kandang", "Belum masuk project");
        } catch (JSONException e) {
            //e.printStackTrace();
        }
    }

    public String getInstance_id() {
        return instance_id;
    }

    public String getKandang_id() {
        return kandang_id;
    }

    public String getKandang_name() {
        return kandang_name;
    }

    public String getKandang_anak() {
        return kandang_anak;
    }

    public String getKandang_lokasi() {
        return kandang_lokasi;
    }

    public String getSample() {
        return sample;
    }

    public String getTahun() {
        return tahun;
    }

    public String getBulan() {
        return bulan;
    }

    public JSONObject getData() {
        return data;
    }

    public String getId() {
        return id;
    }

    public String getInstanceId() {
        return instance_id;
    }

    public String getWeight() {
        return weight;
    }

    public JSONObject getJSONData() {
        return data;
    }

    private String getInstanceJSON(JSONObject data, String when_fail) {
        try {
            JSONObject inst = data.getJSONObject("instance");
            return inst.getString("name");
        } catch (Exception e) {
            return when_fail;
        }
    }

    private void putData(String name, String value) {
        try {
            this.data.put(name, value);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void putData(String name, Double value) {
        this.putData(name, String.format("%.0f", value));
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.instance_id);
        dest.writeString(this.kandang_id);
        dest.writeString(this.kandang_name);
        dest.writeString(this.kandang_anak);
        dest.writeString(this.kandang_lokasi);
        dest.writeString(this.sample);
        dest.writeString(this.weight);
        dest.writeString(this.tahun);
        dest.writeString(this.bulan);
    }

    private DataBobot(Parcel in) {
        this.id = in.readString();
        this.instance_id = in.readString();
        this.kandang_id = in.readString();
        this.kandang_name = in.readString();
        this.kandang_anak = in.readString();
        this.kandang_lokasi = in.readString();
        this.sample = in.readString();
        this.weight = in.readString();
        this.tahun = in.readString();
        this.bulan = in.readString();
    }

    public static final Parcelable.Creator<DataBobot> CREATOR = new Parcelable.Creator<DataBobot>() {
        @Override
        public DataBobot createFromParcel(Parcel source) {
            return new DataBobot(source);
        }

        @Override
        public DataBobot[] newArray(int size) {
            return new DataBobot[size];
        }
    };

    @Override
    public String toString() {
        return "DataBobot{" +
                "id='" + id + '\'' +
                ", instance_id='" + instance_id + '\'' +
                ", kandang_id='" + kandang_id + '\'' +
                ", kandang_name='" + kandang_name + '\'' +
                ", kandang_anak='" + kandang_anak + '\'' +
                ", kandang_lokasi='" + kandang_lokasi + '\'' +
                ", sample='" + sample + '\'' +
                ", weight='" + weight + '\'' +
                ", tahun='" + tahun + '\'' +
                ", bulan='" + bulan + '\'' +
                ", data=" + data +
                '}';
    }
}
