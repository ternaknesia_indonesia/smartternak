package com.ternaknesia.sobaternak.DataModel;

import com.ternaknesia.sobaternak.Utils.Item;

/**
 * Created by A MADRISH on 9/15/2016
 */
public class DataFeed extends Item {

    private String mId;
    private String mUser;
    private String mAvatar;
    private String mDate;
    private String mImage;
    private String mCaption;

    public DataFeed(String id, String user, String img_avatar, String date_post, String img_post, String status_post) {
        mId = id;
        mUser = user;
        mAvatar = img_avatar;
        mDate = date_post;
        mImage = img_post;
        mCaption = status_post;
    }

    public String getmId() {
        return mId;
    }

    public String getmUser() {
        return mUser;
    }

    public String getmAvatar() {
        return mAvatar;
    }

    public String getmDate() {
        return mDate;
    }

    public String getmImage() {
        return mImage;
    }

    public String getmCaption() {
        return mCaption;
    }
}