package com.ternaknesia.sobaternak.DataModel.sales_order;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data implements Parcelable {

    @SerializedName("sales_order_line_id")
    @Expose
    private Integer salesOrderLineId;
    @SerializedName("sales_order_id")
    @Expose
    private Integer salesOrderId;
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("market_id")
    @Expose
    private Integer marketId;
    @SerializedName("product_id")
    @Expose
    private Integer productId;
    @SerializedName("nama")
    @Expose
    private String nama;
    @SerializedName("harga")
    @Expose
    private Integer harga;
    @SerializedName("jumlah")
    @Expose
    private Integer jumlah;
    @SerializedName("subtotal")
    @Expose
    private Integer subtotal;
    @SerializedName("prepared")
    @Expose
    private Integer prepared;
    @SerializedName("terkirim")
    @Expose
    private Integer terkirim;
    @SerializedName("tags")
    @Expose
    private String tags;
    @SerializedName("weight")
    @Expose
    private String weight;
    @SerializedName("courier_name")
    @Expose
    private String courierName;
    @SerializedName("courier_phone")
    @Expose
    private String courierPhone;
    @SerializedName("courier_carry")
    @Expose
    private Integer courierCarry;
    @SerializedName("courier_note")
    @Expose
    private String courierNote;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("deleted_at")
    @Expose
    private String deletedAt;
    @SerializedName("soid")
    @Expose
    private String soid;
    @SerializedName("tanggal")
    @Expose
    private String tanggal;
    @SerializedName("no_so")
    @Expose
    private String noSo;
    @SerializedName("no_invoice")
    @Expose
    private String noInvoice;
    @SerializedName("bank_id")
    @Expose
    private String bankId;
    @SerializedName("no_akun")
    @Expose
    private String noAkun;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("telp")
    @Expose
    private String telp;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("promo_id")
    @Expose
    private Integer promoId;
    @SerializedName("diskon")
    @Expose
    private Integer diskon;
    @SerializedName("grand_total")
    @Expose
    private Integer grandTotal;
    @SerializedName("min_downpayment")
    @Expose
    private String minDownpayment;
    @SerializedName("downpayment_percent")
    @Expose
    private String downpaymentPercent;
    @SerializedName("ongkos_kirim")
    @Expose
    private Integer ongkosKirim;
    @SerializedName("terbayar")
    @Expose
    private Integer terbayar;
    @SerializedName("kode_unik")
    @Expose
    private Integer kodeUnik;
    @SerializedName("waktu_kirim")
    @Expose
    private String waktuKirim;
    @SerializedName("batas_pembayaran")
    @Expose
    private String batasPembayaran;
    @SerializedName("is_disalurkan")
    @Expose
    private Integer isDisalurkan;
    @SerializedName("metode_pengiriman")
    @Expose
    private String metodePengiriman;
    @SerializedName("provinsi_kirim_id")
    @Expose
    private String provinsiKirimId;
    @SerializedName("kota_kirim_id")
    @Expose
    private String kotaKirimId;
    @SerializedName("kecamatan_kirim_id")
    @Expose
    private String kecamatanKirimId;
    @SerializedName("desa_kirim_id")
    @Expose
    private Integer desaKirimId;
    @SerializedName("alamat_kirim")
    @Expose
    private String alamatKirim;
    @SerializedName("kode_pos")
    @Expose
    private Integer kodePos;
    @SerializedName("catatan")
    @Expose
    private String catatan;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("delivery_status")
    @Expose
    private String deliveryStatus;
    @SerializedName("cancel_message")
    @Expose
    private String cancelMessage;
    @SerializedName("review_id")
    @Expose
    private Integer reviewId;
    @SerializedName("is_preorder")
    @Expose
    private Integer isPreorder;
    @SerializedName("is_invoiced")
    @Expose
    private Integer isInvoiced;
    @SerializedName("biaya_transaksi")
    @Expose
    private Integer biayaTransaksi;
    @SerializedName("ref_code")
    @Expose
    private String refCode;
    @SerializedName("category_id")
    @Expose
    private Integer categoryId;
    @SerializedName("deskripsi")
    @Expose
    private String deskripsi;
    @SerializedName("peternak_id")
    @Expose
    private Integer peternakId;
    @SerializedName("penyaluran")
    @Expose
    private String penyaluran;
    @SerializedName("lembaga_donasi_id")
    @Expose
    private Integer lembagaDonasiId;
    @SerializedName("dibutuhkan")
    @Expose
    private Integer dibutuhkan;
    @SerializedName("terkumpul")
    @Expose
    private Integer terkumpul;
    @SerializedName("due_date")
    @Expose
    private String dueDate;
    @SerializedName("berat_min")
    @Expose
    private Integer beratMin;
    @SerializedName("berat_max")
    @Expose
    private Integer beratMax;
    @SerializedName("tinggi_min")
    @Expose
    private Integer tinggiMin;
    @SerializedName("tinggi_max")
    @Expose
    private Integer tinggiMax;
    @SerializedName("beef")
    @Expose
    private Integer beef;
    @SerializedName("harga_asli")
    @Expose
    private Integer hargaAsli;
    @SerializedName("stok")
    @Expose
    private Integer stok;
    @SerializedName("is_special")
    @Expose
    private Integer isSpecial;
    @SerializedName("is_unlimited")
    @Expose
    private Integer isUnlimited;
    @SerializedName("prod_image")
    @Expose
    private String prodImage;
    @SerializedName("sales_order")
    @Expose
    private SalesOrder salesOrder;
    @SerializedName("market")
    @Expose
    private Market market;
    @SerializedName("product")
    @Expose
    private Product product;

    public Integer getSalesOrderLineId() {
        return salesOrderLineId;
    }

    public void setSalesOrderLineId(Integer salesOrderLineId) {
        this.salesOrderLineId = salesOrderLineId;
    }

    public Integer getSalesOrderId() {
        return salesOrderId;
    }

    public void setSalesOrderId(Integer salesOrderId) {
        this.salesOrderId = salesOrderId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getMarketId() {
        return marketId;
    }

    public void setMarketId(Integer marketId) {
        this.marketId = marketId;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public Integer getHarga() {
        return harga;
    }

    public void setHarga(Integer harga) {
        this.harga = harga;
    }

    public Integer getJumlah() {
        return jumlah;
    }

    public void setJumlah(Integer jumlah) {
        this.jumlah = jumlah;
    }

    public Integer getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(Integer subtotal) {
        this.subtotal = subtotal;
    }

    public Integer getPrepared() {
        return prepared;
    }

    public void setPrepared(Integer prepared) {
        this.prepared = prepared;
    }

    public Integer getTerkirim() {
        return terkirim;
    }

    public void setTerkirim(Integer terkirim) {
        this.terkirim = terkirim;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getCourierName() {
        return courierName;
    }

    public void setCourierName(String courierName) {
        this.courierName = courierName;
    }

    public String getCourierPhone() {
        return courierPhone;
    }

    public void setCourierPhone(String courierPhone) {
        this.courierPhone = courierPhone;
    }

    public Integer getCourierCarry() {
        return courierCarry;
    }

    public void setCourierCarry(Integer courierCarry) {
        this.courierCarry = courierCarry;
    }

    public String getCourierNote() {
        return courierNote;
    }

    public void setCourierNote(String courierNote) {
        this.courierNote = courierNote;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(String deletedAt) {
        this.deletedAt = deletedAt;
    }

    public String getSoid() {
        return soid;
    }

    public void setSoid(String soid) {
        this.soid = soid;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }

    public String getNoSo() {
        return noSo;
    }

    public void setNoSo(String noSo) {
        this.noSo = noSo;
    }

    public String getNoInvoice() {
        return noInvoice;
    }

    public void setNoInvoice(String noInvoice) {
        this.noInvoice = noInvoice;
    }

    public String getBankId() {
        return bankId;
    }

    public void setBankId(String bankId) {
        this.bankId = bankId;
    }

    public String getNoAkun() {
        return noAkun;
    }

    public void setNoAkun(String noAkun) {
        this.noAkun = noAkun;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTelp() {
        return telp;
    }

    public void setTelp(String telp) {
        this.telp = telp;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getPromoId() {
        return promoId;
    }

    public void setPromoId(Integer promoId) {
        this.promoId = promoId;
    }

    public Integer getDiskon() {
        return diskon;
    }

    public void setDiskon(Integer diskon) {
        this.diskon = diskon;
    }

    public Integer getGrandTotal() {
        return grandTotal;
    }

    public void setGrandTotal(Integer grandTotal) {
        this.grandTotal = grandTotal;
    }

    public String getMinDownpayment() {
        return minDownpayment;
    }

    public void setMinDownpayment(String minDownpayment) {
        this.minDownpayment = minDownpayment;
    }

    public String getDownpaymentPercent() {
        return downpaymentPercent;
    }

    public void setDownpaymentPercent(String downpaymentPercent) {
        this.downpaymentPercent = downpaymentPercent;
    }

    public Integer getOngkosKirim() {
        return ongkosKirim;
    }

    public void setOngkosKirim(Integer ongkosKirim) {
        this.ongkosKirim = ongkosKirim;
    }

    public Integer getTerbayar() {
        return terbayar;
    }

    public void setTerbayar(Integer terbayar) {
        this.terbayar = terbayar;
    }

    public Integer getKodeUnik() {
        return kodeUnik;
    }

    public void setKodeUnik(Integer kodeUnik) {
        this.kodeUnik = kodeUnik;
    }

    public String getWaktuKirim() {
        return waktuKirim;
    }

    public void setWaktuKirim(String waktuKirim) {
        this.waktuKirim = waktuKirim;
    }

    public String getBatasPembayaran() {
        return batasPembayaran;
    }

    public void setBatasPembayaran(String batasPembayaran) {
        this.batasPembayaran = batasPembayaran;
    }

    public Integer getIsDisalurkan() {
        return isDisalurkan;
    }

    public void setIsDisalurkan(Integer isDisalurkan) {
        this.isDisalurkan = isDisalurkan;
    }

    public String getMetodePengiriman() {
        return metodePengiriman;
    }

    public void setMetodePengiriman(String metodePengiriman) {
        this.metodePengiriman = metodePengiriman;
    }

    public String getProvinsiKirimId() {
        return provinsiKirimId;
    }

    public void setProvinsiKirimId(String provinsiKirimId) {
        this.provinsiKirimId = provinsiKirimId;
    }

    public String getKotaKirimId() {
        return kotaKirimId;
    }

    public void setKotaKirimId(String kotaKirimId) {
        this.kotaKirimId = kotaKirimId;
    }

    public String getKecamatanKirimId() {
        return kecamatanKirimId;
    }

    public void setKecamatanKirimId(String kecamatanKirimId) {
        this.kecamatanKirimId = kecamatanKirimId;
    }

    public Integer getDesaKirimId() {
        return desaKirimId;
    }

    public void setDesaKirimId(Integer desaKirimId) {
        this.desaKirimId = desaKirimId;
    }

    public String getAlamatKirim() {
        return alamatKirim;
    }

    public void setAlamatKirim(String alamatKirim) {
        this.alamatKirim = alamatKirim;
    }

    public Integer getKodePos() {
        return kodePos;
    }

    public void setKodePos(Integer kodePos) {
        this.kodePos = kodePos;
    }

    public String getCatatan() {
        return catatan;
    }

    public void setCatatan(String catatan) {
        this.catatan = catatan;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDeliveryStatus() {
        return deliveryStatus;
    }

    public void setDeliveryStatus(String deliveryStatus) {
        this.deliveryStatus = deliveryStatus;
    }

    public String getCancelMessage() {
        return cancelMessage;
    }

    public void setCancelMessage(String cancelMessage) {
        this.cancelMessage = cancelMessage;
    }

    public Integer getReviewId() {
        return reviewId;
    }

    public void setReviewId(Integer reviewId) {
        this.reviewId = reviewId;
    }

    public Integer getIsPreorder() {
        return isPreorder;
    }

    public void setIsPreorder(Integer isPreorder) {
        this.isPreorder = isPreorder;
    }

    public Integer getIsInvoiced() {
        return isInvoiced;
    }

    public void setIsInvoiced(Integer isInvoiced) {
        this.isInvoiced = isInvoiced;
    }

    public Integer getBiayaTransaksi() {
        return biayaTransaksi;
    }

    public void setBiayaTransaksi(Integer biayaTransaksi) {
        this.biayaTransaksi = biayaTransaksi;
    }

    public String getRefCode() {
        return refCode;
    }

    public void setRefCode(String refCode) {
        this.refCode = refCode;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public Integer getPeternakId() {
        return peternakId;
    }

    public void setPeternakId(Integer peternakId) {
        this.peternakId = peternakId;
    }

    public String getPenyaluran() {
        return penyaluran;
    }

    public void setPenyaluran(String penyaluran) {
        this.penyaluran = penyaluran;
    }

    public Integer getLembagaDonasiId() {
        return lembagaDonasiId;
    }

    public void setLembagaDonasiId(Integer lembagaDonasiId) {
        this.lembagaDonasiId = lembagaDonasiId;
    }

    public Integer getDibutuhkan() {
        return dibutuhkan;
    }

    public void setDibutuhkan(Integer dibutuhkan) {
        this.dibutuhkan = dibutuhkan;
    }

    public Integer getTerkumpul() {
        return terkumpul;
    }

    public void setTerkumpul(Integer terkumpul) {
        this.terkumpul = terkumpul;
    }

    public String getDueDate() {
        return dueDate;
    }

    public void setDueDate(String dueDate) {
        this.dueDate = dueDate;
    }

    public Integer getBeratMin() {
        return beratMin;
    }

    public void setBeratMin(Integer beratMin) {
        this.beratMin = beratMin;
    }

    public Integer getBeratMax() {
        return beratMax;
    }

    public void setBeratMax(Integer beratMax) {
        this.beratMax = beratMax;
    }

    public Integer getTinggiMin() {
        return tinggiMin;
    }

    public void setTinggiMin(Integer tinggiMin) {
        this.tinggiMin = tinggiMin;
    }

    public Integer getTinggiMax() {
        return tinggiMax;
    }

    public void setTinggiMax(Integer tinggiMax) {
        this.tinggiMax = tinggiMax;
    }

    public Integer getBeef() {
        return beef;
    }

    public void setBeef(Integer beef) {
        this.beef = beef;
    }

    public Integer getHargaAsli() {
        return hargaAsli;
    }

    public void setHargaAsli(Integer hargaAsli) {
        this.hargaAsli = hargaAsli;
    }

    public Integer getStok() {
        return stok;
    }

    public void setStok(Integer stok) {
        this.stok = stok;
    }

    public Integer getIsSpecial() {
        return isSpecial;
    }

    public void setIsSpecial(Integer isSpecial) {
        this.isSpecial = isSpecial;
    }

    public Integer getIsUnlimited() {
        return isUnlimited;
    }

    public void setIsUnlimited(Integer isUnlimited) {
        this.isUnlimited = isUnlimited;
    }

    public String getProdImage() {
        return prodImage;
    }

    public void setProdImage(String prodImage) {
        this.prodImage = prodImage;
    }

    public SalesOrder getSalesOrder() {
        return salesOrder;
    }

    public void setSalesOrder(SalesOrder salesOrder) {
        this.salesOrder = salesOrder;
    }

    public Market getMarket() {
        return market;
    }

    public void setMarket(Market market) {
        this.market = market;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.salesOrderLineId);
        dest.writeValue(this.salesOrderId);
        dest.writeValue(this.userId);
        dest.writeValue(this.marketId);
        dest.writeValue(this.productId);
        dest.writeString(this.nama);
        dest.writeValue(this.harga);
        dest.writeValue(this.jumlah);
        dest.writeValue(this.subtotal);
        dest.writeValue(this.prepared);
        dest.writeValue(this.terkirim);
        dest.writeString(this.tags);
        dest.writeString(this.weight);
        dest.writeString(this.courierName);
        dest.writeString(this.courierPhone);
        dest.writeValue(this.courierCarry);
        dest.writeString(this.courierNote);
        dest.writeString(this.createdAt);
        dest.writeString(this.updatedAt);
        dest.writeString(this.deletedAt);
        dest.writeString(this.soid);
        dest.writeString(this.tanggal);
        dest.writeString(this.noSo);
        dest.writeString(this.noInvoice);
        dest.writeString(this.bankId);
        dest.writeString(this.noAkun);
        dest.writeString(this.name);
        dest.writeString(this.telp);
        dest.writeString(this.email);
        dest.writeString(this.address);
        dest.writeValue(this.promoId);
        dest.writeValue(this.diskon);
        dest.writeValue(this.grandTotal);
        dest.writeString(this.minDownpayment);
        dest.writeString(this.downpaymentPercent);
        dest.writeValue(this.ongkosKirim);
        dest.writeValue(this.terbayar);
        dest.writeValue(this.kodeUnik);
        dest.writeString(this.waktuKirim);
        dest.writeString(this.batasPembayaran);
        dest.writeValue(this.isDisalurkan);
        dest.writeString(this.metodePengiriman);
        dest.writeString(this.provinsiKirimId);
        dest.writeString(this.kotaKirimId);
        dest.writeString(this.kecamatanKirimId);
        dest.writeValue(this.desaKirimId);
        dest.writeString(this.alamatKirim);
        dest.writeValue(this.kodePos);
        dest.writeString(this.catatan);
        dest.writeString(this.status);
        dest.writeString(this.deliveryStatus);
        dest.writeString(this.cancelMessage);
        dest.writeValue(this.reviewId);
        dest.writeValue(this.isPreorder);
        dest.writeValue(this.isInvoiced);
        dest.writeValue(this.biayaTransaksi);
        dest.writeString(this.refCode);
        dest.writeValue(this.categoryId);
        dest.writeString(this.deskripsi);
        dest.writeValue(this.peternakId);
        dest.writeString(this.penyaluran);
        dest.writeValue(this.lembagaDonasiId);
        dest.writeValue(this.dibutuhkan);
        dest.writeValue(this.terkumpul);
        dest.writeString(this.dueDate);
        dest.writeValue(this.beratMin);
        dest.writeValue(this.beratMax);
        dest.writeValue(this.tinggiMin);
        dest.writeValue(this.tinggiMax);
        dest.writeValue(this.beef);
        dest.writeValue(this.hargaAsli);
        dest.writeValue(this.stok);
        dest.writeValue(this.isSpecial);
        dest.writeValue(this.isUnlimited);
        dest.writeString(this.prodImage);
        dest.writeParcelable(this.salesOrder, flags);
        dest.writeParcelable(this.market, flags);
        dest.writeParcelable(this.product, flags);
    }

    public Data() {
    }

    protected Data(Parcel in) {
        this.salesOrderLineId = (Integer) in.readValue(Integer.class.getClassLoader());
        this.salesOrderId = (Integer) in.readValue(Integer.class.getClassLoader());
        this.userId = (Integer) in.readValue(Integer.class.getClassLoader());
        this.marketId = (Integer) in.readValue(Integer.class.getClassLoader());
        this.productId = (Integer) in.readValue(Integer.class.getClassLoader());
        this.nama = in.readString();
        this.harga = (Integer) in.readValue(Integer.class.getClassLoader());
        this.jumlah = (Integer) in.readValue(Integer.class.getClassLoader());
        this.subtotal = (Integer) in.readValue(Integer.class.getClassLoader());
        this.prepared = (Integer) in.readValue(Integer.class.getClassLoader());
        this.terkirim = (Integer) in.readValue(Integer.class.getClassLoader());
        this.tags = in.readString();
        this.weight = in.readString();
        this.courierName = in.readString();
        this.courierPhone = in.readString();
        this.courierCarry = (Integer) in.readValue(Integer.class.getClassLoader());
        this.courierNote = in.readString();
        this.createdAt = in.readString();
        this.updatedAt = in.readString();
        this.deletedAt = in.readString();
        this.soid = in.readString();
        this.tanggal = in.readString();
        this.noSo = in.readString();
        this.noInvoice = in.readString();
        this.bankId = in.readString();
        this.noAkun = in.readString();
        this.name = in.readString();
        this.telp = in.readString();
        this.email = in.readString();
        this.address = in.readString();
        this.promoId = (Integer) in.readValue(Integer.class.getClassLoader());
        this.diskon = (Integer) in.readValue(Integer.class.getClassLoader());
        this.grandTotal = (Integer) in.readValue(Integer.class.getClassLoader());
        this.minDownpayment = in.readString();
        this.downpaymentPercent = in.readString();
        this.ongkosKirim = (Integer) in.readValue(Integer.class.getClassLoader());
        this.terbayar = (Integer) in.readValue(Integer.class.getClassLoader());
        this.kodeUnik = (Integer) in.readValue(Integer.class.getClassLoader());
        this.waktuKirim = in.readString();
        this.batasPembayaran = in.readString();
        this.isDisalurkan = (Integer) in.readValue(Integer.class.getClassLoader());
        this.metodePengiriman = in.readString();
        this.provinsiKirimId = in.readString();
        this.kotaKirimId = in.readString();
        this.kecamatanKirimId = in.readString();
        this.desaKirimId = (Integer) in.readValue(Integer.class.getClassLoader());
        this.alamatKirim = in.readString();
        this.kodePos = (Integer) in.readValue(Integer.class.getClassLoader());
        this.catatan = in.readString();
        this.status = in.readString();
        this.deliveryStatus = in.readString();
        this.cancelMessage = in.readString();
        this.reviewId = (Integer) in.readValue(Integer.class.getClassLoader());
        this.isPreorder = (Integer) in.readValue(Integer.class.getClassLoader());
        this.isInvoiced = (Integer) in.readValue(Integer.class.getClassLoader());
        this.biayaTransaksi = (Integer) in.readValue(Integer.class.getClassLoader());
        this.refCode = in.readString();
        this.categoryId = (Integer) in.readValue(Integer.class.getClassLoader());
        this.deskripsi = in.readString();
        this.peternakId = (Integer) in.readValue(Integer.class.getClassLoader());
        this.penyaluran = in.readString();
        this.lembagaDonasiId = (Integer) in.readValue(Integer.class.getClassLoader());
        this.dibutuhkan = (Integer) in.readValue(Integer.class.getClassLoader());
        this.terkumpul = (Integer) in.readValue(Integer.class.getClassLoader());
        this.dueDate = in.readString();
        this.beratMin = (Integer) in.readValue(Integer.class.getClassLoader());
        this.beratMax = (Integer) in.readValue(Integer.class.getClassLoader());
        this.tinggiMin = (Integer) in.readValue(Integer.class.getClassLoader());
        this.tinggiMax = (Integer) in.readValue(Integer.class.getClassLoader());
        this.beef = (Integer) in.readValue(Integer.class.getClassLoader());
        this.hargaAsli = (Integer) in.readValue(Integer.class.getClassLoader());
        this.stok = (Integer) in.readValue(Integer.class.getClassLoader());
        this.isSpecial = (Integer) in.readValue(Integer.class.getClassLoader());
        this.isUnlimited = (Integer) in.readValue(Integer.class.getClassLoader());
        this.prodImage = in.readString();
        this.salesOrder = in.readParcelable(SalesOrder.class.getClassLoader());
        this.market = in.readParcelable(Market.class.getClassLoader());
        this.product = in.readParcelable(Product.class.getClassLoader());
    }

    public static final Parcelable.Creator<Data> CREATOR = new Parcelable.Creator<Data>() {
        @Override
        public Data createFromParcel(Parcel source) {
            return new Data(source);
        }

        @Override
        public Data[] newArray(int size) {
            return new Data[size];
        }
    };


    @Override
    public String toString() {
        return "Data{" +
                "salesOrderLineId=" + salesOrderLineId +
                ", salesOrderId=" + salesOrderId +
                ", userId=" + userId +
                ", marketId=" + marketId +
                ", productId=" + productId +
                ", nama='" + nama + '\'' +
                ", harga=" + harga +
                ", jumlah=" + jumlah +
                ", subtotal=" + subtotal +
                ", prepared=" + prepared +
                ", terkirim=" + terkirim +
                ", tags='" + tags + '\'' +
                ", weight='" + weight + '\'' +
                ", courierName='" + courierName + '\'' +
                ", courierPhone='" + courierPhone + '\'' +
                ", courierCarry=" + courierCarry +
                ", courierNote='" + courierNote + '\'' +
                ", createdAt='" + createdAt + '\'' +
                ", updatedAt='" + updatedAt + '\'' +
                ", deletedAt='" + deletedAt + '\'' +
                ", soid='" + soid + '\'' +
                ", tanggal='" + tanggal + '\'' +
                ", noSo='" + noSo + '\'' +
                ", noInvoice='" + noInvoice + '\'' +
                ", bankId='" + bankId + '\'' +
                ", noAkun='" + noAkun + '\'' +
                ", name='" + name + '\'' +
                ", telp='" + telp + '\'' +
                ", email='" + email + '\'' +
                ", address='" + address + '\'' +
                ", promoId=" + promoId +
                ", diskon=" + diskon +
                ", grandTotal=" + grandTotal +
                ", minDownpayment='" + minDownpayment + '\'' +
                ", downpaymentPercent='" + downpaymentPercent + '\'' +
                ", ongkosKirim=" + ongkosKirim +
                ", terbayar=" + terbayar +
                ", kodeUnik=" + kodeUnik +
                ", waktuKirim='" + waktuKirim + '\'' +
                ", batasPembayaran='" + batasPembayaran + '\'' +
                ", isDisalurkan=" + isDisalurkan +
                ", metodePengiriman='" + metodePengiriman + '\'' +
                ", provinsiKirimId='" + provinsiKirimId + '\'' +
                ", kotaKirimId='" + kotaKirimId + '\'' +
                ", kecamatanKirimId='" + kecamatanKirimId + '\'' +
                ", desaKirimId=" + desaKirimId +
                ", alamatKirim='" + alamatKirim + '\'' +
                ", kodePos=" + kodePos +
                ", catatan='" + catatan + '\'' +
                ", status='" + status + '\'' +
                ", deliveryStatus='" + deliveryStatus + '\'' +
                ", cancelMessage='" + cancelMessage + '\'' +
                ", reviewId=" + reviewId +
                ", isPreorder=" + isPreorder +
                ", isInvoiced=" + isInvoiced +
                ", biayaTransaksi=" + biayaTransaksi +
                ", refCode='" + refCode + '\'' +
                ", categoryId=" + categoryId +
                ", deskripsi='" + deskripsi + '\'' +
                ", peternakId=" + peternakId +
                ", penyaluran='" + penyaluran + '\'' +
                ", lembagaDonasiId=" + lembagaDonasiId +
                ", dibutuhkan=" + dibutuhkan +
                ", terkumpul=" + terkumpul +
                ", dueDate='" + dueDate + '\'' +
                ", beratMin=" + beratMin +
                ", beratMax=" + beratMax +
                ", tinggiMin=" + tinggiMin +
                ", tinggiMax=" + tinggiMax +
                ", beef=" + beef +
                ", hargaAsli=" + hargaAsli +
                ", stok=" + stok +
                ", isSpecial=" + isSpecial +
                ", isUnlimited=" + isUnlimited +
                ", prodImage='" + prodImage + '\'' +
                ", salesOrder=" + salesOrder +
                ", market=" + market +
                ", product=" + product +
                '}';
    }
}
