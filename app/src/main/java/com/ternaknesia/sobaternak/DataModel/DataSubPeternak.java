package com.ternaknesia.sobaternak.DataModel;

import com.ternaknesia.sobaternak.Utils.Item;
import com.ternaknesia.sobaternak.Utils.JSONGetter;

import org.json.JSONObject;

public class DataSubPeternak extends Item{
    private String id;
    private String name;
    private String instance_code;
    private String address;
    private String count;

    public DataSubPeternak(JSONObject data) {
        this.id = JSONGetter.getString(data, "id", "");
        this.name = JSONGetter.getString(data, "name", "");
        this.instance_code = JSONGetter.getString(data, "instance_code", "");
        this.address = JSONGetter.getString(data, "address", "");
        this.count = JSONGetter.getString(data, "animals", "");
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getInstanceCode() { return instance_code; }

    public String getAddress() { return address; }

    public String getCount() { return count; }
}
