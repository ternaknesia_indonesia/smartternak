package com.ternaknesia.sobaternak.DataModel;


import com.ternaknesia.sobaternak.Utils.Item;

/**
 * Created by A MADRISH on 9/15/2016
 */
public class DataNews extends Item {

    private String mTextId;
    private String mTextName;
    private String mTextAuthor;
    private String mTextLinkImg;
    private String mViews;

    public DataNews(String NewsId, String NewsTitle, String link_img, String Author, String views) {
        mTextId = NewsId;

        if(NewsTitle.length() > 50) {
            if (NewsTitle.charAt(50) == ' ')
                mTextName = NewsTitle.substring(0, 49) + "...";
            else if (NewsTitle.charAt(50) != ' ') {
                int i = 50;
                while (NewsTitle.charAt(i) != ' ') {
                    i = i - 1;
                }
                mTextName = NewsTitle.substring(0, i) + "...";
            }/*else mTextName = NewsTitle.substring(0, 35) + "...";*/
        } else mTextName = NewsTitle;

        mTextAuthor = Author;
        mTextLinkImg = link_img;
        mViews = views;
    }

    public String getmViews() {
        return mViews;
    }

    public String getNewsTitle() {
        return mTextName;
    }

    public String getmTextAuthor() {
        return mTextAuthor;
    }

    public String getNewsId() {
        return mTextId;
    }

    public String getLinkImg() {
        return mTextLinkImg;
    }

}