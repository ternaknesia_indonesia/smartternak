package com.ternaknesia.sobaternak.DataModel.sales_order;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Kota implements Parcelable {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("provinsi_id")
    @Expose
    private String provinsiId;
    @SerializedName("name")
    @Expose
    private String name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProvinsiId() {
        return provinsiId;
    }

    public void setProvinsiId(String provinsiId) {
        this.provinsiId = provinsiId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.provinsiId);
        dest.writeString(this.name);
    }

    public Kota() {
    }

    protected Kota(Parcel in) {
        this.id = in.readString();
        this.provinsiId = in.readString();
        this.name = in.readString();
    }

    public static final Parcelable.Creator<Kota> CREATOR = new Parcelable.Creator<Kota>() {
        @Override
        public Kota createFromParcel(Parcel source) {
            return new Kota(source);
        }

        @Override
        public Kota[] newArray(int size) {
            return new Kota[size];
        }
    };
}
