package com.ternaknesia.sobaternak.Application;

import android.app.Application;
import android.content.Context;

public class App extends Application {

    private static App app;

    public static synchronized App getInstance() {
        return app;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        app = this;
    }

    public static Context getContext() {
        return app.getApplicationContext();
    }
}
