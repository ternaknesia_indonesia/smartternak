package com.ternaknesia.sobaternak.Application;

/**
 * Created by A MADRISH on 11/1/2017
 */

public class Config {

    // global topic to receive app wide push notifications
    public static final String TOPIC_GLOBAL = "global";

    // broadcast receiver intent filters
    public static final String REGISTRATION_COMPLETE = "registrationComplete";
    public static final String PUSH_NOTIFICATION = "pushNotification";

    // id to handle the notification in the notification tray
    public static final int NOTIFICATION_ID = 102;
    public static final int NOTIFICATION_ID_BIG_IMAGE = 103;

    public static final String SHARED_PREF = "ah_firebase";
}