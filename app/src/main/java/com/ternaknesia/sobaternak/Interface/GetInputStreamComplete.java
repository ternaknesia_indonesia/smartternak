package com.ternaknesia.sobaternak.Interface;

import android.graphics.Bitmap;

/**
 * Created by A MADRISH on 12/31/2017
 */

public interface GetInputStreamComplete {
    void onGetInputStreamComplete(Bitmap bitmap, Integer REQUEST_CODE);
}