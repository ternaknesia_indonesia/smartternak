package com.ternaknesia.sobaternak.Interface;
import com.ternaknesia.sobaternak.DataModel.DataProcessOrder;

/**
 * Created by A MADRISH on 12/31/2017
 */
public interface ConfirmProcessOrder
{
    void onConfirmClicked(DataProcessOrder item);
}