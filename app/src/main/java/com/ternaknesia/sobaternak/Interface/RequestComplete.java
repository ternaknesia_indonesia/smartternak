package com.ternaknesia.sobaternak.Interface;

import org.json.JSONObject;

/**
 * Created by A MADRISH on 12/31/2017
 */

public interface RequestComplete {
    void onRequestComplete(JSONObject jsonObject, Integer REQUEST_CODE);
}