package com.ternaknesia.sobaternak.Interface;

/*        Mohon untuk tidak melakukan hal-hal yang melanggar hukum.
        Anda tidak berhak melakukan cracking terhadap system kami tanpa ada persetujuan.
        Terimakasih*/


public interface EndpointAPI {
    String HostStorage = "https://ternaknesia.com/storage/";
    String Host = "https://ternaknesia.com/";

    String HostStorage_ = "http://159.89.206.140/ternaknesia/";
    String Host_ = "http://159.89.206.140/ternaknesia/";

    String Login = Host + "/api/login";
    String Signup = Host + "/api/register";
    String RegistInstance = Host + "/api/register-instance";
    String DetailProfile = Host + "/api/details";
    String EditProfile = Host + "/api/edit_profile";
    String ListBank = Host + "/api/data/bank/list";
    String ChPhotoProfile = Host + "/api/ch_photo";  //multipart
    String ChPass = Host + "/api/change_password";

    String GetVersion = Host + "/api/smartternak_version";

    String SendTokenFirebase = Host + "/api/post_tokenfirebase";
    String ResetPass = Host + "/api/forgot_password";
    String sendBot = "https://api.telegram.org/bot546728453:AAHOp0SrkyANIfk3SJpZeEUJX_s8ip8aaoA/sendMessage?chat_id=";

    String getMyMarket = Host + "/api/qurban/mymarket";
    String updateProduct = Host + "/api/qurban/product/edit";
    String salesOrder = Host + "/api/qurban/market/sales_orders";

    String getPending_Order = Host + "/api/qurban/market/sales_order_lines/pending";
    String getReady_Order = Host + "/api/qurban/market/sales_order_lines/ready";
    String getProcess_Order = Host + "/api/qurban/market/sales_order_lines/process";
    String getFinish_Order = Host + "/api/qurban/market/sales_order_lines/finish";

    String sendOrder = Host + "/api/qurban/market/sales_order_line/delivery";
    String prepare_Order = Host + "/api/qurban/market/sales_order_line/preparation";

    String addTransc = Host + "/api/invest/transaction";
    String listTransc = Host + "/api/invest/list-transaction";
    String updateAct = Host + "/api/feeds-insert";
    String listProyek = Host + "/api/invest/farmer-projects";
    String saldo = Host + "/api/invest/saldo";
    String cat_Trans = Host + "/api/invest/resumecat";

    String myInstances = Host + "/api/scm/myinstances";
    String myInstancesData = Host + "/api/scm/myinstancedata";
    String subInstances = Host + "/api/scm/subinstances";
    String myAnimal = Host + "/api/scm/myanimal";
    String ListBobot = Host + "/api/scm/animal_weight_history";
    String EditBobot = Host + "/api/scm/animal_weight_history/edit";
    String updateAnimal = Host + "/api/scm/animal/edit";
    String batchUpdateAnimal = Host + "/api/scm/animal/batch_edit";
    String getAddHewan = Host + "/api/scm/get_add_hewan";

    String listKandang = Host + "/api/scm/mykandang";
    String editKandang = Host + "/api/scm/kandang/edit";

    String listTypes = Host + "/api/scm/animal_types";

    String generateHewan = Host + "/api/scm/generate_hewan";
    String findHewanByTag = Host + "/api/scm/search_tag";
    String GetPostStatus = Host + "/api/feeds";
    String listCategory = Host + "/api/scm/animal_weight_category";
    String editCategory = Host + "/api/scm/animal_weight_category/edit";

    String ReadNews = Host + "/api/berita/read/";
    String ListTopNews = Host + "/api/berita/top";
    String ListAllNews = Host + "/api/berita/list";
}
