package com.ternaknesia.sobaternak.Interface;

import com.ternaknesia.sobaternak.DataModel.DataPendingOrder;
import com.ternaknesia.sobaternak.DataModel.DataProcessOrder;
import com.ternaknesia.sobaternak.DataModel.DataReadyOrder;

public interface OnClickInterface {
    void onConfirmOrderClicked(DataPendingOrder item, int position);
    void onSendOrderClicked(DataReadyOrder item, int position);
    void onCompleteOrderClicked(DataProcessOrder item, int position);
}
